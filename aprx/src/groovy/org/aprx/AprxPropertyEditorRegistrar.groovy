package org.aprx

import com.cloudmasons.beans.MonetaryAmountEditor;
import com.cloudmasons.beans.PercentageEditor;

import java.text.SimpleDateFormat
import java.text.NumberFormat
import java.util.Date
import org.codehaus.groovy.grails.web.binding.StructuredDateEditor
import org.springframework.beans.PropertyEditorRegistrar
import org.springframework.beans.PropertyEditorRegistry
import org.springframework.beans.propertyeditors.CustomNumberEditor

public class AprxPropertyEditorRegistrar implements PropertyEditorRegistrar {
    
  def g = new org.codehaus.groovy.grails.plugins.web.taglib.ApplicationTagLib()
    
  public void registerCustomEditors(PropertyEditorRegistry registry) {
      NumberFormat moneyFormat = NumberFormat.getCurrencyInstance();
      String dateFormat;
      try {
          dateFormat = g.message(code: 'default.edit.date.format', default: 'MM/dd/yyyy')
      }
      catch (java.lang.IllegalStateException e) {
          //println e
          dateFormat = 'MM/dd/yyyy'
      }
      registry.registerCustomEditor(Date.class, new StructuredDateEditor(new SimpleDateFormat(dateFormat), true))

      registry.registerCustomEditor(BigDecimal.class, new MonetaryAmountEditor());
      registry.registerCustomEditor(BigDecimal.class, 'currentMonthGcr', new PercentageEditor());
      registry.registerCustomEditor(BigDecimal.class, 'currentMonthRebate', new PercentageEditor());
      registry.registerCustomEditor(BigDecimal.class, 'currentMonthComplianceLevels', new PercentageEditor());
      registry.registerCustomEditor(BigDecimal.class, 'januaryGcr', new PercentageEditor());
      registry.registerCustomEditor(BigDecimal.class, 'februaryGcr', new PercentageEditor());
      registry.registerCustomEditor(BigDecimal.class, 'marchGcr', new PercentageEditor());
      registry.registerCustomEditor(BigDecimal.class, 'aprilGcr', new PercentageEditor());
      registry.registerCustomEditor(BigDecimal.class, 'mayGcr', new PercentageEditor());
      registry.registerCustomEditor(BigDecimal.class, 'juneGcr', new PercentageEditor());
      registry.registerCustomEditor(BigDecimal.class, 'julyGcr', new PercentageEditor());
      registry.registerCustomEditor(BigDecimal.class, 'augustGcr', new PercentageEditor());
      registry.registerCustomEditor(BigDecimal.class, 'septemberGcr', new PercentageEditor());
      registry.registerCustomEditor(BigDecimal.class, 'octoberGcr', new PercentageEditor());
      registry.registerCustomEditor(BigDecimal.class, 'novemberGcr', new PercentageEditor());
      registry.registerCustomEditor(BigDecimal.class, 'decemberGcr', new PercentageEditor());
                          
      registry.registerCustomEditor(BigDecimal.class, 'octoberGcr', new PercentageEditor());
      
  }
} 