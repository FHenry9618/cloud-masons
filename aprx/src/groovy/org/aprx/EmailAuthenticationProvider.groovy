package org.aprx

import org.springframework.security.*
import org.springframework.security.providers.*
import org.springframework.security.userdetails.*
import org.springframework.security.context.*
import org.codehaus.groovy.grails.plugins.springsecurity.GrailsUserImpl
import org.aprx.User

class EmailAuthenticationProvider implements AuthenticationProvider {

    def authenticateService

    Authentication authenticate(Authentication customAuth) {
        println "authenticate"
               User.withTransaction { status ->
                    User user = User.findByEmail(customAuth.principal) 
                    if(user) {
                        if (user?.enabled && user?.passwd == authenticateService.encodePassword(customAuth.credentials)) {
                            GrantedAuthorityImpl[] authorities = user.roles.collect {new GrantedAuthorityImpl(it.name)}
                            def userDetails = new GrailsUserImpl(user.email, user.passwd, true, true, true, true, authorities, user)
                            def token = new UsernamePasswordAuthenticationToken(userDetails, user.passwd, userDetails.authorities)
                            token.details = customAuth.details

                            return token
                        }else throw new BadCredentialsException("Log in failed - identity could not be verified");
                    }else {
                        return null
                    }
               }
    }

    boolean supports(Class authentication) {
        return true
    }
}
