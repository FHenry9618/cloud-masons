package org.aprx;


public class DateUtil {

   public static Date getStartOfDay(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date)
        cal.clear(Calendar.AM_PM)
        cal.clear(Calendar.HOUR)
        cal.clear(Calendar.HOUR_OF_DAY)
        cal.clear(Calendar.MINUTE)
        cal.clear(Calendar.SECOND)
        cal.clear(Calendar.MILLISECOND)
        return cal.getTime()
    }
    
    public static Date getStartOfMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(getStartOfDay(date));
        cal.set(Calendar.DAY_OF_MONTH, 1);
        return cal.getTime()
    }
    
    public static Date getEndOfMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(getStartOfDay(date));
        int lastDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH)
        cal.set(Calendar.DAY_OF_MONTH, lastDay);
        return cal.getTime()
    }

    public static Date getStartOfYear(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(getStartOfDay(date));
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.MONTH, Calendar.JANUARY);
        return cal.getTime()
    }
    
    public static Date getEndOfYear(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(getStartOfDay(date));
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.MONTH, Calendar.DECEMBER);
        int lastDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH)
        cal.set(Calendar.DAY_OF_MONTH, lastDay);
        return cal.getTime()
    }

    public static Date getStartOfFiscalYear(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(getStartOfDay(date));
        
        // Set the year back to the previous calendar year if we're in one of the last three months
        // of the fiscal year
        def month = cal.get(Calendar.MONTH)
        if (month == Calendar.JANUARY || month == Calendar.FEBRUARY || month == Calendar.MARCH) {
            cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) - 1)
        }
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.MONTH, Calendar.APRIL);
        return cal.getTime()
    }
    
    public static Date getEndOfFiscalYear(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(getStartOfDay(date));
        cal.set(Calendar.DAY_OF_MONTH, 1);
        
        // Set the cal year up one year if we're not in the last three months of the fiscal year
        def month = cal.get(Calendar.MONTH)
        if (month == Calendar.APRIL || 
            month == Calendar.MAY || 
            month == Calendar.JUNE || 
            month == Calendar.JULY || 
            month == Calendar.AUGUST || 
            month == Calendar.SEPTEMBER || 
            month == Calendar.OCTOBER || 
            month == Calendar.NOVEMBER || 
            month == Calendar.DECEMBER) {
            cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) + 1)
        }

        cal.set(Calendar.MONTH, Calendar.MARCH);
        int lastDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH)
        cal.set(Calendar.DAY_OF_MONTH, lastDay);
        return cal.getTime()
    }


    public static int getWorkingDaysBetweenTwoDates(Date startDate, Date endDate) {
        Calendar startCal;
        Calendar endCal;
        startCal = Calendar.getInstance();
        startCal.setTime(startDate);
        endCal = Calendar.getInstance();
        endCal.setTime(endDate);
        int workDays = 0;
    
        //Return 0 if start and end are the same
        if (startCal.getTimeInMillis() == endCal.getTimeInMillis()) {
            return 0;
        }
    
        if (startCal.getTimeInMillis() > endCal.getTimeInMillis()) {
            startCal.setTime(endDate);
            endCal.setTime(startDate);
        }
    
        while (startCal.getTimeInMillis() < endCal.getTimeInMillis()) {

            if (startCal.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && startCal.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
                ++workDays;
            }
            startCal.add(Calendar.DAY_OF_MONTH, 1);

        }
    
        return workDays;
    }
}