package org.aprx;

/**
 * Utilities for calculating GCR
 */
public class GcrUtil {

    public static final List ABC_LEVELS = [
        [threshold: 0.00, rebate: 0.00],
        [threshold: 0.07, rebate: 0.04],
        [threshold: 0.10, rebate: 0.05],
        [threshold: 0.12, rebate: 0.09],
        [threshold: 0.13, rebate: 0.11],
        [threshold: 0.15, rebate: 0.13],
        [threshold: 0.18, rebate: 0.15]]

    public static final List MD_LEVELS = [
        [threshold: 0.00, rebate: 0.00],
        [threshold: 0.05, rebate: 0.05],
        [threshold: 0.08, rebate: 0.075],
        [threshold: 0.10, rebate: 0.10],
        [threshold: 0.11, rebate: 0.11],
        [threshold: 0.12, rebate: 0.14],
        [threshold: 0.13, rebate: 0.17],
        [threshold: 0.15, rebate: 0.20],
        [threshold: 0.18, rebate: 0.24]]
   
    public static BigDecimal calculateGcr(WholesalerAccount account, BigDecimal brandedTotal, BigDecimal genericTotal) {
        if (genericTotal == null || brandedTotal == null) return 0.0
        if (genericTotal == 0.0 && brandedTotal == 0.0) return 0.0
        return genericTotal / (genericTotal + brandedTotal);
    }

    public static BigDecimal calculateRebate(WholesalerAccount account, BigDecimal brandedTotal, BigDecimal genericTotal) {
        Map level  = getComplianceLevel(account, brandedTotal, genericTotal)
        println "Rebate level ${level}"
        return level?.rebate ?: 0.0;
    }

    /**
     * Get the contract data for the level of compliance for the given wholesaler
     * account and the volume of sales.
     */
    public static Map getComplianceLevel(WholesalerAccount account, BigDecimal brandedTotal, BigDecimal genericTotal) {
        BigDecimal gcr = calculateGcr(account, brandedTotal, genericTotal);

        Map level = null
        List levels = getComplianceLevels(account, brandedTotal, genericTotal) ?: []
        return levels?.find { it.currentLevel == true }

    }
    
    /**
     *
     */
    public static List getComplianceLevels(WholesalerAccount account, BigDecimal brandedTotal, BigDecimal genericTotal) {
        def levels = (account.wholesaler == 'ABC') ? ABC_LEVELS.collect { it.clone(); } : MD_LEVELS.collect { it.clone() }
        BigDecimal gcr = calculateGcr(account, brandedTotal, genericTotal);
        def previousLevel
        for (Map l in levels) {

            if (l.threshold > gcr) {
                if (previousLevel == null) previousLevel = l
                previousLevel.currentLevel = true
                break;
            }
            previousLevel = l
        }
        println "compliance levels ${levels}"
        return levels;
    }
}