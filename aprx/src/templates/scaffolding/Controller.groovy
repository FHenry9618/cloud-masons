<%=packageName ? "package ${packageName}\n\n" : ''%>class ${className}Controller {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        render include(controller: 'bean', action:'list', params: additionalParams);
    }

    def create = {
        render include(controller: 'bean', action:'create', params: additionalParams);
    }

    def show = {
        def result = include(controller: 'bean', action:'show', id: "\${params.id}", params: additionalParams);
        if (result.toString().length() > 0) {
            render result;
        } else {
            redirect(action: "list");
        }
    }

    def edit = {
        def result = include(controller: 'bean', action:'edit', id: "\${params.id}", params: additionalParams);

        if (result.toString().length() > 0) {
            render result;
        } else {
            redirect(action: "list");
        }
    }

    def save = {
        def result =  include(controller: 'bean', action:'save', id: "\${params.id}", params: additionalParams);

        if (result.toString().length() > 0) {
            render result;
        } else {
            redirect(action: "list");
        }
    }

    def update = {
        def result =  include(controller: 'bean', action:'update', id: "\${params.id}", params: additionalParams);

        if (result.toString().length() > 0) {
            render result;
        } else {
            redirect(action: "list");
        }
    }

    def delete = {
        def result =  include(controller: 'bean', action:'deleteObject', id: "\${params.id}", params: additionalParams);

        if (result.toString().length() > 0) {
            render result;
        } else {
            redirect(action: "list");
        }
    }


    /**
     * Returns a new Map instance with additional parameters needed to forward to
     * the bean controller for default scaffold rendering
     */
    private Map getAdditionalParams() {
        def additionalParams = [:]
        additionalParams.beanClass ='${className}'                            // The name of the bean class
        additionalParams.beanControllerName = '${domainClass.propertyName}'   // This controller name for links
        return additionalParams
    }

}
