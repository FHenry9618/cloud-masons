
import org.aprx.User;
import org.aprx.Role;
import org.aprx.Report;

class BootStrap {

    def defaultService

    def init = { servletContext ->
        if (User.count() == 0 ) {
            new Role(name: 'SYSTEM_ADMINISTRATOR', description: 'System administrator').save(flush: true);
            new Role(name: 'MEMBER_ADMINISTRATOR', description: 'General user').save(flush: true);
            new Role(name: 'MEMBER', description: 'General user').save(flush: true);
           try {
            defaultService.createDefaultFeeds()
            defaultService.loadDefaultFeeds()
            
           } catch (Throwable t) {
                println t
                t.printStackTrace()
           }
        }
        reports.each { r ->
            // Only load reports into the database once.
            if (!Report.findByName(r.name)) {
                Report report = new Report()
                report.properties = r
                if (!report.save()) {
                    println report.errors
                }
            }
        }
        defaultService.createAbcPurchaseDataFeed()
        defaultService.createMemberStoreUpdateFeed()
        defaultService.createMDPurchaseSummaryFeed()
    }
    
    
     def destroy = {
     }
     
////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  Default reports
///////////////////////////////////////////////////////////////////////////////////////////////////////////
   public static final List reports = [
                    [ name:"TRV for Member Stores (Previous Day)", role:'MEMBER_ADMINISTRATOR',
                                          description: "Report showing previous days revenue for stores",
                                          configScript: """
                        entity = 'memberStore'
                        title = 'TRV for Member Stores (Previous Day)'
                                
                        columns = ['name', 'previousDaysPurchases']
                        columnTitles = [:]
                        ${REPORT_AUTO_TEXTS}
                        dataSource = { session, params ->
                            def query = 'from MemberStore as ms'
                            query += '    order by ms.name'
                            org.aprx.MemberStore.executeQuery(query)
                        }
                        fileName = 'member-store-report-trv-day'
                                          """
                                        ],
                    [ name:"TRV for Member Stores (Fiscal Year)", role:'MEMBER_ADMINISTRATOR',
                                          description: "Member store TRV for fiscal year",
                                          configScript: """
                        entity = 'memberStore'
                        title = 'TRV for Member Stores (Year-to-Date)'
                                
                        columns = ['name', 'fiscalYearPurchases', 'fiscalYearGeneric', 'fiscalYearBranded']
                        columnTitles = [:]
                        ${REPORT_AUTO_TEXTS}
                        dataSource = { session, params ->
                            def query = 'from MemberStore as ms'
                            query += '    order by ms.name'
                            org.aprx.MemberStore.executeQuery(query)
                        }
                        fileName = 'member-store-report-trv-fiscal-year'
                                          """
                                        ],
                                        [ name:"TRV for Member Stores (Year To Date)", role:'MEMBER_ADMINISTRATOR',
                                          description: "Member store TRV for year to date",
                                          configScript: """
                        entity = 'memberStore'
                        title = 'TRV for Member Stores (Year-to-Date)'
                                
                        columns = ['name', 'yearToDatePurchases', 'yearToDateGeneric', 'yearToDateBranded']
                        columnTitles = [:]
                        ${REPORT_AUTO_TEXTS}
                        dataSource = { session, params ->
                            def query = 'from MemberStore as ms'
                            query += '    order by ms.name'
                            org.aprx.MemberStore.executeQuery(query)
                        }
                        fileName = 'member-store-report-trv-year'
                                          """
                                        ],
                    [ name:"Member Store Grouped By Area Code", role:'MEMBER_ADMINISTRATOR',
                                          description: "Report showing all member stores grouped by area code",
                                          configScript: """
                        entity = 'memberStore'
                        title = 'Member Store Report (by Area Code)'
                                
                        columns = ['areaCode', 'stockNumber', 'name', 'contactName']
                        
                        columnTitles = [:]
                        ${REPORT_AUTO_TEXTS}
                        groupColumns = ['areaCode']

                        dataSource = { session, params ->
                            def query = 'from MemberStore as ms'
                            query += '    order by ms.name'
                            org.aprx.MemberStore.executeQuery(query)
                        }
                        fileName = 'member-stores-by-areacode'
                                          """
                                        ],
                    [ name:"Member Store Grouped By Stock Holder Report", role:'MEMBER_ADMINISTRATOR',
                                          description: "Report showing all member stores",
                                          configScript: """
                        entity = 'memberStore'
                        title = 'Member Store Report (by Stock Number)'
                                
                        columns = ['stockNumber', 'name', 'contactName']
                        
                        columnTitles = [:]
                        ${REPORT_AUTO_TEXTS}
                        groupColumns = ['stockNumber']
                        dataSource = { session, params ->
                            def query = 'from MemberStore as ms'
                            query += '    order by ms.name'
                            org.aprx.MemberStore.executeQuery(query)
                        }
                        fileName = 'member-store-by-stock-holder'
                                          """
                                        ], 
                    [ name:"Member Store Notes", role:'MEMBER_ADMINISTRATOR',
                                          description: "Report showing notes for all Member stores",
                                          configScript: """
                        entity = 'note'
                        title = 'Member Store Notes'
                                
                                
                        columns = ['memberStore.name', 'description', 'name', 'dateCreated' ]
                        
                        columnTitles = ['memberStore.name': 'Member Store', 'description': 'Notes', 'name': 'Created By', 'dateCreated': 'Timestamp']

                        ${REPORT_AUTO_TEXTS}
                        groupColumns = ['memberStore.name']
                        dataSource = { session, params ->
                            def query = 'from Note as note'
                            query += '    order by note.memberStore.name'
                            org.aprx.MemberStore.executeQuery(query)
                        }
                        fileName = 'member-store-notes'
                                          """
                   ], 
                    [ name:"Notes From Previous Day", role:'MEMBER_ADMINISTRATOR',
                                          description: "Report showing notes from the previous day for all member stores",
                                          configScript: """
                        entity = 'note'
                        title = 'Member Store Notes from the Previous Day'
                                
                        columns = ['memberStore.name', 'description', 'name', 'dateCreated' ]
                        
                        columnTitles = ['memberStore.name': 'Member Store', 'description': 'Notes', 'name': 'Created By', 'dateCreated': 'Timestamp']
 
                        ${REPORT_AUTO_TEXTS}
                        dataSource = { session, params ->
                            def previousDay = new Date().minus(1);
                            def query = 'from Note as note'
                            query += '    where note.dateCreated >= :previousDay order by note.memberStore.name '
                            org.aprx.MemberStore.executeQuery(query, [previousDay: previousDay])
                        }
                        fileName = 'member-store-notes-previous-day'
                                          """
                   ], 
                    [ name:"Notes From Previous Week", role:'MEMBER_ADMINISTRATOR',
                                          description: "Report showing notes from the previous week for all member stores",
                                          configScript: """
                        entity = 'note'
                        title = 'Member Store Notes from the Previous Week'
                                
                                
                        columns = ['memberStore.name', 'description', 'name', 'dateCreated' ]
                        
                        columnTitles = ['memberStore.name': 'Member Store', 'description': 'Notes', 'name': 'Created By', 'dateCreated': 'Timestamp']

                        ${REPORT_AUTO_TEXTS}
                        dataSource = { session, params ->
                            def previousWeek = new Date().minus(7);
                            def query = 'from Note as note'
                            query += '    where note.dateCreated >= :previousWeek order by note.memberStore.name '
                            org.aprx.MemberStore.executeQuery(query, [previousWeek: previousWeek])
                        }
                        fileName = 'member-store-notes-previous-week'
                                          """
                   ], 

                                    [ name:"Generics Price Report", role:'MEMBER_ADMINISTRATOR',
                                        description: "Report showing average price for wholesalers for generic drugs",
                                        configScript: """
                      entity = 'drug'
                      title = 'Generic Drug Price Report'
                              
                      columns = ['name', 'description', 'mdAveragePrice', 'abcAveragePrice']
                      columnTitles = [:]
                      ${REPORT_AUTO_TEXTS}
                      dataSource = { session, params ->
                          def query = 'from Drug as drug'
                          query += '   where drug.type= \\\'generic\\\' '
                          org.aprx.Drug.executeQuery(query)
                      }
                      fileName = 'drug-report-price-generic'
                                        """
                                      ]
                    ]
  public static final String REPORT_FOOTER_VARS = """
    grandTotalLegend = 'Total'
    footerVariables =['name']
    footerOperations = ['COUNT']
      """;
      
  public static final String REPORT_AUTO_TEXTS = """
    autoTexts = 
     [
       new AutoText(AutoText.AUTOTEXT_PAGE_X_OF_Y, AutoText.POSITION_FOOTER, HorizontalBandAlignment.buildAligment(AutoText.ALIGMENT_RIGHT), (byte)0, 150, 25),
       new AutoText(AutoText.AUTOTEXT_CREATED_ON, AutoText.POSITION_FOOTER, HorizontalBandAlignment.buildAligment(AutoText.ALIGMENT_LEFT), AutoText.PATTERN_DATE_DATE_ONLY , 200)
     ]
      """;

} 