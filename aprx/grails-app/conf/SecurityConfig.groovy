security {

	// see DefaultSecurityConfig.groovy for all settable/overridable properties

	active = true

    loginUserDomainClass = "org.aprx.User"
    userName = 'email'
    relationalAuthorities = "roles"
    authorityDomainClass = "org.aprx.Role"
    authorityField = "name"
    requestMapClass="com.aprx.Requestmap"
    
    useRequestMapDomainClass = false

    requestMapString = ''' 
                CONVERT_URL_TO_LOWERCASE_BEFORE_COMPARISON 
                PATTERN_TYPE_APACHE_ANT
                /index.gsp=IS_AUTHENTICATED_ANONYMOUSLY
                /index*=IS_AUTHENTICATED_ANONYMOUSLY
                /login/**=IS_AUTHENTICATED_ANONYMOUSLY
                /securefile/upload=IS_AUTHENTICATED_ANONYMOUSLY
                /js/**=IS_AUTHENTICATED_ANONYMOUSLY
                /css/**=IS_AUTHENTICATED_ANONYMOUSLY
                /plugins/cornerstone*/js/**=IS_AUTHENTICATED_ANONYMOUSLY
                /plugins/cornerstone*/css/**=IS_AUTHENTICATED_ANONYMOUSLY
                /images/**=IS_AUTHENTICATED_ANONYMOUSLY
                /wholesaleraccount/**=IS_AUTHENTICATED_FULLY
                /member/**=IS_AUTHENTICATED_FULLY
                /dashboard/**=IS_AUTHENTICATED_FULLY
                /invoice/**=IS_AUTHENTICATED_FULLY
                /memberstore/**=IS_AUTHENTICATED_FULLY
                /report/**=IS_AUTHENTICATED_FULLY
                /user/**=IS_AUTHENTICATED_FULLY
                /feed/**=IS_AUTHENTICATED_FULLY  
                /beanreport/**=IS_AUTHENTICATED_FULLY
            '''        

}

