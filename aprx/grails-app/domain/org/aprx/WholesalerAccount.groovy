package org.aprx


/**
 * An member store account at a wholesaler
 */
class WholesalerAccount {

    def beanService

    String name                // Account number
    String description
    MemberStore memberStore
    String wholesaler = 'ABC'
    Date dateCreated
    Date lastUpdated
    Boolean dailyFeedEnabled = false
    
    static belongsTo = [memberStore: MemberStore]
    static hasMany = [invoices: Invoice, purchaseSummaries: PurchaseSummary]
    
    static mapping = {
       cache true
    }

    static constraints = {
        memberStore(summary: true)
        name(blank: false, unique: true, summary: true)
        description(nullable: true)
        dateCreated(nullable: true)
        lastUpdated(nullable: true)
        wholesaler(inList: ['MD', 'ABC'], summary: true)
        invoices(group: 'Data', editable: false, role: 'DATA_ADMINISTRATOR')
        purchaseSummaries(group: 'Data', editable: false, role: 'DATA_ADMINISTRATOR')
        purchasesByDateRange(group: 'Purchasing Summary', userSettings: ['from': [label: 'Purchases From Date', format: 'date'], 'to': [label: 'Purchases to Date', format: 'date']])
        brandedByDateRange(group: 'Purchasing Summary', userSettings: ['from': [label: 'Branded Purchases From Date', format: 'date'], 'to': [label: 'Branded Purchases to Date', format: 'date']])
        genericByDateRange(group: 'Purchasing Summary', userSettings: ['from': [label: 'Generic Purchases From Date', format: 'date'], 'to': [label: 'Generic Purchases to Date', format: 'date']])
        
        currentMonthGcr(nullable: true, group: 'GCR Summary')
        januaryGcr(nullable: true, group: 'GCR Summary')
        februaryGcr(nullable: true, group: 'GCR Summary')
        marchGcr(nullable: true, group: 'GCR Summary')
        aprilGcr(nullable: true, group: 'GCR Summary')
        mayGcr(nullable: true, group: 'GCR Summary')
        juneGcr(nullable: true, group: 'GCR Summary')
        julyGcr(nullable: true, group: 'GCR Summary')
        augustGcr(nullable: true, group: 'GCR Summary')
        septemberGcr(nullable: true, group: 'GCR Summary')
        octoberGcr(nullable: true, group: 'GCR Summary')
        novemberGcr(nullable: true, group: 'GCR Summary')
        decemberGcr(nullable: true, group: 'GCR Summary')

    }

    static transients = [ 'currentMonthGcr', 'currentMonthRebate', 'currentMonthComplianceLevels', 
                           'purchasesByDateRange', 'brandedByDateRange', 'genericByDateRange',
                          'januaryGcr', 'februaryGcr', 'marchGcr', 'aprilGcr', 'mayGcr', 'juneGcr',
                          'julyGcr', 'augustGcr', 'septemberGcr', 'octoberGcr', 'novemberGcr', 'decemberGcr', 
                          ]
   static searchable = {
						except = ['memberStore', 'currentMonthGcr', 'currentMonthRebate', 'currentMonthComplianceLevels', 
						  'purchasesByDateRange', 'brandedByDateRange', 'genericByDateRange',
                          'januaryGcr', 'februaryGcr', 'marchGcr', 'aprilGcr', 'mayGcr', 'juneGcr',
                          'julyGcr', 'augustGcr', 'septemberGcr', 'octoberGcr', 'novemberGcr', 'decemberGcr']
						root false
						}
    static actions = 
        [
            [action: 'list', type:'static', label: 'List', role: 'MEMBER_ADMINISTRATOR'],
            [action: 'create', type:'static', label: 'Add Member Store', role: 'SYSTEM_ADMINISTRATOR'],
            [action: 'show', label: 'view',  role: 'MEMBER_ADMINISTRATOR'],
            [action: 'edit', role: 'MEMBER_ADMINISTRATOR'],
            [action: 'delete', role: 'SYSTEM_ADMINISTRATOR']
        ]
    public static final String GCR_QUERY = """select s from PurchaseSummary s
            where s.account = :account and s.periodLength = :periodLength and s.periodStartDate = :periodStart"""


    public static final String TOTAL_PURCHASE_RANGE_QUERY = """select sum(s.brandedTotal) as branded, sum(s.genericTotal) as generic from WholesalerAccount acct, PurchaseSummary s
            where acct.id = :accountId and s.account = acct and s.periodLength = 'day' and s.periodStartDate >= :periodStart and s.periodStartDate <= :periodEnd"""

    // Transients
    
    public BigDecimal getPurchasesByDateRange() {
        if (id == null || !memberStore ) return 0.0;
        
        // Get the period Start
        def fromString = beanService?.getUserSetting(this.getClass(), 'purchasesByDateRange', 'from') ?:
                            beanService?.getUserSetting(this.getClass(), 'brandedByDateRange', 'from') ?:
                              beanService?.getUserSetting(this.getClass(), 'genericByDateRange', 'from') ?: '01/01/2010';
        def today = new Date()
        def startOfYear = DateUtil.getStartOfYear(today);
        def periodStart = com.cloudmasons.util.Transforms.firstValidDate(fromString, startOfYear)
        
        // Geth the period end from a date range
        def toDateString = beanService?.getUserSetting(this.getClass(), 'purchasesByDateRange', 'to') ?:
                            beanService?.getUserSetting(this.getClass(), 'brandedByDateRange', 'to') ?:
                              beanService?.getUserSetting(this.getClass(), 'genericByDateRange', 'to') ?: '01/01/2011';

        def periodEnd = com.cloudmasons.util.Transforms.firstValidDate(toDateString, new Date())
        
        Object result = PurchaseSummary.executeQuery(TOTAL_PURCHASE_RANGE_QUERY, [accountId: id, periodStart: periodStart, periodEnd: periodEnd]);
        if (result[0][0] != null && result[0][1] != null) {
            return result[0][0] + result[0][1];
        } else if (result[0][0] != null) {
            return result[0][0]
        } else if (result[0][1] != null) {
            return result[0][1]
        }
        return 0.0
    }

    public BigDecimal getBrandedByDateRange() {
        if (id == null || !memberStore ) return 0.0;
        // Get the period Start
        def fromString = beanService?.getUserSetting(this.getClass(), 'brandedByDateRange', 'from') ?:
                            beanService?.getUserSetting(this.getClass(), 'purchasesByDateRange', 'from') ?:
                              beanService?.getUserSetting(this.getClass(), 'genericByDateRange', 'from') ?: '01/01/2010';
        def today = new Date()
        def startOfYear = DateUtil.getStartOfYear(today);
        def periodStart = com.cloudmasons.util.Transforms.firstValidDate(fromString, startOfYear)
        
        // Geth the period end from a date range
        def toDateString = beanService?.getUserSetting(this.getClass(), 'brandedByDateRange', 'to') ?:
                            beanService?.getUserSetting(this.getClass(), 'purchasesByDateRange', 'to') ?:
                              beanService?.getUserSetting(this.getClass(), 'genericByDateRange', 'to') ?: '01/01/2011';

        def periodEnd = com.cloudmasons.util.Transforms.firstValidDate(toDateString, new Date())

        Object result = PurchaseSummary.executeQuery(TOTAL_PURCHASE_RANGE_QUERY, [accountId: id, periodStart: periodStart, periodEnd: periodEnd]);
        if (result[0][0] != null) {
            return result[0][0]
        }
        return 0.0
    }

    public BigDecimal getGenericByDateRange() {
        if (id == null || !memberStore ) return 0.0;
        // Get the period Start
        def fromString = beanService?.getUserSetting(this.getClass(), 'genericByDateRange', 'from') ?:
                            beanService?.getUserSetting(this.getClass(), 'purchasesByDateRange', 'from') ?:
                              beanService?.getUserSetting(this.getClass(), 'brandedByDateRange', 'from') ?: '01/01/2011';
        def today = new Date()
        def startOfYear = DateUtil.getStartOfYear(today);
        def periodStart = com.cloudmasons.util.Transforms.firstValidDate(fromString, startOfYear)
        
        // Geth the period end from a date range
        def toDateString = beanService?.getUserSetting(this.getClass(), 'genericByDateRange', 'to') ?:
                            beanService?.getUserSetting(this.getClass(), 'purchasesByDateRange', 'to') ?:
                              beanService?.getUserSetting(this.getClass(), 'brandedByDateRange', 'to') ?: '01/01/2011';

        def periodEnd = com.cloudmasons.util.Transforms.firstValidDate(toDateString, new Date())
        
        Object result = PurchaseSummary.executeQuery(TOTAL_PURCHASE_RANGE_QUERY, [accountId: id, periodStart: periodStart, periodEnd: periodEnd]);
        if (result[0][1] != null) {
            return result[0][1]
        }
        return 0.0
    }


    public BigDecimal getCurrentMonthGcr() {
        if (id == null || !memberStore ) return 0.0;
        def ps = getCurrentMonthPurchaseSummary()
        if (ps) {
            return ps.gcr
        }

        return 0.0
    }
    public BigDecimal getCurrentMonthRebate() {
        if (id == null || !memberStore ) return 0.0;
        def ps = getCurrentMonthPurchaseSummary()
        if (ps) {
            return ps.rebate
        }

        return 0.0
    }

    public List getCurrentMonthComplianceLevels() {
    
        if (id == null || !memberStore) return null;
        def ps = getCurrentMonthPurchaseSummary()
        return GcrUtil.getComplianceLevels(this, ps?.brandedTotal, ps?.genericTotal);
    
    }
    private PurchaseSummary getCurrentMonthPurchaseSummary() {

        def today = new Date()
        def yesterday = today - 1;
        def periodStart = DateUtil.getStartOfMonth(yesterday);
        Object result = WholesalerAccount.executeQuery(GCR_QUERY, [account: this, periodLength: 'month', periodStart: periodStart]);    
        if (result && result[0] != null) {
            return result[0]
        }
        return null
    }
    public BigDecimal getJanuaryGcr() {
        return getGcrForMonth(Calendar.JANUARY);
    }
    public BigDecimal getFebruaryGcr() {
        return getGcrForMonth(Calendar.FEBRUARY);
    }
    public BigDecimal getMarchGcr() {
        return getGcrForMonth(Calendar.MARCH);
    }
    public BigDecimal getAprilGcr() {
        return getGcrForMonth(Calendar.APRIL);
    }
    public BigDecimal getMayGcr() {
        return getGcrForMonth(Calendar.MAY);
    }
    public BigDecimal getJuneGcr() {
        return getGcrForMonth(Calendar.JUNE);
    }
    public BigDecimal getJulyGcr() {
        return getGcrForMonth(Calendar.JULY);
    }
    public BigDecimal getAugustGcr() {
        return getGcrForMonth(Calendar.AUGUST);
    }
    public BigDecimal getSeptemberGcr() {
        return getGcrForMonth(Calendar.SEPTEMBER);
    }
    public BigDecimal getOctoberGcr() {
        return getGcrForMonth(Calendar.OCTOBER);
    }
    public BigDecimal getNovemberGcr() {
        return getGcrForMonth(Calendar.NOVEMBER);
    }
    public BigDecimal getDecemberGcr() {
        return getGcrForMonth(Calendar.DECEMBER);
    }
    
    /*
     * Reterns the given months Gcr (for the month in the past year)
     */
    private BigDecimal getGcrForMonth(int month) {
    try {
        
        if (id == null || !memberStore ) return 0.0
        def today = new Date()
        Calendar cal = Calendar.getInstance();
        cal.setTime(DateUtil.getStartOfMonth(today));
        int currentMonth = cal.get(Calendar.MONTH)
        if (month >= currentMonth) {
            cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) - 1)
        }
        cal.set(Calendar.MONTH, month)

        def periodStart = cal.time        
        Object result = WholesalerAccount.executeQuery(GCR_QUERY, [account: this, periodLength: 'month', periodStart: periodStart]);
        println result
        if (result && result[0] != null) {
            return result[0].gcr
        }
     } catch (Throwable t) {
        //t.printStackTrace();
        println t
     }
        return 0.0
    }
}
