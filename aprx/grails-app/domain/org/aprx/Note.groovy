package org.aprx


/**
 * A note about a member store
 */
class Note {

    String name               
    String description
    Date dateCreated
    
    MemberStore memberStore
    static belongsTo = [memberStore: MemberStore]
    
    static mapping = {
       cache true
    }

    static constraints = {
        name(blank: false)
        description(nullable: true, maxSize: 100000)
    }
	static searchable = { root false}

}
