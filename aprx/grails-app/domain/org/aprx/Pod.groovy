package org.aprx

class Pod {
    String name
    String description
    static hasMany = [memberstores: MemberStore]
    static constraints = {
        name(blank: false, unique: true, summary: true)
        description(nullable:true, summary:true)
        memberstores(nullable:true, display:false, editable:false)
        
    }
    //static transients = ['description']
    //public String getDescription() { return null }
    
}
