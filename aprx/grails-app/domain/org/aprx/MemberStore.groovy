package org.aprx


/**
 * 
 */
class MemberStore {

    def aprxService
    def beanService
    
    String name
    String description
    Integer stockNumber
    User stockHolder
    User manager
    String stockHolderName
    Date dateJoined
    String deaNumber
    String taxNumber
    String contactName
    String contactEmail
    String address
    String city
    String state
    String zipCode
    String areaCode
    String phoneNumber
    String faxNumber
    String website
    String mailingAddress
    String mailingCity
    String mailingState
    String mailingZip
    Pod pod
    User salesRep
    String county
    String nabpnum
    String cellNumber
    Boolean sendNewsletter
    Boolean primaryStore
    String status = 'active'
    List notes
    List accounts


    // prospect information
    String currentWholesaler
    String currentBuyingGroup
    String estimatedMonthlyVolume
    String paymentTerms
    String frontEndOfStore
    
    String pharmacyInformationSystem
    String otherPharmacyInformationSystem
    
    // dtg: tested derived properties
    //Date purchaseDate
    //Long otherPurchases
    
    static hasMany = [accounts:WholesalerAccount, notes: Note, users:MemberStoreUser]
    static transients = [ 'previousDaysPurchases', 
                          'januaryPurchases', 'februaryPurchases', 'marchPurchases', 'aprilPurchases', 'mayPurchases', 'junePurchases',
                          'julyPurchases', 'augustPurchases', 'septemberPurchases', 'octoberPurchases', 'novemberPurchases', 'decemberPurchases',
                          'purchasesByDateRange', 'brandedByDateRange', 'genericByDateRange',
                          'yearToDatePurchases', 'yearToDateBranded', 'yearToDateGeneric',
                          'fiscalYearPurchases', 'fiscalYearBranded', 'fiscalYearGeneric',
                          
                          'currentMonthRevenue','currentMonthGenerics', 'wholesalerInfo']
    
    static searchable = {except: ['previousDaysPurchases', 
                                  'januaryPurchases', 'februaryPurchases', 'marchPurchases', 'aprilPurchases', 'mayPurchases', 'junePurchases',
                                  'julyPurchases', 'augustPurchases', 'septemberPurchases', 'octoberPurchases', 'novemberPurchases', 'decemberPurchases',
                                  'purchasesByDateRange', 'brandedByDateRange', 'genericByDateRange',
                                  'yearToDatePurchases', 'yearToDateBranded', 'yearToDateGeneric',
                                  'fiscalYearPurchases', 'fiscalYearBranded', 'fiscalYearGeneric', 'currentMonthRevenue', 'currentMonthGenerics', 'wholesalerInfo']
                        accounts component: true
                        notes component: true
                        name boost: 2.0}
    
    static mapping = {
       cache true
       notes sort: "dateCreated", cascade: "all-delete-orphan"
       accounts sort: "name"
       users sort: "id"

       //purchaseDate formula: 'SYSDATE()'
       //otherPurchases formula: '(SELECT SUM(ms.id) FROM member_store ms)'
    }
        
    static actions = 
        [
            [action: 'list', type:'static', label: 'List', role: 'MEMBER_ADMINISTRATOR'],
            [action: 'create', type:'static', label: 'Add Member Store', role: 'SYSTEM_ADMINISTRATOR'],
            [action: 'createProspect', type:'static', label: 'Add Prospect', role: 'MEMBER_ADMINISTRATOR'],
            [action: 'show', label: 'view',  role: 'MEMBER_ADMINISTRATOR'],
            [action: 'addNote', label: 'add note', role: 'MEMBER_ADMINISTRATOR'],
            [action: 'edit', role: 'MEMBER_ADMINISTRATOR'],
            [action: 'delete', role: 'SYSTEM_ADMINISTRATOR']
        ]
    
    static constraints = {
        name(blank: false, unique: true, summary: true, group: 'General')
        description(nullable: true,  group: 'General')
        status(nullable: false, inList: ['unknown', 'prospect', 'active', 'inactive'], group: 'General', role: 'MEMBER_ADMINISTRATOR')
        stockNumber(nullable: true, summary: true, group: 'General', role: 'SYSTEM_ADMINISTRATOR')
        wholesalerInfo(group: 'General')
        stockHolder(nullable: true, group: 'General', role: 'SYSTEM_ADMINISTRATOR')
        stockHolderName(nullable: true, group: 'General')
        manager(nullable: true, group: 'General', role: 'SYSTEM_ADMINISTRATOR')
        dateJoined(nullable: true, group: 'General', role: 'SYSTEM_ADMINISTRATOR')
        deaNumber(nullable: true, group: 'General', role: 'SYSTEM_ADMINISTRATOR')
        taxNumber(nullable: true, group: 'General', role: 'SYSTEM_ADMINISTRATOR')

        contactName(nullable: true, summary: true, group: 'Contact Information')
        contactEmail(nullable: true, group: 'Contact Information')
        address(nullable: true, group: 'Contact Information')
        city(nullable: true, group: 'Contact Information')
        state(nullable: true, group: 'Contact Information')
        zipCode(nullable: true, group: 'Contact Information')
        areaCode(nullable: true, group: 'Contact Information')
        phoneNumber(nullable: true, group: 'Contact Information')
        faxNumber(nullable: true, group: 'Contact Information')
        website(nullable: true, group: 'Contact Information')
        
        currentWholesaler(nullable: true, group: 'Prospect Information')
        currentBuyingGroup(nullable: true, group: 'Prospect Information')
        estimatedMonthlyVolume(nullable: true, group: 'Prospect Information')
        paymentTerms(nullable: true, group: 'Prospect Information')
        frontEndOfStore(nullable: true, group: 'Prospect Information')

        pharmacyInformationSystem(nullable: true, group: 'Prospect Information')
        otherPharmacyInformationSystem(nullable: true, group: 'Prospect Information')
        
        currentMonthRevenue(group: 'Purchasing Summary', label: 'Current Month Revenues')
        currentMonthGenerics(group: 'Purchasing Summary', label: 'Current Month Generic Revenues')

        previousDaysPurchases(group: 'Purchasing Summary')

        yearToDatePurchases(group: 'Purchasing Summary')
        yearToDateBranded(group: 'Purchasing Summary')
        yearToDateGeneric(group: 'Purchasing Summary')
        fiscalYearPurchases(group: 'Purchasing Summary')
        fiscalYearBranded(group: 'Purchasing Summary') 
        fiscalYearGeneric(group: 'Purchasing Summary')
        januaryPurchases(group: 'Purchasing Summary')
        februaryPurchases(group: 'Purchasing Summary')
        marchPurchases(group: 'Purchasing Summary')
        aprilPurchases(group: 'Purchasing Summary')
        mayPurchases(group: 'Purchasing Summary')
        junePurchases(group: 'Purchasing Summary')
        julyPurchases(group: 'Purchasing Summary')
        augustPurchases(group: 'Purchasing Summary')
        septemberPurchases(group: 'Purchasing Summary')
        octoberPurchases(group: 'Purchasing Summary')
        novemberPurchases(group: 'Purchasing Summary')
        decemberPurchases(group: 'Purchasing Summary')
        purchasesByDateRange(group: 'Purchasing Summary', userSettings: ['from': [label: 'Purchases From Date', format: 'date'], 'to': [label: 'Purchases to Date', format: 'date']])
        brandedByDateRange(group: 'Purchasing Summary', userSettings: ['from': [label: 'Branded Purchases From Date', format: 'date'], 'to': [label: 'Branded Purchases to Date', format: 'date']])
        genericByDateRange(group: 'Purchasing Summary', userSettings: ['from': [label: 'Generic Purchases From Date', format: 'date'], 'to': [label: 'Generic Purchases to Date', format: 'date']])

        mailingAddress(nullable:true, group:'Contact Information')
        mailingCity(nullable:true, group:'Contact Information')
        mailingState(nullable:true, group:'Contact Information')
        mailingZip(nullable:true, group:'Contact Information')
        pod(nullable:true, group:'Prospect Information', role: 'MEMBER_ADMINISTRATOR')
        salesRep(nullable: true, allowedValues: { Role.findByName('MEMBER_ADMINISTRATOR')?.users; }, group: 'Prospect Information', role: 'MEMBER_ADMINISTRATOR')
        county(nullable:true, group:'General')
        nabpnum(nullable:true, group:'General')
        cellNumber(nullable:true, group:'Contact Information')
        sendNewsletter(nullable:true, group:'General')
        primaryStore(nullable:true, group:'General')
        
        accounts(group: 'Wholesaler Account(s)', role: 'SYSTEM_ADMINISTRATOR')
        notes(group: 'Notes', role: 'MEMBER_ADMINISTRATOR')
        users(group: 'Stockholders/Store Managers/Owners',  role: 'SYSTEM_ADMINISTRATOR')
    }
    
    public void addWholesalerAccount(String wholesaler, String accountNumber) {
        addToAccounts(new WholesalerAccount(name: accountNumber, wholesaler: wholesaler));
    }

    public static final String TOTAL_PURCHASE_QUERY = """select sum(s.brandedTotal) as branded, sum(s.genericTotal) as generic from WholesalerAccount acct, PurchaseSummary s
            where acct.memberStore.id = :memberStoreId and s.account = acct and s.periodLength = :periodLength and s.periodStartDate = :periodStart"""

    public static final String TOTAL_PURCHASE_RANGE_QUERY = """select sum(s.brandedTotal) as branded, sum(s.genericTotal) as generic from WholesalerAccount acct, PurchaseSummary s
            where acct.memberStore.id = :memberStoreId and s.account = acct and s.periodLength = 'day' and s.periodStartDate >= :periodStart and s.periodStartDate <= :periodEnd"""

    // Transients
    
    public BigDecimal getPurchasesByDateRange() {
        if (id == null) return 0.0;
        
        // Get the period Start
        def fromString = beanService?.getUserSetting(this.getClass(), 'purchasesByDateRange', 'from') ?:
                            beanService?.getUserSetting(this.getClass(), 'brandedByDateRange', 'from') ?:
                              beanService?.getUserSetting(this.getClass(), 'genericByDateRange', 'from') ?: '01/01/2010';
        def today = new Date()
        def startOfYear = DateUtil.getStartOfYear(today);
        def periodStart = com.cloudmasons.util.Transforms.firstValidDate(fromString, startOfYear)
        
        // Geth the period end from a date range
        def toDateString = beanService?.getUserSetting(this.getClass(), 'purchasesByDateRange', 'to') ?:
                            beanService?.getUserSetting(this.getClass(), 'brandedByDateRange', 'to') ?:
                              beanService?.getUserSetting(this.getClass(), 'genericByDateRange', 'to') ?: '01/01/2011';

        def periodEnd = com.cloudmasons.util.Transforms.firstValidDate(toDateString, new Date())
        
        Object result = PurchaseSummary.executeQuery(TOTAL_PURCHASE_RANGE_QUERY, [memberStoreId: id, periodStart: periodStart, periodEnd: periodEnd]);
        if (result[0][0] != null && result[0][1] != null) {
            return result[0][0] + result[0][1];
        } else if (result[0][0] != null) {
            return result[0][0]
        } else if (result[0][1] != null) {
            return result[0][1]
        }
        return 0.0
    }

    public BigDecimal getBrandedByDateRange() {
        if (id == null || !accounts) return 0.0;
        // Get the period Start
        def fromString = beanService?.getUserSetting(this.getClass(), 'brandedByDateRange', 'from') ?:
                            beanService?.getUserSetting(this.getClass(), 'purchasesByDateRange', 'from') ?:
                              beanService?.getUserSetting(this.getClass(), 'genericByDateRange', 'from') ?: '01/01/2010';
        def today = new Date()
        def startOfYear = DateUtil.getStartOfYear(today);
        def periodStart = com.cloudmasons.util.Transforms.firstValidDate(fromString, startOfYear)
        
        // Geth the period end from a date range
        def toDateString = beanService?.getUserSetting(this.getClass(), 'brandedByDateRange', 'to') ?:
                            beanService?.getUserSetting(this.getClass(), 'purchasesByDateRange', 'to') ?:
                              beanService?.getUserSetting(this.getClass(), 'genericByDateRange', 'to') ?: '01/01/2011';

        def periodEnd = com.cloudmasons.util.Transforms.firstValidDate(toDateString, new Date())

        Object result = PurchaseSummary.executeQuery(TOTAL_PURCHASE_RANGE_QUERY, [memberStoreId: id, periodStart: periodStart, periodEnd: periodEnd]);
        if (result[0][0] != null) {
            return result[0][0]
        }
        return 0.0
    }

    public BigDecimal getGenericByDateRange() {
        if (id == null || !accounts) return 0.0;
        // Get the period Start
        def fromString = beanService?.getUserSetting(this.getClass(), 'genericByDateRange', 'from') ?:
                            beanService?.getUserSetting(this.getClass(), 'purchasesByDateRange', 'from') ?:
                              beanService?.getUserSetting(this.getClass(), 'brandedByDateRange', 'from') ?: '01/01/2011';
        def today = new Date()
        def startOfYear = DateUtil.getStartOfYear(today);
        def periodStart = com.cloudmasons.util.Transforms.firstValidDate(fromString, startOfYear)
        
        // Geth the period end from a date range
        def toDateString = beanService?.getUserSetting(this.getClass(), 'genericByDateRange', 'to') ?:
                            beanService?.getUserSetting(this.getClass(), 'purchasesByDateRange', 'to') ?:
                              beanService?.getUserSetting(this.getClass(), 'brandedByDateRange', 'to') ?: '01/01/2011';

        def periodEnd = com.cloudmasons.util.Transforms.firstValidDate(toDateString, new Date())
        
        Object result = PurchaseSummary.executeQuery(TOTAL_PURCHASE_RANGE_QUERY, [memberStoreId: id, periodStart: periodStart, periodEnd: periodEnd]);
        if (result[0][1] != null) {
            return result[0][1]
        }
        return 0.0
    }

    public BigDecimal getYearToDatePurchases() {
        if (id == null) return 0.0;
        def today = new Date()
        def periodStart = DateUtil.getStartOfYear(today);
        Object result = PurchaseSummary.executeQuery(TOTAL_PURCHASE_QUERY, [memberStoreId: id, periodLength: 'year', periodStart: periodStart]);
        if (result[0][0] != null && result[0][1] != null) {
            return result[0][0] + result[0][1];
        } else if (result[0][0] != null) {
            return result[0][0]
        } else if (result[0][1] != null) {
            return result[0][1]
        }
        return 0.0
    }

    public BigDecimal getYearToDateBranded() {
        if (id == null || !accounts) return 0.0;
        def today = new Date()
        def periodStart = DateUtil.getStartOfYear(today);
        Object result = PurchaseSummary.executeQuery(TOTAL_PURCHASE_QUERY, [memberStoreId: id, periodLength: 'year', periodStart: periodStart]);
        if (result[0][0] != null) {
            return result[0][0]
        }
        return 0.0
    }

    public BigDecimal getYearToDateGeneric() {
        if (id == null || !accounts) return 0.0;
        def today = new Date()
        def periodStart = DateUtil.getStartOfYear(today);
        Object result = PurchaseSummary.executeQuery(TOTAL_PURCHASE_QUERY, [memberStoreId: id, periodLength: 'year', periodStart: periodStart]);
        if (result[0][1] != null) {
            return result[0][1]
        }
        return 0.0
    }

    public BigDecimal getCurrentMonthGenerics() {
        if (id == null || !accounts) return 0.0;
        def today = new Date()
        def periodStart = DateUtil.getStartOfMonth(today)
        Object result = PurchaseSummary.executeQuery(TOTAL_PURCHASE_QUERY, [memberStoreId: id, periodLength: 'month', periodStart: periodStart]);
        if (result[0][1] != null) {
            return result[0][1]
        }
        
        return 0.0
    }
    
    public BigDecimal getCurrentMonthRevenue() {
        Calendar cal = Calendar.getInstance();
        int currentMonth = cal.get(Calendar.MONTH)
        return getPurchasesForMonth(currentMonth);
    }
    
    public BigDecimal getFiscalYearPurchases() {
        if (id == null || !accounts) return 0.0;
        def today = new Date()
        def periodStart = DateUtil.getStartOfFiscalYear(today);
        Object result = PurchaseSummary.executeQuery(TOTAL_PURCHASE_QUERY, [memberStoreId: id, periodLength: 'fiscal-year', periodStart: periodStart]);
        if (result[0][0] != null && result[0][1] != null) {
            return result[0][0] + result[0][1];
        } else if (result[0][0] != null) {
            return result[0][0]
        } else if (result[0][1] != null) {
            return result[0][1]
        }
        return 0.0
    }

    public BigDecimal getFiscalYearBranded() {
        if (id == null || !accounts) return 0.0;
        def today = new Date()
        def periodStart = DateUtil.getStartOfFiscalYear(today);
        Object result = PurchaseSummary.executeQuery(TOTAL_PURCHASE_QUERY, [memberStoreId: id, periodLength: 'fiscal-year', periodStart: periodStart]);
        if (result[0][0] != null) {
            return result[0][0]
        }
        return 0.0
    }

    public BigDecimal getFiscalYearGeneric() {
        if (id == null || !accounts) return 0.0;
        def today = new Date()
        def periodStart = DateUtil.getStartOfFiscalYear(today);
        Object result = PurchaseSummary.executeQuery(TOTAL_PURCHASE_QUERY, [memberStoreId: id, periodLength: 'fiscal-year', periodStart: periodStart]);
        if (result[0][1] != null) {
            return result[0][1]
        }
        return 0.0
    }

    public BigDecimal getPreviousDaysPurchases() {
        if (id == null || !accounts) return 0.0;
        def today = new Date()
        def yesterday = today - 1;
        def periodStart = DateUtil.getStartOfDay(yesterday);
        Object result = PurchaseSummary.executeQuery(TOTAL_PURCHASE_QUERY, [memberStoreId: id, periodLength: 'day', periodStart: periodStart]);
        if (result[0][0] != null && result[0][1] != null) {
            return result[0][0] + result[0][1];
        } else if (result[0][0] != null) {
            return result[0][0]
        } else if (result[0][1] != null) {
            return result[0][1]
        }
        return 0.0
    }

    public BigDecimal getJanuaryPurchases() {
        return getPurchasesForMonth(Calendar.JANUARY);
    }
    public BigDecimal getFebruaryPurchases() {
        return getPurchasesForMonth(Calendar.FEBRUARY);
    }
    public BigDecimal getMarchPurchases() {
        return getPurchasesForMonth(Calendar.MARCH);
    }
    public BigDecimal getAprilPurchases() {
        return getPurchasesForMonth(Calendar.APRIL);
    }
    public BigDecimal getMayPurchases() {
        return getPurchasesForMonth(Calendar.MAY);
    }
    public BigDecimal getJunePurchases() {
        return getPurchasesForMonth(Calendar.JUNE);
    }
    public BigDecimal getJulyPurchases() {
        return getPurchasesForMonth(Calendar.JULY);
    }
    public BigDecimal getAugustPurchases() {
        return getPurchasesForMonth(Calendar.AUGUST);
    }
    public BigDecimal getSeptemberPurchases() {
        return getPurchasesForMonth(Calendar.SEPTEMBER);
    }
    public BigDecimal getOctoberPurchases() {
        return getPurchasesForMonth(Calendar.OCTOBER);
    }
    public BigDecimal getNovemberPurchases() {
        return getPurchasesForMonth(Calendar.NOVEMBER);
    }
    public BigDecimal getDecemberPurchases() {
        return getPurchasesForMonth(Calendar.DECEMBER);
    }
    
    public String getWholesalerInfo() {
    
        String info = "";
        accounts?.each { account ->
           if (account != null) {
               info += "${account.wholesaler}-${account.name}; ";
           }
        }
        return info
    }
    /*
     * Reterns the given months purchases (for the month in the past year)
     */
    private BigDecimal getPurchasesForMonth(int month) {
        if (id == null || !accounts) return 0.0;
        def today = new Date()
        Calendar cal = Calendar.getInstance();
        cal.setTime(DateUtil.getStartOfMonth(today));
        int currentMonth = cal.get(Calendar.MONTH)
        if (month > currentMonth) {
            cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) - 1)
        }
        cal.set(Calendar.MONTH, month)

        def periodStart = cal.time        
        Object result = PurchaseSummary.executeQuery(TOTAL_PURCHASE_QUERY, [memberStoreId: id, periodLength: 'month', periodStart: periodStart]);
        if (result[0][0] != null && result[0][1] != null) {
            return result[0][0] + result[0][1];
        } else if (result[0][0] != null) {
            return result[0][0]
        } else if (result[0][1] != null) {
            return result[0][1]
        }
        return 0.0
    }
    
    
}
