package org.aprx
import java.math.BigDecimal;

/**
 * Represents one order for a pharmacy member store through their wholesaler account.
 * Each invoice has multipe line items.
 */
class Invoice {
    
    static belongsTo = [account:WholesalerAccount]
    static hasMany = [lineItems:LineItem]
    
    String name
    String description
    Date purchaseDate
    WholesalerAccount account

    static constraints = {
        name(summary: true)
        description(nullable: true)
        purchaseDate(nullable: true, summary: true)
        lineItems(nullable: true, group: 'Line Items');
    }
}
