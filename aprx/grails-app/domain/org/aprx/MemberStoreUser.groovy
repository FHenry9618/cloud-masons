package org.aprx

class MemberStoreUser {
    MemberStore memberStore
    User user
    String description
    static belongsTo = [MemberStore]
    String relationship
    
    
    static constraints = {
        memberStore(editable: false);
        user(nullable: true, summary: true,  allowedValues: { Role.findByName('MEMBER')?.users; });
        description(nullable: true, summary: true);
        relationship(summary: true, inList: ['Stockholder', 'Manager', 'Owner', 'Other']);
    }
    static mapping = {
    }
    static transients = ['name']
    
    public String getName() { return "${user?.name ?: description}"; } 
}
