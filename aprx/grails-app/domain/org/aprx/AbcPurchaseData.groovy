package org.aprx
import java.math.BigDecimal;

/**
 * Staging table for the ABC purchase data feed
 */
class AbcPurchaseData {
    
    String accountNumber
    String accountName
    String deaNumber
    String invoiceNumber
    String address
    String city
    String state
    String zipCode
    String zipCodeExtension
    
    Date   invoiceDate
    BigDecimal genericTotal
    BigDecimal brandedTotal
    BigDecimal netTotal
    
    WholesalerAccount account

    static constraints = {
        accountNumber(nullable: true)
        accountName(nullable: true)
        deaNumber(nullable: true)
        invoiceNumber(nullable: true)
        address(nullable: true)
        city(nullable: true)
        state(nullable: true)
        zipCode(nullable: true)
        zipCodeExtension(nullable: true)
        invoiceDate(nullable: true)
        genericTotal(nullable: true)
        brandedTotal(nullable: true)
        netTotal(nullable: true)
        account(nullable: true)
    }

    static transients = [ 'name', 'description' ]
    
    
    public String getName() {
        return "${invoiceDate} - ${accountNumber}"
    }
    
    public String getDescription() {
        return "Generic ${genericTotal}/Branded: ${brandedTotal}"
    }

}
