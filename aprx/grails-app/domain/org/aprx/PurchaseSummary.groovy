package org.aprx
import java.math.BigDecimal;

/**
 * Represents a summary of purchases for on a wholesaler account for the
 * specified period.
 */
class PurchaseSummary {
    
    static belongsTo = [account:WholesalerAccount]
    
    String periodLength = 'day'
    Date periodStartDate
    BigDecimal genericTotal
    BigDecimal brandedTotal
    BigDecimal gcr
    BigDecimal rebate
    
    WholesalerAccount account

    static constraints = {
        periodLength(summary: true, inList: ['day', 'month', 'year', 'fiscal-year'])
        periodStartDate(nullable: false, summary: true)
        genericTotal(nullable: true, summary: true)
        brandedTotal(nullable: true, summary: true)
        gcr(nullable: true, summary: true)
        rebate(nullable: true)    
    }
    static actions = 
        [
            [action: 'list', type:'static', label: 'List', role: 
                ['MEMBER_ADMINISTRATOR,SYSTEM_ADMINISTRATOR': [filter: { eq ('periodLength', 'day');}]] ],
            [action: 'create', type:'static', label: 'Add Member Store', role: 'SYSTEM_ADMINISTRATOR'],
            [action: 'show', label: 'view',  role: 'MEMBER_ADMINISTRATOR'],
            [action: 'edit', role: 'MEMBER_ADMINISTRATOR'],
            [action: 'delete', role: 'SYSTEM_ADMINISTRATOR']
        ]
    static transients = [ 'name', 'description' ]
    
    
    public String getName() {
        return "${periodLength} - ${periodStartDate}"
    }
    
    public String getDescription() {
        return "Generic ${genericTotal}/Branded: ${brandedTotal}"
    }

}
