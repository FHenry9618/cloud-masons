package org.aprx

/**
 * 
 */
class User {
    static transients = ['pass']
    static hasMany = [roles: Role]
    static belongsTo = Role
    /** */
    String alias
    
    /** Member Real Name*/
    String name
    
    /** MD5 Password */
    String passwd
    /** enabled */
    boolean enabled = true

    String email
    boolean emailShow = true

    /** description */
    String description = ''

    /** plain password to create a MD5 password */
    String pass = '[secret]'
    
    String passwdResetKey // key to reset the password
    Date passwdResetKeyExpiration 
    String theme = 'black'  // todo: selectable theme with settings per theme

    static mapping = {
       cache true
    }
    static actions = 
        [
            [action: 'list', type:'static', label: 'List', role: 'SYSTEM_ADMINISTRATOR'],
            [action: 'create', type:'static', label: 'Add', role: 'SYSTEM_ADMINISTRATOR'],
            [action: 'show', label: 'view',  role: 'MEMBER_ADMINISTRATOR'],
            [action: 'edit', role: 'SYSTEM_ADMINISTRATOR'],
            [action: 'delete', role: 'SYSTEM_ADMINISTRATOR']
        ]
    static constraints = {
        alias(blank: false, unique: true, summary: true, group: 'Properties')
        name(blank: false, summary: true, group: 'Properties')
        theme(inList: ['black', 'green', 'blue', 'brown', 'purple', 'red'], group: 'Properties', editable: false)
        emailShow(editable: false)
        passwd(blank: false, password: true, group: 'Password')
        passwdResetKey(nullable: true, editable: false)
        passwdResetKeyExpiration(nullable: true, editable: false)
        enabled(group: 'Security')
        roles(group: 'Security')
    }

    public void addRole(String roleName) {
        Role role = Role.findByName(roleName)
        if (role) {
            addToRoles(role)
        }
    }
}
