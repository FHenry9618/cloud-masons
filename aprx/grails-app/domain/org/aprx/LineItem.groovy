package org.aprx


class LineItem {

    String name             //National Drug Code Number
    String description      //Item Description
    Drug drug
    Integer amount 
    BigDecimal price

    Invoice invoice
    
    static belongsTo = [invoice: Invoice]
    static mapping = {
        cache true
    }
    
    static constraints = {
        name(blank: false, summary: true)
        description(nullable: true, summary: true)
        price(summary: true)
        amount(nullable: true)
    }

}
