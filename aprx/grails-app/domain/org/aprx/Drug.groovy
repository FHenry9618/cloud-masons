package org.aprx

import java.math.BigDecimal;

class Drug {

    String name
    String description
    String ndc
    String type = 'generic'
    static searchable = [except: ['mdAveragePrice', 'abcAveragePrice']]
    
    static constraints = {
        name(nullable: true, summary: true, group: 'General')
        ndc(nullable: true, summary: true, group: 'General')
        description(nullable: true, summary: true, group: 'General')
        type(nullable: true, summary: true, inList:['generic', 'branded'], group: 'General')
        mdAveragePrice(summary: true, group: 'General')
        abcAveragePrice(summary: true, group: 'General')
    }
    static actions = 
        [
            [action: 'list', type:'static', label: 'List', role: 'MEMBER_ADMINISTRATOR'],
            [action: 'create', type:'static', label: 'Add', role: 'SYSTEM_ADMINISTRATOR'],
            [action: 'show', label: 'view',  role: 'MEMBER_ADMINISTRATOR'],
            [action: 'edit', role: 'SYSTEM_ADMINISTRATOR'],
            [action: 'delete', role: 'SYSTEM_ADMINISTRATOR']
        ]
        
    static transients = [ 'mdAveragePrice', 'abcAveragePrice']

    public BigDecimal getMdAveragePrice() {
        String query = """select avg(item.price) from WholesalerAccount acct, Invoice i, LineItem item
            where item.drug.id = ${this.id} AND item.invoice = i AND i.account = acct AND acct.wholesaler='MD' """
        Object result = MemberStore.executeQuery(query);
        println result[0];
        if (!result[0].equals(null)) {
            return BigDecimal.valueOf(result[0]).setScale(4, BigDecimal.ROUND_HALF_UP);
        }
        return result[0];
        }

    public BigDecimal getAbcAveragePrice() {
        String query = """select avg(item.price) from WholesalerAccount acct, Invoice i, LineItem item
            where item.drug.id = ${this.id} AND item.invoice = i AND i.account = acct AND acct.wholesaler='ABC' """ 
        Object result = MemberStore.executeQuery(query);
        println result[0];
        if (!result[0].equals(null)) {
            return BigDecimal.valueOf(result[0]).setScale(4, BigDecimal.ROUND_HALF_UP);
        }
        return result[0];
    }    
}
