

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'report.label', default: 'Reports')}" />
        <title>Reports</title>
    </head>
    <body>
        <bean:reports />
    </body>
</html>
