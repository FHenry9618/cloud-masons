<g:applyLayout name="global">
 <head>
<title><g:layoutTitle /></title>
<g:layoutHead />
<link rel="stylesheet" type="text/css" href="${resource(dir:'css',file:'overrides.css')}" media="screen, projection, tv" />
<!-- Change name of the stylesheet to change colors (blue/red/black/green/brown/orange/purple) -->
 </head>
 <body>
  <div id="header">
   <div class="inner-container clearfix">
    <h1 id="logo">
     <g:link controller="dashboard" action="index">American Pharmacies - CENTCOM</g:link>
    </h1>
    <g:isLoggedIn>
     <div id="userbox" class="userbox-right">
      <a id="logout" title="Click to logout" href="${createLink(controller:'logout')}"> <span class="ir"></span> <span
       class="ar">log out</span>
      </a>
     </div>

     <div id="userbox" class="userbox-left">
      <g:link controller="user" action="settings">
       <strong><g:loggedInUserInfo field="name" /></strong>
       <ul class="clearfix">
        <li>settings</li>
       </ul>
      </g:link>
     </div>
    </g:isLoggedIn>
   </div>
  </div>
  <!-- #header -->
  <div id="nav">
   <div class="inner-container clearfix">
    <div id="h-wrap">
     <div class="inner">
      <h2>
       <nav:currentItem group="*" var="item" altTitle="${g.layoutTitle()}">
        <span class="h-ico ico-${item.icon}"><span> ${item.title}
        </span></span>
       </nav:currentItem>
       <span class="h-arrow"></span>
      </h2>
      <ul class="clearfix">

       <nav:eachItem group="tabs" var="item">
        <li><a class="h-ico ico-${item.icon}" href="${item.link}"><span> ${item.title}
         </span></a></li>
       </nav:eachItem>
       <nav:eachItem group="admintabs" var="item">
        <li><a class="h-ico ico-${item.icon}" href="${item.link}"><span> ${item.title}
         </span></a></li>
       </nav:eachItem>

      </ul>
     </div>
    </div>
    <!-- #h-wrap -->
     <!-- Search form -->
     <g:form controller="dashboard" action="search" method="get">
        <fieldset>
          <label class="a-hidden" for="q">Search query:</label>
          <input id="q" class="text fl search" type="text" name="q" size="20" value="Search..." style="height: 24px"/>
          <input class="hand fr" type="image" src="${resource(plugin:'cornerstone',dir:'css/boxie/img',file:'search-button.png')}" alt="Search" />
        </fieldset>
     </g:form>
   </div>
   <!-- .inner-container -->
  </div>
  <!-- #nav -->


  <div id="container">
   <div class="inner-container">
    <g:layoutBody />

    <div id="footer">
     <!-- footer, maybe you do not need it -->
     <p>
      Created and maintained by <a href="#">Cloudmasons</a>
     </p>
    </div>

   </div>
   <!-- .inner-container -->
  </div>
  <!-- #container -->

<script type="text/javascript">
$(document).ready(function() {
  var q = $('#q');
  q.focus(function() {
    if (q.val().match('Search...')) {
      q.val('');
    }
  });
});
</script>
 </body>
</g:applyLayout>
