<g:applyLayout name="global">
    <head>
    <title><g:layoutTitle/></title>
      <g:layoutHead />
    </head>
    <body>
        <div id="header">
            <div class="inner-container clearfix">
                <h1 id="logo">
                    <a class="home" href="#" title="">
                        <g:message code="application.title"/>
                        <%--<span class="ir"></span> --%>
                    </a><br />
                </h1>
                <div id="userbox">
                  <g:isLoggedIn>
                    <div class="inner">
                        <strong><g:loggedInUserInfo field="name"/></strong>
                        <ul class="clearfix">
                            <li><g:link controller="user" action="settings">settings</g:link></li>
                            <li><g:link controller="logout">logout</g:link></li>
                        </ul>
                    </div>
                    <a id="logout" title="Click to logout" href="${createLink(controller:'logout')}">log out<span class="ir"></span></a>
                  </g:isLoggedIn>
                  <g:isNotLoggedIn>
                    <div class="inner">
                        <strong>Welcome</strong>
                        <ul class="clearfix">
                            <li><g:link controller="user" action="register">register</g:link></li>
                            <li><g:link controller="login">login</g:link></li>
                        </ul>
                    </div>
                    <a id="logout" title="Click to login" href="${createLink(controller:'login')}">log out<span class="ir"></span></a>
                  </g:isNotLoggedIn>

                </div><!-- #userbox -->
            </div><!-- .inner-container -->
        </div><!-- #header -->
                       <g:layoutBody />
 
    </body>
</g:applyLayout>