<html>
    <head>
        <title>American Pharmacies - Login</title>
        <meta name="layout" content="login" />
    </head>

    <body>

      <content tag="loginForm">

        <div class="msg msg-info">
            <g:if test='${flash.message}'>
                <p>${flash.message}</p>
            </g:if>
            <g:else>
                 <p>Provide your user email, password, and click <strong>Login</strong>.</p>
            </g:else>
        </div>
        <table cellspacing="0">
            <tr>
                <th><label for="j_username">Email:</label></th>
                <td><input type='text' class='text_' name='j_username' id='j_username' value='${request.remoteUser}' /></td>
            </tr>
            <tr>
                <th><label for='j_password'>Password</label></th>
                <td><input type='password' class='text_' name='j_password' id='j_password' /></td><!-- class error for wrong filled inputs -->
            </tr>
            <tr>
                <th></th>
                <td class="tr proceed">
                    <input class="button" type="submit" value="Login" />
                </td>
            </tr>
            <tr>
                <th></th>
                <td>
                <a id="show-reset-form" href="#">Can't access your account?</a>
                </td>
            </tr>
            <tr>
                <th></th>
                <td>
                <a id="show-register-form" href="#">Need to register?</a>
                </td>
            </tr>

        </table>
      </content>

      <content tag="resetForm">
        <div class="msg msg-info">
            <g:if test='${flash.message}'>
                <p>${flash.message}</p>
            </g:if>
            <g:else>
                 <p>To reset your password, type your full email address and click <strong>Submit</strong> then <b>check your email</b>.</p>
            </g:else>
        </div>
        <table cellspacing="0">
            <tr>
                <th><label for="email">Email:</label></th>
                <td><input type='text' class='text_' name='email' id='email' value='${request.email}' /></td>
            </tr>
            <tr>
                <th></th>
                <td class="tr proceed">
                    <input class="button" type="submit" value="Submit" />
                </td>
            </tr>
            <tr>
                <th></th>
                <td>
                <a id="show-login-form" href="#">Remembered Password?</a>
                </td>
            </tr>
            <tr>
                <th></th>
                <td>
                <a id="show-register-form2" href="#">Need to register?</a>
                </td>
            </tr>

        </table>
     </content>

     <content tag="registerForm">
        <div class="msg msg-info">
            <g:if test='${flash.message}'>
                <p>${flash.message}</p>
            </g:if>
            <g:else>
                 <p>Provide your email, stock holder #, primary wholesaler account number and click <b>Submit</b> then <b>check your email</b>.</p>
            </g:else>
        </div>
        <table cellspacing="0">
            <tr>
                <th><label for="email">Email:</label></th>
                <td><input type='text' class='text_' name='email' id='email' /></td>
            </tr>
            <tr>
                <th><label for='stockholderNumber'>Stockholder&nbsp;#:</label></th>
                <td><input type='text' class='text_' name='stockholderNumber' id='stockholderNumber' /></td><!-- class error for wrong filled inputs -->
            </tr>
            <tr>
                <th><label for='wholesalerAccount'>Wholesaler&nbsp;Account&nbsp;#:&nbsp;&nbsp;</label></th>
                <td><input type='text' class='text_' name='wholesalerAccount' id='wholesalerAccount' /></td><!-- class error for wrong filled inputs -->
            </tr>

            <tr>
                <th></th>
                <td class="tr proceed">
                    <input class="button" type="submit" value="Submit" />
                </td>
            </tr>
         </table>
      </content>
    </body>
</html>
