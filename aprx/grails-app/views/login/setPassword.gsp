<html>
    <head>
        <title>American Pharmacies - Login</title>
        <meta name="layout" content="global" />
        <script type="text/javascript" src="${resource(dir:'js', file: 'jquery.password_strength_plugin.js')}"></script>
        
    </head>
     <script type="text/javascript">
        var focusInput = function() {
            document.getElementById("newPassword").focus();
        }
    </script>
    <body id="login">

        <div class="box box-50 altbox">
            <div id="login-panel" class="boxin">
                <div class="header">
                    <h3>American Pharmacies</h3>
                </div>
                <div id="login-form" class="content">  
                    <form class="table" action="savePasswordAndLogin" method="post" id='loginForm'><!-- Default forms (table layout) -->
                        <g:hiddenField name='key' value="${user.passwdResetKey}" />
                    
                        <div class="inner-form">
                            <div class="msg msg-info">
                                <g:if test='${flash.message}'>
                                    <p>${flash.message}</p>
                                </g:if>
                                <g:else>
                                     <p>Please provide a new password and confirm the password below..</p>
                                </g:else>
                            </div>
                            <table cellspacing="0">
                                <tr>
                                    <th><label for='newPasswordd'>Password: </label></th>
                                    <td><input type='password' class='text_ password' name='newPassword' id='newPassword' /></td>
                                </tr>
                                <tr>
                                    <th><label for='confirmPassword'>Confirm&nbsp;Password:&nbsp;&nbsp; </label></th>
                                    <td><input type='password' class='text_ password' name='confirmPassword' id='confirmPassword' /></td><!-- class error for wrong filled inputs -->
                                </tr>
                                <tr>
                                    <th></th>
                                    <td class="tr proceed">
                                        <input class="button" type="submit" value="Submit" />
                                    </td>
                                </tr>
  

                            </table>
                        </div>
                    </form>

                 <script type="text/javascript">
                 $(document).ready(function() {
                    var focusInput = function() {
                        document.getElementById("newPassword").focus();
                    }

            
                });

                </div>
                </script> 
            </div>
        </div>
     </body>
</html>
