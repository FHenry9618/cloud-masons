<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="member" />
        <title>Store Dashboard</title>
    </head>
    <body>
  <div id="nav">
    <div class="inner-container clearfix">
Hello <b>${user.name}</b>.  Please review your store information below.
    </div><!-- .inner-container -->
  </div><!-- #nav -->


<div id="container">
    <div class="inner-container">

            
             
    
    <g:if test="${flash.message}">
        <div class="msg msg-ok">${flash.message}</div>
    </g:if>


    <g:each in="${stores}" var="store">

        <g:include controller="memberStore" action="summaryPanel" id="${store.id}"/>
    </g:each>
            
    
    <script>
    $(function() {
       $( ".button" ).button();
    });
    </script>
    
  <div id="footer"><!-- footer, maybe you don't need it -->
                    <p>Created and maintained by <a href="#">Cloudmasons</a></p>
               </div>
            
            </div><!-- .inner-container -->
        </div><!-- #container -->   
    </body>
</html>
