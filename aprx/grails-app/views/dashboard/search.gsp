<%@ page import="org.springframework.util.ClassUtils" %>
<%@ page import="grails.util.GrailsNameUtils" %>
<%@ page import="org.codehaus.groovy.grails.plugins.searchable.SearchableUtils" %>
<%@ page import="org.codehaus.groovy.grails.plugins.searchable.lucene.LuceneUtils" %>
<%@ page import="org.codehaus.groovy.grails.plugins.searchable.util.StringQueryUtils" %>
<html>
  <head>
    <meta name="layout" content="main" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title><g:if test="${params.q && params.q?.trim() != ''}">${params.q} - </g:if>Search Results</title>
     <script type="text/javascript">
        var focusQueryInput = function() {
            document.getElementById("q").focus();
        }
    </script>
  </head>
  <body onload="focusQueryInput();">
    <g:set var="haveQuery" value="${params.q?.trim()}" />
    <g:set var="haveResults" value="${searchResult?.results}" />
  
                  <div class="box box-75"><!-- box 50% width -->
                    <div class="boxin">
                        <div class="header">
                            <h3>        
                            <g:if test="${haveQuery && haveResults}">
          Showing <strong>${searchResult.offset + 1}</strong> - <strong>${searchResult.results.size() + searchResult.offset}</strong> of <strong>${searchResult.total}</strong>
          results for <strong>${params.q}</strong>
        </g:if>
        <g:else>
        &nbsp;
        </g:else></h3>
                        </div>
                        <div class="content">
                        
    <g:if test="${haveQuery && !haveResults && !parseException}">
      <p>Nothing matched your query - <strong>${params.q}</strong></p>
    </g:if>

    <g:if test="${searchResult?.suggestedQuery}">
      <p>Did you mean <g:link controller="dashboard" action="search" params="[q: searchResult.suggestedQuery]">${StringQueryUtils.highlightTermDiffs(params.q.trim(), searchResult.suggestedQuery)}</g:link>?</p>
    </g:if>

    <g:if test="${parseException}">
      <p>Your query - <strong>${params.q}</strong> - is not valid.</p>
      <p>Suggestions:</p>
      <ul>
        <li>Fix the query: see <a href="http://lucene.apache.org/java/docs/queryparsersyntax.html">Lucene query syntax</a> for examples</li>
        <g:if test="${LuceneUtils.queryHasSpecialCharacters(params.q)}">
          <li>Remove special characters like <strong>" - [ ]</strong>, before searching, eg, <em><strong>${LuceneUtils.cleanQuery(params.q)}</strong></em><br />
              <em>Try this: <g:link controller="dashboard" action="search" params="[q: LuceneUtils.cleanQuery(params.q)]">Search again with special characters removed</g:link></em>
          </li>
          <li>Escape special characters like <strong>" - [ ]</strong> with <strong>\</strong>, eg, <em><strong>${LuceneUtils.escapeQuery(params.q)}</strong></em><br />
              <em>Try this: <g:link controller="dashboard" action="search" params="[q: LuceneUtils.escapeQuery(params.q)]">Search again with special characters escaped</g:link></em><br />
              <em>Or: <g:link controller="dashboard" action="search" params="[q: params.q, escape: true]">Search again with the <strong>escape</strong> option enabled</g:link></em>
          </li>
        </g:if>
      </ul>
    </g:if>

    <g:if test="${tooVague}">
      <p>Your query - <strong>${params.q}</strong> - is not specific enough and matches too many records.</p>
      <p>Suggestions:</p>
      <ul>
        <li>Add more search terms or make the search term more specific</li>
        <li>For example: Instead of searching for 301, search for 3013-123</li>
      </ul>
    </g:if>
    <g:if test="${haveResults}">
       <ul class="simple"><!-- ul.simple for simple listings of pages, categories, etc. -->

        <g:each var="result" in="${searchResult.results}" status="index">

          <div class="result">
            <g:set var="className" value="${ClassUtils.getShortName(result.getClass())}" />
            <g:set var="classNaturalName" value="${GrailsNameUtils.getNaturalName(className)}" />
            <g:set var="editLink" value="${createLink(controller: className[0].toLowerCase() + className[1..-1], action: 'edit', id: result.id)}" />
            <g:set var="showLink" value="${createLink(controller: className[0].toLowerCase() + className[1..-1], action: 'show', id: result.id)}" />
            <g:set var="addNoteLink" value="${createLink(controller: className[0].toLowerCase() + className[1..-1], action: 'addNote', id: result.id)}" />
            <g:set var="desc" value="${result.toString()}" />
            <g:if test="${className == 'MemberStore'}">
            <g:set var="memberstore" value="${org.aprx.MemberStore.get(result.id)}" />
            <g:if test="${desc.size() > 120}"><g:set var="desc" value="${desc[0..120] + '...'}" /></g:if>
            <li>
               <strong> <a href="${showLink}">${memberstore.name}   -   ${memberstore.city}</a>   -   -   ${memberstore.contactName} 
               </strong>
               <span><a href="${addNoteLink}">[ Add a note ] &nbsp;&nbsp;</a></span><span><a href="${editLink}">[ Edit ]&nbsp;&nbsp;</a></span>
            </li>
            </g:if>
            <g:if test="${className == 'Drug'}">
            <g:set var="drug" value="${org.aprx.Drug.get(result.id)}" />
            <g:if test="${desc.size() > 120}"><g:set var="desc" value="${desc[0..120] + '...'}" /></g:if>
            <li>
               <strong> <a href="${showLink}">${drug.name}  -  ${drug.ndc}  -  ${drug.type}</a> 
               </strong>
               <span><a href="${editLink}">Edit</a></span>
            </li>
            </g:if>
          </div>
        </g:each>
      </ul>

      <div>
        <div class="paging">
          <g:if test="${haveResults}">
              Page:
              <g:set var="totalPages" value="${Math.ceil(searchResult.total / searchResult.max)}" />
              <g:if test="${totalPages == 1}"><span class="currentStep">1</span></g:if>
              <g:else><g:paginate controller="dashboard" action="search" params="[q: params.q]" total="${searchResult.total}" prev="&lt; previous" next="next &gt;"/></g:else>
          </g:if>
        </div>
      </div>
    </g:if>
                        </div>
                    </div>
                </div>
  
  </body>
</html>
