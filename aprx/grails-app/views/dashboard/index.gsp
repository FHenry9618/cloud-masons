<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title>Administrator Dashboard</title>
    </head>
    <body>
    
    <g:if test="${flash.message}">
        <div class="msg msg-ok">${flash.message}</div>
    </g:if>

<%--
    <theme:panel width="50">
    
       <div class="header">
         <h3>Member Store Purchase Summary</h3>
          <a class="button altbutton" href="#">Full Report</a>

       </div>
           <div class="content">
        <table cellspacing="0">
         <thead>
            <tr>
               <th>Month</th>
               <th>Branded</th>
               <th>Generic</th>
               <th>GCR</th>
               <th>Discount</th>
           </tr>
         </thead>
         </thead>
         <tbody>
           <tr class="first">
               <td>October</td>
               <td>$10,000,000</td>
               <td>$900,000</td>
               <td>9%</td>
               <td>8%</td>
           </tr>
           <tr>
               <td>September</td>
               <td>$60,000,000</td>
               <td>$6,000,000</td>
               <td>10%</td>
               <td>8%</td>
           </tr>
           <tr>
               <td>August</td>
               <td>$50,000,000</td>
               <td>$7,500,000</td>
               <td>15%</td>
               <td>14%</td>
           </tr>
           <tr>
               <td>July</td>
               <td>$40,000,000</td>
               <td>$4,000,000</td>
               <td>10%</td>
               <td>8%</td>
           </tr>

         </tbody>
       </table>
     </div>
    </theme:panel>
 
    
    <theme:panel width="50">
    
    <div class="header">
         <h3>Closest to GCR Threshold</h3>
          <a class="button altbutton" href="#">Full Report</a>
          <a class="button altbutton" href="#">Send Alert</a>
       </div>
                        <div class="content">
        <table cellspacing="0">
         <thead>
            <tr>
               <th>Store</th>
               <th>GCR</th>
               <th>Closest Threshold</th>
           </tr>
         </thead>
         </thead>
         <tbody>
           <tr class="first">
               <td>Carvajal Pharmacy</td>
               <td>14.2%</td>
               <td>14%</td>
           </tr>
           <tr>
               <td>Rogers Pharmacy</td>
               <td>13.2%</td>
               <td>14%</td>
           </tr>
           <tr>
               <td>Peoples Pharmacy</td>
               <td>15.2%</td>
               <td>14%</td>
           </tr>

         </tbody>
        </table>
        <br/>
       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*&nbsp;&nbsp;<i>To send an email alert to the stockholders for these stores click <b>Send Alert</b>.</i>
    </div>
    </theme:panel>
 --%>
     
    <theme:panel width="50">
    
	    <div class="header">
	         <h3>Member Stores</h3>
	       </div>
	    <div class="content">
	    <g:form controller='memberStore' method='get'>
	       Click <b>View Member Stores</b> button to see the member stores within CENTCOM.
	       Click <b>Add Member Store</b> to add a new member store.<br/>
	       <br/><br/>
	       <center>
	       <g:actionSubmit  value="View Member Stores" action="list" /> 
	       <g:actionSubmit  value="Add Member Store" action="create" /> 
			</center>
	       </p>
	    </g:form>
	    </div>

    </theme:panel>
    <theme:panel width="50">
    
	    <div class="header">
	         <h3>Prospects Stores</h3>
	       </div>
	    <div class="content">
	    <g:form controller='memberStore' method='get'>
	       Click  <b>View Propspects</b> below to see the propsects within CENTCOM. 
	       Click <b>Add Prospect</b> to add a new prospect.
	       <input type="hidden" name="filter.op.status" value="Equal"/>
	       <input type="hidden" name="filter.status" value="prospect"/>
	       <br/><br/><br/>
	       <center>
	       <g:actionSubmit  value="View Propspects" action="list" /> 
	       <g:actionSubmit  value="Add Propspect" action="createProspect" /> 
			</center>
	       </p>
	    </g:form>
	    </div>

    </theme:panel>
 
    <bean:reports formats="${['XLS', 'PDF']}"/>                
    
    <script>
    $(function() {
       $( ".button" ).button();
    });
    </script>

    </body>
</html>
