<b>Totals</b>
  <theme:propertyGroup bean="${beanInstance}"  readonly="true" name="otherSummaries" title="Purchasing Summary" 
      props="${['previousDaysPurchases', 'currentMonthRevenue', 'currentMonthGenerics',
                'yearToDatePurchases', 'yearToDateBranded', 'yearToDateGeneric',
                'fiscalYearPurchases', 'fiscalYearBranded', 'fiscalYearBranded'
                ]}" />
                
<b>Totals By Month</b>
  <theme:propertyGroup bean="${beanInstance}"  readonly="true" name="otherSummaries" title="Purchasing Summary" 
      props="${['januaryPurchases', 'februaryPurchases', 'marchPurchases',
                'aprilPurchases', 'mayPurchases', 'junePurchases',
                'julyPurchases', 'augustPurchases', 'augustPurchases',
                'octoberPurchases', 'novemberPurchases', 'decemberPurchases'

                ]}" />
