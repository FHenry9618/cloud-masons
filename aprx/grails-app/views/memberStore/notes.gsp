

  <g:if test="${readonly != 'true'}">
    Add Note<br/>
    <g:textArea id="newNote" name="newNote" cols="70" rows="10"></g:textArea>

  </g:if>
  <%
    def notes = bean.notes.collect { it; } ?: []
   %>
  <g:if test="${notes}">
    <table class="compact">
      <thead>
        <tr>
          <th>Notes</th>
          <th>Created By</th>
          <th>Date</th>
        </tr>
      </thead>
      <tbody>
        <g:each in="${notes?.sort {it?.dateCreated}?.reverse()}" status="i" var="note">
           <%
               Calendar cal = Calendar.getInstance();
               cal.add(Calendar.HOUR, - 4);
               Date editableTime = cal.getTime();
        
           %>
           <tr>
             <g:if test="${editableTime < note?.dateCreated}">
               <td>
                <g:textArea cols="50" name="notes[${notes.size() - i -1}].description" value="${note?.description}" />
               </td>
             </g:if>
             <g:else>
               <td>${note?.description}</td>
             </g:else>
             <td>${note?.name}</td>
             <td>${note?.dateCreated}</td>
           </tr>
        </g:each>
      <tbody>
      <tfoot>
      </tfoot>
      
    </table>
  </g:if>
  <g:else>
      <br/>
    <g:if test="${readonly == true}">
      <i>No notes for this member store.</i>
    </g:if>
    <g:else>
      <i>Please add notes for this member store.</i>
    </g:else>
  </g:else>
    
