
import org.aprx.*

class NightlyNotesReportJob {

    static triggers = {
        cron name: 'notesTrigger', cronExpression: "0 0 4 ? * *"
    }

    def aprxService
    def group = "Notes"
    def grailsApplication

    def execute() {
        def reportMailAddress = grailsApplication.config.grails.mail.reportMailAddress
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date())
        def reportName = 'Notes from Previous Day'
        def emptyReport = 'There were no notes from the previous day to report'
        if (Calendar.SUNDAY == cal.get(Calendar.DAY_OF_WEEK)) {
            reportName = 'Notes From Previous Week'
            emptyReport = 'There were no notes from the previous week to report'
        }
        def report = Report.findByName(reportName)
        println "Sending Nightly Notes Report to ${reportMailAddress}";
        aprxService.sendReport(reportMailAddress, report, 'PDF', emptyReport);
    }
}
