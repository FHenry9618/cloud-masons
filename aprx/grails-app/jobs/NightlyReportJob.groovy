
import org.aprx.*

class NightlyReportJob {

	static triggers = {
		cron name: 'demoTrigger', cronExpression: "0 30 6 ? * *"
	}

	def myService
    def group = "MyGroup"

    def execute() {
        
        Date currentTime = new Date()
        Date endOfMonth = DateUtil.getEndOfMonth(currentTime)
        int days = DateUtil.getWorkingDaysBetweenTwoDates(currentTime, endOfMonth)
        if (days == 5) {
            println ("Sent GCR report at ${currentTime}");
        } else {
            println ("Nightly Report Task: ${days} work days to the end of the month, waiting to send GCR report");
        }
    }

}
