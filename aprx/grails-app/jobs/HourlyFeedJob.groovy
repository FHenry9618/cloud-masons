
import org.aprx.*

class HourlyFeedJob {

	static triggers = {
		cron name: 'feedTrigger', cronExpression: "0 0/20 * ? * *"
	}

    def concurrent = false
	def aprxService
    def group = "FeedGroup"

    def execute() {
        println ("Starting to read mail for feeds at ${new Date()}");
        aprxService.readMail();
        println ("Completed Reading mail for feeds at ${new Date()}");

    }

}
