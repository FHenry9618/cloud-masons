package org.aprx

import java.text.DateFormat;

class AprxTagLib {
    static namespace="aprx"
  
    
    def complianceLevels = {attrs, body ->
        def account = attrs.account
        def levels = account.getCurrentMonthComplianceLevels();
        out << """<table class="compact">
                                                           <tr><th style=\"padding: 2px\">GCR</th> """
        levels.each { 
            def style = it.currentLevel ? 'style="background: #cceecc; padding: 2px"' : 'style="padding: 2px"'
            out << "<th ${style}>${(it.threshold * 100).intValue()}%</th>"
        }
        out << """</tr><tr><td style=\"padding: 2px\">Rebate</td>"""
        levels.each { 
            def style = it.currentLevel ? 'style="background: #cceecc; padding: 2px"' : 'style="padding: 2px"'
            out << "<td ${style}>${(it.rebate * 100).intValue()}%</td>"
        }
        out << "</tr></table>"
        
    }

}