package org.aprx

import grails.converters.JSON

class ApiController {
    
    def aprxService

    public static final String key = "helloaprxmemberstore"
    
    /**
     * Invoked by Cloudmasons Cloud Manager during member registration process
     */
    def registerMember = {
        def result = [:]
        
        def key = params.key
        println params
        def email = params.email
        def stockholderNumber = params.stockholderNumber
        def wholesalerAccount = params.wholesalerAccount
        def role = params.role
        
        if (key != "helloaprxmemberstore") {
            result.error = "General error"
        } else if (!email || !stockholderNumber || !wholesalerAccount) {
            result.error = "Email, stockholderNumber and wholesalerAccount are requred to register a member"
            
        } else if (!stockholderNumber.isInteger()) {
            result.error = "Stock holder number must be a number like 23"
            params.error = true
            redirect (action: auth,  params: params)
        }
        else {
            def user = null;
            user = User.findByName(email) ?: User.findByEmail(email)

            def stockNumber = stockholderNumber.toInteger()
            wholesalerAccount = wholesalerAccount.replace('-', '');
            wholesalerAccount = wholesalerAccount.replace(' ', '');
            def stores = MemberStore.findAllByStockNumber(stockNumber);
            
            if (user && stores) {
                result.exitingUser = true          
                result.primaryStoreName = stores[0].name
            } else if (stores) {
                // Create a new user with access to the given stores
                user = aprxService.createStockholder(email, stores)
                result.primaryStoreName = stores[0].name
            } else {
                result.error = "Unable to find any stores with the given information.  Please contact Paula Gray to register your account."
            }
        }        
        render result as JSON
    
    } 
}
