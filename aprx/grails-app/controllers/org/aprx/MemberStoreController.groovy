
package org.aprx


class MemberStoreController {
    static navigation = [
            group:'tabs', 
            order:3, 
            title:'Member Stores', 
            action:'list',
            role: 'MEMBER_ADMINISTRATOR',
            icon:'users'
    ]
    def scaffold = true
    def beanService
    def authenticateService

    def beforeInterceptor = {
        response.setHeader('Cache-Control', 'no-cache, no-store, must-revalidate')
        response.setIntHeader('Expires', 0)
        response.setHeader('Pragma', 'no-cache')
        
        println actionUri
        if (actionName == 'update' ) {
        }
        if (actionName == 'save' || actionName == 'saveProspect') {
            
        }
        
        println params
        println "beforeInterceptor params"
    }
    
    def summaryPanel = {
        def memberStore = MemberStore.get(params.id.toLong())
        return [memberStore: memberStore]
    }
    
    
    def createProspect = {
        def props = ['Contact Information': ['name', 'description'], 'Notes': ['notes']]
        return [memberStore: new MemberStore(), propertyGroups: props]
    
    }
    def saveProspect = {
        def memberStore = MemberStore.newInstance();
       
        memberStore.status="prospect"
        
        def member = authenticateService.principal().domainClass
        String changeNote = params.newNote ?: "Prospect Created"
        def paramBase = "notes[0]"
        if (changeNote) {
            params["${paramBase}.name"] = member.alias
            params["${paramBase}.description"] = changeNote
        }
        memberStore.properties = params
        println(params)
        if (memberStore.save(flush: true)) {
            flash.message = "New prospect created"
            redirect(action: "list")
        }
        else {
            println memberStore.errors
            def model = [:]
            memberStore.notes = null
            render(view: 'createProspect', model: [memberStore: memberStore])
            return
        }
    }  
    
    def save = {
        def memberStore = MemberStore.newInstance();
        
        def member = authenticateService.principal().domainClass
        String changeNote = params.newNote ?: "Member Store Created"
        def paramBase = "notes[0]"
        if (changeNote) {
            params["${paramBase}.name"] = member.alias
            params["${paramBase}.description"] = changeNote
        }
        memberStore.properties = params
        println(params)
        if (memberStore.save(flush: true)) {
            flash.message = "New member store created"
            redirect(action: "list")
        }
        else {
            println memberStore.errors
            def model = [:]
            memberStore.notes = null
            render(view: 'create', model: [beanInstance: memberStore, beanControllerName: "memberStore"])
            return
        }
    }

    def update = {
        def memberStore = MemberStore.get(params.id);
        def member = authenticateService.principal().domainClass
        def size = memberStore?.notes?.size() ?: 0;
        def paramBase = "notes[${size}]"

        def addParams = [beanClass: 'org.aprx.MemberStore', beanControllerName: 'memberStore']
        def props = ['status', 'stockNumber', 'contactName', 'phoneNumber']
        String changeNote = params.newNote ?: produceChangeNote(props, memberStore, params);
        
        if (changeNote) {
            addParams["${paramBase}.name"] = member.alias
            addParams["${paramBase}.description"] = changeNote
        } 
        //println(params)
        def result =  include(controller: 'bean', action:'update', id: "${params.id}", params: addParams);

        if (result.toString().length() > 0) {
            render result;
        } else {
            redirect(action: "list");
        }
    }
    
    private String produceChangeNote(props, memberStore, params) {
        if (params.fromAddNote) return null;
        String note = "";
        props?.each { prop ->
            def oldValueTemp = memberStore."${prop}" ?: 'not specified'
            def oldValue = "${oldValueTemp}";
            def newValue =  params."${prop}" ?: 'not specified'
            if (oldValue != newValue) {
                def naturalName = grails.util.GrailsNameUtils.getNaturalName(prop);
                note += "${naturalName} changed from '${oldValue}' to '${newValue}'; ";
            }
        }
        return note;
    }
    def addNote = {
        def memberStore = MemberStore.get(params.id.toLong())
        return [memberStore: memberStore]    
    }     
}
