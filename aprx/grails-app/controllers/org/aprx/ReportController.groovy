package org.aprx

class ReportController {
    def scaffold = true
    def beanService
    def authenticateService
    
    static navigation = [
                         group:'tabs', 
                         order:30, 
                         title:'Reports', 
                         action:'list',
                         icon:'media',
                         role: 'SYSTEM_ADMINISTRATOR'
                     ]
    
}
