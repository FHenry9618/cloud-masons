package org.aprx

import org.compass.core.engine.SearchEngineQueryParseException

class DashboardController {

    def authenticateService
    def searchableService
    def aprxService
    def mailService
    static navigation = [
        group:'tabs', 
        order:2, 
        title:'Dashboard', 
        action:'index',
        icon:'dashboard'
    ]
    
    
    def index = { 
        if( authenticateService.ifAnyGranted('SYSTEM_ADMINISTRATOR,MEMBER_ADMINISTRATOR')) {
            def model = [:]
            return model
        } else {
            redirect(action: 'member');
        }
    }

    def member = { 
        User user = aprxService.getLoggedInUser();
        List stores = aprxService.getManagedStores(user);
        
        def model = [stores: stores, user: user]
        return model
    }

    def sendGcrEmail = {
        User user = aprxService.getLoggedInUser();
        List stores = aprxService.getManagedStores(user);
        String htmlContent = "<p>GCR Summary of feeds for ${new Date()}.  </p><p>"
        htmlContent += g.link(controller: 'dashboard', absolute: true, action: 'member') { "Click here to see your current GCR dashboard" }
        htmlContent += "</p>"
        stores.each {
            htmlContent += g.include(controller:'memberStore', action:'summaryPanel', id:it.id);
        }
        mailService.sendMail {
                to user.email
                subject "GCR Report ${new Date()}"
                html htmlContent
        }
        redirect(action: 'member')
    }
        
    /**
     * Index page with search form and results
     */
    def search = {
        def query = params.q?.trim()
        if (!query) {
            return [:]
        }
        try {
            return [searchResult: searchableService.search(query, params)]
        } catch (SearchEngineQueryParseException ex) {
            return [parseException: true]
        } catch (org.apache.lucene.search.BooleanQuery$TooManyClauses ex) {
            return [tooVague: true]
        }
    }
    

}
