package org.aprx

class PodController {

        static navigation = [
                group:'tabs',
                order:10,
                title:'Pods',
                action:'list',
                role: 'MEMBER_ADMINISTRATOR',
                icon:'color'
        ]
        def scaffold = true
        def beanService
        def PodDomainClass
        
        def podProperties = {
            podDomain.properties.each{
                println "${it.name} ${it.oneToMany}"
            }
        }
    }
