package org.aprx


class DrugController {
    static navigation = [
            group:'tabs', 
            order:5, 
            title:'Drugs', 
            action:'list',
            role: 'MEMBER_ADMINISTRATOR',
            icon:'color'
    ]
    def scaffold = true
    def beanService
    
}
