package org.aprx

class SendController {

    def aprxService
    
    def index = { }
    
    def send = {
		def f = request.getFileMap().values().iterator()
        aprxService.contactUser(params.user_name, params.email, f)
        render "Completed"
    }

    def report = {
        def reportId = params.id ? params.id.toLong() : 1
        def email = params.email ?: 'will@nsquard.biz'
        Report r = Report.get(reportId)
        aprxService.sendReport(email, r)
        render "Report ${r.name} sent"
    }
    
    def read = {
        aprxService.readMail()
        render "Completed"
    }
}
