package org.aprx


class InvoiceController {
    static navigation = [
            group:'tabs', 
            order:4, 
            title:'Invoices', 
            action:'list',
            role: 'SYSTEM_ADMINISTRATOR',
            icon:'color'
    ]
    def scaffold = true
    def beanService
    
}
