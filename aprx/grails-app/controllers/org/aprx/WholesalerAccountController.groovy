package org.aprx


class WholesalerAccountController {
    static navigation = [
            group:'tabs', 
            order:3, 
            title:'Wholesaler Accounts', 
            action:'list',
            role: 'MEMBER_ADMINISTRATOR',
            icon:'color'
    ]
    def scaffold = true
    def beanService
    
    def beforeInterceptor = {
        response.setHeader('Cache-Control', 'no-cache, no-store, must-revalidate')
        response.setIntHeader('Expires', 0)
        response.setHeader('Pragma', 'no-cache')
    }


}
