package org.aprx

import org.codehaus.groovy.grails.plugins.springsecurity.RedirectUtils
import org.grails.plugins.springsecurity.service.AuthenticateService

import org.springframework.security.AuthenticationTrustResolverImpl
import org.springframework.security.DisabledException
import org.springframework.security.providers.UsernamePasswordAuthenticationToken as AuthToken
import org.springframework.security.context.SecurityContextHolder as SCH
import org.springframework.security.ui.AbstractProcessingFilter
import org.springframework.security.ui.webapp.AuthenticationProcessingFilter

/**
 * Login Controller (Example).
 */
class LoginController {

	/**
	 * Dependency injection for the authentication service.
	 */
	def authenticateService
    def daoAuthenticationProvider
    def mailService
    def aprxService
    def grailsApplication
    
	/**
	 * Dependency injection for OpenIDConsumer.
	 */
	def openIDConsumer

	/**
	 * Dependency injection for OpenIDAuthenticationProcessingFilter.
	 */
	def openIDAuthenticationProcessingFilter

	private final authenticationTrustResolver = new AuthenticationTrustResolverImpl()

	def index = {
		if (isLoggedIn()) {
			redirect uri: '/'
		}
		else {
			redirect action: auth, params: params
		}
	}

    def status = {
    
    }
    
    def startPasswordReset = {
        def email = params.email ?: ''

        def user = User.findByName(email) ?: User.findByEmail(email)
        if (!user) {
            params.error = true
            flash.message = "We were unable to send the password reset email to the given address.  Please confirm your email address and try again.  Contact Paula Gray to reset password if you are still having problems."
            redirect(action: auth)        
        }
        else if (sendPasswordResetEmail(user)) {
            flash.message ="Please check your email for a password reset notification.  Click on the link in this email to reset your password.  Thank you!"
            redirect(action: status)
        } else {
            flash.message("We were unable to send the password reset email to the given address.  Please contact Paula Gray to reset password")
            redirect(action: auth)
        }      
    }
    
    def startRegistration = {
        def email = params.email
        def stockholderNumber = params.stockholderNumber
        def wholesalerAccount = params.wholesalerAccount
        def role = params.role
        
        if (!email || !stockholderNumber || !wholesalerAccount) {
            params.error = true
            redirect (action: auth,  params: params)
        } else if (!stockholderNumber.isInteger()) {
            flash.message = "Stock holder number must be a number like 23"
            params.error = true
            redirect (action: auth,  params: params)
        }
        else {
            def user = null;
            user = User.findByName(email) ?: User.findByEmail(email)

            def stockNumber = stockholderNumber.toInteger()
            wholesalerAccount = wholesalerAccount.replace('-', '');
            wholesalerAccount = wholesalerAccount.replace(' ', '');
            def stores = MemberStore.findAllByStockNumber(stockNumber);
            
            if (user) {
                // just reset password
                if (sendPasswordResetEmail(user)) {
                    flash.message = "An account already exists for the given email address. We've sent a password reset link to ${user.email} to allow you to reset your password and access your account.  Thank you!"
                    redirect(action: status)
                } else {
                    flash.message = "We were unable to send the password reset email to the given address.  Please contact Paula Gray to reset password"
                    redirect(action: auth)
                }            
            } else if (stores) {
                // Create a new user with access to the given stores
                user = aprxService.createStockholder(email, stores)
                 if (sendRegistrationEmail(user)) {
                    flash.message = "Please check your ${user.email} email address for a link to complete the registration.  Thank you!"
                    redirect(action: status)
                } else {
                    flash.message = "We were unable to send the a registration email to ${user.email}.  Please contact Paula Gray to reset password"
                    redirect(action: auth)
                }
            } else {
                flash.message = "Unable to register with the given information.  Please confirm the information and resubmit or contact Paula Gray to have an account manually created".
                params.error = true
                params.tab = 'register'
                rediect(action: auth, params: params)
            }
        }
    }

    private boolean sendPasswordResetEmail(User user) {
               
        def today = new Date()
        user.passwdResetKey = UUID.randomUUID().toString()
        user.passwdResetKeyExpiration = today + 1
                
        if (user.save()) {
            def resetLink = g.createLink(absolute: true, controller: 'login', action: 'setPassword', params: [key: user.passwdResetKey]);
            def emailBody = """
                    
                    Here is the link to set your American Pharmacy password and access the site:
                    ${resetLink}
                    
                    If you did not initiate this request please forward this email to aprx@nsquard.biz.
                    
                      Thanks,
                      American Pharmacies
                    """
            mailService.sendMail {
                to "${user.email}"
                subject "American Pharmacies GCR Site - Password Reset"
                body emailBody
            }
            return true;
        } else {
            return false;
        }
    }    
    
    private boolean sendRegistrationEmail(User user) {
        if (!user) {
            return false;
        }
        def today = new Date()
        user.passwdResetKey = UUID.randomUUID().toString()
        user.passwdResetKeyExpiration = today + 1
                
        if (user.save()) {   
            def resetLink = g.createLink(absolute: true, controller: 'login', action: 'setPassword', params: [key: user.passwdResetKey]);
            def emailBody = """
                    
                    Here is the link to set your American Pharmacy password and access the site:
                    ${resetLink}
                    
                    If you did not initiate a password reset please forward this email to aprx@cloudmasons.com.
                    
                      Thanks,
                      American Pharmacies
                    """
            mailService.sendMail {
                to "${user.email}"
                subject "American Pharmacies GCR Site"
                body emailBody
            }
        } else {
            return false;
        }
    }
 
    def setPassword = {
        def user = User.findByPasswdResetKey(params.key)
        if (!user) {
            flash.message = "Unable to find password reset token.  Please contact Paula Gray to reset you password"
            redirect(action: auth);
        } 
        return [user: user, nomatch: params.nomatch]
    }

    def savePasswordAndLogin = {
        def user = User.findByPasswdResetKey(params.key)
        String postUrl
        def config = authenticateService.securityConfig.security
        postUrl = "${request.contextPath}${config.filterProcessesUrl}"
        
        if (!user) {
           redirect(action: auth);
        }  else {
            if (params.newPassword == params.confirmPassword) {
                user.passwd = authenticateService.encodePassword(params.newPassword)
                //member.passwdResetKey = null;
                //member.passwdResetKeyExpiration = null;
                if (!user.save(flush:true)) {
                    println "Unable to save user?"
                }
                
                def auth = new AuthToken(user.email, params.newPassword)
                def authtoken = daoAuthenticationProvider.authenticate(auth)
                SCH.context.authentication = authtoken
                
                redirect(controller: 'dashboard');
            } else {
                flash.message = "The provided passwords did not match, please try again"
                redirect(action: 'setPassword', params: [key: params.key])
            }
        }
        return [user: user]
    }

	/**
	 * Show the login page.
	 */
	def auth = {

		nocache response

		if (isLoggedIn()) {
			redirect uri: '/'
			return
		}

        if (grailsApplication.config.grails.cas.enabled == true &&
            grailsApplication.config.grails.cas.fullLoginURL) {
            redirect(url: grailsApplication.config.grails.cas.fullLoginURL)
            return;
        }

		String view
		String postUrl
		def config = authenticateService.securityConfig.security
		if (config.useOpenId) {
			view = 'openIdAuth'
			postUrl = "${request.contextPath}/login/openIdAuthenticate"
		}
		else if (config.useFacebook) {
			view = 'facebookAuth'
			postUrl = "${request.contextPath}${config.facebook.filterProcessesUrl}"
		}
		else {
			view = 'auth'
			postUrl = "${request.contextPath}${config.filterProcessesUrl}"
		}

		render view: view, model: [postUrl: postUrl]
	}

	/**
	 * Form submit action to start an OpenID authentication.
	 */
	def openIdAuthenticate = {
		String openID = params['j_username']
		try {
			String returnToURL = RedirectUtils.buildRedirectUrl(
					request, response, openIDAuthenticationProcessingFilter.filterProcessesUrl)
			String redirectUrl = openIDConsumer.beginConsumption(request, openID, returnToURL)
			redirect url: redirectUrl
		}
		catch (org.springframework.security.ui.openid.OpenIDConsumerException e) {
			log.error "Consumer error: $e.message", e
			redirect url: openIDAuthenticationProcessingFilter.authenticationFailureUrl
		}
	}

	// Login page (function|json) for Ajax access.
	def authAjax = {
		nocache(response)
		//this is example:
		render """
		<script type='text/javascript'>
		(function() {
			loginForm();
		})();
		</script>
		"""
	}

	/**
	 * The Ajax success redirect url.
	 */
	def ajaxSuccess = {
		nocache(response)
		render '{success: true}'
	}

	/**
	 * Show denied page.
	 */
	def denied = {
		if (isLoggedIn() && authenticationTrustResolver.isRememberMe(SCH.context?.authentication)) {
			// have cookie but the page is guarded with IS_AUTHENTICATED_FULLY
			redirect action: full, params: params
		}
	}

	/**
	 * Login page for users with a remember-me cookie but accessing a IS_AUTHENTICATED_FULLY page.
	 */
	def full = {
		render view: 'auth', params: params,
			model: [hasCookie: authenticationTrustResolver.isRememberMe(SCH.context?.authentication)]
	}

	// Denial page (data|view|json) for Ajax access.
	def deniedAjax = {
		//this is example:
		render "{error: 'access denied'}"
	}

	/**
	 * login failed
	 */
	def authfail = {

		def username = session[AuthenticationProcessingFilter.SPRING_SECURITY_LAST_USERNAME_KEY]
		def msg = ''
		def exception = session[AbstractProcessingFilter.SPRING_SECURITY_LAST_EXCEPTION_KEY]
		if (exception) {
			if (exception instanceof DisabledException) {
				msg = "[$username] is disabled."
			}
			else {
				msg = "[$username] wrong username/password."
			}
		}

		if (isAjax()) {
			render "{error: '${msg}'}"
		}
		else {
			flash.message = msg
			redirect action: auth, params: params
		}
	}

	/**
	 * Check if logged in.
	 */
	private boolean isLoggedIn() {
		return authenticateService.isLoggedIn()
	}

	private boolean isAjax() {
		return authenticateService.isAjax(request)
	}

	/** cache controls */
	private void nocache(response) {
		response.setHeader('Cache-Control', 'no-cache') // HTTP 1.1
		response.addDateHeader('Expires', 0)
		response.setDateHeader('max-age', 0)
		response.setIntHeader ('Expires', -1) //prevents caching at the proxy server
		response.addHeader('cache-Control', 'private') //IE5.x only
	}
}
