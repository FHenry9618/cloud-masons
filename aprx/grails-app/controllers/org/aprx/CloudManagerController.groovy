package org.aprx


class CloudManagerController {

	// Removed as part of work to remove cloudmanager from aprx
	static navigation = [
			group:'tabs',
			order:1,
			title:'Cloud Manager',
			action:'index',
			role: 'NOONE',
			icon:'advanced'
	]

	def index = {
		//redirect(url:"https://springboard.cloudmasons.com/cloudmanager/cloud/dashboard/6")
		redirect(controller: 'dashboard', action: 'index');
	}
}