    
package org.aprx


import java.util.Properties;

import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Part;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.search.AndTerm;
import javax.mail.search.FlagTerm;
import javax.mail.search.NotTerm;
import javax.mail.search.SearchTerm;
import javax.mail.Flags;
import javax.mail.Flags.Flag;
import java.security.MessageDigest


import com.cloudmasons.beans.Feed

class AprxService {

    boolean transactional = false
    
    def authenticateService
    def mailService
    def feedService
    def beanReportService
	def grailsApplication
    
    public List getManagedStores() {
        return getManagedStores(getLoggedInUser())
    }
    
    public User getLoggedInUser() {
        
        def userPrincipal = authenticateService.principal()
        return userPrincipal?.domainClass
    }
    
    public List getManagedStores(User user) {
        String query = "select store from MemberStore as store where store.manager = :user OR store.stockHolderName = :user order by store.name"
        List stores = MemberStore.executeQuery(query, [user: user])    
        return stores
    }
    
    public MemberStore findStoreWithWholesalerAccount(List stores, String wholesalerNumber) {
        if (stores == null) return null
        MemberStore result
        for (store in stores) {
            result = store.accounts?.find { it.name == wholesalerNumber }
            if (result) break;
        }
        return result    
    }
    
    public User createStockholder(String email, List stores) {
        def emailArray = email.split('@')
        def alias = (emailArray) ? emailArray[0] : email
        def name = alias
        
        def user = new User(alias: alias, name: name, email: email)
        user.addToRoles(Role.findByName('MEMBER'))
        user.passwd = authenticateService.encodePassword( UUID.randomUUID().toString() )
        user.enabled = true
        
        def result = user.save(flush: true)
        if (!result) {
            println user.errors
            return null
        } else {
            user = result;
        }
        stores?.each { s ->
            s.stockHolder = user
            s.save();
        }
        return  user;   
    }

    /**
     * Calculate monthly, YTD and fiscal year TRV for a store
     */
    def calculateTRVandGCR(WholesalerAccount account, Date date) {
    

        String query = """select sum(summary.genericTotal) as generic, sum(summary.brandedTotal) as branded from WholesalerAccount acct, PurchaseSummary summary
            where acct.id = '${account.id}' and summary.account = acct and 
            summary.periodLength = 'day' and
            summary.periodStartDate >= :periodStart and summary.periodStartDate <= :periodEnd """

        // Query to get average TRV for the given month
        Date periodStart = DateUtil.getStartOfMonth(date)
        Date periodEnd = DateUtil.getEndOfMonth(date)
        Object monthResult = PurchaseSummary.executeQuery(query, [periodStart: periodStart, periodEnd: periodEnd]);
        //println "$periodStart - $periodEnd"
        println "Month: $monthResult"
        setTRVandGCR(account, periodStart, 'month', monthResult[0][0], monthResult[0][1]);
        
        // Query to YTD average TRV
        periodStart = DateUtil.getStartOfYear(date)
        periodEnd = DateUtil.getEndOfYear(date)
        Object yearResult = PurchaseSummary.executeQuery(query, [periodStart: periodStart, periodEnd: periodEnd]);
        //println "$periodStart - $periodEnd"
        //println "Year: $yearResult"
        setTRVandGCR(account, periodStart, 'year', yearResult[0][0], yearResult[0][1]);
        
        // Query to get fiscal year TRV
        periodStart = DateUtil.getStartOfFiscalYear(date)
        periodEnd = DateUtil.getEndOfFiscalYear(date)
        Object fiscalYearResult = PurchaseSummary.executeQuery(query, [periodStart: periodStart, periodEnd: periodEnd]);
        //println "$periodStart - $periodEnd"
        //println "Fiscal Year: $fiscalYearResult"
        setTRVandGCR(account, periodStart, 'fiscal-year', fiscalYearResult[0][0], fiscalYearResult[0][1]);
        
        
    }
    /**
     * look for an existing TRV calculation for the given data
     * if it doesn't exist then create a new one otherwise 
     */
    def setTRVandGCR(WholesalerAccount account, Date periodStartDate, String periodLength, BigDecimal genericTotal, BigDecimal brandedTotal) {
        def summary = new PurchaseSummary(account: account, periodStartDate: periodStartDate, periodLength: periodLength)
        
        String query = """select s from PurchaseSummary s
            where s.account = :account and s.periodLength = :periodLength and s.periodStartDate = :periodStartDate"""
        def result= PurchaseSummary.executeQuery(query, [account: account, periodLength: periodLength, periodStartDate: periodStartDate])
        //println result
        def s = (result && result.size() > 0) ? result[0] : summary
        //def s = summary
        s.genericTotal = genericTotal;
        s.brandedTotal = brandedTotal;
        s.gcr = GcrUtil.calculateGcr(account, brandedTotal, genericTotal);
        s.rebate = GcrUtil.calculateRebate(account, brandedTotal, genericTotal);
        s.save(validate: false, flush: true)
    }
    

    def contactUser(user_name, email, attachments) {
        mailService.sendMail {
                multipart true
                to email
                subject "Hello World"
                body "${user_name}"
                while(attachments.hasNext()) {
                    def attachment = attachments.next()
                    attachBytes attachment.getFileItem().getName(), attachment.getContentType(),  attachment.getBytes()
                }
        }
    }
    def sendGcrEmail(User user) {
        mailService.sendMail {
                to email
                subject "${report.name} ${new Date()}"
                body "Please see the attached ${report.name} generated on ${new Date()}"
        }
    }
   
    def sendReport(String email, Report report, String reportFormat='XLS', emptyReport='') {
        // determine the support address 
        def supportAddress = grailsApplication.config.grails.mail.supportAddress

	// generate the report attachment
        byte[] reportData = generateReport(report, reportFormat)

	// check for an empty report
        if (reportData.length < 1024) {
            mailService.sendMail {
                    to email
                    bcc supportAddress
                    subject "${report.name} ${new Date()}"
                    body emptyReport
            }
        } else {
            // determine the mime type
	    def mimeType = determineMineType(reportFormat)
            // send the email to the following addresses
            mailService.sendMail {
                    multipart true
                    to email
                    bcc supportAddress
                    subject "${report.name} ${new Date()}"
                    body "Please see the attached ${report.name} generated on ${new Date().format('MM-dd-yyyy')}"
                    attachBytes "report.${reportFormat}", mimeType, reportData
            }
        }
    }

    byte[] generateReport(Report report, String format) {
        def bos = new ByteArrayOutputStream();
        beanReportService.generateReport(report, bos, format);
        return bos.toByteArray()
    }

    private determineMineType(String format) {
        switch (format) {
            case 'XLS':
                return 'application/vnd.ms-excel'
            case 'PDF':
                return 'application/pdf'
        }
        return ''
    }

    def readMail() {
        String supportMessage = "";
        Properties props = System.getProperties();
        props.setProperty("mail.store.protocol", "imaps");
            try {
                Session session = Session.getDefaultInstance(props, null);
                Store store = session.getStore("imaps");
                store.connect("imap.gmail.com", "aprx@nsquard.biz", "ph@rm@cy");
                System.out.println(store);

                Folder inbox = store.getFolder("Inbox");
                inbox.open(Folder.READ_WRITE);
                
                NotTerm notDeleted = new NotTerm( new FlagTerm(new Flags(Flags.Flag.DELETED), true) );
                NotTerm notSeen = new NotTerm( new FlagTerm(new Flags(Flags.Flag.SEEN), true) );
                SearchTerm searchTerm = new AndTerm(notDeleted, notSeen);
                def messages = inbox.search(searchTerm)
                // Go through the messages and see if any are a MD invoice feed
                //def messages = inbox.getMessages();
                
                for(Message message:messages) {
                    System.out.println(message.subject);
                    
                    def attachments = getAttachments(message) ?: []
                    if (message.subject.startsWith("FW: API Generic Purchases Summary All") && attachments) {
                        def parsedSubject = message.subject.split(':')
                        def account = parsedSubject[1].trim();
                        supportMessage += "Processing received MD data"
                        Feed feed = Feed.findByName("Morris Dickson Purchase Summary Feed");
                        for (attachment in attachments) {
                            def defaultProperties = ['Account': account, 'Invoice': attachment.name]
                            feedService.processFileFeed(feed, attachment.file, defaultProperties);
                        }
                    }
                    else if (message.subject.startsWith("FW: API Generic Purchases Summary All") && !attachments) {
                        supportMessage += "MD email received but no data received"
                    }
                    else if (message.subject.startsWith("FW: AP") && attachments) {
                        supportMessage += "Processing AP ABC feed for account ${message.subject}"
                        Feed feed = Feed.findByName("Purchase Summary Feed");
                        for (attachment in attachments) {
                            def defaultProperties = [:]
                            feedService.processFileFeed(feed, attachment.file, defaultProperties);
                        }                        
                    }
                    else if (message.subject.startsWith("FW: AP") && !attachments) {
                        supportMessage += "ABC email received but no data received"
                    }
                    else {
                        supportMessage += "This may indicate spam or quarantine notices arriving at gcr@aprx.org, aprx@nsquard.biz, or a change in the subject line of incoming data feeds from wholesalers"
                    }
                    message.setFlag(Flags.Flag.SEEN,true);
                    
                }
                if (supportMessage && supportMessage != "") {
                    sendToSupport("Data processing report", supportMessage)
                }
                inbox.close(true);
                store.close();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        }

    }
    
    /**
     * Pull off the attachments into an array of maps where each attachment
     * has a map of with the name and bytes key.
     */
    public List getAttachments(Message message) {
        if (!(message.content instanceof Multipart)) return null;
        
        Multipart multipart = (Multipart) message.getContent();
        def attachments = [];
        int i = 0;
        println multipart
        println multipart.getCount();
        multipart.getCount().times {
            
            Part part = multipart.getBodyPart(i);
            println part
            println part.disposition
            i++
            String disposition = part.getDisposition();
            
             if ((disposition != null) && 
                 ((disposition.equalsIgnoreCase(Part.ATTACHMENT)) || 
                  (disposition.equals(Part.INLINE))) )  {
                println("Attchment found ${part.fileName}");
                File file = new File("/tmp/${part.fileName}");
                file.append(part.inputStream)
                attachments.add( [name: part.fileName, file: file] )
             }
        }
        return attachments
    }
    
    def sendMail(def closure) {
        // Send an email in an asychronous thread
        Thread.start {
            mailService.sendMail (closure)
        }
    }
    
    def sendToSupport(String subjectTxt, String bodyTxt) {
        def toAddress = grailsApplication.config.grails.mail.supportAddress
         
        sendMail {
            to toAddress
            subject subjectTxt
            body """
            
            ${bodyTxt}            
                
            """
        }
    }
}

