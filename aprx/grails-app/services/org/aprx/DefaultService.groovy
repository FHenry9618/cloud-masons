package org.aprx;

import java.math.BigDecimal;

import grails.util.GrailsUtil

import com.cloudmasons.beans.Feed
import com.cloudmasons.beans.FieldMapping
import com.cloudmasons.util.Transforms

/**
 * Simple service which create the default domain models for the application
 * when being initialized on a new database. 
 *
 */
class DefaultService {
    private static final String ROLE_FEED = "Roles"
    private static final String USER_FEED = "Users"
    private static final String STORE_FEED = "Stores"
    private static final String MORRIS_DICKSON_FEED = "Morris Dickson Line Item Feed"
    private static final String MORRIS_DICKSON_MONTHLY_SUMMARY_FEED = "Morris Dickson Monthly Summary Feed"

    private static final String ABC_FEED = "ABC Feed"
    private static final String PURCHASE_SUMMARY_FEED = "Purchase Summary Feed"
    private static final String ABC_PURCHASE_SUMMARY_FEED = "ABC Purchase Summary Feed"
    private static final String MD_PURCHASE_SUMMARY_FEED = "MD Purchase Summary Feed"


    private static final DEFAULT_FEED_FILES = [
            [ROLE_FEED, "roles.xls"],
            [USER_FEED, "users.xls"],
            [STORE_FEED, "stores100410.xls"],
            [MORRIS_DICKSON_FEED, "invoices.xls"],
            [ABC_FEED, "abc.xls"],
            [PURCHASE_SUMMARY_FEED, "purchase-summaries.xls"]
        ]
    
    def feedService
    def authenticateService
    def communityService
    def grailsApplication
    
    def createDefaultFeeds() {
        println("Initializing data feeds")


        feedService.createDefaultFeed(ABC_FEED, 
                                      'Feed to load data from ABC',
                                      'org.aprx.LineItem')
        def storeFeed = createMemberStoreFeed();
        def lineItemFeed = createLineItemFeed();
        def purchaseSummaryFeed = createPurchaseSummaryFeed(PURCHASE_SUMMARY_FEED, 'ABC');
        createMDMonthlyPurchaseSummaryFeed();
        
        def userFeed = feedService.createDefaultFeed(USER_FEED, 
                                      'Feed to load system users and their assigned roles',
                                      'org.aprx.User',
                                      ['name', 'alias', 'email'])
        userFeed.addMapping('Password', 'passwd', 'passwordEncoder.encodePassword(value, null);')
        userFeed.addMapping('Enabled', 'enabled', 'value ? value : true')
        userFeed.addMapping('Roles', null, """
          if (value) {
             def roles = value.split(',')
             roles?.each { role ->
                user.addRole(role.trim())
             }
          }
        """);
        userFeed.save()
		  
    }
    def createMemberStoreFeed() {
    
        Feed feed = new Feed(name: STORE_FEED, , description: 'Feed to load stores');
        feed.beanType = 'org.aprx.MemberStore'
        feed.correlator = """
            def name = record['Pharmacy']
            def instance = null
            if (!name) return instance
            instance = domainClass.clazz.findByName(name);
            if (instance) return instance
            
            instance = domainClass.newInstance()
            
            return instance
        """
        
        feed.addMapping('Pharmacy', 'name');
        feed.addMapping('Date Joined', 'dateJoined', Transforms.FIRST_VALID_DATE);
        feed.addMapping('Stk#', 'stockNumber', Transforms.TO_INTEGER);
        feed.addMapping('Contact', 'contactName');
        feed.addMapping('Email', 'contactEmail');
        feed.addMapping('Address', 'address');
        feed.addMapping('State', 'state');
        feed.addMapping('City', 'city');
        feed.addMapping('Zip Code', 'zipCode');
        feed.addMapping('Area', 'areaCode');
        feed.addMapping('Phone Number', 'phoneNumber');
        feed.addMapping('Phone', 'phoneNumber');
        feed.addMapping('Fax', 'phoneNumber');
        feed.addMapping('DEA #', 'deaNumber');
        feed.addMapping('Tax ID #', 'deaNumber');
        feed.addMapping('Stockholder', 'stockHolder', """
            if (value) { return org.aprx.User.findByName(value); } else { return currentValue; }
        """)        
        feed.addMapping('Manager', 'manager', """
            if (value) { return org.aprx.User.findByName(value); } else { return currentValue; }
        """)        

        feed.addMapping('Wholesaler', null, """
          def accountNum = record['Account#']

          if (value && accountNum) {
             def two = value.contains('/')
             if (two) {
                def wholesalers = value.split('/');
                def accountNums = accountNum.split('/');
                accountNum = accountNums[0] ?: 'unknown'
                accountNum = accountNum.replace('-', '');
                accountNum = accountNum.replace(' ', '');

                memberStore.addWholesalerAccount(wholesalers[0], accountNum);
                accountNum = accountNums[1] ?: 'unknown'
                accountNum = accountNum.replace('-', '');
                accountNum = accountNum.replace(' ', '');
                memberStore.addWholesalerAccount(wholesalers[1], accountNum);
             } else {
                accountNum = accountNum.replace('-', '');
                accountNum = accountNum.replace(' ', '');
                memberStore.addWholesalerAccount(value, accountNum);
             }
          }
        """);
        
        def result = feed.save()
        if (result) {
            feed = result
        } else {
            println feed.errors
        }
        return feed
    }

    def createMemberStoreUpdateFeed() {
        if (Feed.findByName('Member Store Update')) {
            return;
        }    
        Feed feed = new Feed(name: 'Member Store Update', , description: 'Feed to load updates to stores');
        feed.beanType = 'org.aprx.MemberStore'
        feed.correlator = """
            def name = record['Pharmacy']
            def instance = null
            if (!name) return instance
            instance = domainClass.clazz.findByName(name);
            if (instance) return instance
            
            instance = domainClass.newInstance()
            
            return instance
        """
        
        feed.addMapping('Pharmacy', 'name');
        feed.addMapping('Date Joined', 'dateJoined', Transforms.FIRST_VALID_DATE);
        feed.addMapping('Stk#', 'stockNumber', Transforms.TO_INTEGER);
        feed.addMapping('Contact', 'contactName');
        feed.addMapping('Email', 'contactEmail');
        feed.addMapping('Address', 'address');
        feed.addMapping('State', 'state');
        feed.addMapping('City', 'city');
        feed.addMapping('Zip Code', 'zipCode');
        feed.addMapping('Area', 'areaCode');
        //        feed.addMapping('Phone Number', 'phoneNumber');
        feed.addMapping('Phone', 'phoneNumber');
        feed.addMapping('Fax', 'faxNumber');
        feed.addMapping('DEA #', 'deaNumber');
        feed.addMapping('Tax ID #', 'deaNumber');
        
        def result = feed.save()
        if (result) {
            feed = result
        } else {
            println feed.errors
        }
        return feed
    }
    def createMDPurchaseSummaryFeed() {
        def wholesaler = "MD"
        def name = "Morris Dickson Purchase Summary Feed";
        if (Feed.findByName(name)) {
            return;
        }
        Feed feed = new Feed(name: name, , description: "Feed to load purchase data for ${wholesaler} accounts");
        feed.beanType = 'org.aprx.PurchaseSummary'
        feed.correlator = """
            def accountNumber = record['AccountNumber'] ?: ''
            accountNumber = accountNumber.replace('-', '');
            accountNumber = accountNumber.replace(' ', '');
            
            def deaNumber = record['DEA']
            def address = record['ShipAddress']
            def storeName = record['AccountName']
            
            //println ('-----------------------------------------  ---------------------------------------------------')
            //println record
            println accountNumber
            //println ('-----------------------------------------  ---------------------------------------------------')


            if (!accountNumber) {
                return null;
            }
            
            def wholesalerAccount = org.aprx.WholesalerAccount.findByName(accountNumber)
            
            if (!wholesalerAccount && deaNumber) {
                def memberStore = org.aprx.MemberStore.findByDeaNumber(deaNumber)
                println '----- deaNumber start-----'
                println memberStore
                wholesalerAccount = memberStore?.accounts?.find { it.wholesaler == '${wholesaler}' ; }
                println wholesalerAccount
                println '----- deaNumber -----'
            }

            def memberStore
            if (!wholesalerAccount && address) {
                memberStore = org.aprx.MemberStore.findByAddress(address)
                wholesalerAccount = memberStore?.accounts?.find { it.wholesaler == '${wholesaler}' ; }
            }
            if (!wholesalerAccount && storeName) {
                memberStore = org.aprx.MemberStore.findByName(storeName)
                wholesalerAccount = memberStore?.accounts?.find { it.wholesaler == '${wholesaler}' ; }
            }
            if (!wholesalerAccount) {
                if (!memberStore) {
                    warnings.append('Unable to find member store: ');
                    memberStore = new org.aprx.MemberStore(name: storeName, address: address)
                    memberStore.status = 'unknown'
                } else {
                    println "Found memberStore without wholesaler account: "
                }
                wholesalerAccount = new org.aprx.WholesalerAccount(name: accountNumber, wholesaler: '${wholesaler}')
                memberStore.addToAccounts(wholesalerAccount)
                memberStore.save(validate: false, flush: true)
                
                warnings.append(accountNumber)
                warnings.append(', ')
                warnings.append(deaNumber)
                        warnings.append(', ' )
                        warnings.append(address)
                        warnings.append(', ')
                        warnings.append(storeName)
                        
                return null;
            }
            
            Date recordDate = Transforms.firstValidDate(record['PurchaseDate'], null)
            if (!recordDate) return null;
            
            def periodStartDate = org.aprx.DateUtil.getStartOfDay(recordDate);
            // todo:  find by the correct date
            String query = 'select summary from PurchaseSummary summary where summary.account = :wholesalerAccount and summary.periodLength = :periodLength and summary.periodStartDate = :periodStart '
            println query
            println periodStartDate
            def result = org.aprx.PurchaseSummary.executeQuery(query, [wholesalerAccount:  wholesalerAccount, periodStart: periodStartDate, periodLength: 'day']); 
            def ps = null
            if (result && result.size() > 0) {
                ps = result[0]
            }
            println ps
            if (!ps) {
                ps = new org.aprx.PurchaseSummary(account: wholesalerAccount, periodLength: 'day', periodStartDate: periodStartDate)
                wholesalerAccount.addToPurchaseSummaries(ps);
                ps.account = wholesalerAccount;
            }
            return ps
        """
        feed.addMapping('TotalSourceGenericExtendedCost', 'genericTotal', 'Transforms.firstValidAmount(value, currentValue)');
        feed.addMapping('TotalRxExtendedCost', 'brandedTotal', '''
              Transforms.firstValidAmount(value, currentValue) - Transforms.firstValidAmount(record['TotalSourceGenericExtendedCost'])
              ''');
        feed.addMapping('PurchaseDate', 'periodStartDate', 'Transforms.firstValidDate(value, currentValue)');
        
        feed.recordPostProcessor = """
           def aprxService = grailsApplication.mainContext.getBean("aprxService")
           println purchaseSummary
           purchaseSummary.save(validate: false, flush: true)
           aprxService.calculateTRVandGCR(purchaseSummary.account, purchaseSummary.periodStartDate);
        """ 
     
        
        def result = feed.save()
        if (result) {
            feed = result
        } else {
            println feed.errors
        }
        return feed

    }

    def createPurchaseSummaryFeed(String name, String wholesaler) {
        Feed feed = new Feed(name: name, , description: "Feed to load purchase data for ${wholesaler} accounts");
        feed.beanType = 'org.aprx.PurchaseSummary'
        feed.correlator = """
            def accountNumber = record['T(\"Customer Account\")'] ?: ''
            accountNumber = accountNumber.replace('-', '');
            accountNumber = accountNumber.replace(' ', '');
            
            def deaNumber = record['T(\"DEA Number\")']
            def address = record['T(\"Street Address\")']
            def storeName = record['Name']
            
            //println ('-----------------------------------------  ---------------------------------------------------')
            //println record
            println accountNumber
            //println ('-----------------------------------------  ---------------------------------------------------')


            if (!accountNumber) {
                return null;
            }
            
            def wholesalerAccount = org.aprx.WholesalerAccount.findByName(accountNumber)
            
            if (!wholesalerAccount && deaNumber) {
                def memberStore = org.aprx.MemberStore.findByDeaNumber(deaNumber)
                println '----- deaNumber start-----'
                println memberStore
                wholesalerAccount = memberStore?.accounts?.find { it.wholesaler == '${wholesaler}' ; }
                println wholesalerAccount
                println '----- deaNumber -----'
            }

            def memberStore
            if (!wholesalerAccount && address) {
                memberStore = org.aprx.MemberStore.findByAddress(address)
                wholesalerAccount = memberStore?.accounts?.find { it.wholesaler == '${wholesaler}' ; }
            }
            if (!wholesalerAccount && storeName) {
                memberStore = org.aprx.MemberStore.findByName(storeName)
                wholesalerAccount = memberStore?.accounts?.find { it.wholesaler == '${wholesaler}' ; }
            }
            if (!wholesalerAccount) {
                if (!memberStore) {
                    warnings.append('Unable to find member store: ');
                    memberStore = new org.aprx.MemberStore(name: storeName, address: address)
                    memberStore.status = 'unknown'
                } else {
                    println "Found memberStore without wholesaler account: "
                }
                wholesalerAccount = new org.aprx.WholesalerAccount(name: accountNumber, wholesaler: '${wholesaler}')
                memberStore.addToAccounts(wholesalerAccount)
                memberStore.save(validate: false, flush: true)
                
                warnings.append(accountNumber)
                warnings.append(', ')
                warnings.append(deaNumber)
                        warnings.append(', ' )
                        warnings.append(address)
                        warnings.append(', ')
                        warnings.append(storeName)
                        
                return null;
            }
            
            Date recordDate = Transforms.firstValidDate(record['T(\"Date\")'], null)
            if (!recordDate) return null;
            
            def periodStartDate = org.aprx.DateUtil.getStartOfDay(recordDate);
            // todo:  find by the correct date
            String query = 'select summary from PurchaseSummary summary where summary.account = :wholesalerAccount and summary.periodLength = :periodLength and summary.periodStartDate = :periodStart '
            println query
            println periodStartDate
            def result = org.aprx.PurchaseSummary.executeQuery(query, [wholesalerAccount:  wholesalerAccount, periodStart: periodStartDate, periodLength: 'day']); 
            def ps = null
            if (result && result.size() > 0) {
                ps = result[0]
            }
            println ps
            if (!ps) {
                ps = new org.aprx.PurchaseSummary(account: wholesalerAccount, periodLength: 'day', periodStartDate: periodStartDate)
                wholesalerAccount.addToPurchaseSummaries(ps);
                ps.account = wholesalerAccount;
            }
            return ps
        """
        feed.addMapping('T(\"G\")', 'genericTotal', 'Transforms.sumFirstValidAmount(value, currentValue)');
        feed.addMapping('T(\"B\")', 'brandedTotal', 'Transforms.sumFirstValidAmount(value, currentValue)');
        feed.addMapping('T(\"Date\")', 'periodStartDate', 'Transforms.firstValidDate(value, currentValue)');
        
        feed.recordPostProcessor = """
           def aprxService = grailsApplication.mainContext.getBean("aprxService")
           println purchaseSummary
           purchaseSummary.save(validate: false, flush: true)
           aprxService.calculateTRVandGCR(purchaseSummary.account, purchaseSummary.periodStartDate);
        """ 
     
        
        def result = feed.save()
        if (result) {
            feed = result
        } else {
            println feed.errors
        }
        return feed
    } 

    def createAbcPurchaseDataFeed() {
        if (Feed.findByName('ABC Purchase Data Feed')) {
            return;
        }
        Feed feed = new Feed(name: 'ABC Purchase Data Feed', , description: "Feed to load raw purchase data from ABC");
        feed.beanType = 'org.aprx.AbcPurchaseData'
        feed.correlator = """
            def accountNumber = record['T(\"Customer Account\")'] ?: ''
            accountNumber = accountNumber.replace('-', '');
            accountNumber = accountNumber.replace(' ', '');
            
            def deaNumber = record['T(\"DEA Number\")']
            def address = record['T(\"Street Address\")']
            def storeName = record['Name']
            
            //println ('-----------------------------------------  ---------------------------------------------------')
            //println record
            println accountNumber
            //println ('-----------------------------------------  ---------------------------------------------------')


            if (!accountNumber) {
                return null;
            }
            
            def wholesalerAccount = org.aprx.WholesalerAccount.findByName(accountNumber)
            
            if (!wholesalerAccount && deaNumber) {
                def memberStore = org.aprx.MemberStore.findByDeaNumber(deaNumber)
                println '----- deaNumber start-----'
                println memberStore
                wholesalerAccount = memberStore?.accounts?.find { it.wholesaler == 'ABC' ; }
                println wholesalerAccount
                println '----- deaNumber -----'
            }

            def memberStore
            if (!wholesalerAccount && address) {
                memberStore = org.aprx.MemberStore.findByAddress(address)
                wholesalerAccount = memberStore?.accounts?.find { it.wholesaler == 'ABC' ; }
            }
            if (!wholesalerAccount && storeName) {
                memberStore = org.aprx.MemberStore.findByName(storeName)
                wholesalerAccount = memberStore?.accounts?.find { it.wholesaler == 'ABC' ; }
            }
            if (!wholesalerAccount) {
                if (!memberStore) {
                    warnings.append('Unable to find member store: ');
                    memberStore = new org.aprx.MemberStore(name: storeName, address: address)
                    memberStore.status = 'unknown'
                } else {
                    println "Found memberStore without wholesaler account: "
                }
                wholesalerAccount = new org.aprx.WholesalerAccount(name: accountNumber, wholesaler: 'ABC')
                memberStore.addToAccounts(wholesalerAccount)
                memberStore.save(validate: false, flush: true)
                
                warnings.append(accountNumber)
                warnings.append(', ')
                warnings.append(deaNumber)
                        warnings.append(', ' )
                        warnings.append(address)
                        warnings.append(', ')
                        warnings.append(storeName)
                        
                return null;
            }
            
            Date recordDate = Transforms.firstValidDate(record['T(\"Date\")'], null)
            if (!recordDate) return null;
            
            def periodStartDate = org.aprx.DateUtil.getStartOfDay(recordDate);
            // todo:  find by the correct date
            String query = 'select summary from AbcPurchaseData data where data.account = :wholesalerAccount data.invoiceDate = :periodStart '
            println query
            println periodStartDate
            def result = org.aprx.AbcPurchaseData.executeQuery(query, [wholesalerAccount:  wholesalerAccount, periodStart: periodStartDate]); 
            def ps = null
            if (result && result.size() > 0) {
                ps = result[0]
            }
            println ps
            if (!ps) {
                ps = new org.aprx.AbcPurchaseData(account: wholesalerAccount, invoiceDate: periodStartDate)
            }
            return ps
        """
        feed.addMapping('T(\"G\")', 'genericTotal', 'Transforms.sumFirstValidAmount(value, currentValue)');
        feed.addMapping('T(\"B\")', 'brandedTotal', 'Transforms.sumFirstValidAmount(value, currentValue)');
        feed.addMapping('T(\"N\")', 'netTotal', 'Transforms.sumFirstValidAmount(value, currentValue)');

        feed.addMapping('T(\"Customer Account\")', 'accountNumber');
        feed.addMapping('Name', 'accountName');
        feed.addMapping('T(\"Street Address\")', 'address');
        feed.addMapping('T(\"City\")', 'city');
        feed.addMapping('T(\"State\")', 'state');
        feed.addMapping('T(\"Zip Code\")', 'zipCode');
        feed.addMapping('Zip', 'zipCodeExtension');
        feed.addMapping('T(\"DEA Number\")', 'deaNumber');
        feed.addMapping('T(\"Invoice Number\")', 'invoiceNumber');
        
        
        feed.recordPostProcessor = """
           //def aprxService = grailsApplication.mainContext.getBean("aprxService")
           //println purchaseSummary
           //purchaseSummary.save(validate: false, flush: true)
           //aprxService.calculateTRVandGCR(purchaseSummary.account, purchaseSummary.periodStartDate);
        """ 
     
        
        def result = feed.save()
        if (result) {
            feed = result
        } else {
            println feed.errors
        }
        return feed
    } 
    
    def createLineItemFeed() {
        Feed feed = new Feed(name: MORRIS_DICKSON_FEED, , description: 'Feed to load invoice line items from morris dickson');
        feed.beanType = 'org.aprx.LineItem'
        feed.correlator = """
            def accountNumber = record['Account']
            def invoiceNumber = record['Invoice']
            def invoice = org.aprx.Invoice.findByName(invoiceNumber)
            if (!invoice) {
                invoice = new org.aprx.Invoice(name: invoiceNumber)
                invoice.account = org.aprx.WholesalerAccount.findByName(accountNumber)
                invoice.purchaseDate = new Date();
                invoice.save()
            }
            if (!invoice || !invoice.account) {
                return null;
            }
            instance = domainClass.newInstance()
            invoice.addToLineItems(instance)
            instance.invoice = invoice
            return instance
        """
        feed.addMapping('M&D No', 'name');             
        feed.addMapping('NDC No', 'drug',
        """
            def ndcno = record['NDC No'];
            def type = record['Type'];
            def itemdescription = record['Item Description'] 
            def drug = org.aprx.Drug.findByNdc(ndcno);
            if (!drug) {
                def d = new org.aprx.Drug(name: itemdescription, description: "a drug", ndc: ndcno, type: type)
                println d
                drug = d.save()
                if (!drug) println d.errors
                
            }   
            println drug
            return drug;
        """);            
        feed.addMapping('Cost', 'price', 'Transforms.firstValidAmount(value, currentValue)');
        feed.addMapping('Qty', 'amount', Transforms.TO_INTEGER);
      
       
     
        
        def result = feed.save()
        if (result) {
            feed = result
        } else {
            println feed.errors
        }
        return feed
    } 

    def createMDMonthlyPurchaseSummaryFeed() {
        Feed feed = new Feed(name: MORRIS_DICKSON_MONTHLY_SUMMARY_FEED, , description: "Feed to load monthly purchase data for MD accounts");
        feed.beanType = 'org.aprx.PurchaseSummary'
        feed.correlator = """
            String accountNumber = record['Cust No'] ?: ''
            accountNumber = accountNumber.replace('-', '');
            accountNumber = accountNumber.replace(' ', '');
            
            def storeName = record['Cust Name']
            
            //println ('-----------------------------------------  ---------------------------------------------------')
            //println record
            println accountNumber
            //println ('-----------------------------------------  ---------------------------------------------------')


            if (!accountNumber) {
                return null;
            }
            
            //def wholesalerAccount = org.aprx.WholesalerAccount.findByName(accountNumber)
            def accounts = org.aprx.WholesalerAccount.executeQuery('select wa from org.aprx.WholesalerAccount as wa where wa.name = :accountNumber', [accountNumber:accountNumber])
            org.aprx.WholesalerAccount wholesalerAccount = accounts ? accounts[0] : null;
            org.aprx.MemberStore memberStore = null;
                               
            if (!wholesalerAccount && storeName) {
                memberStore = org.aprx.MemberStore.findByName(storeName)
                
                if (!memberStore) {
                    warnings.append('Unable to find member store: ');
                    memberStore = new org.aprx.MemberStore(name: storeName)
                    memberStore.status = 'unknown'
                }
                wholesalerAccount = memberStore?.accounts?.find { it.wholesaler == 'MD' ; }
            }
            if (!wholesalerAccount) {
                wholesalerAccount = new org.aprx.WholesalerAccount(name: accountNumber, wholesaler: 'MD')
                memberStore.addToAccounts(wholesalerAccount)
                memberStore.save(validate: false, flush: true)
                
                warnings.append(accountNumber)
                warnings.append(', ')
                warnings.append(storeName)
                        
                return null;
            }

            String dateStr = record['Year-Month'] + '-01'            
            Date recordDate = Date.parse('yyyy-MM-dd', dateStr)
            if (!recordDate) return null;
            
            def periodStartDate = org.aprx.DateUtil.getStartOfMonth(recordDate);
            // todo:  find by the correct date
            String query = 'select summary from PurchaseSummary summary where summary.account = :wholesalerAccount and summary.periodLength = :periodLength and summary.periodStartDate = :periodStart '
            println query
            println periodStartDate
            def result = org.aprx.PurchaseSummary.executeQuery(query, [wholesalerAccount:  wholesalerAccount, periodStart: periodStartDate, periodLength: 'day']); 
            def ps = null
            if (result && result.size() > 0) {
                ps = result[0]
            }
            println ps
            if (!ps) {
                ps = new org.aprx.PurchaseSummary(account: wholesalerAccount, periodLength: 'day', periodStartDate: periodStartDate)
                wholesalerAccount.addToPurchaseSummaries(ps);
                ps.account = wholesalerAccount;
                return ps;
            } else {
                // don't re-process a record
                return null;
            }
        """
        feed.addMapping('Gross Generic Purchases', 'genericTotal', 'Transforms.sumFirstValidAmount(value, currentValue)');
        feed.addMapping('Total Net Purchases Less Generic Purchases', 'brandedTotal', 'Transforms.sumFirstValidAmount(value, currentValue)');
        
        feed.recordPostProcessor = """
           def aprxService = grailsApplication.mainContext.getBean("aprxService")
           println purchaseSummary
           purchaseSummary.save(validate: false, flush: true)

           aprxService.calculateTRVandGCR(purchaseSummary.account, purchaseSummary.periodStartDate);
           println "Hello there-----------------------------";
        """ 
     
        
        def result = feed.save()
        if (result) {
            feed = result
        } else {
            println feed.errors
        }
        return feed
    } 

    public loadDefaultFeeds = {
        def feedDir = grailsApplication.config.feeds.dataDir ?: 'data'
        def envDir =  grailsApplication.config.feeds.envSubDir ?: 'test'  
        
        DEFAULT_FEED_FILES.each { feed->
            def feedName = feed[0]
            // First check for the file in the environment specific directory
            // if not there check to see if it isn the feed directory
            def feedFile = new File("${feedDir}/${envDir}/${feed[1]}")
            if (!feedFile.exists()) {
               feedFile = new File("${feedDir}/${feed[1]}")
            }
            try {
                Feed f = Feed.findByName(feedName);
                if (f) {
                    String result = feedService.processFileFeed(f, feedFile)
                    println result
                }
            } catch (Throwable t) {
                println "Error processing ${feedName}: ${t}"
            }
        }
    }


}
