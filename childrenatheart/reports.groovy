import org.tbch.*

ClientRecord.count()

def startDate = new Date() -120
def endDate = new Date()

def list = ClientRecord.withCriteria {
    between('datePlacement', startDate, endDate)
}

list.size()