
# Create food_allergies column
alter table client_record add column food_allergies varchar(2000);

# Ticket 25
alter table client_record add column grade_level varchar(256);

# Ticket 16
alter table weekly_notes add column author varchar(256);
alter table case_notes add column author varchar(256);
