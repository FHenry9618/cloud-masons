<%=packageName ? "package ${packageName}\n\n" : ''%>

import com.cloudmasons.BeanUtils

class ${className}Controller {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def securityService
    
    def index = {
        redirect(action: "list", params: params)
    }

    def list = {
        def beanDomainClass = BeanUtils.resolveDomainClass(grailsApplication, "${className}")
        if (securityService && !securityService.isAuthorized(beanDomainClass?.clazz, 'list')) {
            redirect(controller:'dashboard', action:'index')
            return
        }
        render include(controller: 'bean', action:'list', params: additionalParams)
    }

    def create = {
        render include(controller: 'bean', action:'create', params: additionalParams)
    }

    def show = {
        def result = include(controller: 'bean', action:'show', id: "\${params.id}", params: additionalParams)
        if (result.toString().length() > 0) {
            render result
        } else {
            if  (flash.redirect && flash.redirection) {
                redirect (controller: flash.redirection.controller, action: flash.redirection.action, params: flash.redirection.params)
                return
            }
            redirect(action: "list")
        }
    }

    def edit = {
        def result = include(controller: 'bean', action:'edit', id: "\${params.id}", params: additionalParams)

        if (result.toString().length() > 0) {
            render result
        } else {
            if  (flash.redirect && flash.redirection) {
                redirect (controller: flash.redirection.controller, action: flash.redirection.action, params: flash.redirection.params)
                return
            }
            redirect(action: "list")
        }
    }

    def save = {
        def result =  include(controller: 'bean', action:'save', id: "\${params.id}", params: additionalParams)
        if (result.toString().length() > 0) {
            render result
        } else {
            if  (flash.redirect && flash.redirection) {
                println "!!!!!!!!!!!!!!!!!!!!!!!!!!!! Redirecting!!!!!"+flash.redirection
                redirect (controller: flash.redirection.controller, action: flash.redirection.action, params: flash.redirection.params)
                return
            }
            println "!!!!!!!!!!!!!!!!!!!!!!!!!!!! NON Redirecting!!!!!"
            redirect(action: "list")
        }
    }

    def update = {
        def result =  include(controller: 'bean', action:'update', id: "\${params.id}", params: additionalParams)

        if (result.toString().length() > 0) {
            render result
        } else {
            if  (flash.redirect && flash.redirection) {
                redirect (controller: flash.redirection.controller, action: flash.redirection.action, params: flash.redirection.params)
                return
            }
            redirect(action: "list")
        }
    }

    def delete = {
        def result =  include(controller: 'bean', action:'deleteObject', id: "\${params.id}", params: additionalParams)

        if (result.toString().length() > 0) {
            render result
        } else {
            if  (flash.redirect && flash.redirection) {
                redirect (controller: flash.redirection.controller, action: flash.redirection.action, params: flash.redirection.params)
                return
            }
            redirect(action: "list")
        }
    }
    
    def print = {
        def result = include(controller: 'bean', action:'printObject', id: "\${params.id}", params: additionalParams)
        String html = result.toString()
        if (html.length() > 0) {
            //org.w3c.tidy.Tidy tidy = new org.w3c.tidy.Tidy()
            //tidy.setXHTML(true)
            //StringWriter wrt = new StringWriter()
            //tidy.parse(new StringReader(html), wrt)
            render html
        } else {
            if  (flash.redirect && flash.redirection) {
                redirect (controller: flash.redirection.controller, action: flash.redirection.action, params: flash.redirection.params)
                return
            }
            redirect(action: "list")
        }
    }


    /**
     * Returns a new Map instance with additional parameters needed to forward to
     * the bean controller for default scaffold rendering
     */
    private Map getAdditionalParams() {
        def additionalParams = [:]
        additionalParams.beanClass ='${className}'                            // The name of the bean class
        additionalParams.beanControllerName = '${domainClass.propertyName}'   // This controller name for links
        return additionalParams
    }

}
