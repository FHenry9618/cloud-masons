package org.tbch

import org.springframework.security.context.SecurityContextHolder

class SecurityUtil {

    static currentUsername = {
        def pricipalInfo = SecurityContextHolder.context?.authentication?.principal
        pricipalInfo?.domainClass?.name
    }
}
