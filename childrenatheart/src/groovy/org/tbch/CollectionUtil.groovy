package org.tbch

class CollectionUtil {

	static def clearNulls(Collection c) {
		c?.findAll { it != null }
	}
}
