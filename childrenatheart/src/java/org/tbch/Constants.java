package org.tbch;

import java.util.Arrays;
import java.util.List;

public class Constants {

    public static final String STATUS_REFERRAL = "referral";
    public static final String STATUS_APPLICATION = "application";
    public static final String STATUS_PLACEMENT = "placement";
    public static final String STATUS_DISCHARGE = "discharge";

    public static final List<String> STATUS_STATES = Arrays.asList(STATUS_REFERRAL, STATUS_APPLICATION,
            STATUS_PLACEMENT, STATUS_DISCHARGE);
}
