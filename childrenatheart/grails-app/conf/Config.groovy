// locations to search for config files that get merged into the main config
// config files can either be Java properties files or ConfigSlurper scripts

grails.config.locations = [ 
  "classpath:${appName}-config.properties",
  "classpath:${appName}-config.groovy",
  "file:${userHome}/.grails/${appName}-config.properties",
  "file:${userHome}/.grails/${appName}-config.groovy"
]

// Mail configuration for sender
grails.mail.default.from='tbch-support@nsquard.biz'
grails.mail.default.to='tbch-incidentreport@nsquard.biz'

grails.logo = 'CAHM.jpg'
grails.organizationName = 'Children At Heart'

grails.mime.file.extensions = true // enables the parsing of file extensions from URLs into the request format
grails.mime.use.accept.header = false
grails.mime.types = [ 
  html: ['text/html','application/xhtml+xml'],
  xml: ['text/xml', 'application/xml'],
  text: 'text/plain',
  js: 'text/javascript',
  rss: 'application/rss+xml',
  atom: 'application/atom+xml',
  css: 'text/css',
  csv: 'text/csv',
  all: '*/*',
  json: ['application/json','text/json'],
  form: 'application/x-www-form-urlencoded',
  multipartForm: 'multipart/form-data'
]
// The default codec used to encode data with ${}
grails.views.default.codec="none" // none, html, base64
grails.views.gsp.encoding="UTF-8"
grails.converters.encoding="UTF-8"
// enable Sitemesh preprocessing of GSP pages
grails.views.gsp.sitemesh.preprocess = true
// scaffolding templates configuration
grails.scaffolding.templates.domainSuffix = 'Instance'

// Set to false to use the new Grails 1.2 JSONBuilder in the render method
grails.json.legacy.builder=false
// enabled native2ascii conversion of i18n properties files
grails.enable.native2ascii = true
// whether to install the java.util.logging bridge for sl4j. Disable fo AppEngine!
grails.logging.jul.usebridge = true
// packages to include in Spring bean scanning
grails.spring.bean.packages = []

cornerstone.security.systemAdminstratorRole = 'ROLE_SYSTEM_ADMINISTRATOR'

    //liquibase migration configuration
    migrations.enabled = false
    migrations.changelogs = ['grails-app//migrations//changelog.xml']
    
    // Activity plugin configuration
    activity.queue.capacity = 10
    activity.db.persistence.enabled = false
    activity.console.output.enabled = false
    
    printPDF.enabled = true
    
    clientId.seed = 1500
    parentSurvey.max.score = 5
    
    form.lock.after = 0
    webform.daily.cronExpression = '0 0/1 * * * ?'
    
    document.types = [
        'Application',
        'Intake Packet',
        'Birth Certificate', 
        'Misc Legal Paperwork',
        'Medical Release', 
        'Insurance card', 
        'Clothing Inventory',
        'Doctor visit', 
        'Dentist visit',
        'Psychiatric Visit', 
        'Immunization Record', 
        'Misc Medical Record', 
        'Report Card', 
        'School Discipline',
        'Testing Results',
        'Routine Correspondence',
        'Signature Page',
        'Counseling/Therapy',
        'Case Notes',
        'Other'
    ]
// set per-environment serverURL stem for creating absolute links
environments {
    production {
        grails.serverURL = "https://scribe.cahmin.org/${appName}"
        feeds.dataDir  = "/tmp/data"
        feeds.envSubDir= "prod"
        
        form.lock.after = 14
        webform.daily.cronExpression = '0 0 2 * * ?'

        uploadedFile.storage.path="/vol/uploadedDocuments/"
        activity.queue.capacity = 1000
        activity.db.persistence.enabled = true
        activity.console.output.enabled = true
        
        grails.cas.enabled = false
        grails.cas.casServerSecure = false
        grails.cas.localhostSecure = false
        
        // Mail-Server configuration for sender
        grails {
           mail {
             host = 'email-smtp.us-east-1.amazonaws.com'
             port = 25
             supportAddress = 'tbch-support@nsquard.biz'
             username = 'AKIAIBKR3SJ6WQJ4NIXA'
             password = 'Auxr9bSWRq8ZRpSF6pO+DcfvJqtF2QJUpE1CN5stzzX6'
             props = [
                 'mail.smtp.auth':'true',
                 'mail.smtp.ssl.enable':'true',
                 'mail.smtp.starttls.enable': 'true',
                 'mail.smtp.from': 'tbch-support@nsquard.biz'
              ]
           }
        }
    }
    development {
        grails.serverURL = "http://localhost:8080/${appName}"
    }
    test {
        grails.serverURL = "http://localhost:8080/${appName}"
        grails.cas.enabled = false
        grails.cas.casServerSecure = false
    }
}

// log4j configuration
log4j = {
    error  'org.codehaus.groovy.grails.web.servlet',  //  controllers
	       'org.codehaus.groovy.grails.web.pages', //  GSP
	       'org.codehaus.groovy.grails.web.sitemesh', //  layouts
	       'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
	       'org.codehaus.groovy.grails.web.mapping', // URL mapping
	       'org.codehaus.groovy.grails.commons', // core / classloading
	       'org.codehaus.groovy.grails.plugins', // plugins
	       'org.codehaus.groovy.grails.orm.hibernate', // hibernate integration
	       'org.springframework',
	       'org.hibernate',
           'net.sf.ehcache.hibernate'

    warn   'org.mortbay.log'
    debug  'org.tbch'
    debug  'com.cloudmasons'
}
