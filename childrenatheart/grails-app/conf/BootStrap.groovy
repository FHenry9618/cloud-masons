
import org.tbch.*;
import org.codehaus.groovy.grails.commons.ApplicationAttributes

class BootStrap {

    def defaultService
    def grailsApplication

    def init = { servletContext ->
        
        println "Set time zone to Central Time"
        TimeZone.setDefault(TimeZone.getTimeZone("America/Chicago"))
        
        def applicationContext = servletContext.getAttribute(ApplicationAttributes.APPLICATION_CONTEXT)
        
        def config = grailsApplication.config
        
        if (config.webform.daily.cronExpression) {
            println "Schedule cron job at ${config.webform.daily.cronExpression} to examinate web forms to see verifying their editability"
            WebFormEditabilityJob.schedule(config.webform.daily.cronExpression, null)
        }
        
        if (User.count() == 0 ) {
            new Role(name: 'ROLE_SYSTEM_ADMINISTRATOR', description: 'System administrator').save(flush: true);
            new Role(name: 'MEMBER_ADMINISTRATOR', description: 'General user').save(flush: true);
            new Role(name: 'MEMBER', description: 'General user').save(flush: true);
            new Role(name: 'HOUSE_PARENT', description: 'House Parent').save(flush: true);
            new Role(name: 'ROLE_REPORTING', description: 'Reporting Staff or Licensing').save(flush: true);
            new Role(name: 'ROLE_CASE_MANAGER', description: 'Case Manager').save(flush: true);
           try {
            defaultService.createDefaultFeeds()
            defaultService.loadDefaultFeeds()
            
           } catch (Throwable t) {
                println t
                t.printStackTrace()
           }
        }
    }
    
    
     def destroy = {
     }
} 
