grails.plugin.location.beans = "../masonry/plugins/beans"
grails.plugin.location.activity = "../masonry/plugins/activity"
grails.plugin.location.cornerstone = "../masonry/plugins/cornerstone"
grails.plugin.location.pdf = "../masonry/plugins/pdf"

grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"

grails.project.dependency.resolution = {
    // inherit Grails' default dependencies
    inherits( "global" ) {
        // uncomment to disable ehcache
        // excludes 'ehcache'
    }
    log "debug" 
    repositories {
        // hangs.. :(
        mavenRepo "http://repo.grails.org/grails/repo"
        mavenLocal()
        mavenCentral()
    }
    dependencies {
        // dependencies do *not* appear to be working
        // anymore so just copying plugins and deps
        // to the lib directory
    }
}

grails.war.resources = { stagingDir -> 
    delete(file:"${stagingDir}/WEB-INF/lib/slf4j-api-1.3.1.jar") 
}
