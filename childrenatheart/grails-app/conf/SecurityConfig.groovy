security {

	// see DefaultSecurityConfig.groovy for all settable/overridable properties

	active = true

    providerNames = ['emailAuthenticationProvider', 'daoAuthenticationProvider','anonymousAuthenticationProvider', 'rememberMeAuthenticationProvider']
    
    loginUserDomainClass = "org.tbch.User"
    userName = 'alias'
    relationalAuthorities = "roles"
    authorityDomainClass = "org.tbch.Role"
    authorityField = "name"
    requestMapClass="com.tbch.Requestmap"
    
    useRequestMapDomainClass = false

    requestMapString = ''' 
                CONVERT_URL_TO_LOWERCASE_BEFORE_COMPARISON 
                PATTERN_TYPE_APACHE_ANT
                /index.gsp=IS_AUTHENTICATED_ANONYMOUSLY
                /index*=IS_AUTHENTICATED_ANONYMOUSLY
                /login/**=IS_AUTHENTICATED_ANONYMOUSLY
                /securefile/upload=IS_AUTHENTICATED_ANONYMOUSLY
                /js/**=IS_AUTHENTICATED_ANONYMOUSLY
                /css/**=IS_AUTHENTICATED_ANONYMOUSLY
                /plugins/cornerstone*/js/**=IS_AUTHENTICATED_ANONYMOUSLY
                /plugins/cornerstone*/css/**=IS_AUTHENTICATED_ANONYMOUSLY
                /images/**=IS_AUTHENTICATED_ANONYMOUSLY
                /clientrecord/**=IS_AUTHENTICATED_FULLY
                /planofservice/**=IS_AUTHENTICATED_FULLY
                /parentsurvey/**=IS_AUTHENTICATED_FULLY
                /user/settings=IS_AUTHENTICATED_FULLY
                /user/updatesettings=IS_AUTHENTICATED_FULLY
                /user/**=ROLE_SYSTEM_ADMINISTRATOR
                /dashboard/**=IS_AUTHENTICATED_FULLY
                /report/**=ROLE_REPORTING,ROLE_SYSTEM_ADMINISTRATOR,ROLE_CASE_MANAGER
                /admissionassessment/**=IS_AUTHENTICATED_FULLY
                /seriousincidentreport/**=IS_AUTHENTICATED_FULLY
                /weeklynotes/**=IS_AUTHENTICATED_FULLY
                /monthlynotes/**=IS_AUTHENTICATED_FULLY
                /preliminaryserviceplan/**=IS_AUTHENTICATED_FULLY
                /addendumrestraintreport/**=IS_AUTHENTICATED_FULLY
                /aftercare/**=IS_AUTHENTICATED_FULLY
                /dischargeform/**=IS_AUTHENTICATED_FULLY
                /visitationform/**=IS_AUTHENTICATED_FULLY
                /feed/**=IS_AUTHENTICATED_FULLY  
                /beanreport/**=IS_AUTHENTICATED_FULLY
                /activity/**=ROLE_SYSTEM_ADMINISTRATOR
                /parent/**=ROLE_SYSTEM_ADMINISTRATOR
                /parent/create=IS_AUTHENTICATED_FULLY
                /parent/save=IS_AUTHENTICATED_FULLY
                /campus/**=IS_AUTHENTICATED_FULLY
                /program/**=IS_AUTHENTICATED_FULLY
                /parentinfo/**=IS_AUTHENTICATED_FULLY
                /livingunit/**=IS_AUTHENTICATED_FULLY
                /role/**=ROLE_SYSTEM_ADMINISTRATOR
            '''        

}

//environments {
//    production {
//        security {
//            // CAS
//            useCAS = true
//            afterLogoutUrl = "https://springboard.cloudmasons.com/cas/logout"
//        }
//    }
//
//    test {
//        security {
//            // CAS
//            useCAS = true
//            afterLogoutUrl = "https://test.cloudmasons.com/cas/logout"
//        }
//    }
//
//}
