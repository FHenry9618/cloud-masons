package org.tbch

class URLRequestHistoryFilters {

    def beanService
    
    def filters = {
        
        dashboard(controller:'dashboard', action:'member') {
            after = {
                beanService.saveRequestHistoryToSession(params,session)
            }
        }
    }
    
    def saveURLRequest(def params, def session) {
        List previousRequests = session.previousRequests
        if (!previousRequests) {
            previousRequests = []
        }
        if (previousRequests.size > 20) {
            previousRequests.remove(0)
        }
        if (params.action) {
            def previousRequest = [:]
            previousRequest.action = params.action
            previousRequest.controller = params.controller
            previousRequests.add(previousRequest)
            
        }
        session.previousRequests = previousRequests
    }
}
