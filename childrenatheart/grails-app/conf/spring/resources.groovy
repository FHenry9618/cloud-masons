import org.tbch.EmailAuthenticationProvider

beans = {
      //customPropertyEditorRegistrar(org.tbch.TbchPropertyEditorRegistrar) 
      // Set up a customer authn provider so users can login with email or username
      emailAuthenticationProvider(EmailAuthenticationProvider) {
         authenticateService = ref("authenticateService")
      }

}