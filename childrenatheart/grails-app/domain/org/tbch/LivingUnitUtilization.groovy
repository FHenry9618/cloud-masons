package org.tbch

class LivingUnitUtilization implements Serializable {

    Date dateCreated
    Date lastUpdated
    
    float utilization
    LivingUnit livingUnit
    
    static belongsTo = LivingUnit
    
    public String getDescription() {
        return "Utilization: ${utilization}"
    }
    
    public String getName() {
        return "livingUnit.name's utilization"
    }
    
    static transients = ['name','description']
    static constraints = {
        utilization(range:0..1000)
        livingUnit(editable:false)
    }

}
