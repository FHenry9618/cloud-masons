package org.tbch

class PreliminaryServicePlan implements Serializable {

    String name
    String description //transcient
    Date date
    
    String physicalNeeds
    String physicalStrategy
    String physicalPerson
    
    String educationalNeeds
    String educationalStrategy
    String educationalPerson
    
    String behavioralNeeds
    String behavioralStrategy
    String behavioralPerson
    
    String familialNeeds
    String familialStrategy
    String familialPerson
    
    String socialNeeds
    String socialStrategy
    String socialPerson
    
    String developedBy
    
    static transients = ['description']
    static belongsTo = ClientRecord
    ClientRecord clientRecord
    SignatureDoc signaturePage
    Date dateCreated
    Date lastUpdated
    
    boolean editable=true
    
    static constraints = {
        
        name(summary: true, blank:false,group: 'Client Information', readOnly:true)
        
        date(summary: true, group: 'Service Plan Info',renderOptions:[defaultValue: { new Date() }])
        developedBy(blank:false, summary: true, group: 'Service Plan Info',renderOptions:[ 
            defaultValue: { def pricipalInfo = org.springframework.security.context.SecurityContextHolder.context?.authentication?.principal
                            def user  = pricipalInfo?.domainClass 
                            return user.name
                          }])
        
        physicalNeeds(nullable:true,maxSize:2000,group:'Immediate Physical Needs',renderOptions:[longLabel:true, defaultValue: { '<<Input Here>>' }, validate: true])
        physicalStrategy(nullable:true,maxSize:2000,group:'Immediate Physical Needs',renderOptions:[defaultValue: { '<<Input Here>>' }, validate: true])
        physicalPerson(nullable:true,group:'Immediate Physical Needs',renderOptions:[defaultValue: { '<<Input Here>>' }, validate: true])
        
        educationalNeeds(nullable:true,maxSize:2000,group:'Immediate Educational Needs',renderOptions:[longLabel:true, defaultValue: { '<<Input Here>>' }, validate: true])
        educationalStrategy(nullable:true,maxSize:2000,group:'Immediate Educational Needs',renderOptions:[defaultValue: { '<<Input Here>>' }, validate: true])
        educationalPerson(nullable:true,group:'Immediate Educational Needs',renderOptions:[defaultValue: { '<<Input Here>>' }, validate: true])
        
        behavioralNeeds(nullable:true,maxSize:2000,group:'Immediate Behavioral Management Needs',renderOptions:[longLabel:true, defaultValue: { '<<Input Here>>' }, validate: true])
        behavioralStrategy(nullable:true,maxSize:2000,group:'Immediate Behavioral Management Needs',renderOptions:[defaultValue: { '<<Input Here>>' }, validate: true])
        behavioralPerson(nullable:true,group:'Immediate Behavioral Management Needs',renderOptions:[defaultValue: { '<<Input Here>>' }, validate: true])
        
        familialNeeds(nullable:true,maxSize:2000,group:'Immediate Familial Needs',renderOptions:[longLabel:true, defaultValue: { '<<Input Here>>' }, validate: true])
        familialStrategy(nullable:true,maxSize:2000,group:'Immediate Familial Needs',renderOptions:[defaultValue: { '<<Input Here>>' }, validate: true])
        familialPerson(nullable:true,group:'Immediate Familial Needs',renderOptions:[defaultValue: { '<<Input Here>>' }, validate: true])
        
        socialNeeds(nullable:true,maxSize:2000,group:'Immediate Social Needs',renderOptions:[longLabel:true, defaultValue: { '<<Input Here>>' }, validate: true])
        socialStrategy(nullable:true,maxSize:2000,group:'Immediate Social Needs',renderOptions:[defaultValue: { '<<Input Here>>' }, validate: true])
        socialPerson(nullable:true,group:'Immediate Social Needs',renderOptions:[defaultValue: { '<<Input Here>>' }, validate: true])
        signaturePage(nullable:true,group:'Signature Page',renderOptions:[unprintable:true])
        clientRecord(group:'Client Record',editable:false)
        editable(editable:false)
        dateCreated(editable:false)
        lastUpdated(editable:false)
    }
    
    String getDescription() {
        return "${name}'s preliminary service plan"
    }
    
    void setName(String name) {
        if (clientRecord) {
            this.name = clientRecord.name
        }
        else {
            this.name = name
        }
    }
    
    String getName() {
        if (clientRecord) {
            return clientRecord.name
        }
        else {
            return this.name
        }
    }
    
    static actions = [
        [action: 'list', type:'static', label: 'List', role: 'ROLE_SYSTEM_ADMINISTRATOR'],
        [action: 'create', type:'static', label: 'Create', role: 'ROLE_SYSTEM_ADMINISTRATOR,ROLE_CASE_MANAGER'],
        [action: 'show', label: 'View',  role: 'ROLE_SYSTEM_ADMINISTRATOR'],
        [action: 'edit', type:'hidden', role: [ 'ROLE_SYSTEM_ADMINISTRATOR': [:],
                                                'ROLE_CASE_MANAGER': [filter: User.caseManagerEditFilter]] ],
        [action: 'delete', role: 'ROLE_SYSTEM_ADMINISTRATOR']
    ]
}
