package org.tbch

class DentalEntry extends PlanEntry {

    static belongsTo = PlanOfService
    //ServicePlanEntry servicePlanEntry
    PlanOfService parentObject
    
    static constraints = {
        //servicePlanEntry(nullable:false)
        parentObject(editable:false)
    }
}
