package org.tbch

class MedicalEntry extends PlanEntry {

    static belongsTo = PlanOfService
    PlanOfService parentObject
    
    static constraints = {
        parentObject(editable:false)
    }
}
