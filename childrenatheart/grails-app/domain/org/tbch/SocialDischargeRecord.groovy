package org.tbch

class SocialDischargeRecord extends PlanEntry {

    static belongsTo = DischargeForm
    DischargeForm parentObject
    
    static constraints = {
        parentObject(editable:false)
    }
    
    public def clone() throws CloneNotSupportedException {
        def clone = new SocialDischargeRecord() 
        clone.properties = this.properties
        clone.properties = super.clone().properties
        return clone
    }
}
