package org.tbch

class Aftercare implements Serializable {

    static final List GRADE_LEVEL = [
        'Kindergarten',
        'Elementary (1-5)',
        'Middle School (6-8)',
        'High School (9-12)',
        'Preschool (not in school)'
    ]

    String name
    String description
    float age
    String grade
    Date placementDate
    Date dischargeDate
    String dichargeType
    String placementAtDischarge
    String relationship
    String address1
    String address2
    String city
    String state
    String zip
    String phoneNumber
    
    String parent
    String parentAddress1
    String parentAddress2
    String parentCity
    String parentState
    String parentZip
    String parentHomePhoneNumber
    String parentWorkPhoneNumber
    String reasonForPlacement
    String circumstancesForDischarge
    String progress
    String dischargeConcern
    String dischargeTime
    boolean medication
    String medicationReason
    boolean specialService
    String serviceType
    boolean counseling
    boolean furtherCounseling
    boolean continueCounseling
    String followUpIssue
    
    Date firstFollowUpDate
    String firstFollowUpCaseManger
    String firstFollowUpNotes
    Date secondFollowUpDate
    String secondFollowUpCaseManger
    String secondFollowUpNotes
    Date thirdFollowUpDate
    String thirdFollowUpCaseManger
    String thirdFollowUpNotes
    Date otherFollowUpDate
    String otherFollowUpCaseManger
    String otherFollowUpNotes
    
    static transients = ['description','age']
    static belongsTo = ClientRecord
    ClientRecord clientRecord
    
    Date dateCreated
    Date lastUpdated
    
    boolean editable=true
    
    String getDescription() {
        return "${name}'s aftercare followup"
    }
    
    void setName(String name) {
        if (clientRecord) {
            this.name = clientRecord.name
        }
        else {
            this.name = name
        }
    }
    
    String getName() {
        if (clientRecord) {
            return clientRecord.name
        }
        else {
            return this.name
        }
    }
    
    float getAge() {
        if (clientRecord) {
            return clientRecord?.age
        }
        else {
            return 0
        }
    }
    
    static constraints = {
        name(summary: true, blank:false,group: 'Aftercare Information')
        
        age(group: 'Aftercare Information',renderOptions:[readOnly:true])
        grade(nullable:true, inList:GRADE_LEVEL, group:'Aftercare Information')
        placementDate(nullable:true, group:'Aftercare Information')
        dischargeDate(nullable:true, group:'Aftercare Information')
        dichargeType(nullable:true, group:'Aftercare Information')
        placementAtDischarge(nullable:true, group:'Aftercare Information')
        relationship(nullable:true, inList: SignificantPerson.RELATIONSHIP, group:'Aftercare Information')
        address1(nullable:true, group:'Aftercare Information')
        address2(nullable:true, group:'Aftercare Information')
        city(nullable:true, group:'Aftercare Information')
        state(nullable:true, group:'Aftercare Information')
        zip(nullable:true, group:'Aftercare Information')
        phoneNumber(nullable:true, group:'Aftercare Information',maxSize:2000,renderOptions:[longLabel:true])
        
        parent(nullable:true, group:'Aftercare Information')
        parentAddress1(nullable:true, group:'Aftercare Information')
        parentAddress2(nullable:true, group:'Aftercare Information')
        parentCity(nullable:true, group:'Aftercare Information')
        parentState(nullable:true, group:'Aftercare Information')
        parentZip(nullable:true, group:'Aftercare Information')
        parentHomePhoneNumber(nullable:true, group:'Aftercare Information')
        parentWorkPhoneNumber(nullable:true, group:'Aftercare Information')
        reasonForPlacement(nullable:true, group:'Aftercare Information',maxSize:2000,renderOptions:[longLabel:true])
        circumstancesForDischarge(nullable:true, group:'Aftercare Information',maxSize:2000,renderOptions:[longLabel:true])
        progress(nullable:true, group:'Aftercare Information',maxSize:2000,renderOptions:[longLabel:true])
        dischargeConcern(nullable:true, group:'Aftercare Information',maxSize:2000,renderOptions:[longLabel:true])
        dischargeTime(nullable:true, group:'Aftercare Information',maxSize:2000,renderOptions:[longLabel:true])
        medication(group:'Aftercare Information')
        medicationReason(nullable:true, group:'Aftercare Information',maxSize:2000,renderOptions:[longLabel:true])
        specialService(group:'Aftercare Information')
        serviceType(nullable:true, group:'Aftercare Information',maxSize:2000,renderOptions:[longLabel:true])
        counseling(group:'Aftercare Information')
        furtherCounseling(group:'Aftercare Information')
        continueCounseling(group:'Aftercare Information')
        followUpIssue(nullable:true, group:'Aftercare Information',maxSize:2000,renderOptions:[longLabel:true])
        
        firstFollowUpDate(nullable:true, group: '30 Day Follow-up')
        firstFollowUpCaseManger(nullable:true, group: '30 Day Follow-up')
        firstFollowUpNotes(nullable:true, group: '30 Day Follow-up',maxSize:2000,renderOptions:[longLabel:true])
        secondFollowUpDate(nullable:true, group: '90 Day Follow-up')
        secondFollowUpCaseManger(nullable:true, group: '90 Day Follow-up')
        secondFollowUpNotes(nullable:true, group: '90 Day Follow-up',maxSize:2000,renderOptions:[longLabel:true])
        thirdFollowUpDate(nullable:true, group: 'One Year Follow-up')
        thirdFollowUpCaseManger(nullable:true, group: 'One Year Follow-up')
        thirdFollowUpNotes(nullable:true, group: 'One Year Follow-up',maxSize:2000,renderOptions:[longLabel:true])
        otherFollowUpDate(nullable:true, group: 'Any Other Follow-up')
        otherFollowUpCaseManger(nullable:true, group: 'Any Other Follow-up')
        otherFollowUpNotes(nullable:true, group: 'Any Other Follow-up',maxSize:2000,renderOptions:[longLabel:true])

        clientRecord(group:'Client Record',editable:false)
        
        editable(editable:false)
        dateCreated(editable:false)
        lastUpdated(editable:false)
    }
    
    static actions = [
        [action: 'list', type:'static', label: 'List', role: 'ROLE_SYSTEM_ADMINISTRATOR'],
        [action: 'create', type:'static', label: 'Create', role: 'ROLE_SYSTEM_ADMINISTRATOR,ROLE_CASE_MANAGER'],
        [action: 'show', label: 'View',  role: 'ROLE_SYSTEM_ADMINISTRATOR'],
        [action: 'edit', type:'hidden', role: [ 'ROLE_SYSTEM_ADMINISTRATOR': [:],
                                                'ROLE_CASE_MANAGER': [filter: User.caseManagerEditFilter]] ],
        [action: 'delete', role: 'ROLE_SYSTEM_ADMINISTRATOR']
    ]
}
