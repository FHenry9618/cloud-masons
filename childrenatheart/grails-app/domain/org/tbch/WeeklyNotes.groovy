package org.tbch

class WeeklyNotes implements Serializable {

    String name
    Date date
    Date endDate
    String description
    
    String physical
    String familyContact
    String educational
    String emotional
    String spiritual
    String social
    String behavioral
    String recreational
    String independentLiving

    String author

    static transients = ['description']
    static belongsTo = ClientRecord
    ClientRecord clientRecord
    
    Date dateCreated
    Date lastUpdated
    
    boolean editable=true
    
    String getDescription() {
        return "${name}'s weekly progress report"
    }
    
    void setName(String name) {
        if (clientRecord) {
            this.name = clientRecord.name
        }
        else {
            this.name = name
        }
    }
    
    String getName() {
        clientRecord?.name ?: this.name
    }

    static mapping = {
        cache true
        sort lastUpdated:'desc'
    }

    static TITLE = 'Notes Details'

    static constraints = {
        name(blank:false,group: TITLE)
        date(summary: true, group: TITLE)
        endDate(summary: true, group: TITLE)
        author(summary: true, group:TITLE, blank: false, nullable: true,
                renderOptions:[defaultValue: User.fullname, validate: true])
        physical(nullable:true,maxSize:2000,group:TITLE)
        familyContact(nullable:true,maxSize:2000,group:TITLE)
        educational(nullable:true,maxSize:2000,group:TITLE)
        emotional(nullable:true,maxSize:2000,group:TITLE)
        spiritual(nullable:true,maxSize:2000,group:TITLE)
        social(nullable:true,maxSize:2000,group:TITLE)
        behavioral(nullable:true,maxSize:2000,group:TITLE)
        recreational(nullable:true,maxSize:2000,group:TITLE)
        independentLiving(nullable:true,maxSize:2000,group:TITLE)
        clientRecord(group:'Client Record',editable:false)


        editable(editable:false)
        dateCreated(editable:false)
        lastUpdated(editable:false)
    }
    
    static actions =
    [
        [action: 'list', type:'static', label: 'List', role: 'ROLE_SYSTEM_ADMINISTRATOR'],
        [action: 'create', type:'static', label: 'Create', role: 'ROLE_SYSTEM_ADMINISTRATOR,ROLE_CASE_MANAGER,HOUSE_PARENT'],
        [action: 'show', label: 'Show',  role: [ 'ROLE_SYSTEM_ADMINISTRATOR': [:],'ROLE_REPORTING':[:],
                                                 'HOUSE_PARENT': [filter: User.houseParentShowFilter],
                                                 'ROLE_CASE_MANAGER': [filter: User.caseManagerShowFilter]] ],
        [action: 'print', label: 'Print', type:'hidden', role: [ 'ROLE_SYSTEM_ADMINISTRATOR': [:],'ROLE_REPORTING':[:],
                                                 'HOUSE_PARENT': [filter: User.caseManagerShowFilter],
                                             'ROLE_CASE_MANAGER': [filter: User.caseManagerShowFilter]] ],
        [action: 'edit', type:'hidden', label: 'Edit', role: [ 'ROLE_SYSTEM_ADMINISTRATOR': [:],
                                                            'HOUSE_PARENT': [filter: User.houseParentEditFilter],
                                                            'ROLE_CASE_MANAGER': [filter: User.caseManagerEditFilter]] ],
        [action: 'delete', role: 'ROLE_SYSTEM_ADMINISTRATOR']
    ]
}
