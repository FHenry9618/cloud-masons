package org.tbch

class CulturalEntry extends PlanEntry {

    static belongsTo = PlanOfService
    PlanOfService parentObject
    
    static constraints = {
        parentObject(editable:false)
    }
}
