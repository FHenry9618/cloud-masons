package org.tbch;

class Report implements Serializable {
    public static final List FORMATS = ['XLS', 'PDF', 'HTML', 'CSV']
    String name
    String description
    String role
    String configScript
    ConfigObject config
    static transients = [ 'config' ]
    
    
    static mapping = {
       cache true
    }

    static constraints = {
        name(summary: true)
        description(nullable: true, summary: true)
        role(nullable: true)
        configScript(nullable: true, maxSize: 6000)
    }
    
    public ConfigObject getConfig() {
        if (config != null) return config
        if (configScript == null) return config
        String script = "${DEFAULT_IMPORTS} ${configScript}"
        //println script
        config = new ConfigSlurper().parse(script)
 
        return config
    }
    
    private static final String DEFAULT_IMPORTS = '''
import ar.com.fdvs.dj.domain.AutoText
import ar.com.fdvs.dj.domain.constants.Font
import ar.com.fdvs.dj.domain.constants.Border
import java.awt.Color
import ar.com.fdvs.dj.domain.constants.Transparency
import ar.com.fdvs.dj.domain.constants.HorizontalAlign
import ar.com.fdvs.dj.core.layout.HorizontalBandAlignment
import ar.com.fdvs.dj.domain.constants.VerticalAlign
import ar.com.fdvs.dj.domain.constants.Rotation
import ar.com.fdvs.dj.domain.constants.Stretching
import ar.com.fdvs.dj.domain.constants.Page

    ''';
}
