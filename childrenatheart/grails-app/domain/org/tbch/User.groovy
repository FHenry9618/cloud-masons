package org.tbch

import java.util.regex.Pattern
import org.springframework.security.context.SecurityContextHolder

/**
 * 
 */
class User implements Serializable {

    static fullname = {
        def principal = SecurityContextHolder.context?.authentication?.principal
        def user  = principal?.domainClass
        user?.refresh()
        return user?.name
    }

    static houseParentEditFilter = {
        def principal = SecurityContextHolder.context?.authentication?.principal
        def user  = principal?.domainClass
        user?.refresh()
        return editable && clientRecord?.livingUnit?.id == user?.livingUnit?.id
    }

    static houseParentOpenEditFilter = {
        def principal = SecurityContextHolder.context?.authentication?.principal
        def user  = principal?.domainClass
        user?.refresh()
        return clientRecord?.livingUnit?.id == user?.livingUnit?.id
    }

    static houseParentShowFilter = houseParentOpenEditFilter

    static caseManagerEditFilter = {
        def principal = SecurityContextHolder.context?.authentication?.principal
        def user  = principal?.domainClass
        user?.refresh()
        return (editable && (clientRecord?.campus == null || clientRecord?.campus?.id == user?.campus?.id))
    }

    static caseManagerOpenEditFilter = {
        def principal = SecurityContextHolder.context?.authentication?.principal
        def user  = principal?.domainClass
        user?.refresh()
        return (clientRecord?.campus == null || clientRecord?.campus?.id == user?.campus?.id)
    }

    static caseManagerShowFilter = caseManagerOpenEditFilter


    static transients = ['pass']
    static hasMany = [roles: Role]
    static belongsTo = Role

    /** */
    String alias

    /** Member Real Name*/
    String name

    /** MD5 Password */
    String passwd
    /** enabled */
    boolean enabled = true

    String email
    boolean emailShow = true

    /** description */
    String description = ''

    /** plain password to create a MD5 password */
    String pass = '[secret]'

    String passwdResetKey // key to reset the password
    Date passwdResetKeyExpiration
    String theme = 'black'  // todo: selectable theme with settings per theme

    Campus campus
    LivingUnit livingUnit

    static mapping = { cache true }
    static actions =
    [
        [action: 'list', type:'static', label: 'List', role: 'ROLE_SYSTEM_ADMINISTRATOR'],
        [action: 'create', type:'static', label: 'Add', role: 'ROLE_SYSTEM_ADMINISTRATOR'],
        [action: 'show', type:'hidden', label: 'view',  role: 'ROLE_SYSTEM_ADMINISTRATOR'],
        [action: 'edit', role: 'ROLE_SYSTEM_ADMINISTRATOR', label: 'Edit'],
        [action: 'delete', role: 'ROLE_SYSTEM_ADMINISTRATOR', label: 'Delete']
    ]
    static constraints = {
        alias(blank: false, unique: true, summary: true, group: 'Properties')
        name(blank: false, summary: true, group: 'Properties')
        email(blank: false, unique: true, group: 'Properties')
        campus(nullable: true, group:'Properties')
        livingUnit(nullable: true, group:'Properties')
        theme(inList: [
            'black',
            'green',
            'blue',
            'brown',
            'purple',
            'red'
        ], group: 'Properties', editable: false)
        emailShow(editable: false)
        passwd(blank: false, password: true, group: 'Password')
        passwdResetKey(nullable: true, editable: false)
        passwdResetKeyExpiration(nullable: true, editable: false)
        enabled(summary: true,group: 'Security')
        roles(group: 'Security')
    }

    public void addRole(String roleName) {
        Role role = Role.findByName(roleName)
        if (role) {
            addToRoles(role)
        }
    }

    private boolean validatePassword(def mason,String password) {
        /*
         * Password
         * Not contain parts of the user's full name that exceed two consecutive characters
         * Contains:
         * minimum 8 characters
         * Contain characters from three of the following four categories:
         * at least 1 upper letter
         * at least 1 lower letter
         * at least 1 number
         * at least 1 special character
         */
        boolean valid = true
        def pattern = /^.*(?=.{8,})((?=.*[a-z])(?=.*[A-Z])(?=.*\d)|(?=.*[a-z])(?=.*[A-Z])(?=.*\W)|(?=.*[a-z])(?=.*\d)(?=.*\W)|(?=.*[A-Z])(?=.*\d)(?=.*\W)).*$/

        def matcher = (password =~ pattern)

        if (matcher?.getCount()) {
            int length = mason.name?.size() ?: 0
            for(int i=0; i < length-1; i++) {
                pattern = Pattern.compile("(${mason.name.substring(i,i+2)})+")
                matcher = (password =~ pattern)
                if (matcher?.getCount()) {
                    valid = false
                    return false
                }
            }
            return valid
        }
        else {
            valid = false
        }
        return valid
    }
}
