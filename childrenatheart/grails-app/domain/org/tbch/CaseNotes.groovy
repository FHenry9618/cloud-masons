package org.tbch

class CaseNotes implements Serializable {

    String name
    String narrative
    boolean attemptedContact
    String typeOfContact

    boolean editable=true

    String author

    Date date
    String personContacted
    
    static transients = ['description']
    static belongsTo = ClientRecord
    ClientRecord clientRecord
    
    Date dateCreated
    Date lastUpdated
    
    String getDescription() {
        return "${name}'s Case Note"
    }
    
    void setName(String name) {
        this.name = clientRecord ? clientRecord.name : name
    }
    
    String getName() {
        clientRecord?.name ?: this.name
    }

    static TITLE = 'Notes Details'
 
    static constraints = {
        date(summary: true, group: TITLE,renderOptions:[defaultValue: { new Date() }])
        author(nullable: true, summary: true, group:TITLE, blank: false, renderOptions:[defaultValue: User.fullname, validate: true])
        name(blank:false,group: TITLE,renderOptions:[readOnly: true])
        personContacted(summary:true, group:TITLE)
        attemptedContact(group:TITLE)
        typeOfContact(inList:['N/A', 'Phone To', 'Phone From', 'Email', 'Face to Face', 'Other'],group:TITLE)
        narrative(nullable:true,maxSize:2000,group:TITLE)
 
        clientRecord(group:'Client Record',editable:false)

        editable(editable:false)

        dateCreated(editable:false)
        lastUpdated(editable:false)
    }
    
    static actions =
    [
        [action: 'list', type:'static', label: 'List', role: 'ROLE_SYSTEM_ADMINISTRATOR'],
        [action: 'create', type:'static', label: 'Create', role: 'ROLE_SYSTEM_ADMINISTRATOR,ROLE_CASE_MANAGER,HOUSE_PARENT'],
        [action: 'show', label: 'Show',  role: [ 'ROLE_SYSTEM_ADMINISTRATOR': [:],'ROLE_REPORTING':[:],
                                                 'HOUSE_PARENT': [filter: User.houseParentShowFilter],
                                                 'ROLE_CASE_MANAGER': [filter: User.caseManagerShowFilter]] ],
        [action: 'print', label: 'Print', type:'hidden', role: [ 'ROLE_SYSTEM_ADMINISTRATOR': [:],'ROLE_REPORTING':[:],
                                                 'HOUSE_PARENT': [filter: User.caseManagerShowFilter],
                                             'ROLE_CASE_MANAGER': [filter: User.caseManagerShowFilter]] ],
        [action: 'edit', type:'hidden', label: 'Edit', role: [ 'ROLE_SYSTEM_ADMINISTRATOR': [:],
                                                            'HOUSE_PARENT': [filter: User.houseParentEditFilter],
                                                            'ROLE_CASE_MANAGER': [filter: User.caseManagerEditFilter]] ],
        [action: 'delete', role: 'ROLE_SYSTEM_ADMINISTRATOR']
    ]
}
