package org.tbch

class DevelopmentalEntry extends PlanEntry {

    static belongsTo = PlanOfService
    PlanOfService parentObject
    
    static constraints = {
        parentObject(editable:false)
    }
}
