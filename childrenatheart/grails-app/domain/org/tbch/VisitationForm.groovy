package org.tbch

class VisitationForm implements Cloneable, Serializable {

    String name
    String description
    Date date
    Date departureDate
    Date returnDate
    String departureTime
    String returnTime
    String type
    String cottage
    String toVisitWith
    String address1
    String address2
    String city
    String state
    String zip
    String homePhone
    String workPhone
    String departureTransportationType
    String departureTransportation
    String returnTransportationType
    String returnTransportation
    String specialInstructions

    Date dateCreated
    Date lastUpdated

    boolean editable=true

    static transients = ['description']
    static belongsTo = ClientRecord
    ClientRecord clientRecord

    static mapping = {
        sort lastUpdated:'desc'
    }

    String getDescription() {
        return "${name}'s visitation"
    }

    void setName(String name) {
        if (clientRecord) {
            this.name = clientRecord.name
        }
        else {
            this.name = name
        }
    }

    String getName() {
        if (clientRecord) {
            return clientRecord.name
        }
        else {
            return this.name
        }
    }

    static constraints = {

        type(summary: true, inList:['Sponsor', 'Family', 'Other'],group: 'Visitation Details')
        name(blank:false,group: 'Visitation Details')
        cottage(blank:false,group: 'Visitation Details')
        toVisitWith(blank:false,group: 'Visitation Details')
        address1(nullable:true,group: 'Visitation Details')
        address2(nullable:true,group: 'Visitation Details')
        city(nullable:true,group: 'Visitation Details')
        state(nullable:true,group: 'Visitation Details')
        zip(nullable:true,group: 'Visitation Details')
        homePhone(nullable:true,group: 'Visitation Details')
        workPhone(nullable:true,group: 'Visitation Details')
        departureDate(summary: true, group: 'Visitation Details')
        departureTime(group: 'Visitation Details', renderOptions: ['timepicker' : true])
        departureTransportationType(inList:['Sponsor', 'Family', 'Other'],group: 'Visitation Details')
        departureTransportation(nullable:true,group: 'Visitation Details')
        returnDate(summary: true, group: 'Visitation Details')
        returnTime(group: 'Visitation Details', renderOptions: ['timepicker' : true])
        returnTransportationType(inList:['Sponsor', 'Family', 'Other'],group: 'Visitation Details')
        returnTransportation(nullable:true,group: 'Visitation Details')
        specialInstructions(nullable:true,maxSize:2000,group: 'Visitation Details')
        date(group: 'Visitation Details')
        clientRecord(group:'Client Record',editable:false)

        editable(editable:false)
        dateCreated(editable:false)
        lastUpdated(editable:false)
    }

    static actions =
    [
        [action: 'list', type:'static', label: 'List', role: 'ROLE_SYSTEM_ADMINISTRATOR'],
        [action: 'create', type:'static', label: 'Create', role: 'ROLE_SYSTEM_ADMINISTRATOR,ROLE_CASE_MANAGER,HOUSE_PARENT'],
        [action: 'show', label: 'Show',  role: [ 'ROLE_SYSTEM_ADMINISTRATOR': [:],'ROLE_REPORTING':[:],
                'HOUSE_PARENT': [filter: User.houseParentShowFilter],
                'ROLE_CASE_MANAGER': [filter: User.caseManagerShowFilter]] ],
        [action: 'edit', type:'hidden', label: 'Edit', role: [ 'ROLE_SYSTEM_ADMINISTRATOR': [:],
                'HOUSE_PARENT': [filter: User.houseParentEditFilter],
                'ROLE_CASE_MANAGER': [filter: User.caseManagerEditFilter]] ],
        [action: 'delete', label: 'Delete', role: 'ROLE_SYSTEM_ADMINISTRATOR'],
        [action: 'clone', label: 'Clone', role: 'ROLE_SYSTEM_ADMINISTRATOR,ROLE_CASE_MANAGER,HOUSE_PARENT']
    ]

    def clone() throws CloneNotSupportedException {
        def clone = new VisitationForm()
        clone.properties = this.properties
        clone.dateCreated = null
        clone.lastUpdated = null
        clone.returnDate = null
        clone.departureDate = null
        return clone
    }
}
