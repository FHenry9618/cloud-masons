package org.tbch

class LivingUnit implements Serializable {
    static belongsTo = Program
    String unitName
    Program program

    int capacity = 1
    public String getDescription() {
        return getName()
    }
    
    public String getName() {
        return campusAndProgramName.size() > 0 ? campusAndProgramName+':'+unitName : unitName
    }
    
    public String getCampusAndProgramName() {
        String campusName = program?.campus?.name ? "${program?.campus?.name}:" : ''
        String programName = program?.name ? "${program?.name}" : ''
        return campusName+programName
    }
    
    static transients = ['name','description','campusAndProgramName',]
    static constraints = {
        unitName(blank:false, group:'Living Unit')
        capacity(range:1..1000, group:'Living Unit')
        program(group:'Living Unit',editable:false)
        campusAndProgramName(nullable:true,group:'Living Unit')
    }

    static actions =
    [
        [action: 'list', type:'static', label: 'List', role: 'ROLE_SYSTEM_ADMINISTRATOR'],
        [action: 'create', type:'static', label: 'Create', role: 'ROLE_SYSTEM_ADMINISTRATOR'],
        [action: 'show', label: 'Show',  role: 'ROLE_SYSTEM_ADMINISTRATOR'],
        [action: 'edit', type:'hidden', label: 'Edit', role: 'ROLE_SYSTEM_ADMINISTRATOR'],
        [action: 'delete', role: 'ROLE_SYSTEM_ADMINISTRATOR']
    ]
    
    String toString() {
        return this.name
    }
}
