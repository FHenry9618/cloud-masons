package org.tbch

import java.util.Date;
import org.codehaus.groovy.grails.commons.ConfigurationHolder

class ParentSurvey implements Serializable {

    static final int MAX_SCORE = ConfigurationHolder.config.parentSurvey.max.score instanceof Integer ? ConfigurationHolder.config.parentSurvey.max.score : 5
    String name

    String description
    
    int safePlaceRating
    int socialSkillRating
    int admissionProcessRating
    int placementTimeRating
    int communicationRating
    int familyWeekendRating
    int planOfServiceRating
    int spirtualNeedRating
    int placementCareRating
    int visitRating
    String comment
    
    Date dateCreated
    Date lastUpdated
    
    static transients = ['name','description']
    //static belongsTo = ClientRecord
    
    public String getName() {
        return "Parent survey"
    }
    
    public String getDescription() {
        return "TBCH was a safe place?  Score:${safePlaceRating}/${MAX_SCORE}"
    }
    

    
    static constraints = {
        safePlaceRating(range:1..MAX_SCORE, group:'Ratings (Ex. 1 = strongly disagree, '+MAX_SCORE+' = strongly agree)')
        socialSkillRating(range:1..MAX_SCORE, group:'Ratings (Ex. 1 = strongly disagree, '+MAX_SCORE+' = strongly agree)')
        admissionProcessRating(range:1..MAX_SCORE, group:'Ratings (Ex. 1 = strongly disagree, '+MAX_SCORE+' = strongly agree)')
        placementTimeRating(range:1..MAX_SCORE, group:'Ratings (Ex. 1 = strongly disagree, '+MAX_SCORE+' = strongly agree)')
        communicationRating(range:1..MAX_SCORE, group:'Ratings (Ex. 1 = strongly disagree, '+MAX_SCORE+' = strongly agree)')
        familyWeekendRating(range:1..MAX_SCORE, group:'Ratings (Ex. 1 = strongly disagree, '+MAX_SCORE+' = strongly agree)')
        planOfServiceRating(range:1..MAX_SCORE, group:'Ratings (Ex. 1 = strongly disagree, '+MAX_SCORE+' = strongly agree)')
        spirtualNeedRating(range:1..MAX_SCORE, group:'Ratings (Ex. 1 = strongly disagree, '+MAX_SCORE+' = strongly agree)')
        placementCareRating(range:1..MAX_SCORE, group:'Ratings (Ex. 1 = strongly disagree, '+MAX_SCORE+' = strongly agree)')
        visitRating(range:1..MAX_SCORE, group:'Ratings (Ex. 1 = strongly disagree, '+MAX_SCORE+' = strongly agree)')
        comment(nullable: true,maxSize:5000,group:'Additional Information',renderOptions: [longLabel:true])
        dateCreated(editable:false)
        lastUpdated(editable:false)
    }
        
    static actions = [
        [action: 'list', type:'static', label: 'List', role: 'ROLE_SYSTEM_ADMINISTRATOR'],
        [action: 'create', type:'static', label: 'Create', role: 'ROLE_SYSTEM_ADMINISTRATOR,ROLE_CASE_MANAGER'],
        [action: 'show', label: 'Show',  role: 'ROLE_SYSTEM_ADMINISTRATOR'],
        [action: 'edit', type:'hidden', label: 'Edit',  role: 'ROLE_SYSTEM_ADMINISTRATOR'],
        [action: 'delete', role: 'ROLE_SYSTEM_ADMINISTRATOR']
    ]
}
