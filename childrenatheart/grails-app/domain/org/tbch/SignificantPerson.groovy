package org.tbch

class SignificantPerson implements Serializable {

    String name //transcient
    String lastName
    String firstName
    String middleInitial
    String description //transcient
    String relationship

    String email
    String address1
    String address2
    String city
    String state
    String zip
    String phoneNumber

    boolean managingConservator

    Date dateCreated
    Date lastUpdated


    static transients = ['description','name']
    static mapping = {
       cache true
        sort lastUpdated:'desc'
    }

    static final List RELATIONSHIP = [
        'DFPS',
        'Adoptive Parent',
        'Other Relative',
        'Natural Parent',
        'Grandparent',
        'Court',
        'Sponsor',
        'Other',
        'Self'
    ]

    static belongsTo = ClientRecord
    ClientRecord clientRecord

    static actions =
    [
        [action: 'list', type:'static', label: 'List', role: 'ROLE_SYSTEM_ADMINISTRATOR'],
        [action: 'create', type:'static', label: 'Create', role: 'ROLE_SYSTEM_ADMINISTRATOR,ROLE_CASE_MANAGER'],
        [action: 'show', label: 'Show',  role: [ 'ROLE_SYSTEM_ADMINISTRATOR': [:],'ROLE_REPORTING':[:],
                                                 'HOUSE_PARENT': [filter: User.houseParentShowFilter],
                                                 'ROLE_CASE_MANAGER': [filter: User.caseManagerShowFilter]] ],
        [action: 'edit', type:'hidden', label: 'Edit', role: [ 'ROLE_SYSTEM_ADMINISTRATOR': [:],
                                                            'ROLE_CASE_MANAGER': [filter: User.caseManagerOpenEditFilter]] ],
        [action: 'delete', role: 'ROLE_SYSTEM_ADMINISTRATOR']
    ]

    static constraints = {
        firstName(summary: true, blank:false,group:'Person Info')
        middleInitial(nullable: true, group:'Person Info')
        lastName(summary: true, blank:false,group:'Person Info')

        managingConservator(group:'Person Info')
        relationship(summary: true, inList: RELATIONSHIP, nullable:true,group:'Person Info')
        email(nullable:true, group:'Person Info')
        address1(nullable:true, group:'Person Info')
        address2(nullable:true, group:'Person Info')
        city(nullable:true, group:'Person Info')
        state(nullable:true, group:'Person Info')
        zip(nullable:true, group:'Person Info')
        phoneNumber(nullable:true, group:'Person Info')
        clientRecord(group:'Client Record',editable:false)

        dateCreated(editable:false)
        lastUpdated(editable:false)
    }

    public String getDescription() {
        return "${clientRecord.name}'s {relationship}"
    }

    public String getName() {
        return "${firstName ?: ''} ${middleInitial ?: ''} ${lastName ?: ''}"
    }


}
