package org.tbch

import org.springframework.security.context.SecurityContextHolder;

class AddendumRestraintReport implements Serializable {
    
    static Closure houseParentShowFilter = {
        def pricipalInfo = SecurityContextHolder.context?.authentication?.principal
        def user  = pricipalInfo?.domainClass
        user?.refresh()
        return incidentReport?.clientRecord?.livingUnit?.id == user?.livingUnit?.id
    }
    
    static Closure caseManagerEditFilter = {
        def pricipalInfo = SecurityContextHolder.context?.authentication?.principal
        def user  = pricipalInfo?.domainClass
        user?.refresh()
        return (incidentReport?.editable && (incidentReport?.clientRecord?.campus == null || incidentReport?.clientRecord?.campus?.id == user?.campus?.id))
    }
    
    static Closure caseManagerShowFilter = {
        def pricipalInfo = SecurityContextHolder.context?.authentication?.principal
        def user  = pricipalInfo?.domainClass
        user?.refresh()
        return (incidentReport?.clientRecord?.campus == null || incidentReport?.clientRecord?.campus?.id == user?.campus?.id)
    }

    Date date
    String time
    
    String responsibleStaff
    String involvedStaff
    String monitoringStaff
    String length
    boolean selfDanger
    boolean otherDanger
    String precipatatingDescription

    boolean verbalRedirection
    boolean clearing
    boolean noResistance
    boolean distracted
    boolean privateConversation
    boolean quiet
    boolean suggestion
    boolean other
    String otherStrategy
    String alternativeStrategy
    
    boolean standingHold
    boolean standingHoldMultiple
    boolean handGrab
    boolean fingernailGrab
    boolean clothingGrab
    boolean choking
    boolean bite
    boolean hairPull

    String allAttempt
    String sustainedInjury
    String caregiverAction
    String childReaction
     
    boolean offerDiscussion
    Date offerDiscussionDate
    String offerDiscussionTime
    String offerDiscussionStaff
    boolean discussion
    Date discussionDate
    String discussionTime
    String discussionStaff

    Date dateCreated
    Date lastUpdated

    static mapping = {
        sort lastUpdated:'desc'
    }

    static transients = ['description','name', 'editable', 'clientRecord']
    static belongsTo = SeriousIncidentReport
    SeriousIncidentReport incidentReport
    
    String getDescription() {
        return getName()
    }
    
    String getName() {
        return "${incidentReport?.name} restraint report"
    }
    
    boolean isEditable() {
        incidentReport.editable
    }
    
    ClientRecord getClientRecord() {
        incidentReport.clientRecord
    }

    static constraints = {
        
        responsibleStaff(nullable: true, group: 'Personal Restraint')
        involvedStaff(nullable: true, group: 'Personal Restraint')
        monitoringStaff(nullable: true, group: 'Personal Restraint')
        date(group: 'Personal Restraint')
        time(group: 'Personal Restraint',renderOptions:[timepicker:true])
        length(nullable: true, group: 'Personal Restraint')
        
        
        selfDanger(group: 'Precipatating Behavior')
        otherDanger(group: 'Precipatating Behavior')
        precipatatingDescription(nullable: true, maxSize:5000, group: 'Precipatating Behavior',renderOptions:[longLabel:true])
        
        
        verbalRedirection(group: 'Alternative strategies attempted before personal restraint')
        clearing(group: 'Alternative strategies attempted before personal restraint')
        noResistance(group: 'Alternative strategies attempted before personal restraint')
        distracted(group: 'Alternative strategies attempted before personal restraint')
        privateConversation(group: 'Alternative strategies attempted before personal restraint')
        quiet(group: 'Alternative strategies attempted before personal restraint')
        suggestion(group: 'Alternative strategies attempted before personal restraint')
        other(group: 'Alternative strategies attempted before personal restraint')
        otherStrategy(nullable: true, maxSize:5000, group: 'Alternative strategies attempted before personal restraint',renderOptions:[longLabel:true])
        alternativeStrategy(nullable: true, maxSize:5000, group: 'Alternative strategies attempted before personal restraint',renderOptions:[longLabel:true])

        standingHold(group:'The Specific Restraint Techniques used')
        standingHoldMultiple(group:'The Specific Restraint Techniques used')
        handGrab(group:'The Specific Restraint Techniques used')
        fingernailGrab(group:'The Specific Restraint Techniques used')
        clothingGrab(group:'The Specific Restraint Techniques used')
        choking(group:'The Specific Restraint Techniques used')
        bite(group:'The Specific Restraint Techniques used')
        hairPull(group:'The Specific Restraint Techniques used')
        
        allAttempt(nullable: true, maxSize:5000, group: 'Detailed Descriptions',renderOptions:[longLabel:true])
        sustainedInjury(nullable: true, maxSize:5000, group: 'Detailed Descriptions',renderOptions:[longLabel:true])
        caregiverAction(nullable: true, maxSize:5000, group: 'Detailed Descriptions',renderOptions:[longLabel:true])
        childReaction(nullable: true, maxSize:5000, group: 'Detailed Descriptions',renderOptions:[longLabel:true])
        
        offerDiscussion(group:'Discussion Details')
        offerDiscussionDate(nullable: true, group:'Discussion Details')
        offerDiscussionTime(nullable: true, group:'Discussion Details',renderOptions:[timepicker:true])
        offerDiscussionStaff(nullable: true, group:'Discussion Details')
        discussion(group:'Discussion Details')
        discussionDate(nullable: true, group:'Discussion Details')
        discussionTime(nullable: true, group:'Discussion Details',renderOptions:[timepicker:true])
        discussionStaff(nullable: true, group:'Discussion Details')
        
        incidentReport(group:'Serious Incident Report',editable:false)
        
        dateCreated(editable:false)
        lastUpdated(editable:false)
    }
    static actions =
    [
        [action: 'list', type:'static', label: 'List', role: 'ROLE_SYSTEM_ADMINISTRATOR'],
        [action: 'create', type:'static', label: 'Create', role: 'ROLE_SYSTEM_ADMINISTRATOR,ROLE_CASE_MANAGER,HOUSE_PARENT'],
        [action: 'show', label: 'Show',  role: [ 'ROLE_SYSTEM_ADMINISTRATOR': [:],'ROLE_REPORTING':[:],
                                                 'HOUSE_PARENT': [filter: User.houseParentShowFilter],
                                                 'ROLE_CASE_MANAGER': [filter: User.caseManagerShowFilter]] ],
        [action: 'edit', type:'hidden', label: 'Edit', role: [ 'ROLE_SYSTEM_ADMINISTRATOR': [:],
                                                            'HOUSE_PARENT': [filter: User.houseParentEditFilter],
                                                            'ROLE_CASE_MANAGER': [filter: User.caseManagerEditFilter]] ],
                                                        
        [action: 'delete', label:'Delete', role: 'ROLE_SYSTEM_ADMINISTRATOR']
    ]
}
