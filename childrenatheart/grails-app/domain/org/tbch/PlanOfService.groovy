package org.tbch

class PlanOfService implements Cloneable, Serializable {

    static final List PLANS = [
        'medical',
        'dental',
        'therapeutic',
        'intellectual',
        'developmental',
        'educational',
        'behavioral',
        'social',
        'community',
        'cultural',
        'familial'
    ]

    String name
    Date date //Effective Date
    Date dateOfBirth
    Date dateOfPlacement
    Date reportingPeriodStart
    Date reportingPeriodEnd
    String managingConservator
    String caseManager
    String description
    String type = 'Initial Report'
    String circumstances

    String medicalNeeds
    String dentalNeeds
    String therapeuticNeeds
    String intellectualNeeds
    String developmentalNeeds
    String educationalNeeds
    String behavioralNeeds
    String socialNeeds
    String communityNeeds
    String culturalNeeds
    String familialNeeds

    String supervisionLevel

    String tripPlans

    String healthEducation

    String independentLiving

    String safetyPlan

    String continuedPlacementReason

    String medicationEvaluation

    String interventionEvaluation

    String expectedPlacementOutcome

    String dischargePlan

    SignatureDoc signaturePage
    Date dateCreated
    Date lastUpdated

    boolean editable = true

    static mapping = {
        sort date:'desc'
    }

    static transients = [
        'description',
        'dateOfBirth',
        'dateOfPlacement'
    ]
    static hasMany = [medicalPlans      : MedicalEntry,
                      dentalPlans       : DentalEntry,
                      therapeuticPlans  : TherapeuticEntry,
                      intellectualPlans : IntellectualEntry,
                      developmentalPlans: DevelopmentalEntry,
                      educationalPlans  : EducationalEntry,
                      behavioralPlans   : BehavioralEntry,
                      socialPlans       : SocialEntry,
                      communityPlans    : CommunityEntry,
                      culturalPlans     : CulturalEntry,
                      familialPlans     : FamilialEntry
    ]
    static belongsTo = ClientRecord
    ClientRecord clientRecord

    String getDescription() {
        return "${name}'s plan of service ${type}"
    }

    Date getDateOfBirth() {
        clientRecord?.dateOfBirth
    }

    Date getDateOfPlacement() {
        clientRecord?.datePlacement
    }

    String getCircumstances() {
        //println "getCircumstances"+circumstances
        if ((!circumstances || circumstances == '<<Input Here>>') && clientRecord && clientRecord.admissionAssessment && clientRecord.admissionAssessment.referralReason) {
            return clientRecord.admissionAssessment.referralReason
        }
        return circumstances
    }

    void setName(String name) {
        if (clientRecord) {
            this.name = clientRecord.name
        }
        else {
            this.name = name
        }
    }

    String getName() {
        clientRecord ? clientRecord.name : this.name
    }

    static TITLE_PI = 'Plan Information'

    static constraints = {
        name(summary: true, blank:false,group: 'Client Information',renderOptions:[readOnly: true, unprintable: true])
        dateOfBirth(nullable:true,group: 'Client Information',renderOptions:[readOnly: true])
        dateOfPlacement(nullable:true,group: 'Client Information',renderOptions:[readOnly: true])
        managingConservator(nullable:true,group: 'Client Information',renderOptions:[defaultValue: { '<<Input Here>>' }, validate: true])
        caseManager(nullable:true,group: 'Client Information',renderOptions:[defaultValue:org.tbch.SecurityUtil.currentUsername])

        date(summary: true,group: TITLE_PI,renderOptions:[defaultValue: { new Date() }])
        type(summary: true,inList:['Initial Report', 'Review'],blank:false,group: TITLE_PI,renderOptions:[defaultValue: { 'Review' }])
        reportingPeriodStart(summary: true,group: TITLE_PI,renderOptions:[defaultValue: { new Date() }])
        reportingPeriodEnd(summary: true,group: TITLE_PI,renderOptions:[defaultValue: { (new Date())+180 }])
        circumstances(nullable:true,maxSize:2000,group: TITLE_PI,renderOptions:[longLabel:true,  defaultValue: {'<<Input Here>>'}, validate:true])

        supervisionLevel(nullable:true,maxSize:2000,group: TITLE_PI,renderOptions:[longLabel:true, defaultValue: {'<<Input Here>>'}, validate:true])
        tripPlans(nullable:true,maxSize:2000,group: TITLE_PI,renderOptions:[longLabel:true, defaultValue: {'<<Input Here>>'}, validate:true])
        healthEducation(nullable:true,maxSize:2000,group: TITLE_PI,renderOptions:[longLabel:true, defaultValue: {'<<Input Here>>'}, validate:true])
        independentLiving(nullable:true,maxSize:2000,group: TITLE_PI,renderOptions:[longLabel:true, defaultValue: {'<<Input Here>>'}, validate:true])
        safetyPlan(nullable:true,maxSize:2000,group: TITLE_PI,renderOptions:[longLabel:true, defaultValue: {'<<Input Here>>'}, validate:true])
        continuedPlacementReason(nullable:true,maxSize:2000,group: TITLE_PI,renderOptions:[longLabel:true, defaultValue: {'<<Input Here>>'}, validate:true])
        medicationEvaluation(nullable:true,maxSize:2000,group: TITLE_PI,renderOptions:[longLabel:true, defaultValue: {'<<Input Here>>'}, validate:true])
        interventionEvaluation(nullable:true,maxSize:2000,group: TITLE_PI,renderOptions:[longLabel:true, defaultValue: {'<<Input Here>>'}, validate:true])
        expectedPlacementOutcome(nullable:true,maxSize:2000,group: TITLE_PI,renderOptions:[longLabel:true, defaultValue: {'<<Input Here>>'}, validate:true])
        dischargePlan(nullable:true,maxSize:2000,group: TITLE_PI,renderOptions:[longLabel:true, defaultValue: {'<<Input Here>>'}, validate:true])

        medicalNeeds(nullable:true,maxSize:2000,group:'Medical',renderOptions:[longLabel:true, defaultValue: {'<<Input Here>>'}, validate:true])
        medicalPlans(nullable:true,editable: true, group:'Medical',renderOptions:[longLabel:true])

        dentalNeeds(nullable:true,maxSize:2000,group:'Dental',renderOptions:[longLabel:true, defaultValue: {'<<Input Here>>'}, validate:true])
        dentalPlans(nullable:true,editable: true, group:'Dental',renderOptions:[longLabel:true])

        therapeuticNeeds(nullable:true,maxSize:2000,group:'Therapeutic',renderOptions:[longLabel:true, defaultValue: {'<<Input Here>>'}, validate:true])
        therapeuticPlans(nullable:true,editable: true, group:'Therapeutic',renderOptions:[longLabel:true])

        intellectualNeeds(nullable:true,maxSize:2000,group:'Intellectual',renderOptions:[longLabel:true, defaultValue: {'<<Input Here>>'}, validate:true])
        intellectualPlans(nullable:true,editable: true, group:'Intellectual',renderOptions:[longLabel:true])

        developmentalNeeds(nullable:true,maxSize:2000,group:'Developmental',renderOptions:[longLabel:true, defaultValue: {'<<Input Here>>'}, validate:true])
        developmentalPlans(nullable:true,editable: true, group:'Developmental',renderOptions:[longLabel:true])

        educationalNeeds(nullable:true,maxSize:2000,group:'Educational',renderOptions:[longLabel:true, defaultValue: {'<<Input Here>>'}, validate:true])
        educationalPlans(nullable:true,editable: true, group:'Educational',renderOptions:[longLabel:true])

        behavioralNeeds(nullable:true,maxSize:2000,group:'Behavioral and Discipline',renderOptions:[longLabel:true, defaultValue: {'<<Input Here>>'}, validate:true])
        behavioralPlans(nullable:true,editable: true, group:'Behavioral and Discipline',renderOptions:[longLabel:true])

        socialNeeds(nullable:true,maxSize:2000,group:'Social, Recreations, and Leisure Activities',renderOptions:[longLabel:true, defaultValue: {'<<Input Here>>'}, validate:true])
        socialPlans(nullable:true,editable: true, group:'Social, Recreations, and Leisure Activities',renderOptions:[longLabel:true])

        communityNeeds(nullable:true,maxSize:2000,group:'Community Integration',renderOptions:[longLabel:true, defaultValue: {'<<Input Here>>'}, validate:true])
        communityPlans(nullable:true,editable: true, group:'Community Integration',renderOptions:[longLabel:true])

        culturalNeeds(nullable:true,maxSize:2000,group:'Cultural Identity',renderOptions:[longLabel:true, defaultValue: {'<<Input Here>>'}, validate:true])
        culturalPlans(nullable:true,editable: true, group:'Cultural Identity',renderOptions:[longLabel:true])

        familialNeeds(nullable:true,maxSize:2000,group:'Family Relationships',renderOptions:[longLabel:true, defaultValue: {'<<Input Here>>'}, validate:true])
        familialPlans(nullable:true,editable: true, group:'Family Relationships',renderOptions:[longLabel:true])
        signaturePage(nullable:true,group:'Signature Page',renderOptions:[unprintable:true])
        clientRecord(group:'Client Record',editable:false)

        editable(editable:false)
        dateCreated(editable:false)
        lastUpdated(editable:false)
    }

    static actions =
    [
        [action: 'list', type:'static', label: 'List', role: 'ROLE_SYSTEM_ADMINISTRATOR'],
        [action: 'create', type:'static', label: 'Create', role: 'ROLE_SYSTEM_ADMINISTRATOR,ROLE_CASE_MANAGER'],
        [action: 'show', label: 'Show',  role: [ 'ROLE_SYSTEM_ADMINISTRATOR': [:],'ROLE_REPORTING':[:],
                'HOUSE_PARENT': [filter: User.houseParentShowFilter],
                'ROLE_CASE_MANAGER': [filter: User.caseManagerShowFilter]] ],
        [action: 'print', type:'hidden', role: [ 'ROLE_SYSTEM_ADMINISTRATOR': [:],'ROLE_REPORTING':[:],
                'HOUSE_PARENT': [filter: User.houseParentShowFilter],
                'ROLE_CASE_MANAGER': [filter: User.caseManagerShowFilter]] ],
        [action: 'edit', type:'hidden', label: 'Edit', role: [ 'ROLE_SYSTEM_ADMINISTRATOR': [:],
                'ROLE_CASE_MANAGER': [filter: User.caseManagerEditFilter]] ],
        [action: 'clone', label: 'Clone', role: [ 'ROLE_SYSTEM_ADMINISTRATOR': [:],
                'ROLE_CASE_MANAGER': [filter: User.caseManagerShowFilter]]],
        [action: 'delete', label:'Delete', role: 'ROLE_SYSTEM_ADMINISTRATOR']
    ]
}
