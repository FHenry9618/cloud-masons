package org.tbch

class BehavioralEntry extends PlanEntry {

    static belongsTo = PlanOfService
    PlanOfService parentObject
    
    static constraints = {
        parentObject(editable:false)
    }
}
