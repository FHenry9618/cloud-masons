package org.tbch

class ServicePlanEntry implements Cloneable, Serializable {

    static houseParentShowFilter = {
        def pricipalInfo = org.springframework.security.context.SecurityContextHolder.context?.authentication?.principal
        def user  = pricipalInfo?.domainClass
        user?.refresh()
        return  planEntry?.parentObject?.clientRecord?.livingUnit?.id == user?.livingUnit?.id
    }
    
    static caseManagerEditFilter = {
        def pricipalInfo = org.springframework.security.context.SecurityContextHolder.context?.authentication?.principal
        def user  = pricipalInfo?.domainClass
        user?.refresh()
        if (planEntry?.parentObject instanceof DischargeForm) {
            return false
        }
        return (planEntry?.parentObject?.editable && (planEntry?.parentObject?.clientRecord?.campus == null || planEntry?.parentObject?.clientRecord?.campus?.id == user?.campus?.id))
    }
    
    static caseManagerShowFilter = {
        def pricipalInfo = org.springframework.security.context.SecurityContextHolder.context?.authentication?.principal
        def user  = pricipalInfo?.domainClass
        user?.refresh()
        return (planEntry?.parentObject?.clientRecord?.campus == null || planEntry?.parentObject?.clientRecord?.campus?.id == user?.campus?.id)
    }
    
    Date date
    String responsibleParty
    Date targetDate
    int scale
    Date dateResolved
    String goals
    String strategies
    
    Date dateCreated
    Date lastUpdated

    static mapping = {
        sort lastUpdated:'desc'
    }

    static transients = ['name','description']

    String getDescription() {
        return "${date?.getDateString()}"
    }
    
    String getName() {
        return "${date?.getDateString()}"
    }
    static constraints = {
        date(summary: true,group: 'Entry')
        goals(nullable:true,maxSize:2000,group:'Entry')
        strategies(nullable:true,maxSize:2000,group:'Entry')
        responsibleParty(nullable:true,group: 'Entry')
        targetDate(nullable:true,group: 'Entry')
        scale(inList:[-2,-1,0,1,2,3],group: 'Entry')
        dateResolved(nullable:true,group: 'Entry')
        
        dateCreated(editable:false)
        lastUpdated(editable:false)
    }
    
    static actions =
    [
        [action: 'list', type:'static', label: 'List', role: 'ROLE_SYSTEM_ADMINISTRATOR'],
        [action: 'create', type:'static', label: 'Create', role: 'ROLE_SYSTEM_ADMINISTRATOR,ROLE_CASE_MANAGER'],
        [action: 'show', label: 'Show',  role: [ 'ROLE_SYSTEM_ADMINISTRATOR': [:],'ROLE_REPORTING':[:],
                                                 'HOUSE_PARENT': [filter: houseParentShowFilter],
                                                 'ROLE_CASE_MANAGER': [filter: caseManagerShowFilter]] ],
        [action: 'print', type:'hidden', role: ''],
        [action: 'edit', type:'hidden', label: 'Edit', role: [ 'ROLE_SYSTEM_ADMINISTRATOR': [:],
                                                            'ROLE_CASE_MANAGER': [filter: caseManagerEditFilter]] ],
        [action: 'delete', label:'Delete', role: [ 'ROLE_SYSTEM_ADMINISTRATOR': [:],
                                                            'ROLE_CASE_MANAGER': [filter: caseManagerEditFilter]] ]
    ]
}
