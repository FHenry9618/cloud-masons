package org.tbch

class PlanEntry implements Cloneable, Serializable {
    static boolean skipPrint = true

    static houseParentShowFilter = {
        def pricipalInfo = org.springframework.security.context.SecurityContextHolder.context?.authentication?.principal
        def user  = pricipalInfo?.domainClass
        user?.refresh()
        return  parentObject?.clientRecord?.livingUnit?.id == user?.livingUnit?.id
    }
    
    static caseManagerEditFilter = {
        def pricipalInfo = org.springframework.security.context.SecurityContextHolder.context?.authentication?.principal
        def user  = pricipalInfo?.domainClass
        user?.refresh()
        return (parentObject?.editable && (parentObject?.clientRecord?.campus == null || parentObject?.clientRecord?.campus?.id == user?.campus?.id))
    }
    
    static caseManagerShowFilter = {
        def pricipalInfo = org.springframework.security.context.SecurityContextHolder.context?.authentication?.principal
        def user  = pricipalInfo?.domainClass
        user?.refresh()
        return (parentObject?.clientRecord?.campus == null || parentObject?.clientRecord?.campus?.id == user?.campus?.id)
    }

    String getDescription() {
        servicePlanEntry.description
    }

    String getName() {
        servicePlanEntry.name
    }

    Date getDate() {
        servicePlanEntry.date
    }

    String getResponsibleParty() {
        servicePlanEntry.responsibleParty
    }

    Date getTargetDate() {
        servicePlanEntry.targetDate
    }

    int getScale() {
        servicePlanEntry.scale
    }

    Date getDateResolved() {
        servicePlanEntry.dateResolved
    }

    String getGoals() {
        servicePlanEntry.goals
    }

    String getStrategies() {
        servicePlanEntry.strategies
    }

    static transients = ['name', 'description',
                         'date',
                         'responsibleParty',
                         'targetDate',
                         'scale',
                         'dateResolved',
                         'goals',
                         'strategies'
    ]
    Date dateCreated
    Date lastUpdated

    static hasOne = [servicePlanEntry: ServicePlanEntry]
    
    static actions =
    [
        [action: 'list', type:'static', label: 'List', role: 'ROLE_SYSTEM_ADMINISTRATOR'],
        [action: 'create', type:'static', label: 'Create', role: 'ROLE_SYSTEM_ADMINISTRATOR,ROLE_CASE_MANAGER'],
        [action: 'show', label: 'Show',  role: [ 'ROLE_SYSTEM_ADMINISTRATOR': [:],'ROLE_REPORTING':[:],
                                                 'HOUSE_PARENT': [filter: houseParentShowFilter],
                                                 'ROLE_CASE_MANAGER': [filter: caseManagerShowFilter]] ],
        [action: 'print', type:'hidden', role: ''],
        [action: 'edit', type:'hidden', label: 'Edit', role: [ 'ROLE_SYSTEM_ADMINISTRATOR': [:],
                                                            'ROLE_CASE_MANAGER': [filter: caseManagerEditFilter]] ],
        [action: 'delete', label:'Delete', role: [ 'ROLE_SYSTEM_ADMINISTRATOR': [:],
                                                            'ROLE_CASE_MANAGER': [filter: caseManagerEditFilter]] ]
    ]
    
    static constraints = {
        servicePlanEntry(nullable:true,editable: true, group:'Plan Entry')
        
        dateCreated(editable:false)
        lastUpdated(editable:false)
    }
}
