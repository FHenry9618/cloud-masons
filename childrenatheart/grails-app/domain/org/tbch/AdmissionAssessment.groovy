package org.tbch

import org.codehaus.groovy.grails.commons.ConfigurationHolder

class AdmissionAssessment implements Serializable {
    
    static final List SEX = ['Male', 'Female']
    static final List RACE = [ 'Black', 'Caucasian', 'Hispanic', 'Asian', 'American Indian', 'Mixed', 'Other']
    static final List EDUCATIONAL_PLACEMENT = ['Regular', 'Special Ed.', 'Closed', 'Honors', 'Not Applicable']
    static final List MEDICATION_STATUS = ['Yes', 'No', 'Unknown']
    static final List LEGAL_STATUS = ['Natural Parent', 'Adoptive Parent','DFPS', 'Court', 'Other']
    static final List PARENTS_STATUS = ['Together', 'Divorced', 'Separated', 'Deceased Father', 'Deceased Mother', 'Deceased Both', 'PRS', 'Other', 'Self']

    private final long clientIdInitialSeed = ConfigurationHolder.config.clientId.seed instanceof Long ? ConfigurationHolder.config.clientId.seed : 1500
    private long clientIdSeed = clientIdInitialSeed
    
    String clientId
    Date date
    String name  //transient
    String lastName
    String firstName
    String middleInitial
    Date dateOfBirth
    String description //transient
    String socialSecurityNumber
    String sex
    String race
    
    String parent
    String address1
    String address2
    String city
    String state
    String zip
    String telephone
    String referredBy
    float age
    
    String parentsStatus
    String legalStatusType
    String legalStatus

    String referralReason
    String behavior
    String abuseHistory
    String medicalDentalStatus
    String allergies
    String chronicHealthConditions
    String knownContraindications
    String treatmentIdentification
    String mentalAbuseStatus
    String developmentalLevel
    String educationalLevel
    String educationalPlacement
    
    String levelDocumentation
    String immediateGoal
    String parentExpectation
    String placementUnderstanding
    String determination
    String rationale
    String admissionDocumentation
    boolean courtOrder
    boolean courtOrderCopy
    String managingConservatorInfo
    String siblingInfo
    Date admissionDate
    
    String medicationStatus
    String medication
    
    String preInitialServicePlanInfo
    String socialHistory
    String homeEnvironment
    String birthHistory
    String developmentalHistory
    String mentalAbuseHistory
    String schoolHistory
    String placementHistory
    String criminalHistory
    String skill
    String historyDocumentation
    String services
    String assessmentRecommendation
    String behaviorManagement
    String historyDetermination
    String historyRationale
    String personConductAssessment
    
    SignatureDoc signaturePage
    Date dateCreated
    Date lastUpdated
    boolean editable=true
    
    static transients = ['description','name','dateOfBirth','age']
    static belongsTo = ClientRecord
    ClientRecord clientRecord
    //static searchable = true
    static constraints = {
        
        clientId(summary: true, unique: true, blank:false, group:'Client Information')
        firstName(summary: true, blank:false,group:'Client Information')
        middleInitial(nullable: true, group:'Client Information')
        lastName(summary: true, blank:false,group:'Client Information')
        age(group: 'Client Information',renderOptions:[readOnly:true])
        dateOfBirth(nullable:true,group: 'Client Information',renderOptions:[readOnly:true])
        socialSecurityNumber(nullable:true,group: 'Client Information',renderOptions:[defaultValue: { '<<Input Here>>' }, validate: true])
        sex(inList: SEX, blank:false,group:'Client Information')
        race(nullable:true, inList: RACE, group:'Client Information')
        parent(nullable:true, group:'Client Information',renderOptions:[defaultValue: { '<<Input Here>>' }, validate: true])
        address1(nullable:true, group:'Client Information',renderOptions:[defaultValue: { '<<Input Here>>' }, validate: true])
        address2(nullable:true, group:'Client Information')
        city(nullable:true, group:'Client Information',renderOptions:[defaultValue: { '<<Input Here>>' }, validate: true])
        state(nullable:true, group:'Client Information',renderOptions:[defaultValue: { '<<Input Here>>' }, validate: true])
        zip(nullable:true, group:'Client Information',renderOptions:[defaultValue: { '<<Input Here>>' }, validate: true])
        telephone(nullable:true, group:'Client Information',renderOptions:[defaultValue: { '<<Input Here>>' }, validate: true])
        allergies(nullable:true,maxSize:2000,group:'Client Information',renderOptions:[longLabel:true, defaultValue: { '<<Input Here>>' }, validate: true])
        chronicHealthConditions(nullable:true,maxSize:2000,group:'Client Information',renderOptions:[longLabel:true, defaultValue: { '<<Input Here>>' }, validate: true])
        
        date(nullable:true,group: 'Detailed Assessment',renderOptions:[defaultValue: { new Date() }])
        referredBy(nullable:true,maxSize:500,group:'Client Information',renderOptions:[longLabel:true, readOnly:true])
        parentsStatus(nullable:true, maxSize:128, group:'Client Information', inList:PARENTS_STATUS)
        legalStatusType(nullable:true, maxSize:128, group:'Client Information', inList:LEGAL_STATUS)
        legalStatus(nullable:true,maxSize:500,group:'Client Information',renderOptions:[longLabel:true, defaultValue: { '<<Input Here>>' }, validate: true])
        referralReason(nullable:true,maxSize:2000,group:'Client Information',renderOptions:[longLabel:true, readOnly:true])
        behavior(nullable:true,maxSize:2000,group:'Detailed Assessment',renderOptions:[longLabel:true, defaultValue: { '<<Input Here>>' }, validate: true])
        abuseHistory(nullable:true,maxSize:2000,group:'Detailed Assessment',renderOptions:[longLabel:true, defaultValue: { '<<Input Here>>' }, validate: true])
        medicalDentalStatus(nullable:true,maxSize:2000,group:'Detailed Assessment',renderOptions:[longLabel:true, defaultValue: { '<<Input Here>>' }, validate: true])

        knownContraindications(nullable:true,maxSize:2000,group:'Detailed Assessment',renderOptions:[longLabel:true, defaultValue: { '<<Input Here>>' }, validate: true])
        treatmentIdentification(nullable:true,maxSize:2000,group:'Detailed Assessment',renderOptions:[longLabel:true, defaultValue: { '<<Input Here>>' }, validate: true])
        mentalAbuseStatus(nullable:true,maxSize:2000,group:'Detailed Assessment',renderOptions:[longLabel:true, defaultValue: { '<<Input Here>>' }, validate: true])
        developmentalLevel(nullable:true,maxSize:2000,group:'Detailed Assessment',renderOptions:[longLabel:true, defaultValue: { '<<Input Here>>' }, validate: true])
        educationalPlacement(nullable:true, group:'Detailed Assessment', inList:EDUCATIONAL_PLACEMENT)
        educationalLevel(nullable:true,maxSize:2000,group:'Detailed Assessment',renderOptions:[longLabel:true, defaultValue: { '<<Input Here>>' }, validate: true])
        levelDocumentation(nullable:true,maxSize:2000,group:'Detailed Assessment',renderOptions:[longLabel:true, defaultValue: { '<<Input Here>>' }, validate: true])
        immediateGoal(nullable:true,maxSize:2000,group:'Detailed Assessment',renderOptions:[longLabel:true, defaultValue: { '<<Input Here>>' }, validate: true])
        parentExpectation(nullable:true,maxSize:2000,group:'Detailed Assessment',renderOptions:[longLabel:true, defaultValue: { '<<Input Here>>' }, validate: true])
        placementUnderstanding(nullable:true,maxSize:2000,group:'Detailed Assessment',renderOptions:[longLabel:true, defaultValue: { '<<Input Here>>' }, validate: true])
        determination(nullable:true,maxSize:2000,group:'Detailed Assessment',renderOptions:[longLabel:true, defaultValue: { '<<Input Here>>' }, validate: true])
        rationale(nullable:true,maxSize:2000,group:'Detailed Assessment',renderOptions:[longLabel:true, defaultValue: { '<<Input Here>>' }, validate: true])
        admissionDocumentation(nullable:true,maxSize:2000,group:'Detailed Assessment',renderOptions:[longLabel:true, defaultValue: { '<<Input Here>>' }, validate: true])
        courtOrder(group:'Detailed Assessment')
        courtOrderCopy(group:'Detailed Assessment')
        managingConservatorInfo(nullable:true,maxSize:2000,group:'Detailed Assessment',renderOptions:[longLabel:true, defaultValue: { '<<Input Here>>' }, validate: true])
        siblingInfo(nullable:true,maxSize:2000,group:'Detailed Assessment',renderOptions:[longLabel:true, defaultValue: { '<<Input Here>>' }, validate: true])
        admissionDate(nullable:true,group:'Detailed Assessment',renderOptions:[defaultValue: { new Date() }])
        medicationStatus(nullable:true, maxSize:32, group:'Detailed Assessment', inList:MEDICATION_STATUS)
        medication(nullable:true,maxSize:2000,group:'Detailed Assessment',renderOptions:[longLabel:true, defaultValue: { '<<Input Here>>' }, validate: true])
        preInitialServicePlanInfo(nullable:true,maxSize:2000,group:'Detailed Assessment',renderOptions:[longLabel:true, labelOnly: true])
        socialHistory(nullable:true,maxSize:2000,group:'Detailed Assessment',renderOptions:[longLabel:true, defaultValue: { '<<Input Here>>' }, validate: true])
        homeEnvironment(nullable:true,maxSize:2000,group:'Detailed Assessment',renderOptions:[longLabel:true, defaultValue: { '<<Input Here>>' }, validate: true])
        birthHistory(nullable:true,maxSize:2000,group:'Detailed Assessment',renderOptions:[longLabel:true, defaultValue: { '<<Input Here>>' }, validate: true])
        developmentalHistory(nullable:true,maxSize:2000,group:'Detailed Assessment',renderOptions:[longLabel:true, defaultValue: { '<<Input Here>>' }, validate: true])
        mentalAbuseHistory(nullable:true,maxSize:2000,group:'Detailed Assessment',renderOptions:[longLabel:true, defaultValue: { '<<Input Here>>' }, validate: true])
        schoolHistory(nullable:true,maxSize:2000,group:'Detailed Assessment',renderOptions:[longLabel:true, defaultValue: { '<<Input Here>>' }, validate: true])
        placementHistory(nullable:true,maxSize:2000,group:'Detailed Assessment',renderOptions:[longLabel:true, defaultValue: { '<<Input Here>>' }, validate: true])
        criminalHistory(nullable:true,maxSize:2000,group:'Detailed Assessment',renderOptions:[longLabel:true, defaultValue: { '<<Input Here>>' }, validate: true])
        skill(nullable:true,maxSize:2000,group:'Detailed Assessment',renderOptions:[longLabel:true, defaultValue: { '<<Input Here>>' }, validate: true])
        historyDocumentation(nullable:true,maxSize:2000,group:'Detailed Assessment',renderOptions:[longLabel:true, defaultValue: { '<<Input Here>>' }, validate: true])
        services(nullable:true,maxSize:2000,group:'Detailed Assessment',renderOptions:[longLabel:true, defaultValue: { '<<Input Here>>' }, validate: true])
        assessmentRecommendation(nullable:true,maxSize:2000,group:'Detailed Assessment',renderOptions:[longLabel:true, defaultValue: { '<<Input Here>>' }, validate: true])
        behaviorManagement(nullable:true,maxSize:2000,group:'Detailed Assessment',renderOptions:[longLabel:true, defaultValue: { '<<Input Here>>' }, validate: true])
        historyDetermination(nullable:true,maxSize:2000,group:'Detailed Assessment',renderOptions:[longLabel:true, defaultValue: { '<<Input Here>>' }, validate: true])
        historyRationale(nullable:true,maxSize:2000,group:'Detailed Assessment',renderOptions:[longLabel:true, defaultValue: { '<<Input Here>>' }, validate: true])
        personConductAssessment(nullable:true,maxSize:2000,group:'Detailed Assessment',renderOptions:[longLabel:true, defaultValue: { '<<Input Here>>' }, validate: true])
        signaturePage(nullable:true,group:'Signature Page',renderOptions:[unprintable:true])
        clientRecord(group:'Client Record',editable:false)
        editable(editable:false)
        dateCreated(editable:false)
        lastUpdated(editable:false)
    }
    
    static actions =
    [
        [action: 'list', type:'static', label: 'List', role: 'ROLE_SYSTEM_ADMINISTRATOR'],
        [action: 'create', type:'static', label: 'Create', role: 'ROLE_SYSTEM_ADMINISTRATOR,ROLE_CASE_MANAGER'],
        [action: 'show', label: 'View',  role: 'ROLE_SYSTEM_ADMINISTRATOR'],
        [action: 'edit', type:'hidden', role: [ 'ROLE_SYSTEM_ADMINISTRATOR': [:],
                                                'ROLE_CASE_MANAGER': [filter: User.caseManagerEditFilter]] ],
        [action: 'delete', role: 'ROLE_SYSTEM_ADMINISTRATOR']
    ]
    
    public String getDescription() {
        return "${name}'s admission assessment"
    }
    
    public String getClientId() {
        if (clientId) {
            return clientId
        }
        else {
            return generateUniqueClientId()
        }
    }
    
    private String generateUniqueClientId() {
        String id
        id = clientIdSeed+'-'+ Calendar.instance.get(Calendar.YEAR).toString().substring(2,4)
        while (AdmissionAssessment.findByClientId(id)) {
            id = (++clientIdSeed)+'-'+ Calendar.instance.get(Calendar.YEAR).toString().substring(2,4)
        }
        return id
    }
    
    public String getName() {
        return "${firstName ?: ''} ${middleInitial ?: ''} ${lastName ?: ''}"
    }
    
    public String getFirstName() {
        return "${firstName ?: (clientRecord?.firstName ?: '')}"
    }
    
    public String getlastName() {
        return "${lastName ?: (clientRecord?.lastName ?: '')}"
    }
    
    public String getMiddleInitial() {
        return "${middleInitial ?: (clientRecord?.middleInitial ?: '')}"
    }
    
    public String getReferredBy() {
        if (clientRecord && editable) {
            return clientRecord.referredBy
        }
        return referredBy
    }
    
    public Date getDateOfBirth() {
        if (clientRecord) {
            return clientRecord?.dateOfBirth
        }
        else {
            return null
        }
    }
    
    
    public float getAge() {
        if (clientRecord) {
            return clientRecord?.age
        }
        else {
            return 0
        }
    }
    
    public String getReferralReason() {
        if (clientRecord && editable) {
            return clientRecord.reasonForCall
        }
        return referralReason
    }
}
