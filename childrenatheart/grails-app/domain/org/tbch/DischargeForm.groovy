package org.tbch

import java.util.List;

class DischargeForm implements Serializable {

    final static List TYPES = [
        'Completed Primary POS Goals',
        'Family Able to Reunite',
        'Unwilling to Complete Program',
        'Place Other Facility',
        'HS Graduation',
        'Other'
    ]
    
    String name
    Date date //Date of Discharge
    String description
    String dischargeType
    
    String dischargePlanningPerson
    String dischargePlanningInvitee
    String dischargerName
    String dischargerRelationship
    String dischargerAddress1
    String dischargerAddress2
    String dischargerCity
    String dischargerState
    String dischargerZip
    String dischargerNumber
    
    String dischargeCircumstance
    Date childNotified
    String childCareAdmin
    String justification
    
    String medication
    String supportResources
    
    String aftercareRecommendation
    
    String medicalNeeds
    List medicalRecords
    String medicalRemainingNeeds
    
    String dentalNeeds
    List dentalRecords
    String dentalRemainingNeeds
    
    String therapeuticNeeds
    List therapeuticRecords
    String therapeuticRemainingNeeds
    
    String intellectualNeeds
    List intellectualRecords
    String intellectualRemainingNeeds
    
    String developmentalNeeds
    List developmentalRecords
    String developmentalRemainingNeeds
    
    String educationalNeeds
    List educationalRecords
    String educationalRemainingNeeds
    
    String behavioralNeeds
    List behavioralRecords
    String behavioralRemainingNeeds
    
    String socialNeeds
    List socialRecords
    String socialRemainingNeeds
    
    String communityNeeds
    List communityRecords
    String communityRemainingNeeds
    
    String culturalNeeds
    List culturalRecords
    String culturalRemainingNeeds
    
    String familialNeeds
    List familialRecords
    String familialRemainingNeeds
    
    String healthEducation
    
    String independentLiving
    SignatureDoc signaturePage
    Date dateCreated
    Date lastUpdated
    
    boolean editable=true
    
    static transients = ['description']
    
    static hasMany = [
        medicalRecords : MedicalDischargeRecord, 
        dentalRecords : DentalDischargeRecord, 
        therapeuticRecords : TherapeuticDischargeRecord, 
        intellectualRecords : IntellectualDischargeRecord,
        developmentalRecords : DevelopmentalDischargeRecord, 
        educationalRecords : EducationalDischargeRecord, 
        behavioralRecords : BehavioralDischargeRecord, 
        socialRecords : SocialDischargeRecord,
        communityRecords : CommunityDischargeRecord, 
        culturalRecords : CulturalDischargeRecord, 
        familialRecords : FamilialDischargeRecord
    ]
    
    static belongsTo = ClientRecord
    ClientRecord clientRecord
    
    
    public String getDescription() {
        return "${name}'s discharge summary"
    }
    
    public void setName(String name) {
        if (clientRecord) {
            this.name = clientRecord.name
        }
        else {
            this.name = name
        }
    }
    
    public String getName() {
        if (clientRecord) {
            return clientRecord.name
        }
        else {
            return this.name
        }
    }
    
    static constraints = {
        name(summary:true, blank: false,group:'Discharge Summary',renderOptions:[readOnly: true])
        date(summary:true, group:'Discharge Summary', renderOptions:[defaultValue: { new Date() }])
        dischargeType(summary:true, group:'Discharge Summary', inList:TYPES, nullable:true, validate:true)
        dischargePlanningPerson(blank: false,group:'Discharge Summary',renderOptions:[defaultValue: {'<<Input Here>>'}, validate:true])
        dischargePlanningInvitee(blank: false,group:'Discharge Summary',renderOptions:[defaultValue: {'<<Input Here>>'}, validate:true])
        dischargerName(blank: false,group:'Discharge Summary',renderOptions:[defaultValue: {'<<Input Here>>'}, validate:true])
        dischargerRelationship(blank: false,group:'Discharge Summary',renderOptions:[defaultValue: {'<<Input Here>>'}, validate:true])
        dischargerAddress1(blank: false,group:'Discharge Summary',renderOptions:[defaultValue: {'<<Input Here>>'}, validate:true])
        dischargerAddress2(nullable:true,group:'Discharge Summary')
        dischargerCity(blank: false,group:'Discharge Summary',renderOptions:[defaultValue: {'<<Input Here>>'}, validate:true])
        dischargerState(blank: false,group:'Discharge Summary',renderOptions:[defaultValue: {'<<Input Here>>'}, validate:true])
        dischargerZip(blank: false,group:'Discharge Summary',renderOptions:[defaultValue: {'<<Input Here>>'}, validate:true])
        dischargerNumber(blank: false,group:'Discharge Summary',renderOptions:[defaultValue: {'<<Input Here>>'}, validate:true])
        
        dischargeCircumstance(blank: false,maxSize:2000,group:'Discharge Summary',renderOptions:[longLabel:true,defaultValue: {'<<Input Here>>'}, validate:true])
        childNotified(group:'Discharge Summary',renderOptions:[defaultValue: { new Date() }])
        childCareAdmin(blank: false,group:'Discharge Summary',renderOptions:[longLabel:true,defaultValue: {'<<Input Here>>'}, validate:true])
        justification(blank: false,maxSize:2000,group:'Discharge Summary',renderOptions:[longLabel:true,defaultValue: {'<<Input Here>>'}, validate:true])
        
        medication(blank: false,maxSize:2000,group:'Discharge Summary',renderOptions:[longLabel:true, defaultValue: {'<<Input Here>>'}, validate:true])
        supportResources(blank: false,maxSize:2000,group:'Discharge Summary',renderOptions:[longLabel:true, defaultValue: {'<<Input Here>>'}, validate:true])
        
        aftercareRecommendation(blank: false,maxSize:2000,group:'Discharge Summary',renderOptions:[longLabel:true, defaultValue: {'<<Input Here>>'}, validate:true])
        
        healthEducation(nullable:true,maxSize:2000,group: 'Discharge Summary',renderOptions:[longLabel:true])
        independentLiving(nullable:true,maxSize:2000,group: 'Discharge Summary',renderOptions:[longLabel:true])
        
        medicalNeeds(nullable:true,maxSize:2000,group:'Medical',renderOptions:[longLabel:true])
        medicalRecords(nullable:true,editable: true, group:'Medical',renderOptions:[longLabel:true])
        medicalRemainingNeeds(blank: false,maxSize:2000,group:'Medical',renderOptions:[longLabel:true, defaultValue: {'<<Input Here>>'}, validate:true])
        
        dentalNeeds(nullable:true,maxSize:2000,group:'Dental',renderOptions:[longLabel:true])
        dentalRecords(nullable:true,editable: true, group:'Dental',renderOptions:[longLabel:true])
        dentalRemainingNeeds(blank: false,maxSize:2000,group:'Dental',renderOptions:[longLabel:true, defaultValue: {'<<Input Here>>'}, validate:true])
        
        therapeuticNeeds(nullable:true,maxSize:2000,group:'Therapeutic',renderOptions:[longLabel:true])
        therapeuticRecords(nullable:true,editable: true, group:'Therapeutic',renderOptions:[longLabel:true])
        therapeuticRemainingNeeds(blank: false,maxSize:2000,group:'Therapeutic',renderOptions:[longLabel:true, defaultValue: {'<<Input Here>>'}, validate:true])
        
        intellectualNeeds(nullable:true,maxSize:2000,group:'Intellectual',renderOptions:[longLabel:true])
        intellectualRecords(nullable:true,editable: true, group:'Intellectual',renderOptions:[longLabel:true])
        intellectualRemainingNeeds(blank: false,maxSize:2000,group:'Intellectual',renderOptions:[longLabel:true, defaultValue: {'<<Input Here>>'}, validate:true])
        
        developmentalNeeds(nullable:true,maxSize:2000,group:'Developmental',renderOptions:[longLabel:true])
        developmentalRecords(nullable:true,editable: true, group:'Developmental',renderOptions:[longLabel:true])
        developmentalRemainingNeeds(blank: false,maxSize:2000,group:'Developmental',renderOptions:[longLabel:true, defaultValue: {'<<Input Here>>'}, validate:true])
        
        educationalNeeds(nullable:true,maxSize:2000,group:'Educational',renderOptions:[longLabel:true])
        educationalRecords(nullable:true,editable: true, group:'Educational',renderOptions:[longLabel:true])
        educationalRemainingNeeds(blank: false,maxSize:2000,group:'Educational',renderOptions:[longLabel:true, defaultValue: {'<<Input Here>>'}, validate:true])
        
        behavioralNeeds(nullable:true,maxSize:2000,group:'Behavioral and Discipline',renderOptions:[longLabel:true])
        behavioralRecords(nullable:true,editable: true, group:'Behavioral and Discipline',renderOptions:[longLabel:true])
        behavioralRemainingNeeds(blank: false,maxSize:2000,group:'Behavioral and Discipline',renderOptions:[longLabel:true, defaultValue: {'<<Input Here>>'}, validate:true])
        
        socialNeeds(nullable:true,maxSize:2000,group:'Social, Recreations, and Leisure Activities',renderOptions:[longLabel:true])
        socialRecords(nullable:true,editable: true, group:'Social, Recreations, and Leisure Activities',renderOptions:[longLabel:true])
        socialRemainingNeeds(blank: false,maxSize:2000,group:'Social, Recreations, and Leisure Activities',renderOptions:[longLabel:true, defaultValue: {'<<Input Here>>'}, validate:true])
        
        communityNeeds(nullable:true,maxSize:2000,group:'Community Integration',renderOptions:[longLabel:true])
        communityRecords(nullable:true,editable: true, group:'Community Integration',renderOptions:[longLabel:true])
        communityRemainingNeeds(blank: false,maxSize:2000,group:'Community Integration',renderOptions:[longLabel:true, defaultValue: {'<<Input Here>>'}, validate:true])
        
        culturalNeeds(nullable:true,maxSize:2000,group:'Cultural Identity',renderOptions:[longLabel:true])
        culturalRecords(nullable:true,editable: true, group:'Cultural Identity',renderOptions:[longLabel:true])
        culturalRemainingNeeds(blank: false,maxSize:2000,group:'Cultural Identity',renderOptions:[longLabel:true, defaultValue: {'<<Input Here>>'}, validate:true])
        
        familialNeeds(nullable:true,maxSize:2000,group:'Family Relationships',renderOptions:[longLabel:true])
        familialRecords(nullable:true,editable: true, group:'Family Relationships',renderOptions:[longLabel:true])
        familialRemainingNeeds(blank: false,maxSize:2000,group:'Family Relationships',renderOptions:[longLabel:true, defaultValue: {'<<Input Here>>'}, validate:true])
        
//        continuationGoals(blank: false,maxSize:2000,group:'Discharge Summary',renderOptions:[longLabel:true])
//        continuationAssessment(blank: false,maxSize:2000,group:'Discharge Summary',renderOptions:[longLabel:true])
//        continuationRating(range:-2..3, group:'Discharge Summary')
//        continuationRemainingNeeds(blank: false,maxSize:2000,group:'Discharge Summary',renderOptions:[longLabel:true])
        signaturePage(nullable:true,group:'Signature Page',renderOptions:[unprintable:true])
        clientRecord(group:'Client Record',editable:false)
        editable(editable:false)
        dateCreated(editable:false)
        lastUpdated(editable:false)
    }
        
    static actions = [
        [action: 'list', type:'static', label: 'List', role: 'ROLE_SYSTEM_ADMINISTRATOR'],
        [action: 'create', type:'static', label: 'Create', role: 'ROLE_SYSTEM_ADMINISTRATOR,ROLE_CASE_MANAGER'],
        [action: 'show', label: 'View',  role: [ 'ROLE_SYSTEM_ADMINISTRATOR': [:],
                                                'ROLE_CASE_MANAGER': [filter: User.caseManagerShowFilter]] ],
        [action: 'edit', type:'hidden', role: [ 'ROLE_SYSTEM_ADMINISTRATOR': [:],
                                                'ROLE_CASE_MANAGER': [filter: User.caseManagerEditFilter]] ],
        [action: 'delete', role: 'ROLE_SYSTEM_ADMINISTRATOR']
    ]
}
