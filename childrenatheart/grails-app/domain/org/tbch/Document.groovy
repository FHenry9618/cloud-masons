package org.tbch

import org.codehaus.groovy.grails.commons.ConfigurationHolder

@com.cloudmasons.domain.SecureFile
class Document implements Serializable {

    static final List DOC_TYPES = ConfigurationHolder.config.document.types instanceof List ? ConfigurationHolder.config.document.types : ['Signature Page','Other']
    
    String type = 'Other'
    Date dateCreated
    Date lastUpdated
    
    static mapping = {
        cache true
        sort dateCreated:'desc'
     }

    String description
    
    static actions =
    [
        [action: 'create', type:'hidden', label: 'Create', role: 'ROLE_SYSTEM_ADMINISTRATOR,ROLE_CASE_MANAGER'],
        [action: 'download', label: 'Download', role:[ 'ROLE_SYSTEM_ADMINISTRATOR': [:],'ROLE_REPORTING':[:],
                                                            'HOUSE_PARENT': [filter: User.houseParentShowFilter],
                                                            'ROLE_CASE_MANAGER': [filter: User.caseManagerShowFilter]] ],
        [action: 'edit', label: 'Edit', role: [ 'ROLE_SYSTEM_ADMINISTRATOR': [:],
                                                        'ROLE_CASE_MANAGER': [filter: User.caseManagerShowFilter]] ],
        [action: 'delete',label: 'Delete', role: 'ROLE_SYSTEM_ADMINISTRATOR']
    ]
    
    static constraints = {
        type(summary: true,inList: DOC_TYPES, maxSize:200)
        description(summary: true)
        clientRecord(group:'Client Record',editable:false)
        
        dateCreated(editable:false)
        lastUpdated(editable:false)
    }
    static belongsTo = ClientRecord
    ClientRecord clientRecord
}
