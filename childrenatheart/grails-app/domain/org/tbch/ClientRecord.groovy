package org.tbch

import org.springframework.security.context.SecurityContextHolder

class ClientRecord implements Serializable {

    static final List REFERRAL_REASON = [
            'Full',
            'Over 17 yrs',
            'Too Young',
            'IQ Below 70',
            'Higher Level of Care Recommended',
            'Significant Medical Needs Required',
            'Did not pursue placement'
    ]

    static final List LEVEL_OF_CARE = ['Basic', 'Moderate', 'Advance']

    static houseParentFilter = {
        def pricipalInfo = SecurityContextHolder.context?.authentication?.principal
        def user = pricipalInfo?.domainClass
        user?.refresh()
        return livingUnit?.id == user?.livingUnit?.id
    }

    static houseParentListFilter = houseParentFilter

    static caseManagerFilter = {
        def pricipalInfo = SecurityContextHolder.context?.authentication?.principal
        def user = pricipalInfo?.domainClass
        user?.refresh()
        return (campus == null || campus?.id == user?.campus?.id)
    }

    static caseManagerListFilter = caseManagerFilter


    static livingUnitAllowedValues = {
        def pricipalInfo = SecurityContextHolder.context?.authentication?.principal
        def user = pricipalInfo?.domainClass
        user?.refresh()
        if (user.livingUnit) {
            return [user.livingUnit]
        }
        if (user.campus) {
            def units = []
            user.campus.programs?.each {
                units.addAll(it?.livingUnits ?: [])
            }
            return units
        }
        return LivingUnit.list()
    }

    static campusAllowedValues = {
        def pricipalInfo = SecurityContextHolder.context?.authentication?.principal
        def user = pricipalInfo?.domainClass
        user?.refresh()
        if (user.campus) {
            return [user.campus]
        }
        return Campus.list()
    }

    String name //transient
    String lastName
    String firstName
    String middleInitial
    Date dateOfBirth
    float age
    String clientId //transient
    String description //transient
    String foodAllergies
    // default to after care if it doesn't exist
    String gradeLevel = aftercare?.grade


    String referredBy
    String reasonForCall

    String status = 'application'

    Date dateApplication
    Date datePlacement
    Date dateDischarge

    Date dateReferral
    boolean referralOut = false
    String referralReason

    Date dateCreated
    Date lastUpdated

    AdmissionAssessment admissionAssessment
    PreliminaryServicePlan preliminaryServicePlan
    DischargeForm dischargeForm
    Aftercare aftercare

    List monthlyNotes
    List weeklyNotes
    List caseNotes
    List seriousIncidentReport
    List planOfService
    List visitationForm
    List significantPersons
    List medications

    LivingUnit livingUnit
    Campus campus

    String levelOfCare
    
    String normalcyDecisionBy1
    String normalcyDecisionBy2
    String normalcyDecisionBy3
    String normalcyDecisionBy4

    static transients = [
            'description',
            'name',
            'clientId',
            'age'
    ]
    List documents
    static hasMany = [
            documents            : Document,
            weeklyNotes          : WeeklyNotes,
            monthlyNotes         : MonthlyNotes,
            caseNotes            : CaseNotes,
            significantPersons   : SignificantPerson,
            seriousIncidentReport: SeriousIncidentReport,
            planOfService        : PlanOfService,
            visitationForm       : VisitationForm,
            medications          : Medication
    ]
    static searchable = true
    static mapping = {
        cache true
        sort lastUpdated:'desc'
    }


    static actions =
            [
                    [action: 'list', type: 'static', label: 'List', role:
                            ['ROLE_SYSTEM_ADMINISTRATOR': [:], 'ROLE_REPORTING': [:],
                             'HOUSE_PARENT'             : [filter: houseParentListFilter],
                             'ROLE_CASE_MANAGER'        : [filter: caseManagerListFilter]]],
                    [action: 'create', type: 'static', label: 'Create', role: 'ROLE_SYSTEM_ADMINISTRATOR,ROLE_CASE_MANAGER'],
                    [action: 'show', label: 'Show', role: [
                            'ROLE_SYSTEM_ADMINISTRATOR': [:],
                            'ROLE_REPORTING'           : [:],
                            'HOUSE_PARENT'             : [filter: houseParentFilter],
                            'ROLE_CASE_MANAGER'        : [filter: caseManagerFilter]
                    ]
                    ],
                    [action: 'print', label: 'Print', type: 'hidden', role: ['ROLE_SYSTEM_ADMINISTRATOR': [:], 'ROLE_REPORTING': [:],
                                                                             'HOUSE_PARENT'             : [filter: houseParentFilter],
                                                                             'ROLE_CASE_MANAGER'        : [filter: caseManagerFilter]]],
                    [action: 'edit', type: 'hidden', label: 'Edit', role: ['ROLE_SYSTEM_ADMINISTRATOR': [:],
                                                                           'ROLE_CASE_MANAGER'        : [filter: caseManagerFilter]]],
                    [action: 'delete', label: 'Delete', role: 'ROLE_SYSTEM_ADMINISTRATOR']
            ]


    static constraints = {
        firstName(summary: true, blank: false, group: 'Child Info')
        middleInitial(nullable: true, group: 'Child Info')
        lastName(summary: true, blank: false, group: 'Child Info')
        dateOfBirth(nullable: true, group: 'Child Info')
        age(group: 'Child Info', renderOptions: [readOnly: true])
        clientId(nullable: true, group: 'Child Info')
        campus(nullable: true, group: 'Child Info', allowedValues: campusAllowedValues)
        livingUnit(nullable: true, group: 'Child Info', allowedValues: livingUnitAllowedValues)
        foodAllergies(nullable: true, group: 'Child Info',maxSize:2000)
        gradeLevel(nullable: true, group: 'Child Info')

        normalcyDecisionBy1(nullable: true, group: 'Child Info')
        normalcyDecisionBy2(nullable: true, group: 'Child Info')
        normalcyDecisionBy3(nullable: true, group: 'Child Info')
        normalcyDecisionBy4(nullable: true, group: 'Child Info')

        significantPersons(nullable: true, editable: true, group: 'Significant Persons')
        status(inList: Constants.STATUS_STATES, summary: true, group: 'Case Info')
        levelOfCare(nullable: true, inList: LEVEL_OF_CARE, group: 'Case Info')
        referredBy(nullable: true, maxSize: 500, group: 'Case Info')
        reasonForCall(nullable: true, maxSize: 2000, group: 'Case Info')
        dateApplication(nullable: true, group: 'Case Info')
        datePlacement(nullable: true, group: 'Case Info')
        dateDischarge(nullable: true, group: 'Case Info')
        dateReferral(nullable: true, group: 'Case Info')
        referralOut(group: 'Case Info')
        referralReason(nullable: true, inList: REFERRAL_REASON, group: 'Case Info')
        medications(nullable: true, editable: true, group: 'Medications')
        admissionAssessment(nullable: true, editable: true, group: 'Admission Assessment')
        preliminaryServicePlan(nullable: true, editable: true, group: 'Preliminary Service Plan')
        planOfService(nullable: true, editable: true, group: 'Plan of Service')
        weeklyNotes(nullable: true, editable: true, group: 'Weekly Notes')
        monthlyNotes(nullable: true, editable: true, group: 'Monthly Notes')
        caseNotes(nullable: true, editable: true, group: 'Case Notes')
        seriousIncidentReport(nullable: true, editable: true, group: 'Serious Incident Reports')
        visitationForm(nullable: true, editable: true, group: 'Visitation Forms')
        dischargeForm(nullable: true, editable: true, group: 'Discharge Form')
        aftercare(nullable: true, editable: true, group: 'Aftercare')
        documents(nullable: true, editable: true, group: 'Uploaded Documents', renderOptions: [unprintable: true])
        dateCreated(editable: false)
        lastUpdated(editable: false)
    }

    String getDescription() {
        return "${name}'s record"
    }

    String getName() {
        "${firstName ?: ''} ${middleInitial ?: ''} ${lastName ?: ''}"
    }

    void setDatePlacement(Date date) {
        this.datePlacement = admissionAssessment?.admissionDate ?: date
    }

    Date getDatePlacement() {
        admissionAssessment?.admissionDate ?: datePlacement
    }

    void setDateDischarge(Date date) {
        this.dateDischarge = dischargeForm?.date ?: date
    }

    Date getDateDischarge() {
        dischargeForm?.date ?: dateDischarge
    }

    String getClientId() {
        admissionAssessment?.clientId ?: 'N/A'
    }

    float getAge() {
        dateOfBirth ? ((new Date() - dateOfBirth) / 365) : 0
    }

    Campus getCampus() {
        if (!campus && !dateCreated) {
            def pricipalInfo = SecurityContextHolder.context?.authentication?.principal
            def user = pricipalInfo?.domainClass
            user?.refresh()
            return user?.campus
        }
        campus
    }

}
