package org.tbch

import groovy.lang.Closure;

import java.util.Date;
import org.codehaus.groovy.grails.commons.ConfigurationHolder

@com.cloudmasons.domain.SecureFile
class SignatureDoc implements Serializable {

    static Closure houseParentFilter = {
        def pricipalInfo = org.springframework.security.context.SecurityContextHolder.context?.authentication?.principal
        def user  = pricipalInfo?.domainClass
        user?.refresh()
        return assoicatedForm?.clientRecord?.livingUnit?.id == user?.livingUnit?.id
    }
    
    static Closure caseManagerFilter = {
        def pricipalInfo = org.springframework.security.context.SecurityContextHolder.context?.authentication?.principal
        def user  = pricipalInfo?.domainClass
        user?.refresh()
        return (assoicatedForm?.clientRecord?.campus == null || assoicatedForm?.clientRecord?.campus?.id == user?.campus?.id)
    }
    
    Date dateCreated
    Date lastUpdated
    
    static mapping = {
        cache true
     }

    static actions =
    [
        [action: 'create', type:'hidden', label: 'Create', role: ''],
        [action: 'showDetails', type:'hidden', label: 'showDetails', role: ''],
        [action: 'download', label: 'Download', role:[ 'ROLE_SYSTEM_ADMINISTRATOR': [:],'ROLE_REPORTING':[:],
                                                            'HOUSE_PARENT': [filter: houseParentFilter],
                                                            'ROLE_CASE_MANAGER': [filter: caseManagerFilter]] ],
        [action: 'edit', label: 'Edit', role: [ 'ROLE_SYSTEM_ADMINISTRATOR': [:]] ],
        [action: 'delete',label: 'Delete', role: 'ROLE_SYSTEM_ADMINISTRATOR']
    ]
    
    static constraints = {
        assoicatedForm(group:'Associated Form',editable:false)
        dateCreated(editable:false)
        lastUpdated(editable:false)
    }
    static belongsTo = [Object]
    Object assoicatedForm
}
