package org.tbch


/**
 * Authority domain class.
 */
class Role implements Serializable {

    
    static hasMany = [users: User]

    /** description */
    String description
    /** ROLE String */
    String name

    static mapping = {
       cache true
    }

    static constraints = {
        name(blank: false, unique: true)
        description(nullable: true)
        users(editable:false)
    }

}
