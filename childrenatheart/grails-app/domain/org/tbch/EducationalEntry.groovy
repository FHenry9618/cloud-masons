package org.tbch

class EducationalEntry extends PlanEntry {

    static belongsTo = PlanOfService
    PlanOfService parentObject
    
    static constraints = {
        parentObject(editable:false)
    }
}
