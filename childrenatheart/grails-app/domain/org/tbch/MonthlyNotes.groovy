package org.tbch

class MonthlyNotes implements Serializable {

    String name
    Date date
    String description
    String significantContacts
    String summaryOfProgress
    String outstandingIssues
    
    Date dateCreated
    Date lastUpdated
    
    boolean editable=true
    static transients = ['description']
    static belongsTo = ClientRecord
    ClientRecord clientRecord

    static mapping = {
        sort lastUpdated:'desc'
    }

    String getDescription() {
        return "${name}'s monthly notes"
    }
    static constraints = {
        date(summary: true, group: 'Notes Details',renderOptions:[defaultValue: { new Date() }])
        name(blank:false,group: 'Notes Details',renderOptions:[readOnly: true])
        significantContacts(nullable:true,maxSize:2000,group:'Notes Details')
        summaryOfProgress(nullable:true,maxSize:2000,group:'Notes Details')
        outstandingIssues(nullable:true,maxSize:2000,group:'Notes Details')
        clientRecord(group:'Client Record',editable:false)
        
        editable(editable:false)
        dateCreated(editable:false)
        lastUpdated(editable:false)
    }
    
    void setName(String name) {
        if (clientRecord) {
            this.name = clientRecord.name
        }
        else {
            this.name = name
        }
    }
    
    String getName() {
        if (clientRecord) {
            return clientRecord.name
        }
        else {
            return this.name
        }
    }
    
    static actions =
    [
        [action: 'list', type:'static', label: 'List', role: 'ROLE_SYSTEM_ADMINISTRATOR'],
        [action: 'create', type:'static', label: 'Create', role: 'ROLE_SYSTEM_ADMINISTRATOR,ROLE_CASE_MANAGER'],
        [action: 'show', label: 'Show',  role: [ 'ROLE_SYSTEM_ADMINISTRATOR': [:],'ROLE_REPORTING':[:],
                                                 'HOUSE_PARENT': [filter: User.houseParentShowFilter],
                                                 'ROLE_CASE_MANAGER': [filter: User.caseManagerShowFilter]] ],
        [action: 'edit', type:'hidden', label: 'Edit', role: [ 'ROLE_SYSTEM_ADMINISTRATOR': [:],
                                                            'ROLE_CASE_MANAGER': [filter: User.caseManagerEditFilter]] ],
        [action: 'delete', role: 'ROLE_SYSTEM_ADMINISTRATOR']
    ]
    
}
