package org.tbch

class Medication implements Serializable {

    String name
    String dosage
    String provider
    String phoneNumber
    Date datePrescribed = new Date()

    boolean active = true

    Date dateCreated
    Date lastUpdated

    static mapping = {
        cache true
        sort lastUpdated:'desc'
    }

    static belongsTo = ClientRecord
    ClientRecord clientRecord

    static actions = [
            [action: 'list', type: 'static', label: 'List',
                role: 'ROLE_SYSTEM_ADMINISTRATOR,ROLE_CASE_MANAGER,HOUSE_PARENT'],
            [action: 'create', type: 'static', label: 'Create',
                role: 'ROLE_SYSTEM_ADMINISTRATOR,ROLE_CASE_MANAGER,HOUSE_PARENT'],
            [action: 'show', label: 'Show', role: [
                    'ROLE_SYSTEM_ADMINISTRATOR': [:],
                    'ROLE_REPORTING'           : [:],
                    'HOUSE_PARENT'             : [filter: User.houseParentShowFilter],
                    'ROLE_CASE_MANAGER'        : [filter: User.caseManagerShowFilter]
                ]
            ],
            [action: 'edit', type: 'hidden', label: 'Edit', role: [
                    'ROLE_SYSTEM_ADMINISTRATOR': [:],
                    'ROLE_REPORTING'           : [:],
                    'HOUSE_PARENT'             : [filter: User.houseParentOpenEditFilter],
                    'ROLE_CASE_MANAGER'        : [filter: User.caseManagerOpenEditFilter]
                ]
            ],
            [action: 'delete', role: 'ROLE_SYSTEM_ADMINISTRATOR']
    ]

    static constraints = {
        name(summary: true, blank: false, group: 'Medication')
        dosage(summary: true, blank: false, group: 'Medication')
        datePrescribed(group: 'Medication')
        provider(nullable: true, group: 'Medication')
        phoneNumber(nullable: true, group: 'Medication')
        active(group: 'Medication')
        clientRecord(group: 'Client Record', editable: false)

        dateCreated(editable: false)
        lastUpdated(editable: false)
    }
}
