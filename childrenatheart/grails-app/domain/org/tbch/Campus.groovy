package org.tbch

class Campus implements Serializable {

    static hasMany = [programs:Program]
    String name
    String description
    List programs
    
    static constraints = {
        name(blank:false, group:'Campus Info')
        description(nullable:true,group:'Campus Info')
        programs(nullable:true, group:'Program Info')
    }

    static actions =
    [
        [action: 'list', type:'static', label: 'List', role: 'ROLE_SYSTEM_ADMINISTRATOR'],
        [action: 'create', type:'static', label: 'Create', role: 'ROLE_SYSTEM_ADMINISTRATOR'],
        [action: 'show', label: 'Show',  role: 'ROLE_SYSTEM_ADMINISTRATOR'],
        [action: 'edit', type:'hidden', label: 'Edit', role: 'ROLE_SYSTEM_ADMINISTRATOR'],
        [action: 'delete', role: 'ROLE_SYSTEM_ADMINISTRATOR']
    ]
    
    
    String toString() {
        return this.name
    }
}
