package org.tbch

import org.codehaus.groovy.grails.commons.ApplicationHolder as AH

class SeriousIncidentReport implements Serializable {    

    String name
    Date date
    String time
    float age
    Date dateOfBirth
    String gender
    String description
    String staffInvolved
    
    boolean death
    boolean criminalBehavior
    boolean verbalAggression
    boolean injuryHospitalization
    boolean theft
    boolean physicalAggression
    boolean allegation
    boolean destruction
    boolean medicationError
    boolean sexual
    boolean injuryToClient
    boolean absent
    boolean suicidalIdeation
    boolean injuryToStaff
    boolean violation
    boolean suicidalGesture
    boolean selfInjury
    boolean other
    String otherDetail
    boolean suicideAttempt
    boolean restraint
 
    String summary
    String outcome
    
    boolean medicalTreatment
    Date medicalTreatmentDate
    String medicalTreatmentTime
    boolean firstAidAdministered
    Date firstAidAdministeredDate
    String firstAidAdministeredTime
    boolean assessmentRecommended
    Date assessmentRecommendedDate
    String assessmentRecommendedTime
    String actionExplanation
     
    boolean supervisor
    Date supervisorNotifiedDate
    String supervisorNotifiedTime
    String supervisorContacted
    boolean caseManager
    Date caseManagerNotifiedDate
    String caseManagerNotifiedTime
    String caseManagerContacted
    boolean therapist
    Date therapistNotifiedDate
    String therapistNotifiedTime
    String therapistContacted
    boolean cps
    Date cpsNotifiedDate
    String cpsNotifiedTime
    String cpsContacted
    boolean parent
    Date parentNotifiedDate
    String parentNotifiedTime
    String parentContacted
    boolean police
    Date policeNotifiedDate
    String policeNotifiedTime
    String policeContacted
    String policeReport
    boolean licensing
    Date licensingNotifiedDate
    String licensingNotifiedTime
    String licensingContacted
    String licensingReport
    boolean otherNotified
    Date otherNotifiedDate
    String otherNotifiedTime
    String otherContacted
    
    String reportingStaff
    Date reportingDate
    String administration
     
    AddendumRestraintReport restraintReport
    //SignatureDoc signaturePage
    Date dateCreated
    Date lastUpdated

    static mapping = {
        sort date:'desc'
    }

    static transients = ['description', 'dateOfBirth','age']
    static belongsTo = ClientRecord
    ClientRecord clientRecord
    
    boolean editable=true

    String getDescription() {
        return "${name}'s serious incident report"
    }
    
    void setName(String name) {
        if (clientRecord) {
            this.name = clientRecord.name
        }
        else {
            this.name = name
        }
    }
    
    String getName() {
        clientRecord?.name ?: this.name
    }
    
    static constraints = {
        date(summary: true, group: 'Client Information',renderOptions:[defaultValue: { new Date() }])
        time(group: 'Client Information',renderOptions:[timepicker:true])
        name(summary: true, blank:false,group: 'Client Information',renderOptions:[readOnly: true])
        age(group: 'Client Information',renderOptions:[readOnly: true])
        dateOfBirth(nullable:true, group: 'Client Information',renderOptions:[readOnly: true])
        gender(inList: ['Male', 'Female'], blank:false,group:'Client Information')
        
        death(group:'Type of Incident (check all that apply)')
        criminalBehavior(group:'Type of Incident (check all that apply)')
        verbalAggression(group:'Type of Incident (check all that apply)')
        injuryHospitalization(group:'Type of Incident (check all that apply)')
        theft(group:'Type of Incident (check all that apply)')
        physicalAggression(group:'Type of Incident (check all that apply)')
        allegation(group:'Type of Incident (check all that apply)')
        destruction(group:'Type of Incident (check all that apply)')
        medicationError(group:'Type of Incident (check all that apply)')
        sexual(group:'Type of Incident (check all that apply)')
        injuryToClient(group:'Type of Incident (check all that apply)')
        absent(group:'Type of Incident (check all that apply)')
        suicidalIdeation(group:'Type of Incident (check all that apply)')
        injuryToStaff(group:'Type of Incident (check all that apply)')
        violation(group:'Type of Incident (check all that apply)')
        suicidalGesture(group:'Type of Incident (check all that apply)')
        selfInjury(group:'Type of Incident (check all that apply)')
        other(group:'Type of Incident (check all that apply)')
        otherDetail(nullable: true, group:'Type of Incident (check all that apply)')
        suicideAttempt(group:'Type of Incident (check all that apply)')
        restraint(group:'Type of Incident (check all that apply)')
        
        staffInvolved(nullable: true, maxSize:1000, group:'Incident Details',renderOptions:[longLabel:true, defaultValue: {'<<Input Here>>'}, validate:true])
        summary(nullable: true, maxSize:10000, group:'Incident Details',renderOptions:[longLabel:true, defaultValue: {'<<Input Here>>'}, validate:true])
        outcome(nullable: true, maxSize:10000, group:'Incident Details',renderOptions:[longLabel:true, defaultValue: {'<<Input Here>>'}, validate:true])
        
        medicalTreatment(group:'Action Taken')
        medicalTreatmentDate(nullable: true, group:'Action Taken')
        medicalTreatmentTime(nullable: true, group:'Action Taken',renderOptions:[timepicker:true])
        firstAidAdministered(group:'Action Taken')
        firstAidAdministeredDate(nullable: true, group:'Action Taken')
        firstAidAdministeredTime(nullable: true, group:'Action Taken',renderOptions:[timepicker:true])
        assessmentRecommended(group:'Action Taken')
        assessmentRecommendedDate(nullable: true, group:'Action Taken')
        assessmentRecommendedTime(nullable: true, group:'Action Taken',renderOptions:[timepicker:true])
        actionExplanation(nullable: true, maxSize:10000, group:'Action Taken',renderOptions:[longLabel:true])
        
        supervisor(group:'Notified')
        supervisorNotifiedDate(nullable: true, group:'Notified')
        supervisorNotifiedTime(nullable: true, group:'Notified',renderOptions:[timepicker:true])
        supervisorContacted(nullable: true, group:'Notified')
        caseManager(group:'Notified')
        caseManagerNotifiedDate(nullable: true, group:'Notified')
        caseManagerNotifiedTime(nullable: true, group:'Notified',renderOptions:[timepicker:true])
        caseManagerContacted(nullable: true, group:'Notified')
        therapist(group:'Notified')
        therapistNotifiedDate(nullable: true, group:'Notified')
        therapistNotifiedTime(nullable: true, group:'Notified',renderOptions:[timepicker:true])
        therapistContacted(nullable: true, group:'Notified')
        cps(group:'Notified')
        cpsNotifiedDate(nullable: true, group:'Notified')
        cpsNotifiedTime(nullable: true, group:'Notified',renderOptions:[timepicker:true])
        cpsContacted(nullable: true, group:'Notified')
        parent(group:'Notified')
        parentNotifiedDate(nullable: true, group:'Notified')
        parentNotifiedTime(nullable: true, group:'Notified',renderOptions:[timepicker:true])
        parentContacted(nullable: true, group:'Notified')
        police(group:'Notified')
        policeNotifiedDate(nullable: true, group:'Notified')
        policeNotifiedTime(nullable: true, group:'Notified',renderOptions:[timepicker:true])
        policeContacted(nullable: true, group:'Notified')
        policeReport(nullable: true, group:'Notified')
        licensing(group:'Notified')
        licensingNotifiedDate(nullable: true, group:'Notified')
        licensingNotifiedTime(nullable: true, group:'Notified',renderOptions:[timepicker:true])
        licensingContacted(nullable: true, group:'Notified')
        licensingReport(nullable: true, group:'Notified')
        otherNotified(group:'Notified')
        otherNotifiedDate(nullable: true, group:'Notified')
        otherNotifiedTime(nullable: true, group:'Notified',renderOptions:[timepicker:true])
        otherContacted(nullable: true, group:'Notified')

        reportingStaff(nullable: true, group:'Incident Details',renderOptions:[defaultValue: 
                          { def pricipalInfo = org.springframework.security.context.SecurityContextHolder.context?.authentication?.principal
                            def user  = pricipalInfo?.domainClass 
                            return user.name
                          }])
        administration(nullable: true, group:'Incident Details')
        reportingDate(nullable: true, group:'Incident Details',renderOptions:[defaultValue: { new Date() }])
        
        restraintReport(nullable: true, group:'Restraint Addendum')
        //signaturePage(nullable:true,group:'Signature Page',renderOptions:[unprintable:true])
        clientRecord(group:'Client Record',editable:false)
        editable(editable:false)
        dateCreated(editable:false)
        lastUpdated(editable:false)
    }
    static actions =
    [
        [action: 'list', type:'static', label: 'List', role: 'ROLE_SYSTEM_ADMINISTRATOR'],
        [action: 'create', type:'static', label: 'Create', role: 'ROLE_SYSTEM_ADMINISTRATOR,ROLE_CASE_MANAGER,HOUSE_PARENT'],
        [action: 'show', label: 'Show',  role: [ 'ROLE_SYSTEM_ADMINISTRATOR': [:],'ROLE_REPORTING':[:],
                                                 'HOUSE_PARENT': [filter: User.houseParentShowFilter],
                                                 'ROLE_CASE_MANAGER': [filter: User.caseManagerShowFilter]] ],
        [action: 'edit', type:'hidden', label: 'Edit', role: [ 'ROLE_SYSTEM_ADMINISTRATOR': [:],
                                                            'HOUSE_PARENT': [filter: User.houseParentEditFilter],
                                                            'ROLE_CASE_MANAGER': [filter: User.caseManagerEditFilter]] ],
                                                        
        [action: 'delete', role: 'ROLE_SYSTEM_ADMINISTRATOR']
    ]
    
    Date getDateOfBirth() {
        clientRecord?.dateOfBirth
    }
    
    
    //=======================================================================
    // Events
    //=======================================================================
    def afterInsert() {
         AH.application.mainContext.tbchService.sendSeriousIncidentReport(this)
    }
    
    
    float getAge() {
        clientRecord?.age ?: 0
    }
}
