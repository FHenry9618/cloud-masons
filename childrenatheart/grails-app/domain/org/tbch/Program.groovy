package org.tbch

class Program implements Serializable{

    static belongsTo = Campus
    static hasMany = [livingUnits:LivingUnit]
    String name
    String description
    List livingUnits
    Campus campus
    
    public String getCampusName() {
        return campus?.name
    }
    
    static transients = ['campusName']
    
    static constraints = {
        name(blank:false, group:'Program Info')
        description(nullable:true,group:'Program Info')
        campus(group:'Program Info',editable:false)
        campusName(nullable:true,group:'Program Info')
        livingUnits(nullable:true, group:'Living Units')
    }

    static actions =
    [
        [action: 'list', type:'static', label: 'List', role: 'ROLE_SYSTEM_ADMINISTRATOR'],
        [action: 'create', type:'static', label: 'Create', role: 'ROLE_SYSTEM_ADMINISTRATOR'],
        [action: 'show', label: 'Show',  role: 'ROLE_SYSTEM_ADMINISTRATOR'],
        [action: 'edit', type:'hidden', label: 'Edit', role: 'ROLE_SYSTEM_ADMINISTRATOR'],
        [action: 'delete', role: 'ROLE_SYSTEM_ADMINISTRATOR']
    ]
    
}
