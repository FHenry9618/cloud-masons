<%@ page import="grails.util.GrailsNameUtils" %>
<%
   def dateFormat = message(code: 'default.date.format', default: 'MM/dd/yyyy')
%>
<g:if test="${value}">
    <table class="compact">
      <thead>
        <tr>
          <th>Date Added</th>
          <th>Target Date</th>
          <th>Scale</th>
          <th>Actions</th>
        </tr>  
      </thead>
    
      <tbody>
        <g:each in="${value}" status="i" var="planEntry">
        <%
            def entity = GrailsNameUtils.getShortName(planEntry?.getClass()?.name)
            def controllerName = entity[0].toLowerCase()+entity.substring(1,entity.size())
        %>
          <tr>
            <td>${planEntry?.servicePlanEntry?.date?.format(dateFormat)}</td>
            <td>${planEntry?.servicePlanEntry?.targetDate?.format(dateFormat)}</td>
            <td>${planEntry?.servicePlanEntry?.scale}</td>
            <td><g:link controller="dischargeForm" action="show" id="${beanInstance?.id}" params="[associated: property.name, propertyId: planEntry?.id]">[View]</g:link><br/>
<!--                [ <bean:actionLink beanControllerName="${controllerName}" bean="${planEntry}" action="[action: 'delete']"/> ]-->

<!--            <a class="ico show-${domainClass.propertyName}-edit-${property.name}-form-${i}" title="edit">[Edit]</a>-->
<!--            <a class="ico delete-${domainClass.propertyName}-edit-${property.name}-form-${i}" href="#" id="delete-${property.name}-${i}" title="delete"><img src="${resource(dir:'css/boxie/img/led-ico',file:'delete.png')}" alt="delete" />[Delete]</a>-->
            </td>
          </tr>
         </g:each>
       </tbody>
    </table>
</g:if>
<g:else>
  <br/><i>No entrys are added to this ${domainClass.naturalName}</i><br/>
</g:else>
<br/>
<!--<a class="button" href="${g.createLink(controller: 'planOfService', action: 'addEntry', id:beanInstance?.id, params:[associated: property.name])}">Add Entry</a>-->
<div>
    <script>
    $(function() {
       //$( ".button" ).button();
    });
    </script>
</div>
    