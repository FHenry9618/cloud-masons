
<!-- Clear out any nulls -->
<g:set var="thevalue" value="${value?.findAll{it}}"/>
<g:if test="${thevalue}">
 <g:set var="summaryProps" value="${summaryProperties[0..Math.min(summaryProperties.size()-1,5)]}" />
 <table class="compact">
  <thead>
   <tr>
    <g:each in="${summaryProps}" var="p">
     <th><bean:label bean="${property.referencedDomainClass?.newInstance()}" property="${p}" /></th>
    </g:each>
    <th>Actions</th>
   </tr>
  </thead>
  <tbody>
   <g:each in="${thevalue}" status="i" var="b">
    <tr>
     <g:each in="${summaryProps}" var="p">
      <td><theme:property bean="${b}" name="${p.name}" readonly="true" /></td>
     </g:each>
     <td class="tc"><g:link action="show" controller="clientRecord" id="${beanInstance.id}"
       params="${[associated:property.name,propertyId:b.id]}">Show</g:link></td>
    </tr>
   </g:each>
  </tbody>
 </table>
</g:if>
<g:else>
 <i>No ${property.name} for this ${domainClass.naturalName}</i>
</g:else>


