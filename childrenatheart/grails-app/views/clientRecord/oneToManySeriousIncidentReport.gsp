<g:set var="thevalue" value="${value?.findAll{it}}"/>
<g:if test="${thevalue}">
<g:set var="sortedValue" value="${(thevalue.sort{ it.date })[0..Math.min(thevalue.size()-1,5)]}"/>
<g:set var="propLen" value="${Math.min(summaryProperties.size()-1,6)}"/>
<g:set var="printSummaryProps" value="${summaryProperties[0..propLen]}"/>
<table class="compact">
    <thead>
    <tr>
        <g:each in="${printSummaryProps}" var="p">
            <th>
                <bean:label bean="${property.referencedDomainClass?.newInstance()}" property="${p}"/>
            </th>
        </g:each>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    <g:each in="${sortedValue}" status="i" var="bean">
        <tr>
            <g:each in="${printSummaryProps}" var="p">
                <td>
                    <theme:property bean="${bean}" beanPathPrefix="${beanPathPrefix}" name="${p.name}" readonly="${true}"/>
                </td>
            </g:each>
            <td class="tc">
                <g:link action="show" controller="clientRecord" id="${params.id}"
                        params="${[associated:'seriousIncidentReport',propertyId:bean.id]}">Show
                </g:link>
            </td>
        </tr>
    </g:each>
    </tbody>
</table>
</g:if>
<g:else>
<i>No ${property.name} for this ${domainClass.naturalName}</i>
</g:else>


