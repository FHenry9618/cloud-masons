<g:set var="thevalue" value="${value?.findAll{it}}"/>
<g:if test="${thevalue}">
<table class="compact">
    <thead>
    <tr>
        <th>Type</th>
        <th>Description</th>
        <th>Actions</th>
    </tr>
    </thead>

    <tbody>
    <g:each in="${thevalue.size() > 3 ? thevalue[0..2] : thevalue}" status="i" var="f">
        <tr>
            <td>${f.type}</td>
            <td>${f.description}</td>
            <td>
                <g:link controller="secureFile" action="download" id="${f.id}"
                        params="[beanClass: property.referencedDomainClass.name]">[Download]
                </g:link>
            </td>
        </tr>
    </g:each>
    </tbody>
</table>
</g:if>
<g:else>
<i>No files are attached to this ${domainClass.naturalName}</i><br/>
</g:else>
<br/>

<security:renderIfAuthorized domainClass="${bean.getClass()}" action='create' bean="${bean}">
<a class="button" href="${g.createLink(controller: 'clientRecord', action: 'uploadDocument', id:beanInstance?.id)}">Upload
    Document</a>
</security:renderIfAuthorized>

<div>
<script>
    $(function() {
       //$( ".button" ).button();
    });


</script>
</div>
    