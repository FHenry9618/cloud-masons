<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>
    <title>Document Upload</title>
</head>
<body>
<div id="container">

    <div class="inner-container">
        <g:if test="${flash.message}">
            <div class="msg msg-ok">${flash.message}</div>
        </g:if>


        <div class="box box-75 altbox">
            <div class="boxin">
                <div class="header">
                    <h3>Upload Document</h3>
                </div>
                <div class="content">
                    <fieldset>
                        <g:form action="saveDocument" method="post" class="basic editForm"
                                enctype="multipart/form-data">
                            <g:hiddenField name='id' value="${params.id}"/>
                            <table>
                                <tr>
                                    <td align="right" style="vertical-align: top;">
                                        <label for="name">Document Description:</label>
                                    </td>
                                    <td>
                                        <g:textArea name='description'>${document?.description}</g:textArea>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" style="vertical-align: top;">
                                        <label for="name">Document Type:</label>
                                    </td>
                                    <td>
                                        <g:select name="type" from="${typeConstraint?.inList?.sort()}"
                                                  value="${document?.type}" noSelection="['Other': 'Other']"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" style="vertical-align: top;">
                                        <label for="image">Document to be Uploaded:</label>
                                    </td>
                                    <td><input type="file" name="document" id="document"/></td>
                                </tr>
                            </table>
                        </g:form>
                    </fieldset>
                </div>
            </div>
        </div>
        <div class="box box-25 altbox">
            <div class="boxin">
                <div class="header">

                    <h3>Summary</h3>
                </div>

                <form>
                    <fieldset>

                        <dl class="compact">

                            <dt>Name</dt>
                            <dd> ${clientRecord?.name} &nbsp;</dd>

                            <dt>Status</dt>
                            <dd> ${clientRecord?.status} &nbsp;</dd>

                        </dl>

                        <br/>

                        Please make your changes and then click <b>Submit</b>.

                        <br/><br/>
                        <div class="sep">
                            <a class="button altbutton submitButton">Submit</a>
                            <a class="button altbutton cancelButton">Cancel</a>
                        </div>
                        <div class="sep"></div>
                        <div class="sep"></div>
                    </fieldset>
                </form>
            </div>
        </div>
  <script type="text/javascript">
 $(document).ready(function() {
      $('#accordion').accordion({
                  autoHeight: false,
                  collapsible: true,
                  active: false
            });
     $('.submitButton')
        .button()
        .click(function() {
            $('.editForm').submit();
        });
     $('.cancelButton')
        .button()
        .click(function() {
            if(confirm('Are you sure you wish to cancel?')) {
            window.location = "/scribe/clientRecord/show/${params.id}";
            }
        });

     $('.money').priceFormat({
        prefix: '$ '
     });
});
</script>

    </div>
</div>
</body>
</html>    