<g:set var="thevalue" value="${value?.findAll{it}}"/>
<g:if test="${thevalue}">
<g:set var="summaryValues" value="${summaryProperties[0..Math.min(summaryProperties.size()-1,5)]}"/>
<table class="compact">
    <thead>
    <tr>
        <th>MC</th>
        <g:each in="${summaryValues}" var="p">
            <th>
                <bean:label bean="${property.referencedDomainClass?.newInstance()}" property="${p}"/>
            </th>
        </g:each>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    <g:each in="${thevalue}" status="i" var="bean">
        <g:if test="${bean.managingConservator}">
            <tr>
                <td>
                    <g:if test="${bean.managingConservator}">
                        <b>*</b>Yes
                    </g:if>
                </td>
                <g:each in="${summaryValues}" var="p">
                    <td>
                        <theme:property bean="${bean}" beanPathPrefix="${beanPathPrefix}" name="${p.name}"
                                        readonly="true"/>
                    </td>
                </g:each>
                <td class="tc">
                    <g:link action="show" controller="clientRecord" id="${params.id}"
                            params="${[associated:'significantPersons',propertyId:bean.id]}">Show
                    </g:link>
                </td>
            </tr>
        </g:if>
    </g:each>
    </tbody>
</table>
</g:if>
<g:else>
<i>No ${property.name} for this ${domainClass.naturalName}</i>
</g:else>
