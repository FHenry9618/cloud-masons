<%@page import="org.tbch.LivingUnit"%>
<%@page import="org.tbch.Campus"%>
<%@page import="org.tbch.Constants"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="layout" content="main" />
<beans:listIncludes />
<title>Client Record List</title>
</head>

<body>

 <div id="filterDialog" title="Client Record Filter">
  <g:form method="get" action="list" name="filterForm">
   <g:hiddenField name="offset" value="0" />
   <g:hiddenField name="max" value="${max}" />
   <fieldset>
    <table>
     <tr>
      <td style="vertical-align:top;"><label for="filter.status">Status:</label></td>
      <td><g:select name="filter.status" from="${Constants.STATUS_STATES}" value="${filter.status}"
        multiple="true" /></td>
     </tr>
     <tr>
      <td><label for="filter.campus">Campus:</label></td>
      <td><g:select name="filter.campus" from="${Campus.list()}" value="${filter.campus}" optionKey="id"
        noSelection="['':'-Choose a Campus-']" /></td>
     </tr>
     <tr>
      <td><label for="filter.livingUnit">Living Unit:</label></td>
      <td><g:select name="filter.livingUnit" from="${LivingUnit.list()}" value="${filter.livingUnit}"
        optionKey="id" noSelection="['':'-Choose a living unit-']" /></td>
    </table>
   </fieldset>
  </g:form>
 </div>
 <div class="box box-100 altbox">
  <div class="boxin">
   <div class="header">
    <h3>Client Records</h3>
    <bean:eachAuthorizedStaticAction beanControllerName="${beanControllerName}" bean="${domainClass.clazz}"
         actions="${actions}" params="${params}">
      <g:if test="${action.action != 'list'}">
       <a class="button altbutton"
        href="${g.createLink(controller: beanControllerName, action: action.action, params: action?.params)}"> ${action.label}
       </a>
      </g:if>
    </bean:eachAuthorizedStaticAction>
    <a id="filterButton" class="button altbutton" href="#">Filter</a>
   </div>
   <div class="content">
    <table>
     <thead>
      <tr>
       <g:sortableColumn class="tc" property="lastName" title="Last Name" params="${params}"/>
       <g:sortableColumn class="tc" property="firstName" title="First Name" params="${params}"/>
       <g:sortableColumn class="tc" property="status" title="Status" params="${params}"/>
       <th class="tc">Actions
      </tr>
     </thead>
     <tbody>
     <g:each in="${results}" var="client">
     <tr>
     <td class="tc"><g:fieldValue bean="${client}" field="lastName" /></td>
     <td class="tc"><g:fieldValue bean="${client}" field="firstName" /></td>
     <td class="tc"><g:fieldValue bean="${client}" field="status" /></td>
     <td class="tc">
         <ul class="actions">
          <bean:eachAuthorizedInstanceAction beanControllerName="${beanControllerName}" bean="${client}"
           actions="${actions}" domainClassName="${domainClass.propertyName}" params="${params}">
           <li>[ <bean:actionLink beanControllerName="${beanControllerName}" bean="${client}" action="${action}" />
            ]
           </li>
          </bean:eachAuthorizedInstanceAction>
         </ul>

        </td>
     </tr>
     </g:each>
     
     </tbody>
    </table>
    <div class="pagination">
     <ul>
      <g:paginate controller="clientRecord" action="list" total="${total}" params="${params}" />
      <!-- 
      <li>&nbsp;</li>
      <li class="bottomRightLink"><a href="#" reportFormat="PDF">PDF</a></li>
      <li class="bottomRightLink"><a href="#" reportFormat="XLS">XLS</a></li>
      <li class="bottomRightLink">Download Report:</li>
       -->
     </ul>
    </div>
   </div>
  </div>
 </div>

 <script>
		$(function() {
			$("#filterDialog").dialog({
				autoOpen : false,
				height : 250,
				width : 505,
				modal : true,
				buttons : {
					"Filter" : function() {
						$(this).dialog("close");
						$('#filterForm').submit();
					},
					Cancel : function() {
						$(this).dialog("close");
					}
				},
				close : function() {
				}
			});
			$("#filterButton").button().click(function() {
				$("#filterDialog").dialog("open");
			});
		});
	</script>
</body>
</html>

