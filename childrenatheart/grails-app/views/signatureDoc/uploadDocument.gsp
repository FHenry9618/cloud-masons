<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title>Signature Page Upload</title>
    </head>
    <body>
    <div id="container">

    <div class="inner-container">
        <g:if test="${flash.message}">
                   <div class="msg msg-ok">${flash.message}</div>
        </g:if>
        
          
                <div class="box box-75 altbox">
                    <div class="boxin">
                        <div class="header">
                            <h3>Upload Signature Page</h3>
                        </div>
                        <div class="content">
                            <fieldset>
                            <g:form action="saveDocument" method="post" class="basic editForm"  enctype="multipart/form-data">
                            <g:hiddenField name='id' value="${beanInstance?.id}" />
                            <g:hiddenField name='beanClass' value="${beanInstance?.getClass().name}" />
                             <table>
                               <tr>
                                    <td align="right" style="vertical-align: top;">
                                          <label for="name">Signature Page Description:</label>
                                    </td>
                                    <td><g:textArea name='description'>${document?.description}</g:textArea></td>
                               </tr>
                               <tr>
                                    <td align="right" style="vertical-align: top;">
                                          <label for="image">Signature Page to be Uploaded:</label>
                                    </td>
                                    <td><input type="file" name="document" id="document" /></td>
                               </tr>
                              </table>
                            </g:form>
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div class="box box-25 altbox">
                    <div class="boxin">
                        <div class="header">

                            <h3>Summary</h3>
                        </div>

                            <form>
                            <fieldset>
                                
                                    <dl class="compact">
                                    
                                      <dt>Name</dt><dd> ${beanInstance?.name} &nbsp;</dd>
                                    </dl>
                                
                                <br/>
                                
                                Please make your changes and then click <b>Submit</b>. 
                                
                                <br/><br/>
                                
                                <div class="sep">
                                    <a class="button altbutton submitButton">Submit</a>

                                    <a class="button altbutton cancelButton">Cancel</a>
                                </div>
                                
                                <div class="sep">
                                  &nbsp;
                                </div>
                                <div class="sep">
                                </div>

                            </fieldset>

                        </form>
                    </div>
                </div>
  <script type="text/javascript">
  
     $(document).ready(function() {

         $('.submitButton')
            .button()
            .click(function() {
                $('.editForm').submit();
            });
         $('.cancelButton')
            .button()
            .click(function() {
                if(confirm('Are you sure you wish to cancel?')) {
                window.location = "/scribe/clientRecord/show/${beanInstance.clientRecord?.id}?associated=${beanPropertyName}&propertyId=${beanInstance.id}";
                }
            });
            

     
    });
  </script>
                
        </div>
    </div>
    </body>
</html>    