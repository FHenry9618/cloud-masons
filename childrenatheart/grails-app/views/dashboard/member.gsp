<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="layout" content="main" />
<title>User Dashboard</title>
</head>
<body>
	<div id="nav">
		<div class="inner-container clearfix">
			Hello <b>${user.name}</b>
		</div>
		<!-- .inner-container -->
		<div class="inner-container clearfix">
			<security:renderIfAuthorized domainClass="${org.tbch.ParentSurvey}"
				action='create'>
				<a class="button altbutton"
					href="${g.createLink(controller: 'parentSurvey', action: 'create')}">Create
					Parent Survey</a>
			</security:renderIfAuthorized>
		</div>
		<!-- .inner-container -->
	</div>
	<!-- #nav -->
	<div id="container">
		<div class="inner-container">
			<g:if test="${flash.message}">
				<div class="msg msg-ok">
					${flash.message}
				</div>
			</g:if>
			<g:include controller="clientRecord" action="list" />
			<script>
				$(function() {
					$(".button").button();
				});
			</script>
		</div>
		<!-- .inner-container -->
	</div>
	<!-- #container -->
</body>
</html>
