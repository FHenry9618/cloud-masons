<g:applyLayout name="global">
<html>
    <head>
<link rel="apple-touch-icon" sizes="57x57" href="/scribe/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/scribe/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/scribe/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/scribe/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/scribe/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/scribe/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/scribe/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/scribe/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/scribe/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="/scribe/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/scribe/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/scribe/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/scribe/favicon-16x16.png">
<link rel="manifest" href="/scribe/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/scribe/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">

      <title><g:layoutTitle/></title>
      <g:layoutHead />
      
      <style type="text/css">
.box .content table tr.even th {background: #ffffff; vertical-align: top;}
.box .content table tr.even td {background: #ffffff;}

</style>
    </head>

    <body id="login">

        <div class="box box-75 altbox">
            <div id="login-panel" class="boxin">
                <div class="header">
                    <h3><tbch:organizationName/></h3>
                </div>
                <div id="login-form" class="content">                
                    <table style="width: 85%; ">
                      <tr>
                        <td style="background: #FFFFFF; vertical-align: middle;">
                          <img src="${resource(plugin: 'cornerstone', dir:'images/login',file:'login.png')}" ></img>
                        </td>
                        <td style="background: #FFFFFF; vertical-align: top;">
        
                            <form class="table" action="${postUrl}" method="post" id='loginForm'><!-- Default forms (table layout) -->

                                <div class="inner-form">
        
                                  <g:pageProperty name="page.loginForm"/>
                                </div>
                            </form>
                        </td>
                      </tr>
                    </table>

                </div>
                <div id="reset-form" class="content"> 
                    <table style="width: 85%; ">
                      <tr>
                        <td style="background: #FFFFFF; vertical-align: middle;">
                          <img src="${resource(plugin: 'cornerstone', dir:'images/login',file:'password.png')}" ></img>
                        </td>
                        <td style="background: #FFFFFF; vertical-align: top;">               
                            <form class="table" controller="login" action="startPasswordReset"  method="post" id='loginForm'><!-- Default forms (table layout) -->
                                <div class="inner-form">
                                 <g:pageProperty name="page.resetForm"/>
                                </div>
                            </form>
                        </td>
                      </tr>
                    </table>
                </div>

<!--                <div id="register-form" class="content">                -->
<!--                    <table style="width: 85%; ">-->
<!--                      <tr>-->
<!--                        <td style="background: #FFFFFF; vertical-align: middle;">-->
<!--                          <img src="${resource(plugin: 'cornerstone', dir:'images/login',file:'register.png')}" ></img>-->
<!--                        </td>-->
<!--                        <td style="background: #FFFFFF; vertical-align: top;">-->
<!--                          <form class="table" controller="login" action="startRegistration" method="post" id='registerForm'> Default forms (table layout) -->
<!--                            <div class="inner-form">-->
<!--                               <g:pageProperty name="page.registerForm"/>-->
<!--                            </div>-->
<!--                          </form>-->
<!--                        </td>-->
<!--                      </tr>-->
<!--                    </table>-->
<!--                </div>-->

            </div>
        </div>
 <script type="text/javascript">
      var focusInput = function() {
            document.getElementById("j_username").focus();
      }
      $(document).ready(function() {
        $('#reset-form').hide(); // hide content related to inactive tab by default
        
        $('#register-form').hide(); // hide content related to inactive tab by default
        $('#login-panel .header ul a').click(function(){
            //$('#login-panel .header ul a').removeClass('active');
            //$(this).addClass('active'); // make clicked tab active
            //$('#login-panel .content').hide(); // hide all content
            //$('#login-panel').find('#' + $(this).attr('rel')).show(); // and show content related to clicked tab
            showTab($(this).attr('rel'));
            return false;
        });
        $('#show-login-form').click( function() {
            showTab('login-form');
        });
        $('#show-reset-form').click( function() {
            showTab('reset-form');
        });
        $('#show-register-form').click( function() {
            showTab('register-form');
        });
        $('#show-register-form2').click( function() {
            showTab('register-form');
        });

     });
     
    function showTab(tabName) {
            $('#login-panel .header ul a').removeClass('active');
            $('#'+tabName+'-tab').addClass('active'); // make clicked tab active
            $('#login-panel .content').hide(); // hide all content
            $('#login-panel').find('#' + tabName).show(); // and show content related to clicked tab
    }     
 </script>
    </body>
</html>
</g:applyLayout>
