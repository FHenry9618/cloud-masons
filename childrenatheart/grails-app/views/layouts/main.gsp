<g:applyLayout name="global">
 <head>
<title><g:layoutTitle /></title>

<link rel="apple-touch-icon" sizes="57x57" href="/scribe/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/scribe/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/scribe/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/scribe/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/scribe/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/scribe/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/scribe/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/scribe/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/scribe/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="/scribe/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/scribe/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/scribe/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/scribe/favicon-16x16.png">
<link rel="manifest" href="/scribe/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/scribe/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">

<g:layoutHead />
<link rel="stylesheet" type="text/css" href="${resource(dir:'css',file:'overrides.css')}" media="screen" />
<link rel="stylesheet" type="text/css" href="${resource(dir:'css',file:'print.css')}" media="print" />
<!-- Change name of the stylesheet to change colors (blue/red/black/green/brown/orange/purple) -->
 </head>
 <body>
  <div id="header">
   <div class="inner-container clearfix">
    <h1 id="logo">
     <g:link controller="dashboard" action="index">
      <span class="ir"><tbch:logo/></span>
     </g:link>
    </h1>
    <g:isLoggedIn>
     <div id="userbox" class="userbox-right">
      <a id="logout" title="Click to logout" href="${createLink(controller:'logout')}"> <span class="ir"></span> <span
       class="ar">log out</span>
      </a>
     </div>

     <div id="userbox" class="userbox-left">
      <g:link controller="user" action="settings">
       <strong><g:loggedInUserInfo field="name" /></strong>
       <ul class="clearfix">
        <li>settings</li>
       </ul>
      </g:link>
     </div>
    </g:isLoggedIn>
   </div>
  </div>
  <!-- #header -->
  <div id="nav">
   <div class="inner-container clearfix">
    <div id="h-wrap">
     <div class="inner">
      <h2>
       <nav:currentItem group="*" var="item" altTitle="${g.layoutTitle()}">
        <span class="h-ico ico-${item.icon}"><span> ${item.title}
        </span></span>
       </nav:currentItem>
       <span class="h-arrow"></span>
      </h2>
      <ul class="clearfix">

       <nav:eachItem group="tabs" var="item">
        <li><a class="h-ico ico-${item.icon}" href="${item.link}"><span> ${item.title}
         </span></a></li>
       </nav:eachItem>
       <nav:eachItem group="admintabs" var="item">
        <li><a class="h-ico ico-${item.icon}" href="${item.link}"><span> ${item.title}
         </span></a></li>
       </nav:eachItem>

      </ul>
     </div>
    </div>
    <!-- #h-wrap -->
    <g:form controller="dashboard" action="search" method="get">
     <!-- Search form -->
     <fieldset>
      <label class="a-hidden" for="q">Search query:</label> <input id="q" class="text fl search" type="text" name="q"
       size="20" value="Search..." style="height: 24px" /> <input class="hand fr" type="image"
       src="${resource(dir:'images',file:'search-button.png')}" alt="Search" />
     </fieldset>
    </g:form>
   </div>
   <!-- .inner-container -->
  </div>
  <!-- #nav -->


  <div id="container">
   <div class="inner-container">
    <g:layoutBody />

    <div id="footer">
     <!-- footer, maybe you do not need it -->
     <p>
      Maintained by NSquard Technologies
     </p>
    </div>

   </div>
   <!-- .inner-container -->
  </div>
  <!-- #container -->

 <script type="text/javascript">
$(document).ready(function() {
	$('.search').focus(function() {
		//alert($(this).val());
		if ($(this).val() == "Search...") {
			$(this).val("");
		}
	});
});
</script>
 </body>
</g:applyLayout>
