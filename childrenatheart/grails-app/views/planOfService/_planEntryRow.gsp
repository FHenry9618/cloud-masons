<g:set var="sp" value="${planEntry.servicePlanEntry}" />
<g:set var="rowId" value="planEntry-${planEntry.id}" />
<g:set var="uuid" value="${UUID.randomUUID()}" />
<tr id="${rowId}">
 <td><g:formatDate date="${sp.date}" /></td>
 <td><tbch:shortForm value="${sp.goals}" /></td>
 <td><g:formatDate date="${sp.targetDate}" /></td>
 <td>
   <security:renderIfAuthorized domainClass="${planEntry.getClass()}" action='edit' bean="${planEntry}">
     <g:set var="getUrl" value="${g.createLink(controller:'planOfService',action:'getServicePlanEntryJSON', id:sp.id)}" />
     <a onclick="javascript: editPlanEntry('#${property.name}', '${getUrl}', '${rowId}', '${sp.id}');" href="#${property.name}">[Edit]</a>
   </security:renderIfAuthorized>

   <security:renderIfAuthorized domainClass="${planEntry.getClass()}" action='delete' bean="${planEntry}">
     <g:set var="deleteUrl" value="${g.createLink(controller:'planOfService',action:'deletePlanEntry', id:planEntry.id)}" />
     <a onclick="javascript: deletePlanEntry('${deleteUrl}', '${rowId}');" href="#${property.name}"> [Delete]</a>
   </security:renderIfAuthorized>

   <div id="dialog-${uuid}" title="Show">
    <theme:propertyGroups bean="${sp}" readonly="${true}" />
   </div>
   <a onclick="javascript: $('#dialog-${uuid}').dialog('open');" href="#${property.name}"> [View]</a> 

   <script type="text/javascript">
   (function() {
     // setup all view dialogs
      $('#dialog-${uuid}').dialog({
         height : 280,
         width : 620,
         modal : true,
         autoOpen : false
      });
   })();
   </script>
  </td>
</tr>
