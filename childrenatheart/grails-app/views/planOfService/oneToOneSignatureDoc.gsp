<g:if test="${value != null}">
 <g:link controller='signatureDoc' action='download' id="${value?.id}">View</g:link>
</g:if>
<g:else>
 <i>none</i>
 <br />
 <br/>
 <p>
 <security:renderIfAuthorized domainClass="${beanInstance.getClass()}" action='edit' bean="${beanInstance}">
  <a class="button"
   href="${g.createLink(controller: 'signatureDoc', action: 'uploadDocument', id:bean?.id, params:[beanClass:domainClass.clazz.name])}">Upload
   Signature Page</a>
 </security:renderIfAuthorized>
 </p>
</g:else>

