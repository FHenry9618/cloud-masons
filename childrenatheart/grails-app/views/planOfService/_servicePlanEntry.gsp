<div id="servicePlanEntry-dialog" title="Add Entry">
 <p id="servicePlanEntry-failed" style="visibility: hidden;">
  <span color="red"> *</span>Failed to submit to server please try again.
 </p>
 <g:form name="servicePlanEntry-form" controller="planOfService" action="saveOrUpdateServicePlanEntry" id="${bean.id}">
  <g:hiddenField name="servicePlanEntry-rowId" value="" />
  <g:hiddenField name="servicePlanEntry-property" value="" />
  <g:hiddenField name="servicePlanEntry-theServicePlanEntryId" value="" />
  <fieldset>
   <table>
    <tr>
     <td style="vertical-align: top;"><label for="servicePlanEntry-date">Date:</label></td>
     <td style="white-space: nowrap;"><input class="date txt" type="text" name="servicePlanEntry-date"
      id="servicePlanEntry-date" value="" /> <font color="red"> *</font></td>
    </tr>
    <tr>
     <td style="vertical-align: top;"><label for="servicePlanEntry-goals">Goals:</label></td>
     <td style="white-space: nowrap;"><textarea id="servicePlanEntry-goals" name="servicePlanEntry-goals" cols="60"
       rows="5"></textarea><span color="red"> *</span></td>
    </tr>
    <tr>
     <td style="vertical-align: top;"><label for="servicePlanEntry-strategies">Strategies:</label></td>
     <td style="white-space: nowrap;"><textarea id="servicePlanEntry-strategies" name="servicePlanEntry-strategies"
       cols="60" rows="5"></textarea></td>
    </tr>
    <tr>
     <td style="vertical-align: top;"><label for="servicePlanEntry-responsibleParty">Responsible Party:</label></td>
     <td style="white-space: nowrap;"><input class="txt" type="text" id="servicePlanEntry-responsibleParty"
      name="servicePlanEntry-responsibleParty" size="30" value="" /></td>
    </tr>
    <tr>
     <td style="vertical-align: top;"><label for="servicePlanEntry-targetDate">Target Date:</label></td>
     <td style="white-space: nowrap;"><input class="date txt" type="text" name="servicePlanEntry-targetDate"
      id="servicePlanEntry-targetDate" value="" /></td>
    </tr>
    <tr>
     <td style="vertical-align: top;"><label for="servicePlanEntry-scale">Scale:</label></td>
     <td style="white-space: nowrap;"><select name="servicePlanEntry-scale" id="servicePlanEntry-scale">
       <option value="-2">-2</option>
       <option value="-1">-1</option>
       <option value="0" selected="selected">0</option>
       <option value="1">1</option>
       <option value="2">2</option>
       <option value="3">3</option>
     </select></td>
    </tr>
    <tr>
     <td style="vertical-align: top;"><label for="servicePlanEntry-dateResolved">Date Resolved:</label></td>
     <td style="white-space: nowrap;"><input class="date txt" type="text" name="servicePlanEntry-dateResolved"
      id="servicePlanEntry-dateResolved" value="" /></td>
    </tr>
   </table>
  </fieldset>
 </g:form>
</div>
<script type="text/javascript">
    function deletePlanEntry(url, rowId) {
    	if (confirm('Are you sure you want to delete?')) { 
          $.post(url, function(data) { 
              // remove the row
              $('#' + rowId).remove();
          });
        } else {
            return false;
        }
    }

    function editPlanEntry(prop, url, rowId, speId) {
        $('#servicePlanEntry-property').val(prop);
        $.getJSON(url, function(data) {
            $('#servicePlanEntry-rowId').val(rowId);
            $('#servicePlanEntry-theServicePlanEntryId').val(speId);

        	// set all the string fields
            $('#servicePlanEntry-goals').val(data.goals);
            $('#servicePlanEntry-strategies').val(data.strategies);
            $('#servicePlanEntry-responsibleParty').val(data.responsibleParty);
         
        	// set all the date fields
            $('#servicePlanEntry-date').val(data.date);
            $('#servicePlanEntry-targetDate').val(data.targetDate);
            $('#servicePlanEntry-dateResolved').val(data.dateResolved);

            // set all the other fields
            $('#servicePlanEntry-scale').val(data.scale);

            // assign all the data to the form and show it
            $("#servicePlanEntry-dialog").dialog("open");
         });
    }

	$(document).ready(function() {
		// add callback to 'Add Entry' buttons
		$(".addServicePlanEntry").click(function() {
			// set the hidden field for property name
            $('#servicePlanEntry-rowId').val("");
			$('#servicePlanEntry-property').val($(this).attr('href'));
			$("#servicePlanEntry-dialog").dialog("open");
		});

		var updateRowData = function(data) {
			// remove the no entries label..
			var propertyName = $('#servicePlanEntry-property').val();
			propertyName = propertyName.substring(1, propertyName.length);
            $('#noentries-' + propertyName).remove();

            // remove the previous row..
            var rowId = $('#servicePlanEntry-rowId').val();
            $('#' + rowId).remove();
            
            // prepend the new row..
			$('#table-' + propertyName).prepend(data);
		}

		// add dialog 
		$("#servicePlanEntry-dialog").dialog({
			autoOpen : false,
			height : 480,
			width : 620,
			modal : true,
			buttons : {
				"Submit" : function() {
					// submit the data..
					var form = $('#servicePlanEntry-form');
					$.post(form.attr('action'), form.serialize(), updateRowData);
					// on success close
					$(this).dialog("close");
				},
				Cancel : function() {
					$(this).dialog("close");
				}
			},
			close : function() {
                // clear hidden fields
                $('#servicePlanEntry-theServicePlanEntryId').val("");

				// clear all the text fields
				$('#servicePlanEntry-goals').val("");
				$('#servicePlanEntry-strategies').val("");
				$('#servicePlanEntry-responsibleParty').val("");

				// clear all the date fields
				$('#servicePlanEntry-date').val("");
				$('#servicePlanEntry-targetDate').val("");
				$('#servicePlanEntry-dateResolved').val("");

				// clear the select box
				$('#servicePlanEntry-scale').val("0");

			}
		});
	});
</script>
