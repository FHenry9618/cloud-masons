<table class="compact">
  <thead>
    <tr>
      <th>Date Added</th>
      <th>Goals</th>
      <th>Target Date</th>
      <th>Actions</th>
    </tr>  
  </thead>
  <tbody id="table-${property.name}">
   <g:set var="thevalue" value="${value?.findAll{it}}"/>
   <g:if test="${thevalue}">
    <g:each in="${thevalue.sort{ it.servicePlanEntry.date }.reverse()}" var="planEntry">
      <g:render template="/planOfService/planEntryRow" 
       model="${['planEntry':planEntry, 'beanInstance':beanInstance, 'property':property]}" />
    </g:each>
    </g:if>
    <g:else>
      <tr id="noentries-${property.name}"><td colspan="4"><i>No entries are added to this ${domainClass.naturalName}</i></td></tr>
    </g:else>
   </tbody>
</table>
<br/>
<security:renderIfAuthorized domainClass="${it.getClass()}" action='edit' bean="${it}">
  <a class="button addServicePlanEntry" href="#${property.name}">Add Entry</a>
</security:renderIfAuthorized>


    