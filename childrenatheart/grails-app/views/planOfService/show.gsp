<%@ page import="grails.util.GrailsNameUtils" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="layout" content="main" />
<g:set var="entityName"
 value="${message(code: "${domainClass.propertyName}.label", default: GrailsNameUtils.getNaturalName(domainClass.shortName))}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
 <span> <theme:message messageObj="${flash}" /> <g:hasErrors bean="${bean}">
   <div class="errors">
    <g:renderErrors bean="${bean}" as="list" />
   </div>
  </g:hasErrors>
  <g:set var="label" value="${label ?: "${domainNaturalName}: ${bean.name}"}"/>
  <g:if test="${path}">
   <div class="path">
    <theme:renderPath name="${domainNaturalName}" path="${path}" />
   </div>
   <br />
  </g:if> 
   <security:renderIfAuthorized domainClass="${bean.getClass()}" action='print' bean="${bean}">
    <div class='print-button'>
     <g:pdfLink 
      class='button print-button' 
      pdfController="planOfService" 
      pdfAction="print"
      pdfId="${bean.id}"
      filename="${GrailsNameUtils.getPropertyName(bean.name?.replaceAll('[^\\p{L}\\p{Nd}]+', ''))}">PDF Version</g:pdfLink>
    </div>
   </security:renderIfAuthorized>
  <g:each in="${propertyGroups.keySet()}" status="idx" var="${group}">
   <div class="box box-50 altbox">
    <div class="boxin">
     <div class="header">
      <h3>
       ${group}
      </h3>
      <theme:renderOption bean="${bean}" name="${group}" props="${propertyGroups[group]}" />
     </div>
     <div class="content">
      <theme:propertyGroup readonly="true" bean="${bean}" name="${group}" props="${propertyGroups[group]}" />
     </div>
    </div>
   </div>
  </g:each>
 </span>
 <!-- For each of the service plan entries areas create a dialog -->
 <g:render template="/planOfService/servicePlanEntry"/>
</body>
</html>
