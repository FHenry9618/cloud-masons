<g:set var="uuid" value="${UUID.randomUUID().toString()}" />
<table>
 <thead>
  <tr>
   <th><%= title %></th>
   <th>Count</th>
   <th>Percentage</th>
  </tr>
 </thead>
 <tbody>
  <!-- Calculate the total number of values -->
  <g:set var="totalCount" value="${data[0].sum(0) { it.total }}" />
  <g:each var="item" in="${data[0]}">
   <g:set var="pct" value="${item.total/(totalCount ?:1)}" />
   <tr>
    <td><%= item.name %></td>
    <td style="text-align: right"><g:formatNumber number="${item.total}" /></td>
    <td style="text-align: right"><g:formatNumber type="percent" number="${pct}" /></td>
   </tr>
  </g:each>
 </tbody>
 <tfoot>
  <tr>
   <th>Total Count:</th>
   <th style="text-align: right"><g:formatNumber number="${totalCount}" /></th>
   <th style="text-align: right"><g:if test="${data[1].size()}">
     <a id="button-${uuid}" class="button">Missing</a>
    </g:if></th>
  </tr>
 </tfoot>
</table>
<g:if test="${!params?.printPDF}">

 <g:if test="${data[1].size()}">
  <div id="dialog-${uuid}" title="Missing Information">
   <g:each in="${data[1].sort { it.name} }" var="client" status="idx">
    <g:if test="${idx}">, </g:if>
    <g:link controller="clientRecord" action="show" id="${client.id}">
     <%= client.name %></g:link>
   </g:each>
  </div>
 </g:if>
 <script type="text/javascript">
	$(function() {
        var uuid = "<%= uuid %>";
			$("#dialog-" + uuid).dialog({
				modal : true,
				autoOpen : false,
				buttons : {
					Ok : function() {
						$(this).dialog("close");
					}
				}
			});
			$("#button-" + uuid).button().click(function() {
				$("#dialog-" + uuid).dialog("open");
			});
		});
	</script>
</g:if>