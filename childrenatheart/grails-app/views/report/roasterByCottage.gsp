<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Campus Roster</title>
<g:if test="${!params?.printPDF}">
 <meta name="layout" content="main" />
</g:if>
<g:else>
 <link rel="stylesheet" type="text/css" href="${resource(dir:'css',file:'print.css')}" media="print" />
</g:else>
</head>
<body>
 <b>Total in care ${summaryTotalInCare}</b>

 <g:if test="${!params?.printPDF}">
  <div class="print-button">
   <g:link class="button print-button" action="printRoasterByCottage" params="${params}">PDF Version</g:link>
  </div>
 </g:if>
 <g:each var="livingUnit" in="${livingUnits}">
  <g:render template="livingUnit" bean="${livingUnit}" />
 </g:each>

</body>
</html>
