<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Serious Incident Reports</title>
    <g:if test="${!params?.printPDF}">
        <meta name="layout" content="main"/>
    </g:if>
    <g:else>
        <link rel="stylesheet" type="text/css" href="${resource(dir: 'css', file: 'print.css')}" media="print"/>
    </g:else>
</head>

<body>
<g:if test="${!params?.printPDF}">
    <div class="print-button">
        <g:link class="button print-button" action="printSeriousIncidentReports" params="${params}">
            PDF Version
        </g:link>
    </div>
</g:if>


<div class="box box-100 altbox">
    <div class="boxin">
        <div class="header">
            <h3>Serious Incident Reports</h3>
        </div>

        <g:if test="${instances}">
            <div class="content">
                <table>
                    <tbody>
                    <g:set var="lastClientRecord" value="${null}"/>
                    <g:each in="${instances}" var="seriousIncidentReport" status="idx">
                        <g:if test="${seriousIncidentReport.clientRecord != lastClientRecord}">
                            <g:set var="lastClientRecord" value="${seriousIncidentReport.clientRecord}"/>
                            <%-- Write out the client record header --%>
                            <tr>
                                <td colspan="2"><strong><%=seriousIncidentReport.clientRecord.name%></strong></td>
                            </tr>
                        </g:if>
                        <%-- Write out the serious incident report --%>
                        <tr>
                            <td><g:fieldValue bean="${seriousIncidentReport}" field="date"/></td>
                            <td>
                                <g:link controller="clientRecord"
                                        action="show"
                                        id="${seriousIncidentReport.clientRecord?.id}"
                                        params="[associated: 'seriousIncidentReport', propertyId: seriousIncidentReport?.id]">
                                    Show
                                </g:link>
                            </td>
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
        </g:if>
        <g:else>
            No record found.
        </g:else>
    </div>
</div>

</body>
</html>
