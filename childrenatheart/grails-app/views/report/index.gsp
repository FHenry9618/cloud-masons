<%@page import="org.tbch.Campus"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="layout" content="main" />
<title>Reports</title>
</head>
<body>
 <div class="box box-75 altbox">
  <div class="boxin">
   <g:if test="${flash.message}">
    <div class="msg msg-ok">
     ${flash.message}
    </div>
   </g:if>
   <div class="header">
    <h3>Reports</h3>
   </div>
   <div class="content">
    <fieldset>
     <g:form action="show" class="basic editForm" method="get">
      <g:form>
       <table>
        <tr>
         <td width="20%">Name:</td>
         <td><g:select name="id" noSelection="${['null':'Select One...']}" from='${reports}' optionKey="id"
           optionValue="name"></g:select></td>
        </tr>
        <tr>
         <td>Campus:</td>
         <td><g:select name="campusId" noSelection="${['null':'Select One...']}" from="${Campus.list()}"
           optionKey="id" optionValue="name"></g:select></td>
        </tr>
        <tr>
         <td width="20%">Start Date:</td>
         <td><input type="text" name="startDate" id="startDate"></td>
        </tr>
        <tr>
         <td width="20%">End Date:</td>
         <td><input type="text" name="endDate" id="endDate"></td>
        </tr>
       </table>
      </g:form>
     </g:form>
    </fieldset>
   </div>
  </div>
 </div>
 <div class="box box-25 altbox">
  <div class="boxin">
   <div class="header">
    <h3>Summary</h3>
   </div>
   <form>
    <fieldset>
     Please press <b>Submit</b> after selecting a report and entering the dates. <br /> <br /> <a
      class="button altbutton submitButton">Submit</a>
    </fieldset>
   </form>
  </div>
 </div>
 <script type="text/javascript">
		$(function() {
			$("#endDate").datepicker({
				changeMonth : true,
				changeYear : true
			});
			$("#startDate").datepicker({
				changeMonth : true,
				changeYear : true
			});
			$('.submitButton').button().click(function() {
				// validate the parameters
                // FIXME: Add validation here?
				$('.editForm').submit();
			});
		});
	</script>
</body>
</html>
</html>
