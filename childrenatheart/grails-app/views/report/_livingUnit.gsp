<div class="box box-100 altbox">
 <div class="boxin">
  <div class="header">
   <h3>
    ${it.name}
   </h3>
  </div>
  <div class="content">
   <table>
    <thead>
     <!-- universal table heading -->
     <tr>
      <th class="first">Last Name</th>
      <th>First Name</th>
      <th>DOB</th>
      <th>Age</th>
      <th>Grade</th>
      <th>DOA</th>
      <th>MC</th>
      <th>LOC</th>
     </tr>
    </thead>
    <tfoot>
     <!-- table foot - what to do with selected items -->
    </tfoot>
    <tbody>
     <g:each status="i" in="${it.clients}" var="client">
      <tr>
       <g:render template="client" model="['client':client]" />
      </tr>
     </g:each>
    </tbody>
    <tfoot>
     <tr>
      <th colspan="3">Total In Care: ${it.totalInCare}</th>
      <th colspan="3">Average Age: <g:formatNumber number="${it.avgAge}" type="number" maxFractionDigits="2" /></th>
      <th colspan="3">Average Days Of Care: <g:formatNumber number="${it.avgDaysInCare}" type="number"
        maxFractionDigits="2" /></th>
     </tr>
    </tfoot>
   </table>
  </div>
 </div>
</div>