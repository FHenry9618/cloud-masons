<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="layout" content="main" />
<title>Campus Roster</title>
<g:if test="${!params?.printPDF}">
 <meta name="layout" content="main" />
</g:if>
<g:else>
 <link rel="stylesheet" type="text/css" href="${resource(dir:'css',file:'print.css')}" media="print"></link>
</g:else>
<link rel="stylesheet" type="text/css" href="${resource(dir:'css',file:'grids-min.css')}"></link>
<style>
.yui3-g .content {
	top: 10px;
	margin-right: 10px; /* "column" gutters */
	padding: 1em;
}
</style>

</head>
<body>
 <g:if test="${!params?.printPDF}">
  <div class="print-button">
   <g:link class="button print-button" action="printStatus" params="${params}">PDF Version</g:link>
  </div>
 </g:if>
 <div id="clientRecord" class="box box-100 altbox">
  <div class="boxin">
   <div class="header">
    <h3>A Client Record List</h3>
   </div>
   <div class="yui3-g" id="demo">
    <div class="yui3-u-1-3">
     <div class="content">
      <g:render template="baseTable" model="${[title:'School', data:grade]}"/>
      <br/>
      <g:render template="baseTable" model="${[title:'Sex', data:gender]}"/>
      <br />
      <g:render template="baseTable" model="${[title:'Discharge Reason', data:discharge]}"/>
     </div>
    </div>
    <div class="yui3-u-1-3">
     <div class="content">
      <g:render template="baseTable" model="${[title:'Ethnic Background', data:race]}"/>
      <br/>
      <g:render template="baseTable" model="${[title:'Medication Status', data:med]}"/>
      <br />
      <g:render template="baseTable" model="${[title:'Managing Conservator', data:mc]}"/>
     </div>
    </div>
    <div class="yui3-u-1-3">
     <div class="content">
      <g:render template="baseTable" model="${[title:'Parent\'s Status', data:parents]}"/>
      <br/>
      <g:render template="baseTable" model="${[title:'Educational Placement', data:edu]}"/>
      <br/>
      <g:render template="baseTable" model="${[title:'Adopted', data:adopted]}"/>
     </div>
    </div>
   </div>
  </div>
 </div>
</body>
</html>
