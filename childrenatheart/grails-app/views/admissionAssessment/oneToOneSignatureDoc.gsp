<g:if test="${value != null}">
        <g:link controller='signatureDoc' action='download' id="${value?.id}">View</g:link>

</g:if>
<g:else>
<i>none</i>
<br/>
<br/>
<security:renderIfAuthorized domainClass="${beanInstance.getClass()}" action='edit' bean="${beanInstance}">
    <a class="button" href="${g.createLink(controller: 'signatureDoc', action: 'uploadDocument', params:[id:beanInstance?.id, beanClass:beanInstance?.getClass()?.name])}">Upload Signatgure Page</a>
</security:renderIfAuthorized >

</g:else>

