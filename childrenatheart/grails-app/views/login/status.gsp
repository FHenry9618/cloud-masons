<html>
    <head>
        <title><tbch:organizationName/> Scribe - Login</title>
        <meta name="layout" content="global" />
    </head>
     <script type="text/javascript">
        var focusInput = function() {
            document.getElementById("j_username").focus();
        }
    </script>
    <body id="login">

        <div class="box box-50 altbox">
            <div id="login-panel" class="boxin">
                <div class="header">
                    <h3><tbch:organizationName/> Scribe</h3>
                </div>
                <div id="login-form" class="content">                
                    <form class="table" action="${postUrl}" method="post" id='loginForm'><!-- Default forms (table layout) -->
                        <div class="inner-form">
                            <div class="msg msg-info">
                                <g:if test='${flash.message}'>
                                    <p>${flash.message}</p>
                                </g:if>
                                <g:else>
                                     <p>Please check your email and click on the link provided to complete the process.</p>
                                </g:else>
                            </div>
                         </div>
                    </form>
                </div>
 
            </div>
        </div>
     </body>
</html>
