
    <theme:panel width="100">
    
       <div class="header">
         <h3>${memberStore.name}</h3>
         <%--
          <g:link controller="memberStore" absolute="true" action="edit" id="${memberStore.id}" class="button altbutton">Edit Store Information</g:link>
          --%>
       </div>
       <div class="content">
         
                                    <div class="grid"><!-- grid view -->
                                        <div class="line">

                    <g:each in="${memberStore.accounts}" status="i" var="account">

                                            <div class="item">
                                                <div class="inner">
                                                    <div class="data">
                                                        <h4>${account.wholesaler} Account: ${account.name}</h4>
                                                         <dl class="compact">
                                                             <dt>GCR</dt><dd> ${account.currentMonthGcr * 100}% &nbsp;</dd>
                                                             <dt>Rebate</dt><dd> ${account.currentMonthRebate * 100}% &nbsp;</dd>

                                                         </dl>
                                                        <p></p>
                                                        <p><tbch:complianceLevels account="${account}" /> </p>

                                                    </div>
                                                </div>
                                            </div>
                        
                    </g:each>
                                        </div>

                                    </div>

       </div>
    </theme:panel>
 
  
