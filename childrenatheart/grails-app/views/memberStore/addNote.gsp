<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title>Add Note to Member Store</title>
    </head>
    <body>      

    <theme:panel width="75">

            <div class="header">
                <h3>Add Note to ${memberStore.name}</h3>
            </div>
                    <g:if test="${flash.message}">
                <div class="msg msg-ok">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${bean}">
                <div class="msg msg-error">
                    <g:renderErrors bean="${bean}" as="list" />
                </div>
            </g:hasErrors>
            <g:form class="createForm fields" controller="memberStore" action="update" method="post" ${multiPart ? ' enctype="multipart/form-data"' : ''} >
               <g:hiddenField name="id" value="${memberStore.id}" />
               <g:hiddenField name="version" value="${memberStore.version}" />
               <fieldset>
               <theme:propertyGroup bean="${memberStore}"  name="notes" title="Notes" props="${['notes']}" />
               </fieldset>

               <input class="button" type="submit" value="Add Note" />
               
           </g:form>

        </theme:panel>
    </body>
</html>