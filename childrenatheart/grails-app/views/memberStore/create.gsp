
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: "${domainClass.propertyName}.label", default: grails.util.GrailsNameUtils.getNaturalName(domainClass.shortName))}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>      
      <span>
        <theme:create bean="${beanInstance}" beanControllerName="${beanControllerName}"/>
      </span> 
    </body>
</html>
