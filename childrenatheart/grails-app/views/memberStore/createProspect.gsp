<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title>Add New <tbch:organizationName/> Scribe Prospect</title>
    </head>
    <body>      


    <theme:panel width="75">

            <div class="header">
                <h3>Add New Prospect</h3>
            </div>
                    <g:if test="${flash.message}">
                <div class="msg msg-ok">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${bean}">
                <div class="msg msg-error">
                    <g:renderErrors bean="${bean}" as="list" />
                </div>
            </g:hasErrors>
            <g:form class="createForm fields" controller="memberStore" action="saveProspect" method="post" ${multiPart ? ' enctype="multipart/form-data"' : ''} >
               <g:hiddenField name="id" value="${memberStore.id}" />
               <g:hiddenField name="version" value="${memberStore.version}" />
               <g:hiddenField name="errorView" value="/memberStore/createProspect" />

               <theme:propertyGroup bean="${memberStore}" title="Store Information" props="${['name', 'contactName', 'contactEmail', 'address', 'state', 'zipCode', 'phoneNumber', 'faxNumber']}" />
               <theme:propertyGroup bean="${memberStore}" title="Prospect Information" props="${['currentWholesaler', 'currentBuyingGroup', 'estimatedMonthlyVolume', 'paymentTerms', 'frontEndOfStore']}" />

               <fieldset>
                 <legend>Prospect Notes</legend>
               <theme:propertyGroup bean="${memberStore}"  name="notes" title="Notes" props="${['notes']}" />
               </fieldset>

               <input class="button" type="submit" value="Add Prospect" />
               
           </g:form>

        </theme:panel>
    </body>
</html>
