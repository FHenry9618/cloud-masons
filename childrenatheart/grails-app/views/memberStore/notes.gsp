

  <g:if test="${readonly != 'true'}">
    Add Note<br/>
    <g:textArea id="notes[${bean.notes?.size() ?: 0}].description" name="notes[${bean.notes?.size() ?: 0}].description" cols="70" rows="10"></g:textArea>
    <input type="hidden" name="notes[${bean.notes?.size() ?: 0}].name" value="${loggedInUserInfo(field:'alias')}" />    
  </g:if>
  <g:if test="${bean.notes}">
    <table class="compact">
      <thead>
        <tr>
          <th>Notes</th>
          <th>Created By</th>
          <th>Date</th>
        </tr>
      </thead>
      <tbody>
        <g:each in="${bean.notes?.sort {it.dateCreated}?.reverse()}" status="i" var="bean">
           <tr>
             <td>${bean.description}</td>
             <td>${bean.name}</td>
             <td>${bean.dateCreated}</td>             
           </tr>
        </g:each>
     
      <tbody>
      <tfoot>
      </tfoot>
      
    </table>
  </g:if>
  <g:else>
      <br/>
    <g:if test="${readonly == true}">
      <i>No notes for this member store.</i>
    </g:if>
    <g:else>
      <i>Please add notes for this member store.</i>
    </g:else>
  </g:else>
    
