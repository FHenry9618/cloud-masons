

<head>
    <title>Edit Your Settings</title>
    <meta name="layout" content="${layout}" />

</head>

<body>
<div id="container">
    <div class="inner-container">




    <span>

       <g:if test="${flash.message}">
            <div class="msg msg-ok">${flash.message}</div>
        </g:if>
        <g:hasErrors bean="${user}">
            <div class="msg msg-error">
                <g:renderErrors bean="${user}" as="list" />
            </div>
        </g:hasErrors>
        

                <div class="box box-75 altbox">
                    <div class="boxin">
                        <div class="header">
                            <h3>Change your account settings</h3>
                        </div>
                        <g:form class="basic editForm" controller="user" action="updateSettings" method="post">
                           <g:hiddenField name="id" value="${user.id}" />
                           <g:hiddenField name="version" value="${user.version}" />

                           <div id="accordion" class="ui-accordion ui-widget ui-helper-reset ui-accordion-icons">

                                 <h3 class="ui-accordion-header ui-helper-reset ui-corner-top"><a href="#"><strong>Password</strong></a></h3>
                                 <div>
            <fieldset>

             <table>
                <tr>
                    <th><label for='newPasswordd'>Password: </label></th>
                    <td><input type='password' class='text_ password' name='newPassword' id='newPassword' /></td>
                </tr>
                <tr>
                    <th><label for='confirmPassword'>Confirm&nbsp;Password:&nbsp;&nbsp; </label></th>
                    <td><input type='password' class='text_ password' name='confirmPassword' id='confirmPassword' /></td><!-- class error for wrong filled inputs -->
                </tr>
              </table>
            </fieldset>
                                 </div>


                           </div>
                      

                       </g:form>

                    </div>
                </div>


                <div class="box box-25 altbox">
                    <div class="boxin">
                        <div class="header">
                            <h3>Summary</h3>
                        </div>

                            <form>
                            <fieldset>

                                <br/>
                                
                                Please make your changes and then click <b>Submit</b>. 
                                
                                <br/>
                                
                                <div class="sep">
                                    <a class="button altbutton submitButton">Submit</a>
                                    <a class="button altbutton cancelButton">Cancel</a>
                                </div>
                                
                                <div class="sep">
                                  &nbsp;
                                </div>
                                <div class="sep">
                                </div>

                            </fieldset>
                        </form
                    </div>
                </div>
  <script type="text/javascript">
  
     $(document).ready(function() {
          $('#accordion').accordion({
                      autoHeight: false,
                      collapsible: true,
                      active: ${(propertyGroups?.size() > 5) ? 'false' : '0'}
                      
                });
         $('.submitButton')
            .button()
            .click(function() {
                $('.editForm').submit();
            });
         $('.cancelButton')
            .button()
            .click(function() {
                window.location = '${createLink(controller: 'dashboard' )}';
            });
            
         $('.money').priceFormat({
            prefix: '$ '
         }); 
     
    });
  </script>



    </span> 
    </div>
</div>
</body>
