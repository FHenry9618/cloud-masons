package org.tbch

import org.codehaus.groovy.grails.commons.ConfigurationHolder

class LivingUnitUtilizationJob {
    def timeout = 5000000000l // execute job once in 5 seconds

    def execute() {
        // execute task
        println "**************LivingUnitUtilizationJob begin"
        LivingUnit.list().each { unit ->
            def result = ClientRecord.findAllByLivingUnit(unit)
            println "**************"+unit+"***"+result
            LivingUnitUtilization utilization = new LivingUnitUtilization()
            utilization.livingUnit = unit
            utilization.utilization = result.size() / unit.capacity
            utilization.save(flush:true)
        }
        println "**************LivingUnitUtilizationJob done"
    }
}
