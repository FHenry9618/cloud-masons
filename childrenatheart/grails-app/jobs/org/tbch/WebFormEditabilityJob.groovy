package org.tbch

import org.codehaus.groovy.grails.commons.ConfigurationHolder

class WebFormEditabilityJob {

    static triggers = {}
    
    def execute() {
        // execute task
        log.debug "**************WebFormEditabilityJob begin"
        int days = ConfigurationHolder.config.form.lock.after instanceof Integer ? ConfigurationHolder.config.form.lock.after : 14
        Date cutOff = new Date() - days
        log.debug cutOff
        List webForms = ['WeeklyNotes', 'MonthlyNotes', 'VisitationForm']
        webForms.each { form ->
            String query = "select o from ${form} as o where o.dateCreated < :cutOff and o.editable = true"
            List objects = User.executeQuery(query, [cutOff: cutOff])
            objects?.each { 
                println "**************locking form "+it.name+it.getClass().name
                it.editable = false
                it.save(flush:true)
                
            }
        }
        log.debug "**************WebFormEditabilityJob done"
    }
}
