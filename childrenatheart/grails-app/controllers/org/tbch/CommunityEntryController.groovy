package org.tbch

class CommunityEntryController {
    def securityService
    def scaffold=true
    
    def delete = {
        //println "DELETE ${params}"
        def entry = CommunityEntry.get(params.id.toLong())
        PlanOfService pos = entry.parentObject
        pos.removeFromCommunityPlans(entry)
        pos.save()
        entry.delete()
    }
}
