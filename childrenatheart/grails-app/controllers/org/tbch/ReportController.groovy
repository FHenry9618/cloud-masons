package org.tbch

import org.codehaus.groovy.grails.commons.ConfigurationHolder as CH
import org.xhtmlrenderer.pdf.ITextRenderer
/**
 * Used to run the various reports.
 * @author wdroste
 *
 */
class ReportController {

    // auto-wired services
    def reportService
    def defaultService

    static navigation = [
        group:'tabs',
        order:3,
        title:'Reports',
        action:'index',
        icon:'comments',
        anyrole: 'ROLE_REPORTING,ROLE_SYSTEM_ADMINISTRATOR,ROLE_CASE_MANAGER'
    ]

    private static final List reports = [
        ['id':1L, action:'status', name:'Campus Report'],
        ['id':2L, action:'roasterByCottage', name:'Roster By Cottage'],
        ['id':3L, action:'seriousIncidentReports', name:'Serious Incident Report']
    ]
    // actions

    def index = { [reports:reports] }

    def show = { ShowCommand cmd ->
        def report = reports.find { r -> r.id == cmd.id }
        chain(action:report.action, params:params)
    }

    /**
     * Builds the necessary output for a roaster.
     */
    def roasterByCottage = { ShowCommand cmd ->
        // process each of the living units..
        def livingUnits = reportService.buildLivingUnits(cmd.campusId, cmd.startDate, cmd.endDate) { ClientRecord client ->
			def ret = [:]
			// Last Name
			ret['lastName'] = g.fieldValue(bean:client, field:'lastName')
			// First Name
			ret['firstName']  = g.fieldValue(bean:client, field:'firstName')
			// Date of Birth
			ret['dateOfBirth'] = g.fieldValue(bean:client, field:'dateOfBirth')
			// Age
			ret['age'] = client.age as int
			ret['grade'] = client.aftercare?.grade ? client.gradeLevel : ''
			// pure dates for calculations
			ret['datePlacement'] = client.datePlacement
			ret['dateDischarge'] = client.dateDischarge
			ret['dateOfAdmission'] = g.fieldValue(bean:client, field:'datePlacement')
			// Managing Conservator
			def mc = client.significantPersons.find { it.managingConservator }
			if (mc) {
				ret['managingConservator'] = mc.relationship//"${mc.lastName}, ${mc.firstName}"
			}
			ret['levelOfCare'] = client.levelOfCare ?: ''
			ret
		}

        // determine all the summary information..
        def summary = [:]
		int summaryTotalInCare = livingUnits.sum { it.totalInCare }
        int summaryTotalDaysInCare = livingUnits.sum { it.totalCareDays }
		
        // days of care
        // average age
        // average years in care
        [
			'livingUnits': livingUnits, 
			'summaryTotalDaysInCare':summaryTotalDaysInCare,
            'summaryTotalInCare':summaryTotalInCare 
		]
    }

    /**
     * Status report for the campus.
     */
    def status = { ShowCommand cmd ->
        reportService.buildStatusModel(cmd.campusId, cmd.startDate, cmd.endDate)
    }

    /**
     * Status report for case notes based on serious incident reports.
     */
    def seriousIncidentReports = { ShowCommand cmd ->
        reportService.seriousIncidentReportsBetween(cmd.campusId, cmd.startDate, cmd.endDate)
    }

    /**
     * Create a PDF roaster
     */
    def printSeriousIncidentReports = {
        params.action = 'seriousIncidentReports'
        params.filenamePrefix = 'SeriousIncidentReport'
        buildPdf()
    }

    def printRoasterByCottage = {
        params.action = 'roasterByCottage'
        params.filenamePrefix = 'Roster'
        buildPdf()
    }

    def printStatus = {
        params.action = 'status'
        params.filenamePrefix = 'Status'
        buildPdf()
    }

    private buildPdf = {
        log.debug "Parameters: ${params}"
        try{
            def pms = [:]
            pms.putAll(params)
            pms.remove('action')
            pms.printPDF = true
            String content = g.include(action:params.action, 'params':pms)
            // set the repsonse headers..
            def filename = "${params.filenamePrefix}_${new Date().format('MM-dd-yyyy')}.pdf"
            def attachment = "attachment; filename=${filename}"
            response.setContentType('application/pdf')
            response.setHeader('Content-disposition', attachment)
            // render the pdf..
            def renderer = new ITextRenderer()
            def baseUri = CH.config.grails.serverURL.toString()
            renderer.setDocumentFromString(content, baseUri)
            renderer.layout()
            def ops = response.outputStream
            renderer.createPDF(ops)
            ops.flush()
        }
        catch (Throwable e) {
            println "there was a problem with PDF generation ${e}"
            //if(params.template) render(template:params.template)
            redirect(action:params.action, 'params':params)
        }
    }
}

class ShowCommand {
    Long id
    Long campusId
    Date endDate
    Date startDate

    static constraints = {
        id(blank:false)
        endDate(blank: false)
        startDate(blank: false)
    }
}
