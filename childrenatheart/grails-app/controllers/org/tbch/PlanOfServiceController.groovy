package org.tbch

import com.cloudmasons.BeanUtils

class PlanOfServiceController {
    def securityService
    def scaffold=true

    def beanService
    def grailsApplication
    def defaultService
    
    /**
     * Create a plan of service and associate it to the client record so one
     * exists for when they create a medical entry or something of that nature.
     */
    def create = {
        def clientRecord = ClientRecord.get(params.long('id'))
        def beanDomainClass = BeanUtils.resolveDomainClass(grailsApplication, 'org.tbch.PlanOfService')

        if (!securityService.isAuthorized(PlanOfService, 'create')) {
            render "Not Authorized"
            return;
        }

        PlanOfService pos = new PlanOfService()
        pos.date = new Date()
        pos.reportingPeriodStart = new Date()
        pos.reportingPeriodEnd = new Date()

        clientRecord.addToPlanOfService(pos)
        clientRecord.save(failOnError:true)

        def model = [:]
        model.bean = pos
        model.beanAssociated = 'planOfService'
        model.parentBeanId = clientRecord.id
        model.domainClass = beanDomainClass
        model.domainNaturalName = 'Plan Of Service'
        model.propertyGroups = beanService.getPropertyGroups(pos, null)
        model.beanControllerName = 'clientRecord'
        
        // only delete those that can be deleted..
        session.deletePosId = pos.id

        def reqs = session.previousRequests
        if (reqs && reqs.size() > 0) {
            model.previousRequest = session.previousRequests.get(reqs.size()-1)
        }
        model
    }
    
    def show = {        
        def beanDomainClass = BeanUtils.resolveDomainClass(grailsApplication, 'org.tbch.PlanOfService')
        def pos = PlanOfService.get(params.long('propertyId'))
        if (!securityService.isAuthorized(PlanOfService, 'show', pos)) {
            render "Not Authorized"
            return
        }

        def model = [:]
        model.bean = pos
        model.beanAssociated = 'planOfService'
        model.parentBeanId = params.long('id')
        model.domainClass = beanDomainClass
        model.domainNaturalName = 'Plan Of Service'
        model.propertyGroups = beanService.getPropertyGroups(pos, null)
        model.beanControllerName = 'clientRecord'

        def reqs = session.previousRequests
        if (reqs && reqs.size() > 0) {
            model.previousRequest = session.previousRequests.get(reqs.size()-1)
        }
        model
    }
    
    def deleteFromCreate = {
        def id = params.long('id')
        if (id == session.deletePosId) {
            def pos = PlanOfService.get(params.long('id')).delete()
            render 'Ok'
        }
        render 'Failed'
    }

    def clone = {
        def pos = defaultService.clonePlanOfService(params.long('id'))
        def model = [:]
        model.bean = pos
        model.beanAssociated = 'planOfService'
        model.parentBeanId = pos.clientRecord.id
        model.domainClass = BeanUtils.resolveDomainClass(grailsApplication, PlanOfService)
        model.domainNaturalName = 'Plan Of Service'
        model.propertyGroups = beanService.getPropertyGroups(pos, null)

        // only delete those that can be deleted..
        session.deletePosId = pos.id

        def reqs = session.previousRequests
        if (reqs && reqs.size() > 0) {
            model.previousRequest = session.previousRequests.get(reqs.size()-1)
        }
        render(view:'create', 'model':model)
    }

    static final String DT_FMT = 'MM/dd/yyyy'
    
    /**
     * Save or update a service plan entry.
     */
    def saveOrUpdateServicePlanEntry = {
        ServicePlanEntry spe = new ServicePlanEntry()
        def theId = params.long('servicePlanEntry-theServicePlanEntryId')
        if (theId) {
            spe = ServicePlanEntry.get(theId)
        }

        // set all the dates
        spe.date = Date.parse(DT_FMT, params['servicePlanEntry-date'])
        if (params['servicePlanEntry-targetDate']) {
            spe.targetDate = Date.parse(DT_FMT, params['servicePlanEntry-targetDate'])
        }
        if (params['servicePlanEntry-dateResolved']) {
            spe.dateResolved = Date.parse(DT_FMT, params['servicePlanEntry-dateResolved'])
        }
        
        // set all the strings
        spe.goals = params['servicePlanEntry-goals']
        spe.strategies = params['servicePlanEntry-strategies']
        spe.responsibleParty = params['servicePlanEntry-responsibleParty']

        // other
        spe.scale = params.int('servicePlanEntry-scale')

        // 
        def associated = params['servicePlanEntry-property'].replace('#','')
        def thePlanEntry 
        if (theId) {
            thePlanEntry = spe.save(failOnError:true).planEntry
        } else {
            def parentId = params.long('id')
            thePlanEntry = defaultService.addServicePlanEntry(parentId, associated, spe)
        }
        
        
        def model = [
            'planEntry':thePlanEntry, 
            'property':['name':associated],
            'beanInstance':PlanOfService.get(params.long('id'))
        ]
        render(template: "planEntryRow", 'model':model)
    }

    def edit = {
        PlanOfService pos = PlanOfService.get(params.long('id'))
        if (!securityService.isAuthorized(PlanOfService, 'edit', pos)) {
            render "Not Authorized"
            return
        }

        def model = [:]
        model.bean = pos
        model.beanAssociated = 'planOfService'
        model.parentBeanId = pos.clientRecord.id
        model.domainClass = BeanUtils.resolveDomainClass(grailsApplication, 'org.tbch.PlanOfService')
        model.domainNaturalName = 'Plan Of Service'
        def propertyGroups = [:]
        propertyGroups.put(params.group, [])
        model.propertyGroups = beanService.getPropertyGroups(pos, propertyGroups)

        def reqs = session.previousRequests
        if (reqs && reqs.size() > 0) {
            model.previousRequest = session.previousRequests.get(reqs.size()-1)
        }
        model
    }
    
    def deletePlanEntry = {
        PlanEntry.get(params.id).delete()
        render 'Ok'
    }

    /**
     * Resolve the bean's domain class from the given parameters
     */
    private Object getBeanDomainClass(Map params) {
        def beanClassName = params.beanClass
        def beanDomainClass = BeanUtils.resolveDomainClass(grailsApplication, beanClassName)
        return beanDomainClass
    }

    def update = {
        def beanInstance = PlanOfService.get(params.id)
        def beanDomainClass = BeanUtils.resolveDomainClass(grailsApplication, PlanOfService)
        if (!securityService.isAuthorized(beanDomainClass.clazz, 'edit', beanInstance)) {
            render "Not Authorized"
            return
        }

        // determine if the bean instance exists
        if (beanInstance) {
            beanInstance.properties = params
            if (!beanInstance.hasErrors() && beanInstance.save(flush: true)) {
                // determine message for flash..
                flash.message = message(code: 'default.updated.message', args: ['Plan Of Service', beanInstance.name])
                redirect(action: 'show', id: params.parentBeanId, params: ['propertyId': beanInstance.id])
                return
            } else {
                def model = [:]
                model.bean = beanInstance
                model.beanAssociated = 'planOfService'
                model.parentBeanId = beanInstance.clientRecord.id
                model.domainClass = beanDomainClass
                model.domainNaturalName = 'Plan Of Service'
                render(view: 'edit', model: model)
            }
        } else {
            flash.message = message(code: 'default.not.found.message', args: ['Plan Of Service', params.name])
            redirect(action: 'list')
        }
    }
    
    def getServicePlanEntryJSON = {
        def spe = ServicePlanEntry.get(params.long('id'))
        render(contentType:"text/json") {
            // all the dates
            date = spe.date?.format(DT_FMT) ?: ''
            targetDate = spe.targetDate?.format(DT_FMT) ?: ''
            dateResolved = spe.dateResolved?.format(DT_FMT) ?: ''
            // all the strings
            goals = spe.goals
            strategies = spe.strategies
            responsibleParty = spe.responsibleParty
            // other
            scale = spe.scale
    
        }
    }
}
