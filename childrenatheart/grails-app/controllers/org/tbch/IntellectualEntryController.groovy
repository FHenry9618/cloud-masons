package org.tbch

class IntellectualEntryController {
    def securityService
    def scaffold=true
    
    def delete = {
        //println "DELETE ${params}"
        def entry = IntellectualEntry.get(params.id.toLong())
        PlanOfService pos = entry.parentObject
        pos.removeFromIntellectualPlans(entry)
        pos.save()
        entry.delete()
    }
}
