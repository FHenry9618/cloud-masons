package org.tbch

class DentalEntryController {
    def securityService
    def scaffold=true
    def delete = {
        //println "DELETE ${params}"
        def entry = DentalEntry.get(params.id.toLong())
        PlanOfService pos = entry.parentObject
        pos.removeFromDentalPlans(entry)
        pos.save()
        entry.delete()
    }
}
