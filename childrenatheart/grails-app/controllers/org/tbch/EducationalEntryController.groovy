package org.tbch

class EducationalEntryController {
    def securityService
    def scaffold=true
    
    def delete = {
        //println "DELETE ${params}"
        def entry = EducationalEntry.get(params.id.toLong())
        PlanOfService pos = entry.parentObject
        pos.removeFromEducationalPlans(entry)
        pos.save()
        entry.delete()
    }
}
