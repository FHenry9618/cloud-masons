package org.tbch

class CampusController {
    def securityService
    def scaffold=true
    static navigation = [
        group:'tabs',
        order:4,
        title:'Campus',
        action:'list',
        icon:'comments',
        role: 'ROLE_SYSTEM_ADMINISTRATOR'
    ]
}
