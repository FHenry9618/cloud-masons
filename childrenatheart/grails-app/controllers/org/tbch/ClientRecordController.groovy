package org.tbch

import com.cloudmasons.BeanUtils
import org.apache.commons.lang.BooleanUtils
import org.codehaus.groovy.grails.exceptions.InvalidPropertyException
import org.springframework.security.context.SecurityContextHolder

class ClientRecordController {
    def scaffold=true
    def grailsApplication
    def tbchService
    def securityService
    def clientRecordService
    def beanService

    def uploadDocument = {
        def clientRecord = ClientRecord.get(params.id)
        if (!clientRecord) {
            def label = message(code: 'clientRecord.label', default: 'ClientRecord')
            def msg = message(code: 'default.not.found.message', args: [label, params.id])
            flash.message = msg
            return
        }
        def model = [:]
        def domainClass = BeanUtils.resolveDomainClass(grailsApplication, new Document())
        //println domainClass
        def constraints = domainClass?.constrainedProperties
        def typeConstraint = constraints['type']
        model.clientRecord = clientRecord
        model.typeConstraint = typeConstraint

        return model
    }

    def saveDocument = {
        def file = request.getFile('document')
        def id = Long.parseLong(params.id)
        tbchService.processDocument(file, id, params.type, params.description)
        redirect (action:'show',id:id)
    }

    def show = {
        boolean override = false
        def beanDomainClass = BeanUtils.resolveDomainClass(grailsApplication, 'ClientRecord')
        def associatedProperty = null

        // check to see if its a plan of service
        if ('planOfService' == params.associated) {
            if (BooleanUtils.toBoolean(params.create)) {
                render include(controller: 'planOfService', action:'create', id:params.id)
                return
            } else if (params.propertyId){
                render include(controller: 'planOfService', action:'show', id:params.id)
                return
            }
        }

        if (params.associated) {
            try {
                associatedProperty = beanDomainClass.getPropertyByName(params.associated)
            }
            catch (InvalidPropertyException e) {
                redirect (action:'list')
                return
            }
            def beanInstance = beanDomainClass.clazz.get(params.id)
            if (!securityService.isAuthorized(beanDomainClass.clazz, 'show', beanInstance)) {
                render "Not Authorized"
                return
            }

            def model = [:]
            def parentBean = beanInstance // for calculating the path on showing assoicated properties
            if (!beanInstance) {
                flash.message = "${message(code: 'default.not.found.message', args: [message(code: "${domainClass.propertyName}.label", default: beanDomainClass.naturalName), params.id])}"
                redirect(action: "list")
                return
            }
            else if (associatedProperty && associatedProperty.oneToOne && params.associated == 'dischargeForm' && !params.propertyId && beanInstance.planOfService?.size() > 0 ) {
                // create dischargeForm from plan of Service.
                override = true

                def latestPlanOfService = beanInstance.planOfService
                        .findAll{ it }.sort{ it?.date }.reverse()[0]

                def dischargeForm = tbchService.generateDischargeFormFromPlanOfService(latestPlanOfService)
                if (dischargeForm.save(flush:true)) {
                    beanInstance.dischargeForm = dischargeForm
                    beanInstance.save(flush:true)
                }
                else {
                    println dischargeForm.errors
                }

                redirect (action:'show', params:[id:params.id, associated:'dischargeForm', propertyId:dischargeForm.id])
                return
            }
        }

        if (!override) {
            // copy the parameters for sorting/max etc..
            def pms = [:]
            pms.putAll(params)
            [
                'beanClass',
                'beanControllerName'
            ].each { pms.remove(it) }
            pms.putAll([beanClass: 'ClientRecord', beanControllerName: 'clientRecord'])

            def result = include(controller: 'bean', action:'show', id: "${params.id}", params:pms)
            if (result.toString().length() > 0) {
                render result;
            } else {
                if  (flash.redirect && flash.redirection) {
                    redirect (controller: flash.redirection.controller, action: flash.redirection.action, params: flash.redirection.params)
                    return
                }
                redirect(action: "list");
            }
        }
    }


    /**
     * Simple filter based on the set of parameters provided..
     */
    def list = {
        // test for a redirect just in case?
        if  (flash.redirect && flash.redirection) {
            redirect (controller: flash.redirection.controller, action: flash.redirection.action, params: flash.redirection.params)
            return
        }
        // initial setting..
        // query basics..
        int max = params.int('max') ?: 10
        int offset = params.int('offset') ?: 0

        // filter properties..
        def model = [:]
        def user = getCurrentUser()

        def campusId = user.campus?.id
        def livingUnitId = user.livingUnit?.id
        def status = [
            Constants.STATUS_PLACEMENT,
            Constants.STATUS_APPLICATION
        ]
        if (params.containsKey('filter')) {
            status = params.list('filter.status')
            campusId = params.long('filter.campus')
            livingUnitId = params.long('filter.livingUnit')
        }
        model.filter = ['status':status, 'campus':campusId, 'livingUnit': livingUnitId]
        log.debug "Fitler: ${model.filter}"

        // query for the clients..
        model.putAll(clientRecordService.search(model.filter, offset, max, params.sort, params.order))

        params.putAll([beanClass: 'ClientRecord', beanControllerName: 'clientRecord'])
        model.domainClass = BeanUtils.resolveDomainClass(grailsApplication, ClientRecord)
        model.actions = beanService.getActions(model.domainClass, [])

        model
    }

    private getCurrentUser() {
        def pricipalInfo = SecurityContextHolder.context?.authentication?.principal
        def user  = pricipalInfo?.domainClass
        user?.refresh()
        return user
    }
}
