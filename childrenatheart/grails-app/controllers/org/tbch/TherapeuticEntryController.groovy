package org.tbch

class TherapeuticEntryController {
    def securityService
    def scaffold=true
    
    def delete = {
        //println "DELETE ${params}"
        def entry = TherapeuticEntry.get(params.id.toLong())
        PlanOfService pos = entry.parentObject
        pos.removeFromTherapeuticPlans(entry)
        pos.save()
        entry.delete()
    }
}
