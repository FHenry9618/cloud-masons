package org.tbch

import org.compass.core.engine.SearchEngineQueryParseException

class DashboardController {

	def authenticateService
	def searchableService
	def tbchService

	static navigation = [
		group:'tabs',
		order:2,
		title:'User Dashboard',
		action:'index',
		icon:'dashboard'
	]

	def index = {
		forward(action: 'member');
	}

	def list = {
		User user = tbchService.getLoggedInUser();
		def model = [user: user]
		render view:'member', model:model
	}

	def member = {
		User user = tbchService.getLoggedInUser();

		def model = [user: user]
		return model
	}

	/**
	 * Index page with search form and results
	 */
	def search = {
		def query = params.q?.trim()
		if (!query) {
			return [:]
		}
		try {
			return [searchResult: searchableService.search(query, params)]
		} catch (SearchEngineQueryParseException ex) {
			return [parseException: true]
		} catch (org.apache.lucene.search.BooleanQuery$TooManyClauses ex) {
			return [tooVague: true]
		}
	}
}
