package org.tbch

class FamilialEntryController {
    def securityService
    def scaffold=true
    
    def delete = {
        //println "DELETE ${params}"
        def entry = FamilialEntry.get(params.id.toLong())
        PlanOfService pos = entry.parentObject
        pos.removeFromFamilialPlans(entry)
        pos.save()
        entry.delete()
    }
}
