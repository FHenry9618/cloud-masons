package org.tbch

class SocialEntryController {
    def securityService
    def scaffold=true
    
    def delete = {
        //println "DELETE ${params}"
        def entry = SocialEntry.get(params.id.toLong())
        PlanOfService pos = entry.parentObject
        pos.removeFromSocialPlans(entry)
        pos.save()
        entry.delete()
    }
}
