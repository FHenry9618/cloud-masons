package org.tbch

class ParentSurveyController {
    def securityService
    def scaffold=true
    static navigation = [
        group:'tabs', 
        order:3, 
        title:'Parent Survey', 
        action:'list',
        icon:'comments',
        role: 'ROLE_SYSTEM_ADMINISTRATOR'
    ]
    
    def save = {
        def result =  include(controller: 'bean', action:'save', id: "\${params.id}", params: [beanClass:'ParentSurvey',beanControllerName:'parentSurvey']);

        if (result.toString().length() > 0) {
            render result;
        } else {
            redirect(action: "index", controller:"dashboard");
        }
    }

}
