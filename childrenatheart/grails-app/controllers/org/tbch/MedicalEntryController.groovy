package org.tbch

class MedicalEntryController {
    
    static allowedMethods = [delete: "POST"]
    
    def securityService
    def scaffold=true
    
    
    def delete = {
        //println "DELETE ${params}"
        def entry = MedicalEntry.get(params.id.toLong())
        PlanOfService pos = entry.parentObject 
        pos.removeFromMedicalPlans(entry)
        pos.save()
        entry.delete()
    }
}
