package org.tbch

import org.codehaus.groovy.grails.commons.GrailsDomainClass
import com.cloudmasons.BeanUtils

class SignatureDocController {

    def grailsApplication
    def scaffold=true
    def securityService
    def tbchService
    
    def download = { 
        def beanInstance = SignatureDoc.get(params.id.toLong())
        def beanDomainClass = BeanUtils.resolveDomainClass(grailsApplication, beanInstance)
        if (!securityService.isAuthorized(beanDomainClass.clazz, 'download', beanInstance)) {
            render "Not Authorized"
            return;
        }
        redirect (controller:'secureFile', action:'download', id:params.id, params:[beanClass: SignatureDoc])
    }
    
    def uploadDocument = {
        def beanClassName = params.beanClass
        def beanDomainClass = BeanUtils.resolveDomainClass(grailsApplication, beanClassName)
        
        def beanInstance = beanDomainClass.clazz.get(params.id)
        if (!beanInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: '${beanDomainClass.propertyName}.label', default: beanDomainClass.naturalName), params.id])}"
            redirect (controller:'dashboard', action:'index')
        }
        def model = [:]
        model.beanInstance = beanInstance
        model.beanPropertyName = beanDomainClass.propertyName
        return model
    }
        
    def saveDocument = {
        //println params
        def beanClassName = params.beanClass
        def beanDomainClass = BeanUtils.resolveDomainClass(grailsApplication, beanClassName)
        
        def beanInstance = beanDomainClass.clazz.get(params.id)
        if (!beanInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: '${beanDomainClass.propertyName}.label', default: beanDomainClass.naturalName), params.id])}"
            redirect (controller:'dashboard', action:'index')
            return
        }
        
        if (beanInstance.signaturePage) { //only allow one signature page
            redirect (controller:'clientRecord',action:'show',id:beanInstance?.clientRecord?.id)
            return
        }
        def validateError = tbchService.validateFormData(beanInstance, beanDomainClass)
        if (validateError) {
            flash.messages = []
            validateError.each {
                def message = """${message(code: 'default.missing.data.field.message', args: [message(code: "${beanDomainClass.propertyName}.${it}.label", default: it)])}"""
                flash.messages.add(message) 
            }
            flash.error = true
            redirect (controller:'clientRecord',action:'show', params:[id:beanInstance?.clientRecord?.id, associated:beanDomainClass.propertyName, propertyId:beanInstance.id])
            return
        }
        // Get uploaded file from the multi-part request
        def file = request.getFile('document')
        tbchService.processSignatureDocument(file, beanInstance, params.description)
        redirect (controller:'clientRecord',action:'show',id:beanInstance?.clientRecord?.id)
    }
    
//    def edit = {
//        def document = Document.get(params.id.toLong())
//        def model = [:]
//        def domainClass = BeanUtils.resolveDomainClass(grailsApplication, document)
//        if (!securityService.isAuthorized(domainClass.clazz, 'edit', document)) {
//            render "Not Authorized"
//            return;
//        }
//        def constraints = domainClass?.constrainedProperties
//        def typeConstraint = constraints['type']
//        model.clientRecord = document.clientRecord
//        model.typeConstraint = typeConstraint
//        model.document = document
//        return model
//        //render (view: '/clientRecord/uploadDocument', model: model)
//    }
    
    def delete = {
        def beanInstance = SignatureDoc.get(params.id.toLong())
        def model = [:]
        def beanDomainClass = BeanUtils.resolveDomainClass(grailsApplication, beanInstance)
        if (!securityService.isAuthorized(beanDomainClass.clazz, 'delete', beanInstance)) {
            render "Not Authorized"
            return;
        }
        if (beanInstance) {
            try {
                beanInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: beanDomainClass.propertyName+'.label', default: beanDomainClass.naturalName), beanInstance.name])}"
                def file = new File(beanInstance.path + beanInstance.encryptedName);
                if (file.exists()) {
                    file.delete()
                }
                redirect(controller: 'dashboard',action: "index")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: beanDomainClass.propertyName+'.label', default: beanDomainClass.naturalName), beanInstance.name])}"
                redirect(controller: 'dashboard',action: "index")
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: beanDomainClass.propertyName+'.label', default: beanDomainClass.propertyName), params.id])}"
            redirect(controller: 'dashboard',action: "index")
        }
    }
}
