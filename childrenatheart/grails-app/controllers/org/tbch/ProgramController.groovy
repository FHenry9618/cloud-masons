package org.tbch

class ProgramController {
    def securityService
    def scaffold=true
    static navigation = [
        group:'tabs',
        order:5,
        title:'Program',
        action:'list',
        icon:'comments',
        role: 'ROLE_SYSTEM_ADMINISTRATOR'
    ]
}
