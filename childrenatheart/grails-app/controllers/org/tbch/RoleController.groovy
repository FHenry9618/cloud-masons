package org.tbch

class RoleController {
    def securityService
    def scaffold = true
    static navigation = [
        group:'tabs',
        order:4,
        title:'Role',
        action:'list',
        icon:'comments',
        role: 'ROLE_SYSTEM_ADMINISTRATOR'
    ]
}
