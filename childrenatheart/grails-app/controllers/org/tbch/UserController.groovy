package org.tbch

/**
 * User controller.
 */
class UserController {
    def scaffold=true
    def authenticateService
    def securityService
    
    static navigation = [
                         group:'tabs', 
                         order:4, 
                         title:'Users', 
                         action:'list',
                         icon:'comments',
                         role: 'ROLE_SYSTEM_ADMINISTRATOR'
                     ]
    // the delete, save and update actions only accept POST requests
    static Map allowedMethods = [delete: 'POST', save: 'POST', update: 'POST']



    /**
     * Person delete action. Before removing an existing person,
     * he should be removed from those authorities which he is involved.
     */
    def delete = {

        def person = User.get(params.id)
        if (person) {
            def authPrincipal = authenticateService.principal()
            //avoid self-delete if the logged-in User is an admin
            if (!(authPrincipal instanceof String) && authPrincipal.username == person.alias) {
                flash.message = "You can not delete yourself, please login as another admin and try again"
            }
            else {
                //first, delete this person from People_Authorities table.
                person.roles.clear()
                person.delete()
                flash.message = "User $params.id deleted."
            }
        }
        else {
            flash.message = "User not found with id $params.id"
        }

        redirect action: list
    }



    def settings = {
        def user =  authenticateService.principal().domainClass
        if (!user) {
            def label = message(code: 'user.label', default: 'User')
            def msg = message(code: 'default.not.found.message', args: [label, params.id])
            flash.message = msg

            redirect action: list
            return
        }

        def propertyGroups = [ 'Password': []]
        def layout = 'main'
        return [user: user, propertyGroups: propertyGroups, layout: layout]
    }

    /**
     * Person update action.
     */
    def updateSettings = {

        def user = User.get(params.id)
        if (!user) {
            flash.message = "User not found with id $user.id"
            redirect(controller: 'dashboard');
            return
        }
        if (!user) {
            redirect(action: auth);
        } else {
            def currentUser = authenticateService.principal().domainClass
            def isAdmin = authenticateService.ifAnyGranted('ROLE_SYSTEM_ADMINISTRATOR,MEMBER_ADMINISTRATOR')

            if (!isAdmin && currentUser.id != user.id) {
                flash.message = "Access denied"
                redirect(controller: 'dashboard');
                return
            }

            if (params.newPassword && params.confirmPassword) {
                if (params.newPassword == params.confirmPassword) {
                    user.passwd = authenticateService.encodePassword(params.newPassword)

                    if (!user.save(flush: true)) {
                        println "Unable to save user?"
                    }

                    flash.message = "Password changed."
                    redirect(controller: 'dashboard');
                } else {
                    flash.message = "The provided passwords did not match, please try again"
                    redirect(action: 'settings', id.params.id)
                }
            } else {
                flash.message = "Canceled password change."
                redirect(controller: 'dashboard');
            }
        }
    }
    

    /**
     * Person update action.
     */
    def update = {
        def person = User.get(params.id)
        if (!person) {
            flash.message = "User not found with id $params.id"
            redirect action: edit, id: params.id
            return
        }

        // todo: handle password and clearing many-to-many on scaffolding controller
        def oldPassword = person.passwd
        def tmp = person.roles?.collect { it }
        tmp?.each { a ->
            person.removeFromRoles(a)
        }
        person.properties = params
        //println(params.passwd)

        if (params.newPassword && params.newPassword == params.confirmPassword){
            person.passwd = authenticateService.encodePassword(params.newPassword)
        } else {
            person.passwd = oldPassword
        }
        if (person.save()) {
            redirect action: list, id: person.id
        }
        else {
            render view: 'edit', model: [List: Role.list(), person: person]
        }
    }

    /**
     * Person save action.
     */
    def save = {
        def person = new User()
        person.properties = params
        if (params.newPassword) {
          if (params.newPassword == params.confirmPassword) {
              person.passwd = authenticateService.encodePassword(params.newPassword)
          } else {
              flash.message("Password and confirm password did not match") 
          }
        }
        if (person.save()) {
            redirect action: list, id: person.id
        }
        else {
            println person.errors
            render view: 'create', model: [List: Role.list(), person: person]
        }
    }
}
