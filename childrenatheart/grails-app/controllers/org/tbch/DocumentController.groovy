package org.tbch

import org.codehaus.groovy.grails.commons.GrailsDomainClass
import com.cloudmasons.BeanUtils

class DocumentController {

    def grailsApplication
    def scaffold=true
    def securityService
    
    def download = { 
        def beanInstance = Document.get(params.id.toLong())
        def beanDomainClass = BeanUtils.resolveDomainClass(grailsApplication, beanInstance)
        if (!securityService.isAuthorized(beanDomainClass.clazz, 'download', beanInstance)) {
            render "Not Authorized"
            return;
        }
        redirect (controller:'secureFile', action:'download', id:params.id, params:[beanClass: Document])
    }
    
    def edit = {
        def document = Document.get(params.id.toLong())
        def model = [:]
        def domainClass = BeanUtils.resolveDomainClass(grailsApplication, document)
        if (!securityService.isAuthorized(domainClass.clazz, 'edit', document)) {
            render "Not Authorized"
            return;
        }
        def constraints = domainClass?.constrainedProperties
        def typeConstraint = constraints['type']
        model.clientRecord = document.clientRecord
        model.typeConstraint = typeConstraint
        model.document = document
        def reqs = session.previousRequests
        if (reqs && reqs.size() > 0) {
            model.previousRequest = reqs.get(reqs.size()-1)
        }
        return model
        //render (view: '/clientRecord/uploadDocument', model: model)
    }
    
    def delete = {
        def beanInstance = Document.get(params.id.toLong())
        def model = [:]
        def beanDomainClass = BeanUtils.resolveDomainClass(grailsApplication, beanInstance)
        if (!securityService.isAuthorized(beanDomainClass.clazz, 'delete', beanInstance)) {
            render "Not Authorized"
            return;
        }
        if (beanInstance) {
            try {
                def clientRecord = beanInstance.clientRecord
                clientRecord.refresh();
                clientRecord.removeFromDocuments(beanInstance)
                clientRecord.save(flush:true)
                beanInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: beanDomainClass.propertyName+'.label', default: beanDomainClass.naturalName), beanInstance.name])}"
                def file = new File(beanInstance.path + beanInstance.encryptedName);
                if (file.exists()) {
                    file.delete()
                }
                redirect(controller: 'clientRecord',action: "show", params:[id: clientRecord.id, associated:'documents'])
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: beanDomainClass.propertyName+'.label', default: beanDomainClass.naturalName), beanInstance.name])}"
                redirect(controller: 'dashboard',action: "index")
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: beanDomainClass.propertyName+'.label', default: beanDomainClass.propertyName), params.id])}"
            redirect(controller: 'dashboard',action: "index")
        }
    }
}
