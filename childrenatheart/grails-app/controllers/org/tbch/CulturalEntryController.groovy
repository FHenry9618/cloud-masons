package org.tbch

class CulturalEntryController {
    def securityService
    def scaffold=true
    
    def delete = {
        //println "DELETE ${params}"
        def entry = CulturalEntry.get(params.id.toLong())
        PlanOfService pos = entry.parentObject 
        pos.removeFromCulturalPlans(entry)
        pos.save()
        entry.delete()
    }
}
