package org.tbch

class BehavioralEntryController {
    def securityService
    def scaffold=true
    
    def delete = {
        //println "DELETE ${params}"
        def entry = BehavioralEntry.get(params.id.toLong())
        PlanOfService pos = entry.parentObject
        pos.removeFromBehavioralPlans(entry)
        pos.save()
        entry.delete()
    }
}
