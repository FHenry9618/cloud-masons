package org.tbch

class DevelopmentalEntryController {
    def securityService
    def scaffold=true
    
    def delete = {
        //println "DELETE ${params}"
        def entry = DevelopmentalEntry.get(params.id.toLong())
        PlanOfService pos = entry.parentObject
        pos.removeFromDevelopmentalPlans(entry)
        pos.save()
        entry.delete()
    }
}
