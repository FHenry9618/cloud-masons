package org.tbch

class VisitationFormController {
    def securityService
    def scaffold=true

    def clone = {
        def vf = VisitationForm.get(params.long('id'))
        def pms = [:]
        pms.putAll(params)
        pms.put('create', true)
        pms.put('id', vf.clientRecord.id)
        pms.put('beanClass', 'ClientRecord')
        pms.put('associated', 'visitationForm')
        pms.put('beanControllerName', 'clientRecord')
        chain(controller:'bean', action:'show', model:[cloneInstance: vf.clone()], params:pms)
    }
}
