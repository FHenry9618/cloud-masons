package org.tbch

import org.apache.commons.lang.StringUtils
import org.codehaus.groovy.grails.commons.ConfigurationHolder as CH

class TbchTagLib {

    static namespace = "tbch"
    
    def shortForm = { attrs ->
        out << StringUtils.abbreviate(attrs.value, 100)
    }

    def organizationName = { attrs ->
        out << CH.config.grails.organizationName
    }

    def logo = { attrs ->
        def img = g.resource(['dir':'images', 'file': CH.config.grails.logo])
        out << "<img src=\"${img}\"/>"
    }
}
