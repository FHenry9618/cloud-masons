package org.tbch

import static org.tbch.CollectionUtil.clearNulls

class ReportService {

    static transactional = true

    def seriousIncidentReportsBetween(Long campusId, Date startDate, Date endDate) {
        def c = SeriousIncidentReport.createCriteria()
        def list = c.listDistinct {
            between('date', startDate, endDate)
            // filter client records by campus..
            clientRecord {
                campus {
                    idEq(campusId)
                }
            }
            groupProperty("clientRecord")
        }
        ['instances': list]
    }


    /**
     * Build out the model for the status report.
     */
    def buildStatusModel(Long campusId, Date startDate, Date endDate) {
        def model = [:]
        def clients = ClientRecord.withCriteria {
            campus { idEq(campusId) }
            le('datePlacement', endDate)
            or {
                isNull('dischargeForm')
                dischargeForm {
                    or {
                        isNull('date')
                        gt('date', startDate)
                    }
                }   
            }
            //between('datePlacement', startDate, endDate) // le('datePlacement', endDate)
            join('aftercare')
            join('admissionAssessment')
        }

        // build sex..
        model.gender = buildmodel(clients, AdmissionAssessment.SEX) { type, client ->
            client.admissionAssessment?.sex == type
        }

        // build race
        model.race = buildmodel(clients, AdmissionAssessment.RACE) { type, client ->
            client.admissionAssessment?.race == type
        }

        // mediation status
        model.med = buildmodel(clients, AdmissionAssessment.MEDICATION_STATUS) { type, client ->
            client.admissionAssessment?.medicationStatus == type
        }

        // parent's status
        model.parents = buildmodel(clients, AdmissionAssessment.PARENTS_STATUS) { type, client ->
            client.admissionAssessment?.parentsStatus == type
        }

        // educational
        model.edu = buildmodel(clients, AdmissionAssessment.EDUCATIONAL_PLACEMENT) { type, client ->
            client.admissionAssessment?.educationalPlacement == type
        }

        // discharge reason
        model.discharge = buildmodel(clients, DischargeForm.TYPES) { type, client ->
            client.dischargeForm?.dischargeType == type
        }

        // create a unique set of grades, based on client records..
        def gradeLevels = clients.findAll({ it.gradeLevel != null}).collect { it.gradeLevel } as Set
        if (gradeLevels.isEmpty()) {
            // build aftercare grade level if there's nothing in grade levels..
            model.grade = buildmodel(clients, Aftercare.GRADE_LEVEL) { type, client ->
                client.aftercare?.grade == type
            }
        } else {
            model.grade = buildmodel(clients, gradeLevels) { type, client ->
                client.gradeLevel == type
            }
        }

        // managing conservator & adopted
        def mc = buildMc(campusId, startDate, endDate)
        model.mc = [mc.mc, []]
        model.adopted = [mc.adopted, []]
        
        model
    }

    /**
     * Based on the clients, types, and the filter provided determine the count.
     */
    def buildmodel(clients, types, filter) {
        Set clientSet = new HashSet(clients)
        def items = types.collect { type ->
            // pass the type to the closure
            def fltr = filter.clone().curry(type)
            // filter out the items
            def ctls = clientSet.findAll(fltr)
            clientSet.removeAll(ctls)
            ['name':type, 'total':ctls.size()]
        }
        [items, clientSet]
    }

    def buildMc(Long campusId, Date startDate, Date endDate) {
        def model = [:]
        def sigPersons = SignificantPerson.withCriteria {
            clientRecord {
                campus { 
                    idEq(campusId) 
                }
                le('datePlacement', endDate)
                or {
                    isNull('dischargeForm')
                    dischargeForm {
                        or {
                            isNull('date')
                            gt('date', startDate)
                        }
                    }
                }
            }
            eq('managingConservator', true)
        }
        def adopted = [] as Set
        def notAdopted = [] as Set
        def items = SignificantPerson.RELATIONSHIP.collect { type ->
            // filter out the items
            int total = 0
            sigPersons.each { SignificantPerson sigPerson ->
                if (sigPerson.relationship == 'Adoptive Parent') {
                    adopted << sigPerson.clientRecord
                } else {
                    notAdopted << sigPerson.clientRecord
                }
                total += sigPerson.relationship == type ? 1 : 0
            }
            ['name':type, 'total':total]
        }
        ['mc':items, 'adopted':[['name':'Yes', 'total':adopted.size()],['name':'No', 'total':notAdopted.size()]]]
    }

    def buildLivingUnits(Long campusId, Date startDate, Date endDate, transform) {
        // process each of the living units..
        def livingUnits = LivingUnit.list().collect { LivingUnit livingUnit ->
            // filter by campus ID
            if (livingUnit.program.campus.id != campusId) {
                return;
            }
            livingUnitRoaster(startDate, endDate, livingUnit, transform)
        }
        livingUnits = clearNulls(livingUnits)
    }

    private livingUnitRoaster(Date startDate, Date endDate, livingUnit, transform) {
        // find all the client based on the living unit..
        def criteria = {
            // query
            eq('livingUnit', livingUnit)
            le('datePlacement', endDate)
            or {
                isNull('dischargeForm')
                dischargeForm {
                    or {
                        isNull('date')
                        gt('date', startDate)
                    }
                }
            }
            // ordering
            order('lastName')
            // objects that are available GSP processing
            join('aftercare')
            join('livingUnit')
            join('admissionAssessment')
        }
        // query with a transform..
		
		def lst = ClientRecord.withCriteria(criteria)
		def clients = lst.collect(transform)

        // check if there's anything to do..
        if (clients.size() == 0) {
            return
        }

        // determine the sum of all the ages of the clients
        int totalAge = clients.sum { it.age }

        // determine the average number of dates in care..
        int totalCareDays = clients.sum {client ->
            (client.dateDischarge ?: new Date()) - client.datePlacement
        }

        // just take the average
        [
            'name': livingUnit.name,
            'clients':clients,
            'totalInCare':clients.size(),
            'totalAge': totalAge,
            'avgAge': totalAge/clients.size(),
            'totalCareDays':totalCareDays,
            'avgDaysInCare':totalCareDays/clients.size()
        ]
    }
}
