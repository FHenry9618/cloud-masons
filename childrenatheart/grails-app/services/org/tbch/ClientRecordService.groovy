package org.tbch

class ClientRecordService {
    // create a transaction..
    static transactional = true

    /**
     * Return the model for the client records.
     */
    def search(filter, offset, max, order, sort) {
        log.debug("Offset: ${offset}, Max: ${max}, Sort: ${sort}, Order: ${order}")
        log.debug "Filter --"
        log.debug "  Status: ${filter.status}"
        def q = {
            if (filter?.status?.size()) {
                'in'('status', filter.status)
            }
            if (filter.campus) {
                campus {
                    eq('id', filter.campus)
                }
            }
            if (filter.livingUnit) {
                livingUnit {
                    eq('id', filter.livingUnit)
                }
            }
        }
        // get the query results..
        def results = ClientRecord.createCriteria().list {
            q.delegate = delegate
            q.call()
            maxResults(max)
            firstResult(offset)
        }
        // get the count..
        def total = ClientRecord.createCriteria().count {
            q.delegate = delegate
            q.call()
        }
        def model =  ['results':results, 'total':total]
        return model
    }
}
