package org.tbch

import com.cloudmasons.BeanUtils
import com.cloudmasons.beans.Feed
import org.apache.commons.lang.StringUtils
/**
 * Simple service which create the default domain models for the application
 * when being initialized on a new database. 
 *
 */
class DefaultService {
    private static final String ROLE_FEED = "Roles"
    private static final String USER_FEED = "Users"

    private static final DEFAULT_FEED_FILES = [
            [ROLE_FEED, "roles.xls"],
            [USER_FEED, "users.xls"]
        ]
    
    def feedService
    def grailsApplication
    def securityService
   
    def createDefaultFeeds() {
        println("Initializing data feeds")

        def userFeed = feedService.createDefaultFeed(USER_FEED,
                                      'Feed to load system users and their assigned roles',
                                      'org.tbch.User',
                                      ['name', 'alias', 'email'])
        userFeed.addMapping('Password', 'passwd', 'passwordEncoder.encodePassword(value, null);')
        userFeed.addMapping('Enabled', 'enabled', 'value ? value : true')
        userFeed.addMapping('Roles', null, """
          if (value) {
             def roles = value.split(',')
             roles?.each { role ->
                user.addRole(role.trim())
             }
          }
        """);
        userFeed.save()
		  
    }

    public loadDefaultFeeds = {
        def feedDir = grailsApplication.config.feeds.dataDir ?: 'data'
        def envDir =  grailsApplication.config.feeds.envSubDir ?: 'test'  
        
        DEFAULT_FEED_FILES.each { feed->
            def feedName = feed[0]
            // First check for the file in the environment specific directory
            // if not there check to see if it isn the feed directory
            def feedFile = new File("${feedDir}/${envDir}/${feed[1]}")
            if (!feedFile.exists()) {
               feedFile = new File("${feedDir}/${feed[1]}")
            }
            try {
                Feed f = Feed.findByName(feedName);
                if (f) {
                    String result = feedService.processFileFeed(f, feedFile)
                    println result
                }
            } catch (Throwable t) {
                println "Error processing ${feedName}: ${t}"
            }
        }
    }

    def addServicePlanEntry(Long id, String associated, ServicePlanEntry spe) {
        // get the plan of service domain class
        def pos = PlanOfService.get(id)
        def beanDomainClass = BeanUtils.resolveDomainClass(grailsApplication, pos)
        
        // get associated property type
        def associatedProperty = beanDomainClass.getPropertyByName(associated)
        def associatedDomainClass = associatedProperty.getReferencedDomainClass()
        
        // create a new instance of the medical/dental/etc..
        def inst = associatedDomainClass.newInstance()
        inst.servicePlanEntry = spe
        
        pos.('addTo' + StringUtils.capitalize(associated))(inst)
        pos.save(failOnError:true)

        inst
    }

    /**
     * Transaction to clone the plan of service.
     */
    def clonePlanOfService(Long id) {
        def pos = PlanOfService.get(id)
        def clientRecord = pos.clientRecord
        if (!securityService.isAuthorized(PlanOfService, 'create', pos)) {
            render "Not Authorized"
            return
        }

        def clone = new PlanOfService(pos.properties)
        clone.dateCreated = null
        clone.lastUpdated = null
        clone.signaturePage = null
        clone.editable = true

        PlanOfService.PLANS.each { name ->
            clone."${name}Plans" = null
            String methodName = name[0].toUpperCase()+name.substring(1,name.size())+'Plans'
            pos."${name}Plans"?.each { plan ->
                if (plan) {
                    def planClone = plan.class.newInstance()
                    planClone.dateCreated = null
                    planClone.lastUpdated = null
                    planClone.servicePlanEntry = new ServicePlanEntry()
                    planClone.servicePlanEntry.date = plan.servicePlanEntry.date
                    planClone.servicePlanEntry.responsibleParty = plan.servicePlanEntry.responsibleParty
                    planClone.servicePlanEntry.targetDate = plan.servicePlanEntry.targetDate
                    planClone.servicePlanEntry.scale = plan.servicePlanEntry.scale
                    planClone.servicePlanEntry.dateResolved = plan.servicePlanEntry.dateResolved
                    planClone.servicePlanEntry.goals = plan.servicePlanEntry.goals
                    planClone.servicePlanEntry.strategies = plan.servicePlanEntry.strategies

                    clone.('addTo'+ methodName)(planClone)
                }
            }
        }

        clone.date = new Date()
        clone.reportingPeriodStart = new Date()
        clone.reportingPeriodEnd = new Date()

        clientRecord.addToPlanOfService(clone)
        clientRecord.save(failOnError:true)

        return clone
    }
}
