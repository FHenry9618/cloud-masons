package org.tbch

import com.cloudmasons.domain.SecureFile

import java.util.Properties
import com.cloudmasons.BeanUtils

import org.codehaus.groovy.grails.commons.ConfigurationHolder as CH
import org.codehaus.groovy.grails.plugins.web.taglib.ApplicationTagLib

class TbchService {

    static transactional = false

    def authenticateService
    def grailsApplication
    def mailService

    def processDocument(def file, long clientRecordId, String type, String description) {
        def clientRecord = ClientRecord.get(clientRecordId)
        if (clientRecord == null) {
            throw new IllegalArgumentException("No client record.")
        }
        if (file == null) {
            throw new IllegalArgumentException("No file to save.")
        }
        def storagePath = grailsApplication.config.uploadedFile.storage.path ?: '/tmp/'
        // create a new document..
        Document doc = new Document()
        def fileSize = file.getSize()
        def fileName = file.getOriginalFilename()
        doc.properties = [
                name: fileName,
                originalFilename: fileName,
                description: description,
                size:fileSize,
                storageType: SecureFile.LOCAL,
                encryptedName: fileName,
                encryptedSize: fileSize,
                encryptionKey: 'n/a',
                path: storagePath,
                secondaryStorageType: 'S3',
                secondaryPath: 'bucket1234',
                status: 'COPY_TO_SECONDARY']
        doc.type = type

        // get the database id for the file
        clientRecord.addToDocuments(doc)
        clientRecord.save(flush: true)

        // create a new file name based on the id
        doc.encryptedName = "${doc.id}-document"
        doc.save(flush: true)

        // save the document to the file system..
        file.transferTo(new File("${storagePath}/${doc.encryptedName}"))
    }

    public User getLoggedInUser() {
        def userPrincipal = authenticateService.principal()
        return userPrincipal?.domainClass
    }

    public DischargeForm generateDischargeFormFromPlanOfService(PlanOfService plan) {
        def form = new DischargeForm()
        def ClonedPlan = plan.clone()
        form.properties = ClonedPlan.properties
        form.date = new Date()

        form.dischargePlanningPerson = '<<Input Here>>'
        form.dischargePlanningInvitee = '<<Input Here>>'
        form.dischargerName= '<<Input Here>>'
        form.dischargerRelationship= '<<Input Here>>'
        form.dischargerAddress1= '<<Input Here>>'
        form.dischargerCity= '<<Input Here>>'
        form.dischargerState= '<<Input Here>>'
        form.dischargerZip= '<<Input Here>>'
        form.dischargerNumber='<<Input Here>>'

        form.dischargeCircumstance='<<Input Here>>'
        form.childNotified=new Date()
        form.childCareAdmin='<<Input Here>>'
        form.justification='<<Input Here>>'

        form.medication='<<Input Here>>'
        form.supportResources='<<Input Here>>'

        form.aftercareRecommendation='<<Input Here>>'

        PlanOfService.PLANS.each { name ->
            form."${name}RemainingNeeds"='<<Input Here>>'
            String prefix = name[0].toUpperCase()+name.substring(1,name.size())
            String className = "${prefix}DischargeRecord"
            def domainClass = BeanUtils.resolveDomainClass(grailsApplication,className)
            ClonedPlan."${name}Plans"?.each { entry ->
                def record = domainClass.newInstance()
                record.properties = entry.properties
                record.parentObject = form
                form."addTo${prefix}Records"(record)
            }
        }
        return form
    }

    def validateFormData(def beanInstance, def beanDomainClass) {
        def error = []
        //def beanDomainClass = BeanUtils.resolveDomainClass(grailsApplication, 'MonthlyNotes')
        //long id = params.id?.toLong()
        //def beanInstance = beanDomainClass.clazz.get(id)

        def constrainedProperties = beanDomainClass.constrainedProperties
        //println constrainedProperties
        constrainedProperties?.each {
            //println it
            def cp = constrainedProperties[it.key]

            //println beanInstance."${it.key}"
            if (cp?.getAppliedConstraint('renderOptions')?.renderOptions?.validate) {
                def closure = cp?.getAppliedConstraint('renderOptions')?.renderOptions?.defaultValue
                def expectedValue = null
                if (beanInstance."${it.key}" && closure) {
                    closure.resolveStrategy = Closure.DELEGATE_FIRST
                    closure.delegate = beanInstance
                    expectedValue = closure();
                }
                if (expectedValue == beanInstance."${it.key}") {
                    error.add("${it.key}")
                }
            }
        }
        return error
    }

    /**
     * Send a serious incident report notification.
     */
    def sendSeriousIncidentReport(seriousIncidentReport) {
        def mailConf = CH.config.grails.mail
        def sirId = seriousIncidentReport.id
        try {
            mailService.sendMail {
                to mailConf.default.to
                from mailConf.default.from
                subject "New serious incident report created for client - ${seriousIncidentReport.name}"
                html g().render(template:'/seriousIncidentReport/notification', model:[id:sirId])
            }
            log.info("Sent SIR mail for ID: " + sirId)
        } catch (Throwable th) {
            log.error('Failed to send email.', th)
        }
    }

    def g() {
        grailsApplication.mainContext.getBean(ApplicationTagLib)
    }

    def processSignatureDocument(def file, def beanInstance, String description) {
        if (file == null) {
            throw new IllegalArgumentException("No file to save.")
        }
        // refresh the instance to make sure its attached..
        beanInstance.refresh()
        def storagePath = grailsApplication.config.uploadedFile.storage.path ?: '/tmp/'
        // create a new document..
        SignatureDoc doc = new SignatureDoc()
        def fileSize = file.getSize()
        def fileName = file.getOriginalFilename()
        doc.properties = [
                name: fileName,
                originalFilename: fileName,
                description: description,
                size:fileSize,
                storageType: SecureFile.LOCAL,
                encryptedName: fileName,
                encryptedSize: fileSize,
                encryptionKey: 'n/a',
                path: storagePath,
                secondaryStorageType: 'S3',
                secondaryPath: 'bucket1234',
                status: 'COPY_TO_SECONDARY']

        // get the database id for the file
        doc.assoicatedForm = beanInstance
        beanInstance.signaturePage = doc.save(flush:true)
        beanInstance.editable = false
        beanInstance.save(flush:true)

        // create a new file name based on the id
        doc.encryptedName = "${doc.id}-signature_doc"
        doc.save(flush: true)

        // save the document to the file system..
        file.transferTo(new File("${storagePath}/${doc.encryptedName}"))
    }
}



