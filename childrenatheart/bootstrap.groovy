import org.tbch.*


def authenticateService = ctx.authenticateService

def user = new User()
user.alias = 'wdroste'
user.name = 'Will Droste'
user.email = 'will@nsquard.biz'
user.passwdResetKey = 'changeme'
user.passwd = authenticateService.encodePassword('changeme')
user.passwdResetKeyExpiration = new Date() + 365
Role.list().each { it -> user.addToRoles(it) }
user.save(onErrorFail:true)


user = new User()
user.alias = 'ahamilton'
user.name = 'Alex Hamilton'
user.email = 'Alex.Hamilton@miraclefarm.org'
user.passwdResetKey = 'changeme'
user.passwd = authenticateService.encodePassword('changeme')
user.passwdResetKeyExpiration = new Date() + 365
Role.list().each { it -> user.addToRoles(it) }
user.save(onErrorFail:true)


user = new User()
user.alias = 'bbarbieri'
user.name = 'Bettie Barbieri'
user.email = 'bettiebarbieri@hendrickhome.org'
user.passwdResetKey = 'changeme'
user.passwd = authenticateService.encodePassword('changeme')
user.passwdResetKeyExpiration = new Date() + 365
Role.list().each { it -> user.addToRoles(it) }
user.save(onErrorFail:true)


