-- MySQL dump 10.13  Distrib 5.5.49, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: scribe
-- ------------------------------------------------------
-- Server version	5.5.49-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activity`
--

DROP TABLE IF EXISTS `activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activity` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `action` varchar(255) DEFAULT NULL,
  `controller` varchar(255) DEFAULT NULL,
  `detail` varchar(255) DEFAULT NULL,
  `object_class` varchar(255) NOT NULL,
  `object_id` bigint(20) NOT NULL,
  `object_name` varchar(255) DEFAULT NULL,
  `params` longtext,
  `session_id` varchar(255) DEFAULT NULL,
  `timestamp` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `addendum_restraint_report`
--

DROP TABLE IF EXISTS `addendum_restraint_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `addendum_restraint_report` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `all_attempt` longtext,
  `alternative_strategy` longtext,
  `bite` bit(1) NOT NULL,
  `caregiver_action` longtext,
  `child_reaction` longtext,
  `choking` bit(1) NOT NULL,
  `clearing` bit(1) NOT NULL,
  `clothing_grab` bit(1) NOT NULL,
  `date` datetime NOT NULL,
  `date_created` datetime NOT NULL,
  `discussion` bit(1) NOT NULL,
  `discussion_date` datetime DEFAULT NULL,
  `discussion_staff` varchar(255) DEFAULT NULL,
  `discussion_time` varchar(255) DEFAULT NULL,
  `distracted` bit(1) NOT NULL,
  `fingernail_grab` bit(1) NOT NULL,
  `hair_pull` bit(1) NOT NULL,
  `hand_grab` bit(1) NOT NULL,
  `involved_staff` varchar(255) DEFAULT NULL,
  `last_updated` datetime NOT NULL,
  `length` varchar(255) DEFAULT NULL,
  `monitoring_staff` varchar(255) DEFAULT NULL,
  `no_resistance` bit(1) NOT NULL,
  `offer_discussion` bit(1) NOT NULL,
  `offer_discussion_date` datetime DEFAULT NULL,
  `offer_discussion_staff` varchar(255) DEFAULT NULL,
  `offer_discussion_time` varchar(255) DEFAULT NULL,
  `other` bit(1) NOT NULL,
  `other_danger` bit(1) NOT NULL,
  `other_strategy` longtext,
  `precipatating_description` longtext,
  `private_conversation` bit(1) NOT NULL,
  `quiet` bit(1) NOT NULL,
  `responsible_staff` varchar(255) DEFAULT NULL,
  `self_danger` bit(1) NOT NULL,
  `standing_hold` bit(1) NOT NULL,
  `standing_hold_multiple` bit(1) NOT NULL,
  `suggestion` bit(1) NOT NULL,
  `sustained_injury` longtext,
  `time` varchar(255) NOT NULL,
  `verbal_redirection` bit(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `admission_assessment`
--

DROP TABLE IF EXISTS `admission_assessment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admission_assessment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `abuse_history` longtext,
  `address1` varchar(255) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `admission_date` datetime DEFAULT NULL,
  `admission_documentation` longtext,
  `allergies` longtext,
  `assessment_recommendation` longtext,
  `behavior` longtext,
  `behavior_management` longtext,
  `birth_history` longtext,
  `chronic_health_conditions` longtext,
  `city` varchar(255) DEFAULT NULL,
  `client_id` varchar(255) NOT NULL,
  `court_order` bit(1) NOT NULL,
  `court_order_copy` bit(1) NOT NULL,
  `criminal_history` longtext,
  `date` datetime DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `determination` longtext,
  `developmental_history` longtext,
  `developmental_level` longtext,
  `editable` bit(1) NOT NULL,
  `educational_level` longtext,
  `first_name` varchar(255) NOT NULL,
  `history_determination` longtext,
  `history_documentation` longtext,
  `history_rationale` longtext,
  `home_environment` longtext,
  `immediate_goal` longtext,
  `known_contraindications` longtext,
  `last_name` varchar(255) NOT NULL,
  `last_updated` datetime NOT NULL,
  `legal_status` longtext,
  `level_documentation` longtext,
  `managing_conservator_info` longtext,
  `medical_dental_status` longtext,
  `medication` longtext,
  `mental_abuse_history` longtext,
  `mental_abuse_status` longtext,
  `middle_initial` varchar(255) DEFAULT NULL,
  `parent` varchar(255) DEFAULT NULL,
  `parent_expectation` longtext,
  `person_conduct_assessment` longtext,
  `placement_history` longtext,
  `placement_understanding` longtext,
  `pre_initial_service_plan_info` longtext,
  `race` varchar(15) DEFAULT NULL,
  `rationale` longtext,
  `referral_reason` longtext,
  `referred_by` longtext,
  `school_history` longtext,
  `services` longtext,
  `sex` varchar(6) NOT NULL,
  `sibling_info` longtext,
  `signature_page_id` bigint(20) DEFAULT NULL,
  `skill` longtext,
  `social_history` longtext,
  `social_security_number` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `treatment_identification` longtext,
  `zip` varchar(255) DEFAULT NULL,
  `educational_placement` varchar(14) DEFAULT NULL,
  `legal_status_type` varchar(128) DEFAULT NULL,
  `medication_status` varchar(32) DEFAULT NULL,
  `parents_status` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `client_id` (`client_id`),
  KEY `FKB10F44B82751D7F5` (`signature_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `aftercare`
--

DROP TABLE IF EXISTS `aftercare`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aftercare` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `address1` varchar(255) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `circumstances_for_discharge` longtext,
  `city` varchar(255) DEFAULT NULL,
  `continue_counseling` bit(1) NOT NULL,
  `counseling` bit(1) NOT NULL,
  `date_created` datetime NOT NULL,
  `dicharge_type` varchar(255) DEFAULT NULL,
  `discharge_concern` longtext,
  `discharge_date` datetime DEFAULT NULL,
  `discharge_time` longtext,
  `editable` bit(1) NOT NULL,
  `first_follow_up_case_manger` varchar(255) DEFAULT NULL,
  `first_follow_up_date` datetime DEFAULT NULL,
  `first_follow_up_notes` longtext,
  `follow_up_issue` longtext,
  `further_counseling` bit(1) NOT NULL,
  `grade` varchar(255) DEFAULT NULL,
  `last_updated` datetime NOT NULL,
  `medication` bit(1) NOT NULL,
  `medication_reason` longtext,
  `name` varchar(255) NOT NULL,
  `other_follow_up_case_manger` varchar(255) DEFAULT NULL,
  `other_follow_up_date` datetime DEFAULT NULL,
  `other_follow_up_notes` longtext,
  `parent` varchar(255) DEFAULT NULL,
  `parent_address1` varchar(255) DEFAULT NULL,
  `parent_address2` varchar(255) DEFAULT NULL,
  `parent_city` varchar(255) DEFAULT NULL,
  `parent_home_phone_number` varchar(255) DEFAULT NULL,
  `parent_state` varchar(255) DEFAULT NULL,
  `parent_work_phone_number` varchar(255) DEFAULT NULL,
  `parent_zip` varchar(255) DEFAULT NULL,
  `phone_number` longtext,
  `placement_at_discharge` varchar(255) DEFAULT NULL,
  `placement_date` datetime DEFAULT NULL,
  `progress` longtext,
  `reason_for_placement` longtext,
  `relationship` varchar(15) DEFAULT NULL,
  `second_follow_up_case_manger` varchar(255) DEFAULT NULL,
  `second_follow_up_date` datetime DEFAULT NULL,
  `second_follow_up_notes` longtext,
  `service_type` longtext,
  `special_service` bit(1) NOT NULL,
  `state` varchar(255) DEFAULT NULL,
  `third_follow_up_case_manger` varchar(255) DEFAULT NULL,
  `third_follow_up_date` datetime DEFAULT NULL,
  `third_follow_up_notes` longtext,
  `zip` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `campus`
--

DROP TABLE IF EXISTS `campus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campus` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `case_notes`
--

DROP TABLE IF EXISTS `case_notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `case_notes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `client_record_id` bigint(20) NOT NULL,
  `date` datetime NOT NULL,
  `date_created` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  `name` varchar(255) NOT NULL,
  `person_contacted` varchar(255) NOT NULL,
  `case_notes_idx` int(11) DEFAULT NULL,
  `attempted_contact` bit(1) DEFAULT NULL,
  `narrative` longtext,
  `type_of_contact` varchar(255) DEFAULT NULL,
  `editable` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK2D4E1A62E613CB3` (`client_record_id`),
  KEY `FKD7B008F22E6132A2` (`client_record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `client_record`
--

DROP TABLE IF EXISTS `client_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client_record` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `admission_assessment_id` bigint(20) DEFAULT NULL,
  `aftercare_id` bigint(20) DEFAULT NULL,
  `campus_id` bigint(20) DEFAULT NULL,
  `date_application` datetime DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `date_discharge` datetime DEFAULT NULL,
  `date_of_birth` datetime DEFAULT NULL,
  `date_placement` datetime DEFAULT NULL,
  `date_referral` datetime DEFAULT NULL,
  `discharge_form_id` bigint(20) DEFAULT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `last_updated` datetime NOT NULL,
  `living_unit_id` bigint(20) DEFAULT NULL,
  `middle_initial` varchar(255) DEFAULT NULL,
  `preliminary_service_plan_id` bigint(20) DEFAULT NULL,
  `reason_for_call` longtext,
  `referral_out` bit(1) NOT NULL,
  `referral_reason` varchar(34) DEFAULT NULL,
  `referred_by` longtext,
  `status` varchar(11) NOT NULL,
  `level_of_care` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK1F8AAB45610C886B` (`campus_id`),
  KEY `FK1F8AAB451DE4006E` (`discharge_form_id`),
  KEY `FK1F8AAB4595B311E9` (`aftercare_id`),
  KEY `FK1F8AAB457FD9847E` (`living_unit_id`),
  KEY `FK1F8AAB453611523C` (`admission_assessment_id`),
  KEY `FK1F8AAB45CF7ADED7` (`preliminary_service_plan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `discharge_form`
--

DROP TABLE IF EXISTS `discharge_form`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `discharge_form` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `aftercare_recommendation` longtext NOT NULL,
  `behavioral_needs` longtext,
  `behavioral_remaining_needs` longtext NOT NULL,
  `child_care_admin` varchar(255) NOT NULL,
  `child_notified` datetime NOT NULL,
  `community_needs` longtext,
  `community_remaining_needs` longtext NOT NULL,
  `cultural_needs` longtext,
  `cultural_remaining_needs` longtext NOT NULL,
  `date` datetime NOT NULL,
  `date_created` datetime NOT NULL,
  `dental_needs` longtext,
  `dental_remaining_needs` longtext NOT NULL,
  `developmental_needs` longtext,
  `developmental_remaining_needs` longtext NOT NULL,
  `discharge_circumstance` longtext NOT NULL,
  `discharge_planning_invitee` varchar(255) NOT NULL,
  `discharge_planning_person` varchar(255) NOT NULL,
  `discharger_address1` varchar(255) NOT NULL,
  `discharger_address2` varchar(255) DEFAULT NULL,
  `discharger_city` varchar(255) NOT NULL,
  `discharger_name` varchar(255) NOT NULL,
  `discharger_number` varchar(255) NOT NULL,
  `discharger_relationship` varchar(255) NOT NULL,
  `discharger_state` varchar(255) NOT NULL,
  `discharger_zip` varchar(255) NOT NULL,
  `editable` bit(1) NOT NULL,
  `educational_needs` longtext,
  `educational_remaining_needs` longtext NOT NULL,
  `familial_needs` longtext,
  `familial_remaining_needs` longtext NOT NULL,
  `health_education` longtext,
  `independent_living` longtext,
  `intellectual_needs` longtext,
  `intellectual_remaining_needs` longtext NOT NULL,
  `justification` longtext NOT NULL,
  `last_updated` datetime NOT NULL,
  `medical_needs` longtext,
  `medical_remaining_needs` longtext NOT NULL,
  `medication` longtext NOT NULL,
  `name` varchar(255) NOT NULL,
  `signature_page_id` bigint(20) DEFAULT NULL,
  `social_needs` longtext,
  `social_remaining_needs` longtext NOT NULL,
  `support_resources` longtext NOT NULL,
  `therapeutic_needs` longtext,
  `therapeutic_remaining_needs` longtext NOT NULL,
  `discharge_type` varchar(29) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKC00EEA212751D7F5` (`signature_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `document`
--

DROP TABLE IF EXISTS `document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `client_record_id` bigint(20) NOT NULL,
  `date_created` datetime NOT NULL,
  `description` varchar(255) NOT NULL,
  `encrypted_name` varchar(255) NOT NULL,
  `encrypted_size` bigint(20) NOT NULL,
  `encryption_key` varchar(255) NOT NULL,
  `last_updated` datetime NOT NULL,
  `name` varchar(255) NOT NULL,
  `original_filename` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  `secondary_path` varchar(255) NOT NULL,
  `secondary_storage_type` varchar(255) NOT NULL,
  `size` bigint(20) NOT NULL,
  `status` varchar(255) NOT NULL,
  `storage_type` varchar(255) NOT NULL,
  `type` varchar(200) NOT NULL,
  `documents_idx` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK335CD11B2E6132A2` (`client_record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `feed`
--

DROP TABLE IF EXISTS `feed`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feed` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `bean_type` varchar(255) NOT NULL,
  `correlator` longtext NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `record_post_processor` longtext,
  `record_pre_processor` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `feed_mapping`
--

DROP TABLE IF EXISTS `feed_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feed_mapping` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `feed_id` bigint(20) NOT NULL,
  `mappings_idx` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKD933272DAD723DB6` (`feed_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `feed_mapping_source`
--

DROP TABLE IF EXISTS `feed_mapping_source`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feed_mapping_source` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `field_mapping_id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `transform` longtext,
  `sources_idx` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKF3D8416DC9A3B1C1` (`field_mapping_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `feed_mapping_target`
--

DROP TABLE IF EXISTS `feed_mapping_target`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feed_mapping_target` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `field_mapping_id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `reference_resolver` varchar(255) NOT NULL,
  `referenced_bean_type` varchar(255) DEFAULT NULL,
  `referenced_property_name` varchar(255) DEFAULT NULL,
  `transform` longtext,
  `targets_idx` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKF4C64AE3C9A3B1C1` (`field_mapping_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `living_unit`
--

DROP TABLE IF EXISTS `living_unit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `living_unit` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `capacity` int(11) NOT NULL,
  `program_id` bigint(20) NOT NULL,
  `unit_name` varchar(255) NOT NULL,
  `living_units_idx` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK65A3915ABB2A5E49` (`program_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `living_unit_utilization`
--

DROP TABLE IF EXISTS `living_unit_utilization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `living_unit_utilization` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `date_created` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  `living_unit_id` bigint(20) NOT NULL,
  `utilization` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKDDD6E6BD7FD9847E` (`living_unit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `monthly_notes`
--

DROP TABLE IF EXISTS `monthly_notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `monthly_notes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `client_record_id` bigint(20) NOT NULL,
  `date` datetime NOT NULL,
  `date_created` datetime NOT NULL,
  `editable` bit(1) NOT NULL,
  `last_updated` datetime NOT NULL,
  `name` varchar(255) NOT NULL,
  `outstanding_issues` longtext,
  `significant_contacts` longtext,
  `summary_of_progress` longtext,
  `monthly_notes_idx` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK4759440F2E6132A2` (`client_record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `parent_survey`
--

DROP TABLE IF EXISTS `parent_survey`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parent_survey` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `admission_process_rating` int(11) NOT NULL,
  `comment` longtext,
  `communication_rating` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `family_weekend_rating` int(11) NOT NULL,
  `last_updated` datetime NOT NULL,
  `placement_care_rating` int(11) NOT NULL,
  `placement_time_rating` int(11) NOT NULL,
  `plan_of_service_rating` int(11) NOT NULL,
  `safe_place_rating` int(11) NOT NULL,
  `social_skill_rating` int(11) NOT NULL,
  `spirtual_need_rating` int(11) NOT NULL,
  `visit_rating` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `plan_entry`
--

DROP TABLE IF EXISTS `plan_entry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plan_entry` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `date_created` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  `service_plan_entry_id` bigint(20) DEFAULT NULL,
  `class` varchar(255) NOT NULL,
  `parent_object_id` bigint(20) DEFAULT NULL,
  `behavioral_plans_idx` int(11) DEFAULT NULL,
  `community_plans_idx` int(11) DEFAULT NULL,
  `cultural_plans_idx` int(11) DEFAULT NULL,
  `dental_plans_idx` int(11) DEFAULT NULL,
  `developmental_plans_idx` int(11) DEFAULT NULL,
  `educational_plans_idx` int(11) DEFAULT NULL,
  `familial_plans_idx` int(11) DEFAULT NULL,
  `intellectual_plans_idx` int(11) DEFAULT NULL,
  `medical_plans_idx` int(11) DEFAULT NULL,
  `social_plans_idx` int(11) DEFAULT NULL,
  `therapeutic_plans_idx` int(11) DEFAULT NULL,
  `behavioral_records_idx` int(11) DEFAULT NULL,
  `community_records_idx` int(11) DEFAULT NULL,
  `cultural_records_idx` int(11) DEFAULT NULL,
  `dental_records_idx` int(11) DEFAULT NULL,
  `developmental_records_idx` int(11) DEFAULT NULL,
  `educational_records_idx` int(11) DEFAULT NULL,
  `familial_records_idx` int(11) DEFAULT NULL,
  `intellectual_records_idx` int(11) DEFAULT NULL,
  `medical_records_idx` int(11) DEFAULT NULL,
  `social_records_idx` int(11) DEFAULT NULL,
  `therapeutic_records_idx` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK2C1F33DC951A013B` (`parent_object_id`),
  KEY `FK2C1F33DC39FA04A` (`parent_object_id`),
  KEY `FK2C1F33DC54E621B9` (`service_plan_entry_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `plan_of_service`
--

DROP TABLE IF EXISTS `plan_of_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plan_of_service` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `behavioral_needs` longtext,
  `case_manager` varchar(255) DEFAULT NULL,
  `circumstances` longtext,
  `client_record_id` bigint(20) NOT NULL,
  `community_needs` longtext,
  `continued_placement_reason` longtext,
  `cultural_needs` longtext,
  `date` datetime NOT NULL,
  `date_created` datetime NOT NULL,
  `dental_needs` longtext,
  `developmental_needs` longtext,
  `discharge_plan` longtext,
  `editable` bit(1) NOT NULL,
  `educational_needs` longtext,
  `expected_placement_outcome` longtext,
  `familial_needs` longtext,
  `health_education` longtext,
  `independent_living` longtext,
  `intellectual_needs` longtext,
  `intervention_evaluation` longtext,
  `last_updated` datetime NOT NULL,
  `managing_conservator` varchar(255) DEFAULT NULL,
  `medical_needs` longtext,
  `medication_evaluation` longtext,
  `name` varchar(255) NOT NULL,
  `reporting_period_end` datetime NOT NULL,
  `reporting_period_start` datetime NOT NULL,
  `safety_plan` longtext,
  `signature_page_id` bigint(20) DEFAULT NULL,
  `social_needs` longtext,
  `supervision_level` longtext,
  `therapeutic_needs` longtext,
  `trip_plans` longtext,
  `type` varchar(14) NOT NULL,
  `plan_of_service_idx` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK8A581B232E6132A2` (`client_record_id`),
  KEY `FK8A581B232751D7F5` (`signature_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `preliminary_service_plan`
--

DROP TABLE IF EXISTS `preliminary_service_plan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `preliminary_service_plan` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `behavioral_needs` longtext,
  `behavioral_person` varchar(255) DEFAULT NULL,
  `behavioral_strategy` longtext,
  `date` datetime NOT NULL,
  `date_created` datetime NOT NULL,
  `developed_by` varchar(255) NOT NULL,
  `editable` bit(1) NOT NULL,
  `educational_needs` longtext,
  `educational_person` varchar(255) DEFAULT NULL,
  `educational_strategy` longtext,
  `familial_needs` longtext,
  `familial_person` varchar(255) DEFAULT NULL,
  `familial_strategy` longtext,
  `last_updated` datetime NOT NULL,
  `name` varchar(255) NOT NULL,
  `physical_needs` longtext,
  `physical_person` varchar(255) DEFAULT NULL,
  `physical_strategy` longtext,
  `signature_page_id` bigint(20) DEFAULT NULL,
  `social_needs` longtext,
  `social_person` varchar(255) DEFAULT NULL,
  `social_strategy` longtext,
  PRIMARY KEY (`id`),
  KEY `FK5FB121DC2751D7F5` (`signature_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `program`
--

DROP TABLE IF EXISTS `program`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `program` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `campus_id` bigint(20) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `programs_idx` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKED8F1E84610C886B` (`campus_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `report`
--

DROP TABLE IF EXISTS `report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `config_script` longtext,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `role` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `role_users`
--

DROP TABLE IF EXISTS `role_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_users` (
  `role_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`role_id`,`user_id`),
  KEY `FK6CF7C0FF1398DD4B` (`role_id`),
  KEY `FK6CF7C0FFB8C3A12B` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `serious_incident_report`
--

DROP TABLE IF EXISTS `serious_incident_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `serious_incident_report` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `absent` bit(1) NOT NULL,
  `action_explanation` longtext,
  `administration` varchar(255) DEFAULT NULL,
  `allegation` bit(1) NOT NULL,
  `assessment_recommended` bit(1) NOT NULL,
  `assessment_recommended_date` datetime DEFAULT NULL,
  `assessment_recommended_time` varchar(255) DEFAULT NULL,
  `case_manager` bit(1) NOT NULL,
  `case_manager_contacted` varchar(255) DEFAULT NULL,
  `case_manager_notified_date` datetime DEFAULT NULL,
  `case_manager_notified_time` varchar(255) DEFAULT NULL,
  `client_record_id` bigint(20) NOT NULL,
  `cps` bit(1) NOT NULL,
  `cps_contacted` varchar(255) DEFAULT NULL,
  `cps_notified_date` datetime DEFAULT NULL,
  `cps_notified_time` varchar(255) DEFAULT NULL,
  `criminal_behavior` bit(1) NOT NULL,
  `date` datetime NOT NULL,
  `date_created` datetime NOT NULL,
  `death` bit(1) NOT NULL,
  `destruction` bit(1) NOT NULL,
  `editable` bit(1) NOT NULL,
  `first_aid_administered` bit(1) NOT NULL,
  `first_aid_administered_date` datetime DEFAULT NULL,
  `first_aid_administered_time` varchar(255) DEFAULT NULL,
  `gender` varchar(6) NOT NULL,
  `injury_hospitalization` bit(1) NOT NULL,
  `injury_to_client` bit(1) NOT NULL,
  `injury_to_staff` bit(1) NOT NULL,
  `last_updated` datetime NOT NULL,
  `licensing` bit(1) NOT NULL,
  `licensing_contacted` varchar(255) DEFAULT NULL,
  `licensing_notified_date` datetime DEFAULT NULL,
  `licensing_notified_time` varchar(255) DEFAULT NULL,
  `licensing_report` varchar(255) DEFAULT NULL,
  `medical_treatment` bit(1) NOT NULL,
  `medical_treatment_date` datetime DEFAULT NULL,
  `medical_treatment_time` varchar(255) DEFAULT NULL,
  `medication_error` bit(1) NOT NULL,
  `name` varchar(255) NOT NULL,
  `other` bit(1) NOT NULL,
  `other_contacted` varchar(255) DEFAULT NULL,
  `other_detail` varchar(255) DEFAULT NULL,
  `other_notified` bit(1) NOT NULL,
  `other_notified_date` datetime DEFAULT NULL,
  `other_notified_time` varchar(255) DEFAULT NULL,
  `outcome` longtext,
  `parent` bit(1) NOT NULL,
  `parent_contacted` varchar(255) DEFAULT NULL,
  `parent_notified_date` datetime DEFAULT NULL,
  `parent_notified_time` varchar(255) DEFAULT NULL,
  `physical_aggression` bit(1) NOT NULL,
  `police` bit(1) NOT NULL,
  `police_contacted` varchar(255) DEFAULT NULL,
  `police_notified_date` datetime DEFAULT NULL,
  `police_notified_time` varchar(255) DEFAULT NULL,
  `police_report` varchar(255) DEFAULT NULL,
  `reporting_date` datetime DEFAULT NULL,
  `reporting_staff` varchar(255) DEFAULT NULL,
  `restraint` bit(1) NOT NULL,
  `restraint_report_id` bigint(20) DEFAULT NULL,
  `self_injury` bit(1) NOT NULL,
  `sexual` bit(1) NOT NULL,
  `signature_page_id` bigint(20) DEFAULT NULL,
  `staff_involved` longtext,
  `suicidal_gesture` bit(1) NOT NULL,
  `suicidal_ideation` bit(1) NOT NULL,
  `suicide_attempt` bit(1) NOT NULL,
  `summary` longtext,
  `supervisor` bit(1) NOT NULL,
  `supervisor_contacted` varchar(255) DEFAULT NULL,
  `supervisor_notified_date` datetime DEFAULT NULL,
  `supervisor_notified_time` varchar(255) DEFAULT NULL,
  `theft` bit(1) NOT NULL,
  `therapist` bit(1) NOT NULL,
  `therapist_contacted` varchar(255) DEFAULT NULL,
  `therapist_notified_date` datetime DEFAULT NULL,
  `therapist_notified_time` varchar(255) DEFAULT NULL,
  `time` varchar(255) NOT NULL,
  `verbal_aggression` bit(1) NOT NULL,
  `violation` bit(1) NOT NULL,
  `serious_incident_report_idx` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK1D4E1A62E6132A2` (`client_record_id`),
  KEY `FK1D4E1A62751D7F5` (`signature_page_id`),
  KEY `FK1D4E1A64E983DF0` (`restraint_report_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `service_plan_entry`
--

DROP TABLE IF EXISTS `service_plan_entry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_plan_entry` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `date` datetime NOT NULL,
  `date_created` datetime NOT NULL,
  `date_resolved` datetime DEFAULT NULL,
  `goals` longtext,
  `last_updated` datetime NOT NULL,
  `responsible_party` varchar(255) DEFAULT NULL,
  `scale` int(11) NOT NULL,
  `strategies` longtext,
  `target_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `signature_doc`
--

DROP TABLE IF EXISTS `signature_doc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `signature_doc` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `date_created` datetime NOT NULL,
  `description` varchar(255) NOT NULL,
  `encrypted_name` varchar(255) NOT NULL,
  `encrypted_size` bigint(20) NOT NULL,
  `encryption_key` varchar(255) NOT NULL,
  `last_updated` datetime NOT NULL,
  `name` varchar(255) NOT NULL,
  `original_filename` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  `secondary_path` varchar(255) NOT NULL,
  `secondary_storage_type` varchar(255) NOT NULL,
  `size` bigint(20) NOT NULL,
  `status` varchar(255) NOT NULL,
  `storage_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `significant_person`
--

DROP TABLE IF EXISTS `significant_person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `significant_person` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `address1` varchar(255) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `client_record_id` bigint(20) NOT NULL,
  `date_created` datetime NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `last_updated` datetime NOT NULL,
  `managing_conservator` bit(1) NOT NULL,
  `middle_initial` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `relationship` varchar(15) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `zip` varchar(255) DEFAULT NULL,
  `significant_persons_idx` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKE73E49E12E6132A2` (`client_record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `theme`
--

DROP TABLE IF EXISTS `theme`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `theme` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `edit_property_path` varchar(255) NOT NULL,
  `layout` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  `plugin` varchar(255) DEFAULT NULL,
  `show_property_path` varchar(255) NOT NULL,
  `print_property_path` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK69375C9D5D77B88` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `campus_id` bigint(20) DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_show` bit(1) NOT NULL,
  `enabled` bit(1) NOT NULL,
  `living_unit_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `passwd` varchar(255) NOT NULL,
  `passwd_reset_key` varchar(255) DEFAULT NULL,
  `passwd_reset_key_expiration` datetime DEFAULT NULL,
  `theme` varchar(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`),
  UNIQUE KEY `email` (`email`),
  KEY `FK36EBCB610C886B` (`campus_id`),
  KEY `FK36EBCB7FD9847E` (`living_unit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `visitation_form`
--

DROP TABLE IF EXISTS `visitation_form`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `visitation_form` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `address1` varchar(255) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `client_record_id` bigint(20) NOT NULL,
  `cottage` varchar(255) NOT NULL,
  `date` datetime NOT NULL,
  `date_created` datetime NOT NULL,
  `departure_date` datetime NOT NULL,
  `departure_time` varchar(255) NOT NULL,
  `departure_transportation` varchar(255) DEFAULT NULL,
  `departure_transportation_type` varchar(7) NOT NULL,
  `editable` bit(1) NOT NULL,
  `home_phone` varchar(255) DEFAULT NULL,
  `last_updated` datetime NOT NULL,
  `name` varchar(255) NOT NULL,
  `return_date` datetime NOT NULL,
  `return_time` varchar(255) NOT NULL,
  `return_transportation` varchar(255) DEFAULT NULL,
  `return_transportation_type` varchar(7) NOT NULL,
  `special_instructions` longtext,
  `state` varchar(255) DEFAULT NULL,
  `to_visit_with` varchar(255) NOT NULL,
  `type` varchar(7) NOT NULL,
  `work_phone` varchar(255) DEFAULT NULL,
  `zip` varchar(255) DEFAULT NULL,
  `visitation_form_idx` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKDE9987592E6132A2` (`client_record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `weekly_notes`
--

DROP TABLE IF EXISTS `weekly_notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `weekly_notes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `behavioral` longtext,
  `client_record_id` bigint(20) NOT NULL,
  `date` datetime NOT NULL,
  `date_created` datetime NOT NULL,
  `editable` bit(1) NOT NULL,
  `educational` longtext,
  `emotional` longtext,
  `end_date` datetime NOT NULL,
  `family_contact` longtext,
  `independent_living` longtext,
  `last_updated` datetime NOT NULL,
  `name` varchar(255) NOT NULL,
  `physical` longtext,
  `recreational` longtext,
  `social` longtext,
  `spiritual` longtext,
  `weekly_notes_idx` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK27920D832E6132A2` (`client_record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-06-23 21:22:18
