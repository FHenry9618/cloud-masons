-- #23 Normalcy Decisions
alter table client_record add column normalcy_decision_by1 varchar(256);
alter table client_record add column normalcy_decision_by2 varchar(256);
alter table client_record add column normalcy_decision_by3 varchar(256);
alter table client_record add column normalcy_decision_by4 varchar(256);
