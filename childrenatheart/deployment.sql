
update role set name='ROLE_REPORTING' where name = 'REPORTING';
update role set name=’ROLE_CASE_MANAGER’ where name=’CASE_MANAGER’;

CREATE TABLE `case_notes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `client_record_id` bigint(20) NOT NULL,
  `date` datetime NOT NULL,
  `date_created` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  `name` varchar(255) NOT NULL,
  `person_contacted` varchar(255) NOT NULL,
  `case_notes_idx` int(11) DEFAULT NULL,
  `attempted_contact` bit(1) DEFAULT NULL,
  `narrative` longtext,
  `type_of_contact` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK2D4E1A62E613CB3` (`client_record_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

