import org.springframework.context.ApplicationEvent
import org.springframework.context.ApplicationListener
import org.springframework.security.event.authentication.AbstractAuthenticationEvent
import org.springframework.security.event.authentication.AbstractAuthenticationFailureEvent
import org.springframework.security.event.authentication.AuthenticationSuccessEvent
import org.springframework.security.event.authentication.AuthenticationSwitchUserEvent
import org.springframework.security.event.authentication.InteractiveAuthenticationSuccessEvent
import org.springframework.security.event.authorization.*


import org.codehaus.groovy.grails.commons.ConfigurationHolder

/**
*
* ActivityListenerService tracks and stores application requests along with meta-information on a activity queue for archive purposes
*/
class ActivityListenerService implements ApplicationListener {

    boolean transactional = false
    
    def activityService
    /**
    * {@inheritDoc}
    * @see org.springframework.context.ApplicationListener#onApplicationEvent(
    *     org.springframework.context.ApplicationEvent)
    */
    void onApplicationEvent(final ApplicationEvent e) {

      def enablePersistence = ConfigurationHolder.config.activity.db.persistence.enabled instanceof Boolean ? ConfigurationHolder.config.activity.db.persistence.enabled : false
      
      def enableOutput = ConfigurationHolder.config.activity.console.output.enabled instanceof Boolean ? ConfigurationHolder.config.activity.console.output.enabled : true
      
      if (!enablePersistence && !enableOutput) {
          return
      }
      if (e instanceof AbstractAuthenticationEvent) {
         if (e instanceof InteractiveAuthenticationSuccessEvent) {
            // handle InteractiveAuthenticationSuccessEvent
            if (enablePersistence) {
                def authentication = e.getAuthentication()
                def webAuth =  authentication?.getDetails()
                def domainClass = authentication?.principal?.domainClass
                
                Activity activity = new Activity()
                activity.timestamp = new Date(e.getTimestamp())
                activity.controller = 'LoginController'
                activity.action = 'Login'
                activity.objectId = domainClass.id
                activity.objectName = domainClass.name
                activity.objectClass = domainClass.getClass().name
                activity.sessionId = webAuth?.getSessionId()
                activityService.putActivitytoQueue(activity)
            }
            if (enableOutput) {
                
                def authentication = e.getAuthentication()
                def domainClass = authentication?.principal?.domainClass
                
                //println ("[accesslog] ${domainClass.name}: logs in")
                log.info("[accesslog] ${domainClass.name}: logs in")
            }
         }
         else if (e instanceof AbstractAuthenticationFailureEvent) {
            // handle AbstractAuthenticationFailureEvent
            //println ("Authentication failure event ${e}")
         }
         else if (e instanceof AuthenticationSuccessEvent) {
            // handle AuthenticationSuccessEvent
            //println ("Authentication success event ${e}")
         }
         else if (e instanceof AuthenticationSwitchUserEvent) {
            // handle AuthenticationSwitchUserEvent
            //println ("Authentication switch event ${e}")
         }
         else {
            // handle other authentication event
         }
      }
      else if (e instanceof AuthorizedEvent) {
         // handle authorization event
        Activity activity = null
        if (enablePersistence) {
            activity = new Activity()
            activity.timestamp = new Date(e.getTimestamp())
        }
        
        def source = e.source
        def details = ''
        if (source instanceof org.springframework.security.intercept.web.FilterInvocation) {
            details = source.requestUrl
            if (enablePersistence) {
                activity.detail = source.httpRequest.getServletPath()
                activity.params = source.request?.getParameterMap().toString()
            }
        } else {
            details = e.toString()
            if (enablePersistence) {
                activity.detail = e.toString()
            }
        }
        
        def authn = e.authentication
        def  webAuth =  authn?.getDetails();

        def username = ''
        def principal = authn.principal
        if (principal instanceof org.codehaus.groovy.grails.plugins.springsecurity.GrailsUser) {
            username = principal.username
            if (enablePersistence) {
                activity.objectId = principal.domainClass?.id
                activity.objectClass = principal.domainClass?.getClass().name
                activity.objectName = principal.domainClass?.name
                activity.sessionId = webAuth?.getSessionId()
                activityService.putActivitytoQueue(activity)
            }
        } else {
            username = principal.toString()
        }
        if (enableOutput) {
            //println "********"+webAuth
            //println ("[accesslog] ${username}: ${details}")
            log.info("[accesslog] ${username}: ${details}")
        }
      }
   }
}
