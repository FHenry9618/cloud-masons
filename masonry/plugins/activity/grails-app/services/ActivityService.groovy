import java.util.concurrent.ArrayBlockingQueue
import java.util.concurrent.BlockingQueue

import org.codehaus.groovy.grails.commons.ConfigurationHolder

/**
*
* ActivityService handles the activity queue management and triggers the quartz job for presisting activities when needed.
*/
class ActivityService {

    boolean transactional = false
    
    private final int activityQueueCapacity = ConfigurationHolder.config.activity.queue.capacity instanceof Integer ? ConfigurationHolder.config.activity.queue.capacity : 1000
    private BlockingQueue<Activity> activityQueue = new ArrayBlockingQueue<Activity>(activityQueueCapacity)
    
    void putActivitytoQueue(Activity activity) {
        
        if (activityQueue?.remainingCapacity() <= 0) {
            LogActivityJob.triggerNow() //trigger the job to presist activity from queue to database
        }
        activityQueue.put(activity)
        //printQueue()
    }
    
    Activity takeActivityfromQueue() {
       activityQueue.take()
    }
    
    Activity topActivity() {
        return activityQueue.peek()
    }
    
    boolean isQueueEmpty() {
        return topActivity() ? false : true
    }
    
    def flushActivityQueue() {
        LogActivityJob.triggerNow()
    }
    
    def scheduleJob(String cronExpression, Map params) {
        LogActivityJob.schedule(cronExpression, params)
    }
    
    def printQueue() {
        println "printQueue in thread ${Thread.currentThread().id}"
        println activityQueue
        //println activityQueue.remainingCapacity()
    }
}
