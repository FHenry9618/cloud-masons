import java.util.Date;

/**
* Tracks an activity performed in the underlying application
*
*/
class Activity {
    
    static transients = ['name', 'description']
    
    String name
    String description
    
    Date timestamp
    String controller
    String action
    String detail
    String params
    Long objectId
    String objectName
    String objectClass
    String sessionId
    
    static constraints = {
        controller(nullable:true)
        action(nullable:true)
        objectName(nullable:true)
        detail(nullable:true)
        sessionId(nullable:true)
        params(nullable:true,maxSize:2000)
    }
    
    String getName() {
        String objectInstance = objectName ?: "${objectClass} with id:${objectId}"
        String activityInstance = detail ?: "${controller} ${action}"
        return "${objectInstance} : ${activityInstance} at ${timestamp}"
    }
    
    String getDescription() {
        return params
    }
}
