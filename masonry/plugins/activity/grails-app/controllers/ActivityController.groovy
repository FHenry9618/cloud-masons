/**
*
* Scafoldding controller for basic CRUD opertions
*/

class ActivityController {
    def scaffold = true
    def beanService
    def authenticateService
    
    static navigation = [
                         group:'admintabs', 
                         order:5, 
                         title:'Activity', 
                         action:'list',
                         icon:'color',
                         role: '**SYSTEM_ADMINISTRATOR_ROLE**'
                     ]
    
}
