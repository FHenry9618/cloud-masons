import org.codehaus.groovy.grails.commons.ConfigurationHolder

/**
 * 
 * Quartz job for presisting activities from queue to database
 */
class LogActivityJob {
    
    static triggers = {

    }

    def activityService
    
    int maxCount = ConfigurationHolder.config.activity.queue.capacity instanceof Integer ? ConfigurationHolder.config.activity.queue.capacity : 1000
    
    def execute() {
        // execute task
        //println "LogActivity in thread ${Thread.currentThread().id} with max: $maxCount"+new Date()
        int index = 0
        while (index < maxCount && !activityService.isQueueEmpty()) {
            index++
            def activity = activityService.takeActivityfromQueue()
            activity?.save()
        }
    }
}