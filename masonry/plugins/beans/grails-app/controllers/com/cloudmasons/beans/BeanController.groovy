package com.cloudmasons.beans

import com.cloudmasons.BeanUtils
import grails.util.GrailsNameUtils

/**
 * Controller which uses the default theme's scaffolding to manage bean domain classes.
 */
class BeanController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def beanService
    def securityService
    def grailsApplication

    def index = {
        def beans = []
        def beanDCs = grailsApplication.getArtefacts("Domain") as List ?: []
        for (dc in beanDCs) {
            beans.add([domainClass: dc, count: dc.clazz.count()])
        }
        return [beans: beans]
    }

    def list = {

        def beanDomainClass = getBeanDomainClass(params)
        if (!securityService.isAuthorized(beanDomainClass.clazz, 'list')) {
            render "Not Authorized"
            return
        }
        def model = [:]

        model.max = Math.min(params.max ? params.int('max') : 10, 1000)
        params.max = model.max
        model.domainClass = beanDomainClass
        model.entityName = GrailsNameUtils.getShortName(beanDomainClass.clazz.name)
        model.beanList = beanService.filter(params, beanDomainClass?.clazz)
        model.beanTotal = beanService.count(params, beanDomainClass?.clazz)
        model.beanParams = BeanUtils.extractFilterParams(params)
        model.beanControllerName = params.beanControllerName ?: controllerName
        def summaryProperties = beanService.getSummaryProperties(beanDomainClass)

        // Process custom settings for derived properties
        params.propertyUserSettings?.each { key, value ->
            def args = key.split('-')

            def propName = args.length > 0 ? args[0] : null
            def settingName = args.length > 1 ? args[1] : null
            beanService.storeUserSetting(beanDomainClass.clazz, propName, settingName, value)
        }
        log.debug("columns ${params.columns} ${params.columns.getClass().name}")
        if (params.columns) {
            if (params.columns instanceof String[]) { // if there are multiple columns
                model.columns = params.columns as List
            } else {
                model.columns = [params.columns]
            }
        } else {
            model.columns = summaryProperties.collect { it.name }
        }
        params.columns = model.columns
        model
    }


    def create = {
        def beanDomainClass = getBeanDomainClass(params)

        if (!securityService.isAuthorized(beanDomainClass.clazz, 'create')) {
            render "Not Authorized"
            return
        }

        def model = [:]

        model.domainClass = beanDomainClass
        model.entityName = GrailsNameUtils.getShortName(beanDomainClass.clazz.name)
        model.beanControllerName = params.beanControllerName ?: controllerName
        model.beanInstance = chainModel?.cloneInstance ?: beanDomainClass.newInstance()
        model.beanInstance.properties = params

        def reqs = session.previousRequests
        if (reqs && reqs.size() > 0) {
            model.previousRequest = session.previousRequests.get(reqs.size() - 1)
        }
        model
    }

    def save = {
        def beanDomainClass = getBeanDomainClass(params)

        def associatedProperty = null
        if (params.associated) {
            associatedProperty = beanDomainClass.getPropertyByName(params.associated)
        }
        if (associatedProperty) {

            def associatedDomainClass = associatedProperty.getReferencedDomainClass()
            if (!securityService.isAuthorized(associatedDomainClass.clazz, 'create')) {
                render "Not Authorized"
                return
            }
            def beanInstance = chainModel?.cloneInstance ?: associatedDomainClass.newInstance()
            def parentBean = beanDomainClass.clazz.get(params.parentBeanId?.toLong())
            beanInstance.properties = params
            def otherProperty = associatedProperty.otherSide
            beanInstance."${otherProperty.name}" = parentBean // set back pointer to parent bean 

            if (associatedProperty.oneToMany && beanInstance.validate()) {

                String methodName = params.associated[0].toUpperCase() + params.associated.substring(1, params.associated.size())
                parentBean.('addTo' + methodName)(beanInstance)
                if (!parentBean.save(flush: true)) {
                    println parentBean.errors
                }
                flash.message = "${message(code: 'default.created.message', args: [message(code: '${associatedDomainClass.propertyName}.label', default: associatedDomainClass.propertyName), beanInstance.name])}"
                flash.redirect = true
                def reqs = session.previousRequests
                if (reqs && reqs.size() > 1) {
                    def previousRequest = session.previousRequests.get(reqs.size() - 2)
                    flash.redirection = [controller: previousRequest.controller, action: previousRequest.action, params: previousRequest.params]
                } else {
                    flash.redirection = [controller: beanDomainClass.propertyName, action: 'show', params: [id: params.parentBeanId?.toLong(), associated: associatedProperty.name]]
                }
                redirect(action: "show")
                return
            } else if (associatedProperty.oneToOne && beanInstance.save(flush: true)) {
                flash.message = "${message(code: 'default.created.message', args: [message(code: '${associatedDomainClass.propertyName}.label', default: associatedDomainClass.propertyName), beanInstance.name])}"
                parentBean."${associatedProperty.name}" = beanInstance
                if (!parentBean.save(flush: true)) {
                    println parentBean.errors
                }
                flash.redirect = true
                def reqs = session.previousRequests
                if (reqs && reqs.size() > 1) {
                    def previousRequest = session.previousRequests.get(reqs.size() - 2)
                    flash.redirection = [controller: previousRequest.controller, action: previousRequest.action, params: previousRequest.params]
                } else {
                    flash.redirection = [controller: beanDomainClass.propertyName, action: 'show', params: [id: params.parentBeanId?.toLong()]]
                }
                redirect(action: "show")
                return
            } else {
                //println "ERROR---"+beanInstance.errors
                def model = [:]
                model.domainClass = associatedDomainClass
                model.entityName = GrailsNameUtils.getShortName(associatedDomainClass.clazz.name)

                model.beanControllerName = params.beanControllerName ?: controllerName
                model.beanActionName = 'show'
                model.beanParams = [id: parentBean.id]
                model.beanAssociated = associatedProperty.name
                model.parentBean = parentBean
                model.beanInstance = beanInstance
                def parentNode = [:]
                parentNode.beanControllerName = model.beanControllerName
                parentNode.beanActionName = model.beanActionName
                if (associatedProperty.oneToMany) {
                    parentNode.beanParams = [associated: params.associated]
                }
                parentNode.bean = parentBean
                parentNode.name = GrailsNameUtils.getNaturalName(parentBean.getClass().name)
                model.path = [parentNode]
                render(view: 'create', model: model)
                return
            }
        }
        if (!securityService.isAuthorized(beanDomainClass.clazz, 'create')) {
            render "Not Authorized"
            return
        }
        def beanInstance = chainModel?.cloneInstance ?: beanDomainClass.newInstance()
        beanInstance.properties = params
        if (beanInstance.save(flush: true)) {
            flash.message = "${message(code: 'default.created.message', args: [message(code: '${beanDomainClass.propertyName}.label', default: beanDomainClass.propertyName), beanInstance.name])}"
            flash.redirect = true
            def reqs = session.previousRequests
            if (reqs && reqs.size() > 0) {
                def previousRequest = session.previousRequests.get(reqs.size() - 1)
                flash.redirection = [controller: previousRequest.controller, action: previousRequest.action, params: previousRequest.params]
            } else {
                flash.redirection = [controller: beanDomainClass.propertyName, action: 'list']
            }
            redirect(action: "list")
        } else {
            def model = [:]
            model.domainClass = beanDomainClass
            model.entityName = GrailsNameUtils.getShortName(beanDomainClass.clazz.name)
            model.beanControllerName = params.beanControllerName ?: controllerName
            model.beanInstance = beanInstance

            render(view: 'create', model: model)
        }
    }

    def show = {
        def beanDomainClass = getBeanDomainClass(params)
        def beanInstance = beanDomainClass.clazz.get(params.id.toLong())

        if (!securityService.isAuthorized(beanDomainClass.clazz, 'show', beanInstance)) {
            render "Not Authorized"
            return
        }

        def associatedProperty = null
        if (params.associated) {
            try {
                associatedProperty = beanDomainClass.getPropertyByName(params.associated)
            }
            catch (org.codehaus.groovy.grails.exceptions.InvalidPropertyException e) {
                println e
                redirect(action: 'list')
                return
            }
        }

        def model = [:]
        model.printPDF = grailsApplication.config.printPDF.enabled instanceof Boolean ? grailsApplication.config.printPDF.enabled : false
        def parentBean = beanInstance // for calculating the path on showing assoicated properties
        if (!beanInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: '${domainClass.propertyName}.label', default: beanDomainClass.naturalName), params.id])}"
            redirect(action: "list")
        } else if (associatedProperty && associatedProperty.oneToOne) {
            if (params.propertyId && beanInstance."${params.associated}"?.id == params.propertyId.toLong()) {
                // Show the assocated property instances)
                def associatedDomainClass = associatedProperty.getReferencedDomainClass()

                model.domainClass = associatedDomainClass
                model.entityName = GrailsNameUtils.getShortName(associatedDomainClass.clazz.name)

                model.beanControllerName = associatedDomainClass.propertyName
                model.beanActionName = 'show'

                beanInstance = associatedDomainClass.clazz.get(params.propertyId.toLong())
                model.beanInstance = beanInstance
                model.beanInstanceControllerName = GrailsNameUtils.getPropertyName(associatedDomainClass.clazz.name)

                def parentNode = [:]
                parentNode.beanControllerName = params.beanControllerName ?: controllerName
                parentNode.beanActionName = model.beanActionName
                parentNode.bean = parentBean
                parentNode.name = GrailsNameUtils.getNaturalName(parentBean.getClass().name)
                model.path = [parentNode]
                render(view: 'show', model: model)
            } else if (!params.propertyId) { //create a new associated object

                def associatedDomainClass = associatedProperty.getReferencedDomainClass()
                def otherProperty = associatedProperty.otherSide
                model.domainClass = associatedDomainClass
                model.entityName = GrailsNameUtils.getShortName(associatedDomainClass.clazz.name)

                model.beanControllerName = beanDomainClass.propertyName
                model.beanActionName = 'show'
                model.beanParams = [id: parentBean.id]
                model.beanAssociated = associatedProperty.name
                model.parentBean = parentBean

                def associatedInstance = chainModel?.cloneInstance ?: associatedDomainClass.newInstance()
                associatedInstance."${otherProperty.name}" = beanInstance
                model.beanInstance = associatedInstance
                model.beanInstance.properties = params

                def parentNode = [:]
                parentNode.beanControllerName = params.beanControllerName ?: controllerName
                parentNode.beanActionName = model.beanActionName
                parentNode.bean = parentBean
                parentNode.name = GrailsNameUtils.getNaturalName(parentBean.getClass().name)
                model.path = [parentNode]

                def reqs = session.previousRequests
                if (reqs && reqs.size() > 0) {
                    model.previousRequest = reqs.get(reqs.size() - 1)
                }

                render(view: 'create', model: model)
                return
            }

        } else if (associatedProperty && associatedProperty.oneToMany) {
            if (params.propertyId) { // Show the assocated property instances
                def associatedDomainClass = associatedProperty.getReferencedDomainClass()

                model.domainClass = associatedDomainClass
                model.entityName = GrailsNameUtils.getShortName(associatedDomainClass.clazz.name)

                model.beanControllerName = associatedDomainClass.propertyName
                model.beanActionName = 'show'

                beanInstance = associatedDomainClass.clazz.get(params.propertyId.toLong())
                model.beanInstance = beanInstance

                def parentNode = [:]
                parentNode.beanControllerName = params.beanControllerName ?: controllerName
                parentNode.beanActionName = model.beanActionName
                parentNode.beanParams = [associated: params.associated]
                parentNode.bean = parentBean
                parentNode.name = GrailsNameUtils.getNaturalName(parentBean.getClass().name)
                model.path = [parentNode]
                render(view: 'show', model: model)
            } else {
                if (!params.propertyId && params.create == 'true') { //create a new associated object
                    def associatedDomainClass = associatedProperty.getReferencedDomainClass()
                    def otherProperty = associatedProperty.otherSide
                    model.domainClass = associatedDomainClass
                    model.entityName = GrailsNameUtils.getShortName(associatedDomainClass.clazz.name)

                    model.beanControllerName = beanDomainClass.propertyName
                    model.beanActionName = 'show'
                    model.beanParams = [id: parentBean.id]
                    model.beanAssociated = associatedProperty.name
                    model.parentBean = parentBean

                    def associatedInstance = chainModel?.cloneInstance ?: associatedDomainClass.newInstance()
                    associatedInstance."${otherProperty.name}" = beanInstance
                    model.beanInstance = associatedInstance
                    model.beanInstance.properties = params

                    def parentNode = [:]
                    parentNode.beanControllerName = model.beanControllerName
                    parentNode.beanActionName = model.beanActionName
                    parentNode.bean = parentBean
                    parentNode.beanParams = [associated: params.associated]
                    parentNode.name = GrailsNameUtils.getNaturalName(parentBean.getClass().name)
                    model.path = [parentNode]

                    def reqs = session.previousRequests
                    if (reqs && reqs.size() > 0) {
                        model.previousRequest = reqs.get(reqs.size() - 1)
                    }
                    render(view: 'create', model: model)
                    return
                } else { // Show a list of the assocated property instances
                    def associatedDomainClass = associatedProperty.getReferencedDomainClass()


                    model.max = Math.min(params.max ? params.int('max') : 10, 100)
                    params.max = model.max
                    params.associated = associatedProperty.name

                    // Ensure that we only get associated instances.
                    def otherProperty = associatedProperty.otherSide
                    //println otherProperty.name
                    if (!params.filter) params.filter = [:]
                    params.filter."${otherProperty.name}" = ['id': beanInstance.id]
                    if (!params.filter.op) params.filter.op = [:]
                    params.filter.op."${otherProperty.name}" = [id: 'Equal']

                    model.domainClass = associatedDomainClass
                    model.entityName = GrailsNameUtils.getShortName(associatedDomainClass.clazz.name)
                    model.beanList = beanService.filter(params, associatedDomainClass?.clazz)
                    model.beanTotal = beanService.count(params, associatedDomainClass?.clazz)
                    model.beanParams = BeanUtils.extractFilterParams(params)
                    model.beanParams.put('id', parentBean.id)
                    model.beanControllerName = params.beanControllerName ?: controllerName
                    model.beanActionName = 'show'
                    model.beanParams.associated = associatedProperty.name

                    def parentNode = [:]
                    parentNode.beanControllerName = params.beanControllerName ?: controllerName
                    parentNode.beanActionName = model.beanActionName
                    parentNode.bean = parentBean
                    parentNode.name = GrailsNameUtils.getNaturalName(parentBean.getClass().name)
                    model.path = [parentNode]

                    def summaryProperties = beanService.getSummaryProperties(associatedDomainClass)
                    log.debug("columns ${params.columns} ${params.columns.getClass().name}")
                    if (params.columns) {
                        if (params.columns instanceof String[]) { // if there are multiple columns
                            model.columns = params.columns as List
                        } else {
                            model.columns = [params.columns]
                        }
                    } else {
                        model.columns = summaryProperties.collect { it.name }
                    }
                    params.columns = model.columns
                    render(view: 'list', model: model)
                }
            }
        } else {  //

            model.domainClass = beanDomainClass
            model.entityName = GrailsNameUtils.getShortName(beanDomainClass.clazz.name)
            model.beanControllerName = params.beanControllerName ?: controllerName
            model.beanActionName = 'show'
            model.beanInstance = beanInstance
            model.beanInstanceControllerName = model.beanControllerName
            return model
        }
    }

    def edit = {
        def beanDomainClass = getBeanDomainClass(params)
        def beanInstance = beanDomainClass.clazz.get(params.id.toLong())

        if (!securityService.isAuthorized(beanDomainClass.clazz, 'edit', beanInstance)) {
            render "Not Authorized"
            return
        }

        if (!beanInstance) {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: '${domainClass.propertyName}.label', default: beanDomainClass.naturalName), params.id])}"
            redirect(action: "list")
        } else {
            def model = [:]
            model.domainClass = beanDomainClass
            model.entityName = GrailsNameUtils.getShortName(beanDomainClass.clazz.name)
            model.beanControllerName = params.beanControllerName ?: controllerName
            model.beanInstance = beanInstance
            if (params.group) {
                model.propertyGroups = ["${params.group}": null]
            }
            def reqs = session.previousRequests
            if (reqs && reqs.size() > 0) {
                model.previousRequest = session.previousRequests.get(reqs.size() - 1)
            }
            model
        }
    }

    def update = {
        def beanDomainClass = getBeanDomainClass(params)
        def beanInstance = beanDomainClass.clazz.get(params.id.toLong())

        if (!securityService.isAuthorized(beanDomainClass.clazz, 'edit', beanInstance)) {
            render "Not Authorized"
            return
        }

        def model = [:]
        model.domainClass = beanDomainClass
        model.entityName = GrailsNameUtils.getShortName(beanDomainClass.clazz.name)
        model.beanControllerName = params.beanControllerName ?: controllerName
        model.beanInstance = beanInstance

        if (beanInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (beanInstance.version > version) {
                    beanInstance.errors.rejectValue("version", "default.optimistic.locking.failure", [message(code: '${domainClass.propertyName}.label', default: beanDomainClass.naturalName)] as Object[], "Another user has updated this ${beanDomainClass.naturalName} while you were editing")
                    render(view: "edit", model: model)
                    return
                }
            }

            processMoves(params, beanInstance, beanDomainClass)
            processPasswords(params, beanInstance, beanDomainClass)

            beanInstance.properties = params
            beanService.processRemovals(params, beanInstance)

            if (!beanInstance.hasErrors() && beanInstance.save(flush: true)) {

                flash.message = "${message(code: 'default.updated.message', args: [message(code: '${domainClass.propertyName}.label', default: beanDomainClass.naturalName), beanInstance.name])}"
                flash.redirect = true
                def reqs = session.previousRequests
                if (reqs && reqs.size() > 0) {
                    def previousRequest = session.previousRequests.get(reqs.size() - 1)
                    flash.redirection = [controller: previousRequest.controller, action: previousRequest.action, params: previousRequest.params]
                } else {
                    flash.redirection = [controller: beanDomainClass.propertyName, action: 'show', params: [id: beanInstance?.id]]
                }
                redirect(action: "show")
                return
            } else {
                render(view: "edit", model: model)
            }
        } else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: '${domainClass.propertyName}.label', default: beanDomainClass.naturalName), params.name])}"
            redirect(action: "list")
        }
    }

    def printObject = {
        def beanDomainClass = getBeanDomainClass(params)
        def beanInstance = beanDomainClass?.clazz.get(params.long('id') ?: params.long('pdfId'))
        if (!securityService.isAuthorized(beanDomainClass.clazz, 'print', beanInstance)) {
            render "Not Authorized"
        } else if (beanInstance) {
            render(view: "print", model: [beanInstance: beanInstance])
        } else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: beanDomainClass.propertyName + '.label', default: beanDomainClass.propertyName), params.id])}"
            redirect(action: "list")
        }
    }

    def delete = {
        // for some reason delete was not being from the controll scaffolding delete include
        deleteObject()
    }
    def deleteObject = {
        def beanDomainClass = getBeanDomainClass(params)
        //println "Attempting delete of ${params.id}"
        def beanInstance = beanDomainClass.clazz.get(params.id.toLong())

        if (!securityService.isAuthorized(beanDomainClass.clazz, 'delete', beanInstance)) {
            render "Not Authorized"
            return
        }
        flash.redirect = true
        if (beanInstance) {
            if (beanInstance.properties['deleteValidator']) {
                def error = beanInstance.deleteValidator?.call(beanInstance)
                if (error) {
                    flash.message = error
                    def reqs = session.previousRequests
                    if (reqs && reqs.size() > 0) {
                        def previousRequest = session.previousRequests.get(reqs.size() - 1)
                        flash.redirection = [controller: previousRequest.controller, action: previousRequest.action, params: previousRequest.params]
                        redirect(action: 'list')
                    } else {
                        redirect(action: "list", id: params.id)
                    }
                    return
                }
            }
            try {
                beanInstance.delete(flush: true)
                flash.message = "${message(code: 'default.deleted.message', args: [message(code: beanDomainClass.propertyName + '.label', default: beanDomainClass.naturalName), beanInstance.name])}"
                def reqs = session.previousRequests
                if (reqs && reqs.size() > 0) {
                    def previousRequest = session.previousRequests.get(reqs.size() - 1)
                    flash.redirection = [controller: previousRequest.controller, action: previousRequest.action, params: previousRequest.params]
                    redirect(action: 'list')
                } else {
                    redirect(action: 'list')
                }
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: beanDomainClass.propertyName + '.label', default: beanDomainClass.naturalName), beanInstance.name])}"
                def reqs = session.previousRequests
                if (reqs && reqs.size() > 0) {
                    def previousRequest = session.previousRequests.get(reqs.size() - 1)
                    flash.redirection = [controller: previousRequest.controller, action: previousRequest.action, params: previousRequest.params]
                    redirect(action: 'list')
                } else {
                    redirect(action: 'list', id: params.id)
                }
            }
        } else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: beanDomainClass.propertyName + '.label', default: beanDomainClass.propertyName), params.id])}"
            def reqs = session.previousRequests
            if (reqs && reqs.size() > 0) {
                def previousRequest = session.previousRequests.get(reqs.size() - 1)
                flash.redirection = [controller: previousRequest.controller, action: previousRequest.action, params: previousRequest.params]
                redirect(action: 'list')
            } else {
                redirect(action: 'list')
            }
        }
    }

    private void processPasswords(params, bean, domainClass) {

        def constraints = domainClass.constrainedProperties
        domainClass.properties?.each { prop ->
            def cp = constraints[prop.name]
            if (cp?.password == true) {

                def value = params[prop.name]
                if (value != null) {
                    params.remove(prop.name)
                    if (value != 'shhhhhsecret') {
                        bean."${prop.name}" = value
                    }
                }
            }
        }
    }

    private void processMoves(params, bean, domainClass) {

        domainClass.persistentProperties?.each { prop ->
            if (prop.manyToOne) {
                println "Processing moves for ${prop.name}"
                def otherProp = prop.getOtherSide()
                def currentOwner = bean."${prop.name}"
                def newId = params."${prop.name}"?.id
                println newId
                println newId == 'null'
                if (currentOwner && newId && newId != 'null' && "${currentOwner?.id}" != newId) {
                    def newOwner = otherProp.domainClass.clazz.get(newId.toLong())
                    println "Moving many-to-one from ${currentOwner} to ${newOwner}"

                    def methodName = otherProp.name[0].toUpperCase() + otherProp.name[1..-1]
                    def removeMethodName = "removeFrom${methodName}"
                    def addMethodName = "addTo${methodName}"
                    println newId

                    newOwner."${addMethodName}" bean
                    newOwner.save(flush: true)
                    bean."${prop.name}" = newOwner
                }
            }
        }
    }

    /**
     * Resolve the bean's domain class from the given parameters
     */
    private Object getBeanDomainClass(Map params) {
        def beanClassName = params.beanClass
        def beanDomainClass = BeanUtils.resolveDomainClass(grailsApplication, beanClassName)
        return beanDomainClass
    }
}
