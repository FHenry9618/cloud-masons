package com.cloudmasons.beans


/**
 * UI For managing data feeds
 */
class FeedController {
	
	def scaffold = true
    def feedService
    def beanService
    static navigation = [
            group:'admintabs', 
            order:49, 
            title:'Data Feeds', 
            action:'list',
            icon:'advanced',
            role: '**SYSTEM_ADMINISTRATOR_ROLE**'
    ]
    
    def run = {
        Feed feed = Feed.get(params.id)
        return [feed:feed]
    }
    
    // create a progress object on the session..
    def upload = {
        Feed feed = Feed.get(params.id)
        def f = request.getFile('uploadFile')
        if (!f.empty) {
            def filename = f.originalFilename
            log.info("Processing file: ${filename}")
            // save the file off
            File tmpFile = File.createTempFile('beanfeed-', filename)
            try {
                log.info("Transferring to temp file: ${tmpFile}")
                f.transferTo(tmpFile)
                // kick off the import service
                def warnings = feedService.processFileFeed(feed, tmpFile)
                // render the progress template
                if (warnings) {
                    flash.message = warnings
                } else {
                    flash.message = 'Upload complete. Data loaded'
                }
            } finally {
                // delete the temp file..
                tmpFile?.delete()
            }
        }
        else {
           flash.message = 'The uploaded file was empty.'
        }
        redirect(session.redirect ?: [action: 'run'])
    }
}
