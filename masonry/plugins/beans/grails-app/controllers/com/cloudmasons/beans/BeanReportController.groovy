package com.cloudmasons.beans

import grails.converters.JSON
import com.cloudmasons.BeanUtils
import grails.util.GrailsNameUtils

class BeanReportController {

    def beanReportService
    private static Map statusMap = [:].asSynchronized();
    
    def beforeInterceptor = {
        response.setHeader('Cache-Control', 'no-cache, no-store, must-revalidate')
        response.setIntHeader('Expires', 0)
        response.setHeader('Pragma', 'no-cache')
    }
        
    def image = {
        def pluginPath = PluginManagerHolder.pluginManager.getGrailsPlugin('beans').getPluginPath()
        redirect(uri: "${pluginPath}/images/${params.image}.gif")
    }

    def index = {
            dbReport()
    }

    def run = {

        def reportClass = BeanUtils.resolveDomainClass(grailsApplication, params.reportClass ?: 'Report')
        
        def report = reportClass.clazz.get(params.report.toLong())
        def reportFormat = params.reportFormat
        statusMap[params.report] = 'generating'
        return [reportClass: params.reportClass, report: report, reportFormat: reportFormat]
    }

    def runStatus = {
        println statusMap;
        def status = statusMap[params.report]
        println status
        if (status == "generating") {
            def result = ['status': 'generating']
            render result as JSON
            return;
        }
        def result = ['status': 'complete']
        render result as JSON
    }  
      
    def handleError() {
        redirect(uri: '/')
    }
    def dbReport() {
    
        def reportFileName = params.reportFileName ?: getDefaultReportName(params)
         if (reportFileName) { 
            response.addHeader('content-disposition', "attachment; filename=${reportFileName}.${params.reportFormat.toLowerCase()}")
        }   
        try {
            // todo: determine if we need to setContentLength
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            beanReportService.generateReport(params.reportClass, params.report.toLong(), stream, params.reportFormat);
    
            //println stream
            //println stream.size()
            response.setContentLength(stream.size());
            response.outputStream << stream.toByteArray()
        } catch (Exception e) {
            statusMap.remove( params.report );
            println e
            throw e
            handleError()
        }
        statusMap.remove( params.report );
    }
 
    def listReport = {
        
        println "Params: " + params

        def reportFileName = params.reportFileName ?: getDefaultReportName(params)
         if (reportFileName) { 
            response.addHeader('content-disposition', "attachment; filename=${reportFileName}.${params.reportFormat.toLowerCase()}")
        }   
        try {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            
            beanReportService.generateListReport(params, stream);
            response.setContentLength(stream.size());
            response.outputStream << stream.toByteArray()
            

        } catch (Exception e) {
            println e
            throw e
            handleError()
        }
    }


    private String getDefaultReportName(Map params) {
        def domainClass = com.cloudmasons.BeanUtils.resolveDomainClass(grailsApplication, params.entity)
        String base = "report-"
        if (domainClass) {
            base = GrailsNameUtils.getScriptName( GrailsNameUtils.getShortName( domainClass.clazz ) );
            base += '-report-'
        }
        return base +new Date().format('MM-dd-yyyy-HHmm')
    }
}
