
grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"
//grails.project.war.file = "target/${appName}-${appVersion}.war"
grails.project.dependency.resolution = {
    // inherit Grails' default dependencies
    inherits( "global" ) {
        // uncomment to disable ehcache
        // excludes 'ehcache'
    }
    log "warn" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'
    repositories {        
        mavenLocal()
        mavenCentral()
        mavenRepo "http://download.java.net/maven/2/"
        mavenRepo "http://repository.jboss.com/maven2/"
    }
    dependencies {
        // specify dependencies here under either 'build', 'compile', 'runtime', 'test' or 'provided' scopes eg.
        // runtime 'mysql:mysql-connector-java:5.1.5'
        runtime('ar.com.fdvs:DynamicJasper:3.0.13') {
            excludes 'groovy', 'xml-apis', 'spring-core', 'spring-beans', 
               'bctsp-jdk14', 'bcmail-jdk14', 'commons-digester', 'castor', 
               'commons-collections', 'aspectjrt', 'commons-logging'
        }
        runtime('org.apache.poi:poi:3.8') {
            excludes 'commons-codec', 'commons-logging', 'log4j'
        }
    }
}
