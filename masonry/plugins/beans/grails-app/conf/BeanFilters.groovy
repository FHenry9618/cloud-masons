/**
 * Define the filters to save url request when user visit varies controller actions. 
 * The url request history will be used for redirection.
 * 
 **/
class BeanFilters {

    def beanService
    
    def filters = {
        allList(controller:'*', action:'list') {
            after = {
                beanService.saveRequestHistoryToSession(params,session)
            }
        }
        
        allShow(controller:'*', action:'show') {
            after = {
                beanService.saveRequestHistoryToSession(params,session)
            }
        }
    }
    
}
