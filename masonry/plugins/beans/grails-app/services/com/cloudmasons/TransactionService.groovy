package com.cloudmasons

import com.cloudmasons.beans.Feed;
import org.codehaus.groovy.grails.commons.ConfigurationHolder;

/**
 * Retry logic around a transaction
 */
class TransactionService {

    static transactional = false

    private static Long MAX_RETRIES = ConfigurationHolder.config?.grails?.transaction?.maxRetry instanceof Long ? ConfigurationHolder.config?.grails?.transaction?.maxRetry : 3 //number of retry before 
    private static Long RETRY_SLEEP = ConfigurationHolder.config?.grails?.transaction?.sleepTimeBetweenRetry instanceof Long ? ConfigurationHolder.config?.grails?.transaction?.sleepTimeBetweenRetry : 1000 //time to sleep between retries
    
    boolean executeTransaction(Closure transaction) {
        def success = false
        int retry = 0
        while (!success && retry++  < MAX_RETRIES) {
              Feed.withTransaction { status ->
                    try {
                      transaction()
                      success = true
                    } catch(Exception e) {
                       status.setRollbackOnly()
                       println "executeTransaction###########Retry:"+retry
                       Thread.sleep(RETRY_SLEEP)
                    }
              }
              
        }
        return success
   }
}
