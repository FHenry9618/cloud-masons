package com.cloudmasons.beans

import ar.com.fdvs.dj.core.DynamicJasperHelper
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager
import ar.com.fdvs.dj.domain.chart.DJChart;
import ar.com.fdvs.dj.domain.chart.builder.DJPieChartBuilder
import ar.com.fdvs.dj.domain.chart.DJChartOptions;
import ar.com.fdvs.dj.domain.builders.ColumnBuilder;
import ar.com.fdvs.dj.domain.builders.DJChartBuilder;
import ar.com.fdvs.dj.domain.builders.DynamicReportBuilder
import ar.com.fdvs.dj.domain.builders.GroupBuilder;

import ar.com.fdvs.dj.domain.AutoText
import ar.com.fdvs.dj.domain.ColumnProperty
import ar.com.fdvs.dj.domain.DJCalculation
import ar.com.fdvs.dj.domain.DynamicReport
import ar.com.fdvs.dj.domain.DynamicReportOptions
import ar.com.fdvs.dj.domain.Style
import ar.com.fdvs.dj.domain.constants.HorizontalAlign
import ar.com.fdvs.dj.domain.entities.DJGroup
import ar.com.fdvs.dj.domain.entities.DJGroupVariable
import ar.com.fdvs.dj.domain.entities.columns.ExpressionColumn
import ar.com.fdvs.dj.domain.entities.columns.SimpleColumn
import ar.com.fdvs.dj.domain.entities.columns.PropertyColumn

import ar.com.fdvs.dj.output.FormatInfoRegistry

import ar.com.fdvs.dj.output.ReportWriter
import ar.com.fdvs.dj.output.ReportWriterFactory
import ar.com.fdvs.dj.domain.constants.Font;

import com.cloudmasons.BeanUtils
import com.cloudmasons.util.Transforms;

import grails.util.GrailsUtil
import grails.util.GrailsNameUtils

import java.awt.Color;

import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JRDataSource
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource
import net.sf.jasperreports.engine.export.JRHtmlExporterParameter

import org.apache.commons.lang.StringUtils
import org.codehaus.groovy.grails.commons.ConfigurationHolder
import org.codehaus.groovy.grails.commons.DomainClassArtefactHandler
import org.codehaus.groovy.grails.commons.GrailsClass
import org.codehaus.groovy.grails.plugins.PluginManagerHolder


/**
 * Service allowing for generation of reports and listing authorized reports utilizing
 * grails domain class model.
 */ 
class BeanReportService {

    def grailsApplication    // default grails application
    def authenticateService  // From acegi plugin
    def beanService
    
    
    /**
     * Provides a list of the authorized reports for the logged in user.
     */
    def authorizedReports = {
        // Todo:  Iterate domain classes finding report classes rather
        //        than having a hard coded single Report class here
        def domainClass = BeanUtils.resolveDomainClass(grailsApplication, 'Report');
        def reports = domainClass.clazz.list() ?: []
        def authorizedReports = []
        for (r in reports) {
            def role = r.properties['role']
            if (!role || authenticateService.ifAnyGranted(role)) {
                authorizedReports.add(r)
            }
        }
        return authorizedReports
    }

    /**
     * Generate a report to the outputStream given the configuration stored in the report domain object.
     */
    def generateReport(String reportClassName, Long reportId, OutputStream outputStream, String reportFormat='PDF') {
        def reportClass = BeanUtils.resolveDomainClass(grailsApplication, reportClassName)
        def report = reportClass.clazz.get(reportId)
        generateReport(report, outputStream, reportFormat)
    }   

    /**
     * Generate a report to the outputStream given the configuration stored in the report domain object.
     */
    def generateReport(Object report, OutputStream outputStream, String reportFormat='PDF') {
        if (report == null) throw new IllegalArgumentException("Null report passed to beanReportService.generateReport")
        if (outputStream == null) throw new IllegalArgumentException("Null outputStream passed to beanReportService.generateReport")


        def reportConfig = loadConfig(report.config)
        GrailsClass domainClass = com.cloudmasons.BeanUtils.resolveDomainClass(grailsApplication, reportConfig.entity)
        log.debug("Running report on domain class $domainClass")


        DynamicReport dr = buildReport(domainClass, reportConfig, reportFormat)
        def dataSource = getDataSource(domainClass, reportConfig)
        
        writeReport(dr, dataSource, reportConfig, outputStream, reportFormat)
    }

    def generateListReport(Map params, OutputStream outputStream) {

        GrailsClass domainClass = com.cloudmasons.BeanUtils.resolveDomainClass(grailsApplication, params.entity)
        log.debug("Running report on domain class $domainClass")

        def reportConfig = loadAndMergeListConfig(domainClass, params)

        DynamicReport dr = buildReport(domainClass, reportConfig, params.reportFormat)
        
        def items = beanService.filter(params, domainClass.clazz);
        JRDataSource dataSource = new JRBeanCollectionDataSource(items)
        
        writeReport(dr, dataSource, reportConfig, outputStream, params.reportFormat)
    
    }
    
    private void writeReport(report, dataSource, config, outputStream, reportFormat) {
    
        JasperPrint jasperPrint = DynamicJasperHelper.generateJasperPrint(report, new ClassicLayoutManager(), dataSource)
        def reportFileName = config.fileName
        
        JRExporter exporter = FormatInfoRegistry.getInstance().getExporter(reportFormat);
        exporter.setParameters([(JRHtmlExporterParameter.IMAGES_URI): "/beanReport/image?image=".toString(), (JRHtmlExporterParameter.IS_USING_IMAGES_TO_ALIGN): config.isUsingImagesToAlign]);
        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, outputStream);
        exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
        exporter.exportReport();
    }

    /*
     * Build a data source for report
     */
    private JRDataSource getDataSource(def domainClass, def config) {
        List items = null
        if (config.dataSource) {
            items = config.dataSource.call(domainClass, config)
        } else if (groupColumns) {
            items = domainClass.clazz.findAll("from $domainClass.clazz.name as s order by ${groupColumns.join(',')}")
        } else {
            items = domainClass.clazz.list()
        }
        // todo: scalability choke point - Perhaps we can write a JRDataSource that
        //       supports GORM and process items a page at a time rather than loading
        //       all the report items into memory and then processing them.
        JRDataSource dataSource = new JRBeanCollectionDataSource(items)
        
        return dataSource
    }
    
    /*
     * Build a report object for the given domain class, configuration and report format
     */
    private DynamicReport buildReport(def domainClass, def config, String reportFormat) {

        DynamicReportBuilder reportBuilder = new DynamicReportBuilder()
                .setUseFullPageWidth(true)
                .setTitle(config.title ?: "${domainClass?.naturalName} Report")
        setPropertyIfNotNull(reportBuilder, 'titleStyle', getStyle(config.titleStyle))
        setPropertyIfNotNull(reportBuilder, 'subtitleStyle', getStyle(config.subtitleStyle))
        setPropertyIfNotNull(reportBuilder, 'titleHeight', config.titleHeight)
        setPropertyIfNotNull(reportBuilder, 'subtitle', config.subtitle)
        setPropertyIfNotNull(reportBuilder, 'subtitleHeight', config.subtitleHeight)
        setPropertyIfNotNull(reportBuilder, 'detailHeight', config.detailHeight)
        setPropertyIfNotNull(reportBuilder, 'useFullPageWidth', config.useFullPageWidth)
        setPropertyIfNotNull(reportBuilder, 'grandTotalLegend', config.grandTotalLegend)
        setPropertyIfNotNull(reportBuilder, 'columnsPerPage', config.columnsPerPage)
        setPropertyIfNotNull(reportBuilder, 'columnSpace', config.columnSpace)
        setPropertyIfNotNull(reportBuilder, 'pageSizeAndOrientation', config.page)
        
        // Don't put page auto-text variables in xls reports because it doesn't
        // output correctly (the page breaks are inserted as rows).  Also tell
        // the builder to ignore pagination to make this fully work.
        if (reportFormat == 'PDF') {
            if (config.autoTexts) {
                for(AutoText atxt in config.autoTexts) {
                    reportBuilder.addAutoText(atxt)
                }
            }
        } else {
            reportBuilder.setIgnorePagination(true);
        }
        
        def columnNames = config.columns ?: getPropertyValue(domainClass.clazz, 'reportColumns') ?: domainClass.properties.name - ['id', 'version']

        def columns = addColumns(config, domainClass, reportBuilder, columnNames)

        def groupColumns = config.groupColumns
        if (groupColumns) {
            groupColumns.each {groupColumn ->
                GroupBuilder gb = new GroupBuilder()
                gb.criteriaColumn = columns[(groupColumn)]
                config.groupFooterColumns.eachWithIndex { groupFooterColumn, index ->
                    gb.addFooterVariable(columns[(groupFooterColumn)], getPropertyValue(DJCalculation, config.groupOperations[index]))
                }
                DJGroup group = gb.build()
                
                DJPieChartBuilder cb = new DJPieChartBuilder()
                //chart        
                cb.setX(20)
                cb.setY(10)
                cb.setWidth(500)
                cb.setHeight(250)
                cb.setCentered(false)
                cb.setBackColor(Color.LIGHT_GRAY)
                cb.setShowLegend(true)
                cb.setPosition(DJChartOptions.POSITION_FOOTER)
                cb.setLegendColor(Color.DARK_GRAY)
                cb.setLegendFont(Font.COURIER_NEW_MEDIUM_BOLD)
                cb.setLegendBackgroundColor(Color.WHITE)
                cb.setLegendPosition(DJChartOptions.EDGE_BOTTOM)
                cb.setTitlePosition(DJChartOptions.EDGE_TOP)
                cb.setLineStyle(DJChartOptions.LINE_STYLE_DOTTED)
                cb.setLineWidth(1)
                cb.setLineColor(Color.DARK_GRAY)
                cb.setPadding(5)
                //dataset
                cb.setKey((PropertyColumn) columns[(groupColumn)])
                //plot
                //cb.setDepthFactor(0.1F)
                //cb.setCircular(true)
                config.groupFooterColumns.eachWithIndex { groupFooterColumn, index ->
                    cb.addSerie(columns[(groupFooterColumn)])
                }
                //reportBuilder.addChart(cb.build());
                reportBuilder.addGroup(group);
            }
        }
        
        // Add gloabal header and footer variables
        config.headerVariables?.eachWithIndex {varName, index ->
            reportBuilder.addGlobalHeaderVariable(columns[(varName)], getPropertyValue(DJCalculation, config.headerOperations[index]));
        }
        config.footerVariables?.eachWithIndex {varName, index ->
            reportBuilder.addGlobalFooterVariable(columns[(varName)], getPropertyValue(DJCalculation, config.footerOperations[index]));
        }
        
        //reportBuilder.addGlobalFooterVariable(columns['matterNumber'], DJCalculation.COUNT);

        DynamicReport report = reportBuilder.build(); // build the report!
        // todo: get into builder
        //report.options.page = config.page
        report.options.defaultDetailStyle = getStyle(config.detailStyle)
        report.options.defaultHeaderStyle = getStyle(config.headerStyle)
        
        return report;
     }
     
    private Map addColumns(def config, def domainClass, def reportBuilder, def columnNames) {
        def columns = [:]
        if (columnNames == null) return columns
        def scripts = config.scripts ?: [:]
        def constraints = domainClass.constrainedProperties

        for (propertyName in columnNames) {
            def cb = new ColumnBuilder()            
            def property = getProperty(domainClass, propertyName)
            def cp =  constraints[propertyName]

            if (property == null) {
               throw new IllegalArgumentException("Unable to find ${propertyName} on ${domainClass.name}")
            }
            def propertyType = null
            def columnTitle = config.columnTitles[propertyName]  ?: GrailsNameUtils.getNaturalName(propertyName) 
            
            //println "Building column for ${propertyName}"
            //println property.manyToOne
            //println property.oneToOne
            
            // Automatically convert references to a many to one or one to one property to the name of the
            // referenced object
            if (property.manyToOne || property.oneToOne) {
                propertyName = "${propertyName}.name"
                property = getProperty(domainClass, propertyName);
            }
            // Set the width to be a little larger if the property
            // is a larger String property
            if (cp?.maxSize > 250) {
                cb.width = 310;
            }
            if (scripts[propertyName] != null) {
                   // See if it is a script
                cb.customExpression = new GroovyCustomExpression(scripts[propertyName], groovyScriptService)
                propertyType = String
            } else {
                propertyType = property.type
                switch (property.type) {
                    case int:
                        propertyType = Integer
                        break
                    case char:
                        propertyType = Character
                        break
                    case byte:
                    case short:
                    case long:
                    case float:
                    case double:
                    case boolean:
                        propertyType = Class.forName('java.lang.' + StringUtils.capitalize(property.type.name))
                        break
                    case Number:
                    case Boolean:
                    case Character:
                    case Date:
                    case String:
                        propertyType = property.type
                        break
                    default:
                        cb.customExpression = new ToStringCustomExpression(propertyName)
                        propertyType = String
                        // todo: migrate to report builder
                        //reportBuilder.report.fields << new ColumnProperty(propertyName, property.type.name)
                        //column = new ExpressionColumn()
                       // column.expression = new ToStringCustomExpression(propertyName)
                        //propertyType = String
                }
            }
            cb.setColumnProperty(propertyName, propertyType.name)

            //column.columnProperty = new ColumnProperty(propertyName, propertyType.name)
            cb.title = columnTitle // bean.label was here

            def style = getStyle(config.detailStyle)
            if (Number.isAssignableFrom(propertyType) || Date.isAssignableFrom(propertyType)) {
                style.horizontalAlign = HorizontalAlign.RIGHT
            } else {
                style = getStyle(config.detailStyle)
            }

            def propertyPattern
            if (config.patterns?."${propertyName}") {
                propertyPattern = config.patterns?."${propertyName}"
            } else {
                switch (propertyType) {
                    case Byte:
                    case Short:
                    case Integer:
                    case Long:
                        propertyPattern = config.intPattern
                        break;
                    case Float:
                    case Double:
                        propertyPattern = config.floatPattern
                        break;
                    case Date:
                        propertyPattern = config.datePattern
                        break;
                    default:
                        propertyPattern = null
                }
            }
            style.pattern = propertyPattern
            cb.style = style
            def column = cb.build()
            reportBuilder.addColumn(column)
            columns[(propertyName)] = column
        }
        return columns
    }

    def getProperty(def domainClass, def propertyName) {
        def property = null
        propertyName.tokenize('.').each { part ->
            property = domainClass.properties.find { prop ->
                prop.name == part
            }
            if (property) {
                def name = property.type.simpleName
                domainClass = grailsApplication.getArtefactByLogicalPropertyName(DomainClassArtefactHandler.TYPE, name[0].toLowerCase() + name[1 .. - 1])
            }
        }
        return property
    }

    private Object getPropertyValue(def clazz, def propertyName) {
        clazz.metaClass.hasProperty(clazz, propertyName)?.getProperty(clazz)
    }

    private void setPropertyIfNotNull(def target, def propertyName, def value) {
        if (value != null && (!(value instanceof ConfigObject) || !(value.isEmpty()))) {
            target[propertyName] = value
        }
    }

    private Style getStyle(def styleConfig) {
        def style = new Style()
        style.font = styleConfig.font
        if (styleConfig.border) {
            style.border = styleConfig.border
        } else {
            style.borderTop = styleConfig.borderTop
            style.borderBottom = styleConfig.borderBottom
            style.borderLeft = styleConfig.borderLeft
            style.borderRight = styleConfig.borderRight
        }
        style.backgroundColor = styleConfig.backgroundColor
        style.transparency = styleConfig.transparency
        //style.transparent = styleConfig.transparent
        style.textColor = styleConfig.textColor
        style.horizontalAlign = styleConfig.horizontalAlign
        style.verticalAlign = styleConfig.verticalAlign
        style.blankWhenNull = styleConfig.blankWhenNull
        style.borderColor = styleConfig.borderColor
        if (style.padding) {
            style.padding = styleConfig.padding
        } else {
            style.paddingTop = styleConfig.paddingTop
            style.paddingBotton = styleConfig.paddingBotton
            style.paddingLeft = styleConfig.paddingLeft
            style.paddingRight = styleConfig.paddingRight
        }
        //style.pattern = styleConfig.pattern
        style.radius = styleConfig.radius
        style.rotation = styleConfig.rotation
        //FIXME typo in DJ API
        //style.streching = styleConfig.stretching
        //style.stretchWithOverflow = styleConfig.stretchWithOverflow
        style
    }

    /*
     * Load the bean report config
     */
    private ConfigObject loadConfig(ConfigObject reportConfig) {
        def config = ConfigurationHolder.config
        GroovyClassLoader classLoader = new GroovyClassLoader(getClass().classLoader)
        config.merge(new ConfigSlurper(GrailsUtil.environment).parse(classLoader.loadClass('DefaultBeanReportConfig')))
        try {
            config.merge(new ConfigSlurper(GrailsUtil.environment).parse(classLoader.loadClass('BeanReportConfig')))
        } catch (Exception e) {
            // todo:  warn user that config is invalid
        }
        ConfigObject mergedConfig =  new ConfigSlurper(GrailsUtil.environment).parse(new Properties()).merge(config.dynamicJasper)
        if (reportConfig) {
            mergedConfig.merge(reportConfig)
        }
        return mergedConfig
    }
    
    def loadAndMergeListConfig(def domainClass, def params) {
        Properties props = new Properties()
        setPropertyIfNotNull(props, 'entity', params.entity)
        //setPropertyIfNotNull(props, 'title', classConfig.title)
        setPropertyIfNotNull(props, 'columns', params.columns ?: domainClass.properties.name - ['id', 'version'])
        //setPropertyIfNotNull(props, 'patterns', classConfig.patterns)
        //setPropertyIfNotNull(props, 'columnTitles', classConfig.columnTitles)
        //setPropertyIfNotNull(props, 'groupColumns', params.groupColumns?.split(',') ?: classConfig.groupColumns)
        //setPropertyIfNotNull(props, 'groupFooterColumns', params.groupFooterColumns?.split(',') ?: classConfig.groupFooterColumns)
        //setPropertyIfNotNull(props, 'groupOperations', params.groupOperations?.split(',') ?: classConfig.groupOperations ?: ('SUM,' * (props.groupFooterColumns?.size() ?: 0)).split(','))
        //setPropertyIfNotNull(props, 'dataSource', classConfig.dataSource)
        setPropertyIfNotNull(props, 'fileName', params.fileName)
        //setPropertyIfNotNull(props, 'useFullPageWidth', classConfig.useFullPageWidth)
        //setPropertyIfNotNull(props, 'page', classConfig.page)
        //setPropertyIfNotNull(props, 'intPattern', classConfig.intPattern)
        //setPropertyIfNotNull(props, 'floatPattern', classConfig.floatPattern)
        //setPropertyIfNotNull(props, 'datePattern', classConfig.datePattern)
        //setPropertyIfNotNull(props, 'titleStyle', classConfig.titleStyle)
        //setPropertyIfNotNull(props, 'subtitleStyle', classConfig.subtitleStyle)
        //setPropertyIfNotNull(props, 'headerStyle', classConfig.headerStyle)
        //setPropertyIfNotNull(props, 'detailStyle', classConfig.detailStyle)
        //setPropertyIfNotNull(props, 'autoTexts', classConfig.autoTexts)
        //setPropertyIfNotNull(props, 'isUsingImagesToAlign', classConfig.isUsingImagesToAlign)
        def reportConfig = new ConfigSlurper(GrailsUtil.environment).parse(props)
        
        return loadConfig(reportConfig)
    }

}