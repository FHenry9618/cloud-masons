package com.cloudmasons.beans

import java.util.concurrent.ConcurrentHashMap
import grails.util.GrailsNameUtils
import com.cloudmasons.BeanUtils
import org.codehaus.groovy.grails.scaffolding.DomainClassPropertyComparator;
import org.codehaus.groovy.grails.plugins.springsecurity.AuthorizeTools

class BeanService {

    boolean transactional = true
    def grailsApplication
    def securityService

    private static Map SETTING_CACHE = new ConcurrentHashMap()

    def getPropertyGroups(def bean) {
        getPropertyGroups(bean, null)
    }
    
    def storeUserSetting(Class clazz, String propName, String settingName, String value) {
        String classShortName = GrailsNameUtils.getShortName(clazz)
        String key = "${classShortName}-${propName}-${settingName}"
        SETTING_CACHE.put(key, value);
        
    }
    
    private String getUserSetting(Class clazz, String propName, String settingName) {
        String classShortName = GrailsNameUtils.getShortName(clazz);
        String key = "${classShortName}-${propName}-${settingName}"
        return SETTING_CACHE[key];
    }
    
    /**
     * Get the list of actions that can be performed on a bean
     */ 
    def getActions(beanDomainClass, actions) {
        if (actions) return actions
        
        def beanActions = beanDomainClass?.clazz?.newInstance()?.properties['actions']
        if (beanActions) return beanActions
        
        
        actions = 
        [
           [action: 'create', type:'static', label: 'Add'],
           [action: 'edit',   type:'instance'],
           [action: 'delete', type:'instance']
        ]
        
        return actions
    }

    def getPropertyGroups(def bean, def propertyGroups, def excludes = null) {
        def domainClass = BeanUtils.resolveDomainClass(grailsApplication, bean)
        def constraints = domainClass.constrainedProperties
        def comparator = new DomainClassPropertyComparator(domainClass)
        def props = domainClass.persistentProperties.sort(comparator)
        props = props.findAll { constraints[it.name].editable != false }

        // Check of there are any transient properties in the constrained property
        // list that should be added
        constraints.each { name, c ->
            if (c.editable != false && !props.find { it.name == name; }) {
                //println name
                props.add( domainClass.getPropertyByName(name) );
            }
        }


        def groups = [:]

        if (propertyGroups != null) {
            // convert the specified property groups to
            propertyGroups.each { groupName, propNames ->

                if (propNames) {
                    groups[groupName] = propNames.collect {
                        def prop = domainClass.getPropertyByName(it);
                        if (prop == null) throw new IllegalArgumentException("Unknown property ${it}");
                        prop
                    }
                } else {
                    // If no properties were specified, collect the properties for the group
                    def groupProperties = []
                    props?.each { prop ->
                        def propGroupName = constraints[prop.name]?.getAppliedConstraint('group')?.groupName ?: 'Properties'
                        if (propGroupName == groupName) {
                            groupProperties.add(prop)
                        }
                    }
                   groups[groupName] = groupProperties
                }
            }
        } else { // build the defaults based on the group constraint
            props?.each { prop ->
               def groupName = constraints[prop.name]?.getAppliedConstraint('group')?.groupName ?: 'Properties'
               def groupProperties = groups[groupName] ?: []
               groupProperties.add(prop)
               groups[groupName] = groupProperties
            }
        }
        return groups
    }

    def getSummaryProperties(def bean, List overrides=null) {
        def domainClass = BeanUtils.resolveDomainClass(grailsApplication, bean)
        def constraints = domainClass.constrainedProperties
        def comparator = new DomainClassPropertyComparator(domainClass)

        def props = domainClass.properties.sort(comparator)

        List result = []

        props?.each { prop ->
           if (overrides) {
                if (overrides.contains(prop.name)) result.add(prop);
           } else {
               def sConstraint = constraints[prop.name]?.getAppliedConstraint('summary')
               if (sConstraint) {
                   result.add(prop)
               }
           }
        }

        if (!result) {
            result.add(domainClass.getPropertyByName("name"))
            result.add(domainClass.getPropertyByName("description"))
        }
        log.debug result
        return result
    }

    // dtg: I think calling this method 'getProperties/ will mess something up with the service
    //      but I didn't confirm.
    def getProps(def bean, String group) {
        def domainClass = BeanUtils.resolveDomainClass(grailsApplication, bean)
        def constraints = domainClass.constrainedProperties
        def comparator = new DomainClassPropertyComparator(domainClass)
        def props = domainClass.persistentProperties.sort(comparator)

        List result = []

        props?.each { prop ->
           def g = constraints[prop.name]?.getAppliedConstraint('group')?.groupName
           g = g ?: 'Properties'
           log.debug prop.name
           if (!group || g == group) {
               result.add(prop)
           }
        }
        log.debug result
        return result
    }

    def filter(Map params, Class filterClass) {
        return list(params, filterClass, false)
    }


    def count(Map params, Class filterClass) {
        return list(params, filterClass, true)
    }

    def list(Map params, Class filterClass, boolean doCount) {
        log.debug("filtering... params = ${params.toMapString()}")

        def filterParams = params.filter ? params.filter : params
        def filterOpParams = filterParams.op
        def associationList = []
        def domainClass = BeanUtils.resolveDomainClass(grailsApplication, filterClass)

        def roleFilterCriteria = null
        def actionsPresent = domainClass.clazz.metaClass.properties.find { it.name == 'actions' }
        if (actionsPresent) {
            def actionMap =  domainClass.clazz.actions?.find { it.action == 'list' }
            def role = actionMap?.role
            if (role instanceof Map) {
                boolean hasRole = false
                role.each { key, args ->
                    if (!hasRole) {
                        def roleName = securityService.resolveRoleName(key)
                        //println roleName
                        hasRole = AuthorizeTools.ifAnyGranted(securityService.resolveRoleName(key))
                        //println hasRole
                        if (hasRole && args.filter) {
                            //println "Found a role for a user that has a list filter"
                            roleFilterCriteria = args.filter
                        }
                    }
                }
            }
        }

        if (roleFilterCriteria || filterOpParams != null && filterOpParams.size() > 0) {
            def c = filterClass.createCriteria()

            def criteriaClosure = {
                def mc = filterClass.getMetaClass()
                if (filterOpParams != null && filterOpParams.size() > 0) {

                    and {
                        // First pull out the op map and store a list of its keys.
                        def keyList = []
                        keyList.addAll(filterOpParams.keySet())
                        keyList = keyList.sort() // Sort them to get nested properties next to each other.

                        log.debug("op Keys = ${keyList}")

                        // op = map entry.  op.key = property name.  op.value = operator.
                        // params[op.key] is the value
                        keyList.each() { propName ->
                            log.debug("\n=============================================================================.")
                            log.debug("== ${propName}")

                            // Skip associated property entries.  (They'll have a dot in them.)  We'll use the map instead later.
                            if (! propName.contains(".")) {

                                def filterOp = filterOpParams[propName]
                                def rawValue = filterParams[propName]
                                def rawValue2 = filterParams["${propName}To"]
                                // If the filterOp is a Map, then the propName is an association (e.g. Book.author)
                                if (filterOp instanceof Map && rawValue instanceof Map) {

                                    // Are any of the values non-empty?
                                    if (filterOp.values().find {println it; it.length() > 0} != null) {

                                        log.debug("== Adding association ${propName}")
                                        c."${propName}"() {

                                            filterOp.each() { opEntry ->
                                                def associatedDomainProp = BeanUtils.resolveDomainProperty(grailsApplication, domainClass, propName)
                                                def associatedDomainClass = associatedDomainProp.referencedDomainClass
                                                def realPropName = opEntry.key
                                                def realOp = opEntry.value
                                                def realRawValue = rawValue[realPropName]
                                                def realRawValue2 = rawValue2 != null ? rawValue2["${realPropName}To"] : null
                                                def parsingName = "${propName}.${realPropName}"
                                                def thisDomainProp = BeanUtils.resolveDomainProperty(grailsApplication, associatedDomainClass, realPropName)
                                                def val  = this.parseValue(thisDomainProp, realRawValue, filterParams, parsingName)
                                                def val2 = this.parseValue(thisDomainProp, realRawValue2, filterParams, parsingName)
                                                this.addCriterion(c, realPropName, realOp, val, val2, filterParams, thisDomainProp)
                                            }
                                            if (!doCount) {
                                                if (params.sort && params.sort.startsWith("${propName}.")) {
                                                    def parts = params.sort.split("\\.")
                                                    if (parts.size() == 2) {
                                                        associationList << propName
                                                        order(parts[1], params.order ?: 'asc')
                                                    }
                                                }
                                            }
                                        } // end c.propName closure.
                                    } // end if any values not empty.
                                } else {

                                    log.debug("propName is ${propName}")
                                    def thisDomainProp = BeanUtils.resolveDomainProperty(grailsApplication, domainClass, propName)
                                    def val  = this.parseValue(thisDomainProp, rawValue, filterParams, null)
                                    def val2 = this.parseValue(thisDomainProp, rawValue2, filterParams, "${propName}To")
                                    log.debug("== propName is ${propName}, rawValue is ${rawValue}, val is ${val} of type ${val?.class} val2 is ${val2} of type ${val2?.class}")
                                    this.addCriterion(c, propName, filterOp, val, val2, filterParams, thisDomainProp)
                                }
                            }
                            log.debug("===============================================================================")
                        } // end each op
                    } // end and
                }
                if (roleFilterCriteria) {
                    log.debug("------ adding role filter criteria ------------------");
                    roleFilterCriteria.delegate = c
                    roleFilterCriteria()
                }

                if (doCount) {
                    c.projections {
                        rowCount()
                    }
                } else {
                    if (params.offset) {
                        firstResult(params.offset.toInteger())
                    }
                    if (params.max) {
                        maxResults(params.max.toInteger())
                    }
                    if (params.sort) {
                        if (params.sort.indexOf('.') < 0) { // if not an association..
                            order(params.sort, params.order ?: 'asc')
                        } else {
                            def parts = params.sort.split("\\.")
                            if (!associationList.contains(parts[0])) {
                                c."${parts[0]}" {
                                    order(parts[1], params.order ?: 'asc')
                                }
                            }
                        }
                    }
                }
            } // end criteria
            def results = (doCount) ? c.get(criteriaClosure) : c.list(criteriaClosure)
            if (doCount && results instanceof List) {
                results = 0I
            }
            return results
        } else {
            if (doCount) {
                return filterClass.count()//0I
            }
            return filterClass.list(params)
        }
    }
    
    private addCriterion(criteria, propertyName, op, value, value2, filterParams, domainProperty) {
        log.debug("Adding ${propertyName} ${op} ${value} value2 ${value2}")
        boolean added = true

        // GRAILSPLUGINS-1320.  If value is instance of Date and op is Equal and
        // precision on date picker was 'day', turn this into a between from
        // midnight to 1 ms before midnight of the next day.
        boolean isDayPrecision = "y".equals(filterParams["${domainProperty.domainClass.name}.${domainProperty.name}_isDayPrecision"])
        boolean isOpAlterable  = (op == 'Equal' || op == 'NotEqual')
        if (value != null && isDayPrecision == true && Date.isAssignableFrom(value.class) && isOpAlterable) {
            op = (op == 'Equal') ? 'Between' : 'NotBetween'
            value = BeanUtils.getBeginningOfDay(value)
            value2 = BeanUtils.getEndOfDay(value)
            log.debug("Date criterion is Equal to day precision.  Changing it to between ${value} and ${value2}")
        }
        

        if (value) {
            if ((domainProperty.manyToOne || domainProperty.oneToOne) && (op != 'IsNull' && op != 'IsNotNull') ) {
                propertyName = "${propertyName}.id"
                value = value.toLong()
            }
            switch(op) {
                case 'Equal':
                criteria.eq(propertyName, value)
                break
                case 'NotEqual':
                criteria.ne(propertyName, value)
                break
                case 'LessThan':
                criteria.lt(propertyName, value)
                break
                case 'LessThanEquals':
                criteria.le(propertyName, value)
                break
                case 'GreaterThan':
                criteria.gt(propertyName, value)
                break
                case 'GreaterThanEquals':
                criteria.ge(propertyName, value)
                break
                case 'Like':
                if (!value.startsWith('*')) value = "*${value}"
                if (!value.endsWith('*')) value = "${value}*"
                criteria.like(propertyName, value?.replaceAll("\\*", "%"))
                break
                case 'ILike':
                if (!value.startsWith('*')) value = "*${value}"
                if (!value.endsWith('*')) value = "${value}*"
                criteria.ilike(propertyName, value?.replaceAll("\\*", "%"))
                break
                case 'NotLike':
                if (!value.startsWith('*')) value = "*${value}"
                if (!value.endsWith('*')) value = "${value}*"
                criteria.not {
                    criteria.like(propertyName, value?.replaceAll("\\*", "%"))
                }
                break
                case 'NotILike':
                if (!value.startsWith('*')) value = "*${value}"
                if (!value.endsWith('*')) value = "${value}*"
                criteria.not {
                    criteria.ilike(propertyName, value?.replaceAll("\\*", "%"))
                }
                break
                case 'IsNull':
                criteria.isNull(propertyName)
                break
                case 'IsNotNull':
                criteria.isNotNull(propertyName)
                break
                case 'Between':
                criteria.between(propertyName, value, value2)
                break
                case 'NotBetween':
                criteria.not { between(propertyName, value, value2) }
                break
                default:
                break
            } // end op switch
        } else {  // value is null
            added = false
        }
        //println "== addCriterion OUT =="
    }

    def parseValue(def domainProperty, def val, def params, def associatedPropertyParamName) {
        if (val) {
            Class cls = domainProperty.referencedPropertyType ?: domainProperty.type
            String clsName = cls.simpleName.toLowerCase()
            log.debug("domainProperty is ${domainProperty}, type is ${domainProperty.type}, refPropType is ${domainProperty.referencedPropertyType} val is ${val}, clsName is ${clsName}")

            if (domainProperty.isEnum()) {
                val = Enum.valueOf(cls, val.toString())
            } else if ("boolean".equals(clsName)) {
                val = val.toBoolean()
            } else if ("int".equals(clsName) || "integer".equals(clsName)) {
                val = val.toInteger()
            } else if ("long".equals(clsName)) {
                val = val.toLong()
            } else if ("double".equals(clsName)) {
                val = val.toDouble()
            } else if ("float".equals(clsName)) {
                val = val.toFloat()
            } else if ("short".equals(clsName)) {
                val = val.toShort()
            } else if ("bigdecimal".equals(clsName)) {
                val = val.toBigDecimal()
            } else if ("biginteger".equals(clsName)) {
                val = val.toBigInteger()
            } else if (java.util.Date.isAssignableFrom(cls)) {
                def paramName = associatedPropertyParamName ?: domainProperty.name
                val = BeanUtils.parseDateFromDatePickerParams(paramName, params)
            }
        }
        return val
    }
    
    def saveRequestHistoryToSession(def params, def session) {
        List previousRequests = session.previousRequests
        if (!previousRequests) {
            previousRequests = []
        }
        if (previousRequests.size > 20) {
            previousRequests.remove(0)
        }
        if (params.action) {
            def previousRequest = [:]
            previousRequest.action = params.action
            previousRequest.controller = params.controller
            if (params.filter) { 
                params.filter = [] //to eliminate issue for redireting with filters
            }
            previousRequest.params = params
            previousRequests.add(previousRequest)
            
        }
        session.previousRequests = previousRequests
    }
    
    /**
     * Process removals of one-to-many objects during an update of an object
     */
    public processRemovals(params, bean) {

        for(String key in params.keySet()) {
            if (key.startsWith('removeFrom')) {
                def propName = key - 'removeFrom'
                def methodName = propName[0].toUpperCase() + propName[1..-1]

                methodName = 'removeFrom' + methodName
                def paramValue = params[key]
                if (!paramValue) continue

                def databaseIds = paramValue?.split('; ')
                def currentValue = bean."${propName}"
                println currentValue
                def valuesToRemove = []
                databaseIds?.each { databaseId ->
                    def value = currentValue.find { it.id == databaseId.toLong(); }
                    if (value) {
                        valuesToRemove.add(value);
                    }
                } 
                println valuesToRemove
                for (it in valuesToRemove) {
                    if (it.properties['deleteValidator']) {
                        def error =  it.deleteValidator?.call(it)
                        if (error) {
                            bean.errors.rejectValue(propName, 'delete.error.message', error);
                            println bean.errors
                            break;
                        }
                    }
                    bean."${methodName}" it 
                    it.delete()
                }
                println bean."${propName}"
            }
        }
    }

}