package com.cloudmasons.beans

import com.cloudmasons.BeanUtils
import com.cloudmasons.util.Transforms;
import grails.util.GrailsNameUtils

class FeedService {

    def grailsApplication
    def groovyScriptService
    def sessionFactory
    def searchableService
    
    def propertyInstanceMap = org.codehaus.groovy.grails.plugins.DomainClassGrailsPlugin.PROPERTY_INSTANCE_MAP


    def createDefaultFeed(String name, String description, String domain, List props=null) {
    
        Feed feed = new Feed(name: name, description: description);
        feed.beanType = domain
        feed.correlator = """
            def name = record['Name']
            def instance = null
            if (!name) return instance
            instance = domainClass.clazz.findByName(name);
            if (instance) return instance
            
            instance = domainClass.newInstance()
            return instance
        """
        
        def propertyNames = props ?: ['name', 'description']
        propertyNames?.each { p ->
            feed.addMapping(GrailsNameUtils.getNaturalName(p), p)
        }
        
        def result = feed.save()
        if (result) {
            feed = result
        } else {
            println feed.errors
        }
        return feed
    }

    /**
     * Attempts to import any file that is supported.
     */
    def processFileFeed = {  Feed feed, File file, Map defaultRecordValues=null ->
        String warnings = null
        // determine by extension which service to call import from..
        log.debug("Attempting to import file: $file")
        def domainClass = BeanUtils.resolveDomainClass(grailsApplication, feed.beanType)
        if (domainClass == null) {
            def msg = "Unable to determine the type of object being loaded from ${feed.beanType}"
            throw new IllegalArgumentException(msg)
        }
        if (feed == null) {
            // todo:: attempt to determine the feed type
            // based on the filename or meta data in the spreadsheet
            def msg = "A feed definition must be provided to process a file feed"
            throw new IllegalArgumentException(msg)
        }
        def filename = file.name
        if (filename.endsWith('.xls')) {
            def processor = new XLSFeedProcessor()
            processor.defaultRecordValues = defaultRecordValues ?: [:]
            warnings = processFeed(processor, file, feed, domainClass)
        } else if (filename.endsWith('.csv')) {
            def processor = new CSVFeedProcessor()
            processor.defaultRecordValues = defaultRecordValues ?: [:]
            warnings = processFeed(processor, file, feed, domainClass)
        } else {
            def msg = "Unknown file type: $file"
            throw new IllegalArgumentException(msg)
        }
        clearSession()
        return warnings
    }

    def processFeed(feedProcessor, file, feed, domainClass) {
        StringBuilder allWarnings = new StringBuilder()
        searchableService.stopMirroring()
        try {
            feedProcessor.process(file, feed, recordProcessor.curry(allWarnings))
        } finally {
            searchableService.startMirroring()
            // todo: only index the ids that were updated
            searchableService.index()
        }
        String result = null
        if (allWarnings.length() > 15000) {
            result = allWarnings.substring(0,15000) + "</td></tr><tr><td>..................  results truncated..................</td</tr>"
        } else { 
            result = allWarnings.toString()
        }
        if (result) {
            result = "<table width=\"700\"><tbody>" + result + "</tbody></table"
        }
    }

    def recordProcessor = { allWarnings, record, feed ->
        //println "Processing record ${record}"
        StringBuilder warnings = new StringBuilder()
        
        def domainInstance = null
        def domainClass = BeanUtils.resolveDomainClass(grailsApplication, feed.beanType)
        if (feed.correlator != null) {
            def args = [record:record, 
                        warnings: warnings, 
                        grailsApplication: grailsApplication,
                        domainClass: domainClass]
            domainInstance = evaluateGroovy(feed.correlator, args)
        }

        if (domainInstance == null) {
            // todo log error
            if (!warnings.toString()) {
                warnings.append("Unable to create a ${domainClass.naturalName} with given data:<br/> ${record}<br/>")
            }
        } else {
            
            def context = [:]

            // Evaluate the pre-processor
            evaluateScript(feed.recordPreProcessor, record, domainClass, domainInstance, context, warnings);
            
            feed.mappings?.each { mapping ->
                  log.debug "processing record: ${record}<br/><br/>"
            
                def value = evaluateSources(record, mapping.sources, domainClass, domainInstance, context, warnings)
                log.debug "${mapping.sources} ${mapping.targets} ${value}"
                mapping.targets?.each { target ->

                    def targetValue = value
                    def currentValue = null
                    if (target.name) {
                        currentValue = domainInstance.properties[target.name]
                    }
                    if (target.transform) {
                        def args = [(domainClass.propertyName): domainInstance, 
                                    record: record, 
                                    value: value, 
                                    context: context,
                                    target: target,
                                    currentValue: currentValue,
                                    grailsApplication: grailsApplication,
                                    warnings: warnings]
                        
                        String transform = Transforms.getTransform(target.transform)
                        targetValue = evaluateGroovy(transform, args)
                    }
                    
                    log.debug " ${target.name}  = ${targetValue}"
                    // The target can be null if the transformation sets the value
                    if (target.name != null) {
                        domainInstance.properties[target.name] = targetValue
                    }
                }
            }
            

            if (feed.recordPostProcessor) {
                 // Evaluate the post-processor
                evaluateScript(feed.recordPostProcessor, record, domainClass, domainInstance, context, warnings);
            } else {
                def saveResult = domainInstance.save()
                if (!saveResult) {
                    String errors = "${domainInstance.errors}"
                    if (errors.length() > 300) {
                     errors = errors.substring(0, 300) + " truncated..."
                    }
                    warnings.append( "Error saving bean ${errors}")
                    domainInstance.discard()
                }       
            }
        }
        String warn = warnings.toString()
        if (warn.length() > 0) {
             String recordStr = record.toString().encodeAsHTML()
             if (recordStr.length() > 300) {
                recordStr = recordStr.substring(0, 300) + " truncated..."
             }
             allWarnings.append( "<tr><td width=\"700\">${warn.encodeAsHTML()}<br/><br/>While processing record: ${recordStr}</td></tr>")
        }

    }
    private Object evaluateScript(String script, Map record, Object domainClass, Object domainInstance, Map context, Object warnings) {
        if (script == null) return null;
        def args = [(domainClass.propertyName): domainInstance, 
                                    record: record, 
                                    context: context,
                                    grailsApplication: grailsApplication,
                                    warnings: warnings]
        def value = evaluateGroovy(script, args)
        return value;
    }
    private Object evaluateSources(Map record, List sources, Object domainClass, Object domainInstance, Map context, Object warnings) {
        List values = []
        sources?.each { s ->
            if (s.transform != null) {
                def value = record[s.name]
                def args = [(domainClass.propertyName): domainInstance, 
                                    record: record, 
                                    value: value, 
                                    context: context,
                                    grailsApplication: grailsApplication,
                                    warnings: warnings,
                                    currentValue: null]
                String transform = Transforms.getTransform(s.transform)
                def sourceValue = evaluateGroovy(transform, args)
                values.add(sourceValue)
            } else {
                values.add( record[s.name] )
            }
        }
        if (values.size() == 1) {
            return values.first()
        }
        return values
    }
    
    
    
    
    /**
     * Evaluate a groovy script provided as a string with the given map of variables
     * available to the script.
     *
     * @return The result of the groovy evaluation
     */
    private Object evaluateGroovy(String groovy, Map variables) {
        try {
            return groovyScriptService.evaluate(groovy, variables)
        } catch (Exception ex) {
            log.error("Failed to run script:<br/>${groovy}", ex)
        }
    }
    
    def clearSession() {
        def session = sessionFactory.currentSession
        session.flush()
        session.clear()
        propertyInstanceMap.get().clear()
    }

}