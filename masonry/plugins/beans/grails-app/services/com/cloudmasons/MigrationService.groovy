package com.cloudmasons

import liquibase.Liquibase
import liquibase.database.DatabaseFactory
import liquibase.ClassLoaderFileOpener
import javax.sql.DataSource
import org.springframework.core.io.ClassPathResource
 
 /**
  * Liquibase database migration service
  */
class MigrationService {

    boolean transactional = false

    def execute(String changelogFileName, DataSource dataSource) {
        Liquibase liquibase = null
        try {
            def c = dataSource.connection
            if (c == null) {
                throw new RuntimeException("Connection could not be created.");
            }
            def fileOpener = new MigrationFileOpener()
            def database = DatabaseFactory.instance.findCorrectDatabaseImplementation(c)
            database.defaultSchemaName = c.catalog
            liquibase = new Liquibase(changelogFileName, fileOpener, database);
            liquibase.update(null)
        }
        finally {
            if (liquibase && liquibase.database) {
                liquibase.database.close()
            }
        }
    }
}

class MigrationFileOpener extends ClassLoaderFileOpener {

    def InputStream getResourceAsStream(String file) {
        return new FileInputStream(new File(file))
    }

}
