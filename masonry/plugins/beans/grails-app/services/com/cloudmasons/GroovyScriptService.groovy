package com.cloudmasons

/**
 * Service to run scripts as groovy files
 */
class GroovyScriptService {

    def grailsApplication
    def passwordEncoder
    
    static transactional = false
    
    // Cache the compiled scripts.  If we don't this
    // then compiling the same scripts over and over will
    // cause the perm gen space to run out.  
    def scriptCache = [:]
    
    // Imports to be add to each script
    static DEFAULT_IMPORTS = [
        "import com.cloudmasons.*",
        "import com.cloudmasons.util.*"
    ]
    
    /**
     * Evaluate the script (cache the script class created).
     */
    def evaluate(String script) {
        evaluate(script, new Binding())
    }
    def evaluate(String script, Map bindings) {
        evaluate(script, new Binding(bindings ?: [:]))
    }
    def evaluate(String script, String[] bindings) {
        evaluate(script, new Binding(bindings ?: [] as String[]))
    }
    def evaluate(String script, Binding binding) {
        assert script && binding
        def clazz = findScriptClass(script)
        def instance = clazz.newInstance()
        // determine if the log property is there..
        try {
            binding.getVariable('log')
        } catch (MissingPropertyException mpe) {
            log.trace('Adding \'log\' variable.')
            binding.setVariable('log', log)
        }
        // Give the script the ability to encode passwords (if acegi plugin is installed)
        binding.setVariable('passwordEncoder', passwordEncoder)
        instance.setBinding(binding)
        // log all the variables
        if (log.isTraceEnabled()) {
            log.trace("Binding: ${binding}")
        }
        def result = null
        try {
            result = instance.run()
        } catch (Exception ex) {
            log.debug("Failed on script:\n ${script}",ex)
            throw ex
        }
        // log the result
        if (log.isTraceEnabled()) {
            log.trace("Result: ${result}")
        }
        result
    }
    
    /**
     * Cache scripts and generate the groovy.lang.Script class from the
     * text provided.
     */
    def findScriptClass(String script) {
        def scriptClass = null
        synchronized (GroovyScriptService) {
            // key off the unmodified script text
            scriptClass = scriptCache[script]
            if (scriptClass == null) {
                
                // parse into a class representing groovy.lang.Script
                // while adding default imports 
                def classLoader = new GroovyClassLoader(grailsApplication.classLoader)
                def fullScript = buildScript(script)
                scriptClass = classLoader.parseClass(fullScript)
                
                // cache the parsed class using original script
                scriptCache[script] = scriptClass
            }
        }
        scriptClass
    }
    
    /**
     * Add default imports to scripts.
     */
    def buildScript(String script) {
        StringWriter swrt = new StringWriter()
        PrintWriter wrt = new PrintWriter(swrt)
        for (String im : DEFAULT_IMPORTS) {
            wrt.println(im)
        }
        wrt.println(script)
        swrt.getBuffer().toString()
    }
}


