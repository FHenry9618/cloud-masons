<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<g:set var="title"><%= grails.util.GrailsNameUtils.getNaturalName(beanInstance.getClass().name) %></g:set>
<html>
<head>
<title>${title}: ${beanInstance.name}</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <style type="text/css">
label {
  font-family: Helvetica, Arial, sans-serif;
  font-weight: bold;
  font-size: large;
}

h1 {
  margin-bottom: 10px;
  font-size: 2.2em;
  font-weight: bolder;
}

ul {
  list-style-type: none;
}

.property {
  margin-left: 10px;
}
    </style>
<link rel="stylesheet" type="text/css" href="${resource(dir: 'css', file:'pdf.css')}"/>
</head>

<body>
<center>
    <h2>${beanInstance.name} - ${title}</h2>
</center>
<hr/>
<theme:propertyGroups bean="${beanInstance}" style="print"/>
<hr/>
<center>
    <h2>${beanInstance.name} - ${title}</h2>
</center>
</body>
</html>
