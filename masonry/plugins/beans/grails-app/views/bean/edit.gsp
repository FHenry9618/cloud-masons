
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: "${domainClass.propertyName}.label", default: grails.util.GrailsNameUtils.getNaturalName(domainClass.shortName))}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>      
      <span>
        <theme:edit bean="${beanInstance}" beanControllerName="${beanControllerName}" propertyGroups="${propertyGroups}"/>
      </span> 
    </body>
</html>
