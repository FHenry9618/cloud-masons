                <theme:panel>
                        <div class="header">
                            <h3>Run Data Feeds</h3>
                        </div>
                        <div class="content">
                            <ul class="simple">
                              <g:each var="feed" in="${feeds}">
                                <li>
                                  <table>
                                    <tbody>
                                      <tr><td valign="top">
                                      <ul>
                                        <li> <strong><g:link controller="feed" action="edit" id="${feed.id}">${feed.name}</g:link></strong></li>
                                         <li>${feed.description}</li>
                                       </ul>
                                       </td><td valign="top">
                                   <span> 
                                     <g:form method="post" controller="feed" action="upload" enctype="multipart/form-data">
                                        <g:hiddenField name="id" value="${feed.id}" />
                                        <input name="uploadFile" type="file" class="file">
                                        <input type="submit" value="${g.message(code:'Upload')}">
                                        </g:form>
                                   </span>

                                       </td></tr>
                                     </tbody>
                                   </table>
                                 </li>
                              </g:each>
                            </ul>
                        </div>
                </theme:panel>
