
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <beans:listIncludes/>
        <g:set var="entityName" value="${message(code: '${domainClass.propertyName}.label', default: domainClass.shortName)}" />
        <title><g:message code="default.list.label" args="${[entityName]}" /></title>
    </head>
    
    <body>
       <theme:list listStyle="${listStyle}"   beanClass="${domainClass.fullName}" beans="${beanList}" 
                   beanParams="${beanParams}" total="${beanTotal}"                beanControllerName="${beanControllerName}" />
                   
                   
    </body>
</html> 