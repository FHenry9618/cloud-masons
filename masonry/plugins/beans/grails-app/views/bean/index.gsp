
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
       
        <title>Beans</title>
    </head>
    <body>      
      <span>
        <table>
          <thead>
             <tr>
                <th>Bean Class</th>
                <th>Number</th>
             </tr>
          </thead>
          <tbody>
          <g:each var="bean" in="${beans}">
            <tr>
              <td>${bean.domainClass.naturalName}</td>
              <td>${bean.count}</td>
            </tr>
          </g:each>
          </tbody>
        </table>
      </span> 
    </body>
</html>
