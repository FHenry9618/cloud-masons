


                <div class="box box-100 altbox">
                    <div class="boxin">
                        <div class="header">
                            <h3>Reports</h3>
                        </div>
                        <div class="content">
                            <form class="plain" action="" method="post" enctype="multipart/form-data">
                                <fieldset>
                                    <div class="grid"><!-- grid view -->
                    <g:each in="${reports}" status="i" var="report">
                                   <g:if test="${i % 2 == 0L && i > 1}">
                                        </div>
                                   </g:if>
                                   <g:if test="${i % 2 == 0L}">
                                        <div class="line">
                                   </g:if>

                                            <div class="item">
                                                <div class="inner">
                                                    <div class="data">
                                                        <h4>${report.name}</h4>
                                                        <p>${report.description}</p>
                                                        <ul class="actions">
                              <g:each in="${formats}" status="j" var="format">
                                 <li>
                                    ${j > 0 ? '|&nbsp;&nbsp;' : ''}
                                    <a href="${g.createLink(controller: 'beanReport', action: 'run', params: ['report': report.id, 'reportClass': 'Report', 'reportFormat': format ])}">${format}</a>
                                 </li>
                              </g:each>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                        
                    </g:each>

                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
                
