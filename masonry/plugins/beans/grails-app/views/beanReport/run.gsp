
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: "${domainClass.propertyName}.label", default: grails.util.GrailsNameUtils.getNaturalName(domainClass.shortName))}" />
        <title>Generating Report</title>
        
    </head>
    <body> 
    
    <g:if test="${flash.message}">
        <div class="msg msg-ok">${flash.message}</div>
    </g:if>
    <theme:panel width="100">
    
       <div id="cloud-header" class="header">
         <h3>Generating Report: ${report.name}</h3>
         
       </div>
       <div  class="content">                
                    <table style="width: 85%; ">
                      <tr>
                        <td style="background: #FFFFFF; vertical-align: middle; text-align: center;">
                         <img src="${g.resource(absolute: true, plugin: 'beans', dir: 'images', file: 'gears.png')}" alt="Please wait" width="128" /></a>
                        </td>
                        <td style="background: #FFFFFF; vertical-align: middle;">
                            <h3><b>Please wait while your report is being generated...</b></h3>
                        </td>
                      </tr>
                    </table>

       </div>

    </theme:panel>     

 <script type="text/javascript">
 function checkStatus() {
    //alert('checking status');
  $.ajax({
    url: '${g.createLink(controller: 'beanReport', action: 'runStatus', params: ['report': report.id, 'reportClass': reportClass, 'reportFormat': reportFormat ])}'+'&time='+new Date().getTime(), // time
    dataType: 'json',
    error: function(xhr_data) {
      // terminate the script
            alert('error');
            history.go(-1);
            window.location = '${g.createLink(controller: 'dashboard', action: 'index')}';

    },
    success: function(xhr_data) {
      //alert(xhr_data.status);
      if (xhr_data.status == 'generating') {
            setTimeout(function() { checkStatus(); }, 5000); // wait 5 seconds than call ajax request again
      } else {
            //alert('done');
            setTimeout(function() { gotoDashboard(); }, 5000);
      }
    },
    contentType: 'application/json'
  });
  
  function gotoDashboard() {
  
              window.location = '${g.createLink(controller: 'dashboard', action: 'index')}';
  }   
}

$(document).ready(function() {
    setTimeout(function() { checkStatus(); }, 5000); 

    jQuery('<form action="${g.createLink(controller: 'beanReport', action: 'index', params: ['report': report.id, 'reportClass': reportClass, 'reportFormat': reportFormat ])}" method="post"></form>')
           .appendTo('body').submit().remove();

});


 </script>
    </body>
</html>
