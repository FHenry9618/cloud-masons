<html>
    <head>
        <meta http-equiv="Content-Type" content=theme"text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title>Run Data Feed</title>
    </head>
    <body>
      <g:if test="${flash.message}">
        <theme:panel width="100">
            <div class="header">
                <h3>Results</h3>
            </div>
            <div class="content">
              ${flash.message}
            </div>
        </theme:panel>
      </g:if>

      <bean:feeds/> 
   </body>
</html>