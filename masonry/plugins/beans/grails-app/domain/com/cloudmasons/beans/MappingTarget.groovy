package com.cloudmasons.beans

class MappingTarget {
    
    static belongsTo = FieldMapping

    FieldMapping fieldMapping 
    String name                // The name of the property to set
    
    String referencedBeanType  // If the property is a reference
    String referencedPropertyName
    String referenceResolver = '***first***'   // If the property is a collection, then
                               // we need to figure out which members of the
                               // the collection to update      
    
    static mapping = {
        table 'feed_mapping_target'
    }
    
    String transform
    
    static transients = [ 'description' ]
    
    static constraints = {
           name(summary: true)
           transform(nullable: true, maxSize: 2000)
           referencedBeanType(nullable: true)
           referencedPropertyName(nullable: true)
           referencedPropertyName(nullable: true)
    }
    
    
    public String getDescription() {
        return "${name}"
    }

}