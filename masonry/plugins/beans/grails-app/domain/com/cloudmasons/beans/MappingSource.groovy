package com.cloudmasons.beans

class MappingSource {
    
    static belongsTo = FieldMapping

    FieldMapping fieldMapping 
    String name                // The name of the property to read from
    
    String transform           // Transformation of the source value
    
    static transients = [ 'description' ]
    
    static mapping = {
        table 'feed_mapping_source'
    }
    
    static constraints = {
           name(summary: true)
           transform(nullable: true, maxSize: 2000)
    }
    
    
    public String getDescription() {
        return "${name}"
    }

}