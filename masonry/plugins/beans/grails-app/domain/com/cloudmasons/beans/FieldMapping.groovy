package com.cloudmasons.beans

class FieldMapping {
    
    static belongsTo = Feed

    Feed feed 
    List sources
    List targets
    
    static transients = [ 'name', 'description', 'source', 'target' ]
    static hasMany = [ sources : MappingSource, targets: MappingTarget ]

    static constraints = {
        source(summary: true)
        target(summary: true)
    }
    
    static mapping = {
        table 'feed_mapping'
    }
    
    public FieldMapping addSource(String sourceName, String transform = null) {
        MappingSource source = new MappingSource()
        source.name = sourceName
        source.transform = transform
        this.addToSources(source)
        return this
    }
    public FieldMapping addTarget(String targetName, String transform = null) {
        MappingTarget target = new MappingTarget()
        target.name = targetName
        target.transform = transform
        this.addToTargets(target)
        return this
    }

    public String getName() {
        return source
    }

    public String getDescription() {
        return "Field mapping"
    }
    public String getSource() {
        def names = sources?.collect { it.name } ?: []
        return names.join(', ')
    }
    public String getTarget() {
        def names = targets?.collect { it.name } ?: []
        return names.join(', ')
    }

}