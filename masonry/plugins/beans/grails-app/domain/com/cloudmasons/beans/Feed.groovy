package com.cloudmasons.beans


/**
 * Represents a feed of data for a bean
 */
class Feed {

    String name
    String description
    
    String beanType
    String correlator            // Groovy script to find a correlated instance of the beanType in the database
    String recordPreProcessor    // Optional groovy script to run prior to each mappings eval for a record
    String recordPostProcessor   // Optional groovy script to run prior to each mappings eval
    List mappings                // Mappings to run against the feed record and the correlated instance

    public Feed addMapping(FieldMapping m) {
        addToMappings(m)
        return this
    }
    
    public Feed addMapping(String source, String target) {
        return addMapping(source, target, null)
    }
    public Feed addMapping(String source, String target, String transform) {
        def m = new FieldMapping()
        if (target) {
            m.addSource(source)
            m.addTarget(target, transform)
        } else {
            m.addSource(source, transform)
        }
        addToMappings(m)
        return this
    }
    
    static hasMany = [ mappings : FieldMapping ]
    static actions = 
        [
            [action: 'create', type:'static', label: 'Add', role: '**SYSTEM_ADMINISTRATOR_ROLE**'],
            [action: 'edit'],
            [action: 'delete', role: '**SYSTEM_ADMINISTRATOR_ROLE**'],
            [action: 'run',  type: 'static', label: 'Run Feeds', role: '**SYSTEM_ADMINISTRATOR_ROLE**']
        ]
    static constraints = {
       name(summary: true, group: 'Properties')
       beanType(group: 'Properties')
       description(summary: true, nullable: true, group: 'Properties')
          
       mappings(nullable: true, group: 'Mappings')
       correlator(maxSize: 10000, group: 'Advanced')    
       recordPreProcessor(nullable: true, maxSize: 10000, group: 'Advanced')    
       recordPostProcessor(nullable: true, maxSize: 10000, group: 'Advanced')    

    }
}