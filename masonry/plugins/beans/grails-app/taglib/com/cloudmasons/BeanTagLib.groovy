package com.cloudmasons;


import com.cloudmasons.beans.Feed;
import com.opensymphony.module.sitemesh.filter.Buffer;

import grails.util.GrailsNameUtils
import org.apache.commons.lang.StringEscapeUtils
import org.codehaus.groovy.grails.web.util.StreamCharBuffer;

/**
 * 
 */
class BeanTagLib {
    static namespace = "bean"
  
    def beanService
    def beanReportService
    def authenticateService
    def securityService
    def grailsApplication
    
    def feeds = { attrs, body ->
        def feeds = Feed.list()
    
        def template = attrs.template ?: '/bean/feeds'
        out << render(template: template, model:[feeds:feeds], plugin: 'beans')
    }

    def reports = { attrs, body ->
        def reports = beanReportService.authorizedReports()

        def template = attrs.template ?: '/bean/reports'
        def formats = attrs.formats ?:  ['XLS', 'PDF', 'HTML', 'CSV'];
        out << render(template: template, model:[reports:reports, formats: formats], plugin: 'beans')
    }

    def label = { attrs, body ->
        def beanClass = (attrs.bean instanceof Class) ? attrs.bean : attrs.bean?.getClass()
        def beanPropertyName = GrailsNameUtils.getPropertyName(beanClass)
        def property = attrs.property
        
        out << g.message(code: "${beanPropertyName}.${property.name}.label", default: property.naturalName)
    }
    
    def helptip = { attrs, body ->
        def beanClass = (attrs.bean instanceof Class) ? attrs.bean : attrs.bean?.getClass()
        def beanPropertyName = GrailsNameUtils.getPropertyName(beanClass)
        def property = attrs.property

        def id = beanPropertyName+'-'+property.name+'-helptip'
        
        def helptipLink = """<a class="ico show-${id}" href='javascript:void(0)'>
        <img src="${g.resource(dir:'images',file:'helptip.png')}" border="0" alt="?"/>
        </a>"""
        def helptipDialog = """<div id="dialog-${id}" title="">"""+g.message(code: "${beanPropertyName}.${property.name}.helptip", default: 'Not Avaiable')+"</div>"
        def helptipJavascript = """
    <script type="text/javascript">
        \$(document).ready(function() {
            \$("#dialog-${id}").dialog({
                autoOpen: false,
                modal:true,
                height: 300,
                width: 600,
                buttons: {
                    'Close': function() {
                        \$(this).dialog('close');
                        
                    }
                },
                close: function() {
                }
            });
            \$(".show-${id}")
                .button()
                .click(function() {
                    \$("#dialog-${id}").dialog('open');
                });
        });
    </script>
        """
        out << helptipLink+helptipDialog+helptipJavascript
    }

    /**
     * Iterate each property group on a domain.
     */
    def eachAuthorizedStaticAction = { attrs, body ->
        def var = attrs.var ?: 'action'
        def actions = attrs.actions
        def beanClass = attrs.bean
        def beanControllerName = attrs.beanControllerName ?: controllerName
        int index = 0
        def params = attrs.params
        def associated = params?.associated
        actions?.each { action ->
           if (action.type == 'static') {
                String role = null
                if (action.role instanceof Map) {
                    role = action.role.keySet().join(',');
                } else if (action.role instanceof String) {
                    role = action.role;
                }
                if (associated && action.action == 'create' && securityService.isAuthorized(beanClass, action.action, bean)) {
                    out << body([(var):[action: 'show', label:'Create', params: [id: params.id, associated: associated, create: true]], bean:bean, beanControllerName: beanControllerName, idx:index])
                    index++
                }else if (securityService.isAuthorized(beanClass, action.action, bean)) {
                    out << body([(var):action, bean:bean, beanControllerName: beanControllerName, idx:index])
                    index++
                }
           }
        }
    }

    /**
     * Iterate each property group on a domain.
     */
    def eachAuthorizedInstanceAction = { attrs, body ->
        def var = attrs.var ?: 'action'
        def actions = attrs.actions
        def bean = attrs.bean
        def beanControllerName = attrs.beanControllerName ?: controllerName
        int index = 0
        def params = attrs.params
        def associated = params?.associated
        
        def beanDomainClass = BeanUtils.resolveDomainClass(grailsApplication, bean.getClass()?.name)
        actions?.each { action ->
            
           if (!action.type || action.type == 'instance') {
               if (associated && securityService.isAuthorized(bean.getClass(), action.action, bean)) {
                   if (action.action == 'show') {
                       out << body([(var):[action: 'show', label:'Show', params: [associated: associated, id: params.id, propertyId: bean.id]], bean:bean, beanControllerName: beanControllerName, idx:index])
                   } else {
                       out << body([(var):action, bean:bean, 'beanControllerName': beanDomainClass.propertyName, idx:index])
                   }
                   index++
               } else if (securityService.isAuthorized(bean.getClass(), action.action, bean)) {
                out << body([(var):action, bean:bean, beanControllerName: beanControllerName, idx:index])
                index++
               }
           }
       }
    }
    
    def actionLink = { attrs, body ->
        def bean = attrs.bean
        def action = attrs.action
        def beanControllerName = attrs.beanControllerName ?: controllerName

        def help = action.help ?: action.action // todo: lookup in message bundle
        def icon = action?.icon
        boolean confirm = (action.action == 'delete') ? true : false
        def actionLabel = g.message(code: "action.${action.action}.label", 
                                    default: action.label ?: action.action)
        def actionHref = g.createLink(controller: beanControllerName, action:action.action, id: action?.params?.id ?: bean.id, params: action.params, fragment: action.fragment?:null)
        def iconLink = ''
        if (icon) {
           iconLink = "<img src=\"${theme.icon(name: icon)}\" />"
        }
        String confirmation = '' 
        if (confirm) {
           String msg = StringEscapeUtils.escapeJavaScript("Are you sure you want to ${action.action} ${bean.name}?")
           confirmation = "onClick=\"javascript: if (confirm('${msg}')) { \$.post('${actionHref}', function(data) { location.reload(true); });} else {return false};\" "
        }
        out << "<a class=\"ico\" ${confirmation} href=\"${confirm ? '#' : actionHref}\" title=\"${help}\">${iconLink} ${actionLabel}</a>"
      
        
    }
    /**
     * Iterate each property group on a domain.
     */
    def eachPropertyGroup = { attrs, body ->
        def var = attrs.var ?: 'group'
        def groups = beanService.getPropertyGroups(attrs.bean)
        int index = 0
        groups?.each { group ->
           out << body([(var):group, bean:attrs.bean, idx:index])
           index++
        }
    }
    
    /**
     * Iterate for each property
     */
    def eachProperty = { attrs, body ->
        def group = attrs.group ?: '*'
        def var = attrs.var ?: 'property'
        def bean = attrs.bean
        def props = beanService.getProps(attrs.bean, group)
        props?.each { prop ->
            def value = bean?.properties[prop.name]
            out << body([(var):prop, bean:attrs.bean, value:value])
        }
    }

    def column = { attrs ->
		def property = attrs.remove("property")
		def propertyName = property.name
		def beanParams = attrs.remove("params")
		def title = attrs.remove("title")
		def columns = attrs.remove("columns")
		def beanControllerName = attrs.remove("beanControllerName")
		
		boolean sortable = true;
		if (!property.isPersistent()) {
			sortable = false;   // We can't sort transient properties
	    } else if (property.isAssociation()) {
	    	// Only allow sorting of associations that have a persistent name property
	    	def nameProp = property.referencedDomainClass.getPropertyByName('name');
	        if (nameProp && nameProp.isPersistent()) {
	        	propertyName += '.name'
	        } else {
	        	sortable = false;
	        }	
	    }
	    
	    if (sortable) {
	        out << sortableColumn(['beanControllerName': beanControllerName, params: beanParams, property: propertyName, title: title])
	    } else {
	    	out << " <th>${title}</th> "
	    }
 
	}    
	
    def sortableColumn = { attrs ->
        if (!attrs.property) {
            throwTagError("Tag [sortableColumn] is missing required attribute [property]")
        }

        if (!attrs.title && !attrs.titleKey) { throwTagError("Tag [sortableColumn] is missing required attribute [title] or [titleKey]") }

		
		def filterParams = BeanUtils.extractFilterParams(params)
        def property = attrs.remove("property") 
        def action = attrs.action ? attrs.remove("action") : (actionName ?: "list")
        def beanControllerName = attrs.remove("beanControllerName") ?: controllerName

        def defaultOrder = attrs.remove("defaultOrder") 
        if (defaultOrder != "desc") defaultOrder = "asc"

        // current sorting property and order 
        def sort = params.sort 
        def order = params.order

        // add sorting property and params to link params 
        def linkParams = [:] 
        if (params.associated) linkParams.put("associated",params.associated)
        if (params.id) linkParams.put("id",params.id) 

        //if (attrs["params"].columns) linkParams.columns = attrs["params"].columns
        linkParams.sort = property

        // determine and add sorting order for this column to link params 
        attrs.class = (attrs.class ? "${attrs.class} sortable" : "sortable") 
        if (property == sort) { 
            attrs.class = attrs.class + " sorted " + order 
            if (order == "asc") { linkParams.order = "desc" } 
            else { linkParams.order = "asc" } 
        } else { 
            linkParams.order = defaultOrder 
        }
        // todo:: Figure out why this is breaking the sort
        //if (filterParams) linkParams.putAll(filterParams) 

        // determine column title 
        def title = attrs.remove("title") 
        def titleKey = attrs.remove("titleKey") 
        if (titleKey) { 
            if (!title) title = titleKey 
            def messageSource = grailsAttributes.messageSource 
            def locale = RCU.getLocale(request) 
            title = messageSource.getMessage(titleKey, null, title, locale) 
        }

        def link = g.createLink(controller: beanControllerName, action:action, params:linkParams)
        def columns = attrs["params"].columns ?: params.columns
        columns?.each { c ->
            link += "&columns="
            link += c.encodeAsHTML();
        }
        out << "<th " 
        out << "><a href='${link}'> ${ title }</a></th>"
    }

	/**
	 * FIXME: Create a form because filter parameters can be HUGE.
	 */
    def beanReportLink = { attrs ->
		
        def filterParams = BeanUtils.extractFilterParams(params)
        // add sorting property and params to link params 
        def linkParams = [:] 
        
        if (params.id) linkParams.put("id",params.id) 
        if (params.sort) linkParams.put("sort",params.sort) 
        if (params.order) linkParams.put("order",params.order) 
		if (params.columns) linkParams.put("columns", params.columns)
		
        if (filterParams) linkParams.putAll(filterParams) 
        linkParams.entity = attrs.beanClass
        linkParams.reportFormat = attrs.reportFormat ?: 'PDF'
        def title = attrs.remove("title") 
        def titleKey = attrs.remove("titleKey") 
        if (titleKey) { 
            if (!title) title = titleKey 
            def messageSource = grailsAttributes.messageSource 
            def locale = RCU.getLocale(request) 
            title = messageSource.getMessage(titleKey, null, title, locale) 
        }

        out << "${link(controller: 'beanReport', action:'listReport', params:linkParams) { title }}"
    }
    
}
