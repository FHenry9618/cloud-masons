package com.cloudmasons

import com.cloudmasons.BeanUtils

class BeanFilterTagLib {

    static namespace = 'beans'

    /**
     * This map contains available filter operations by type.  It is used when creating the
     * individual rows in the filter pane.  The values in the text maps are key suffixes for the
     * resource bundle.  The prefix used in the valueMessagePrefix attribute will be query.op.
     *
     * Removing Like/ Not like, <= >= 
     *  from this list for simplicity sake. Added default display names
     */
    def availableOpsByType = [
        association: ['Equal', 'NotEqual', 'IsNull', 'IsNotNull'],
        text: ['ILike', 'NotILike', 'Equal', 'NotEqual', 'IsNull', 'IsNotNull'],
        numeric: ['Equal', 'NotEqual', 'LessThan', 'GreaterThan',
                         'Between', 'IsNull', 'IsNotNull'],
        date: ['Equal', 'NotEqual', 'LessThan','GreaterThan',
               'Between', 'IsNull', 'IsNotNull'],
        'boolean': ['Equal', 'NotEqual', 'IsNull', 'IsNotNull'],
        'enum': ['Equal', 'NotEqual']
    ]

    def opDisplayNames = [ 'ILike': 'contains', 'NotILike': 'doesn\'t contain',
                           'Like': 'contains (case sensitive)', 'NotLike': 'doesn\'t contain (case sensitive)',
                           'Equal': 'is', 'NotEqual': 'is not',
                           'LessThan': 'is less than', 'GreaterThan': 'is greater than',
                           'LessThanEquals': 'is less than or is', 'GreaterThanEquals': 'is greater than or is',
                           'Between': 'is between',
                           'IsNull': 'has no value', 'IsNotNull': 'has a value']

    /**
     * Include the necessary css and javascript for filtered bean list to work
     */
    def listIncludes = {
        out << "<link rel=\"stylesheet\" type=\"text/css\" href=\"${g.resource(dir: '/filter', file: 'filter.css', plugin: 'cornerstone')}\" />\n"
        out << "<script type=\"text/javascript\" src=\"${g.resource(dir: '/filter', file: 'filter.js', plugin: 'cornerstone')}\"></script>"
    }

    def isFiltered = { attrs, body ->
        if (BeanUtils.isFilterApplied(params)) {
            out << body()
        }
    }

    def isNotFiltered = { attrs, body ->
        if (! BeanUtils.isFilterApplied(params)) {
            out << body()
        }
    }

    /**
     * Grails pagination tag wrapper for use with the filterpane plugin.
     *
     * attribute total - a custom count to be used in pagination
     * attribute domainBean - the domain bean being filtered.  Ignored if total is specified.
     */
    def paginate = { attrs, body ->
        def filterParams = BeanUtils.extractFilterParams(params)
        def count = 0I

        if (attrs.total != null) {
            count = attrs.total
        } else if (attrs.domainBean) {
            def dc = BeanUtils.resolveDomainClass(grailsApplication, attrs.domainBean)
            //log.debug("paginate dc is ${dc}. clazz is ${dc.clazz}")
            if (dc) count = dc.clazz.count()
        }
        attrs.total = count
        attrs.params = filterParams
        out << g.paginate(attrs, body)
    }

    def currentCriteria = { attrs, body ->
        if (BeanUtils.isFilterApplied(params)) {
            def id = attrs.id ?: 'filterPaneCurrentCriteria'
            def title = attrs.title ? "title=\"${attrs.title}\" " : ''
            def clazz = attrs.class ? "class=\"${attrs.class}\" " : ''
            def style = attrs.style ? "style=\"${attrs.style}\" " : ''
            def dtFmt = attrs.dateFormat ?: 'yyyy-MM-dd HH:mm:ss'
            def filterParams = BeanUtils.extractFilterParams(params)
            def domainBean = BeanUtils.resolveDomainClass(grailsApplication, attrs.domainBean)
            def action = attrs.action ?: 'filter'

            out << """<ul id="${id}" ${clazz}${style}${title}>"""
            filterParams.each { key, filterOp ->
                log.debug "key is ${key}, filterOp is ${filterOp}"
                if (key.startsWith('filter.op') && filterOp != null && ! ''.equals(filterOp)) {
                    def prop = key[10..-1]
                    def domainProp
                    if (prop.contains('.')) { // association.
                        def parts = prop.split('\\.')
                        domainProp = domainBean.getPropertyByName(parts[0])
                        domainProp = domainProp.referencedDomainClass.getPropertyByName(parts[1])
                    } else {
                        domainProp = domainBean.getPropertyByName(prop)
                    }
                    def filterVal = "${filterParams["filter.${prop}"]}"
                    boolean isNumericType = (domainProp.referencedPropertyType
                        ? Number.isAssignableFrom(domainProp.referencedPropertyType)
                        : false)
                    boolean isNumericAndBlank = isNumericType && ! "".equals(filterVal.toString().trim())

                    if (filterVal != null && (!isNumericType || isNumericAndBlank)) {

                        if ('isnull'.equalsIgnoreCase(filterOp) || 'isnotnull'.equalsIgnoreCase(filterOp)) {
                            filterVal = ''
                        } else if (filterVal == 'struct') {
                            filterVal = BeanUtils.parseDateFromDatePickerParams("filter.${prop}", params)
                            if (filterVal) {
                                def dateFormat = dtFmt
                                if (dtFmt instanceof Map) {
                                    dateFormat = dtFmt[prop]
                                }
                                filterVal = "${g.formatDate(format:dateFormat, date:filterVal)}"
                            }
                        }
                        def filterValTo = null
                        if ('between'.equalsIgnoreCase(filterOp)) {
                            filterValTo = filterParams["filter.${prop}To"]
                            if (filterValTo == 'struct') {
                                filterValTo = BeanUtils.parseDateFromDatePickerParams("filter.${prop}To", params)
                                if (filterValTo) {
                                    def dateFormat = dtFmt
                                    if (dtFmt instanceof Map) {
                                        dateFormat = dtFmt[prop]
                                    }
                                    filterValTo = g.formatDate(format:dateFormat, date:filterValTo)
                                }
                            }
                        }
                        def newParams = [:]
                        newParams.putAll(filterParams)
                        newParams[key] = '' // <== This is what removes the criteria from the list.
                        def removeText = attrs.removeImgDir && attrs.removeImgFile ? "<img src=\"${g.resource(dir:attrs.removeImgDir, file:attrs.removeImgFile)}\" alt=\"(X)\" title=\"Remove\" />" : '(X)'
                        def removeLink = """<a href="${g.createLink(action:action,params:newParams)}" class="remove">${removeText}</a>"""
                        if (filterValTo) {
                            filterValTo = " and \"${filterValTo}\""
                        } else {
                            filterValTo = ''
                        }
                        out << """<li>${g.message(code:"${domainProp.domainClass}.${domainProp.name}.label",default:domainProp.naturalName)} ${g.message(code:"query.op.${filterOp}", default:opDisplayNames[filterOp])} "${filterVal}"${filterValTo} ${removeLink}</li>"""
                    } // end if filterVal != null
                }
            }
            out << "</ul>"
        }
    }

    /**
     * This tag generates the filter pane itself.  As of release 0.4, this tag pulls as much filtering information from
     * the domain class as possible by default.  All attributes from 0.3.1 are still supported,
     * but are considered deprecated in favor of more sensible alternatives.
     *
     * TODO: Document attributes.
     *
     */
    def filterPane = {attrs, body ->

        def domain = attrs.domainBean ?: (attrs.filterBean ?: null)
        def beanParams = attrs.params
        def bean = BeanUtils.resolveDomainClass(grailsApplication, domain)
        if (bean == null) {
            log.error("Unable to resolve filter domain class ${domain}")
            return
        }

        String titleKey = attrs.titleKey
        String title
        if (titleKey != null) {
            title = g.message(code:titleKey, default:g.message(code:'fp.tag.filterPane.titleText', default:'List Settings'))
        } else {
            title = attrs.title ?: (attrs.filterPaneTitle ?: g.message(code:'fp.tag.filterPane.titleText', default:'List Settings'))
        }
        String containerId = attrs.filterPaneId ?: (attrs.id ?: 'filterPane')
        String containerClass = attrs['class'] ?: (attrs.styleClass ?: (attrs.filterPaneClass ?: ''))
        String containerStyle = attrs.style ?: (attrs.filterPaneStyle ?: '')
        String formName = attrs.formName ?: (attrs.filterFormName ?: 'filterForm')

        // List constrained properties which allows for the selection of transients
        // note we can't filter on transients but we can display them as columns
        def constraints = bean.constrainedProperties
        def comparator = new org.codehaus.groovy.grails.scaffolding.DomainClassPropertyComparator(bean)
        
        def allProps = bean.properties.sort(comparator)
        
        List props = []
        allProps?.each { prop ->
           if (constraints[prop.name] && !prop.oneToMany) props.add(prop)
        }
        
        def persistentProps = [:]
        List beanPersistentProps = bean.persistentProperties as List
        if (log.isDebugEnabled()) {
            log.debug("${beanPersistentProps.size()} Persistent props: ${beanPersistentProps}")
        }
        List associatedProps = []


        // Remove the last udpated property.  We don't filter this by default.
        def lastUpdated = beanPersistentProps.find { it.name == 'lastUpdated'}
        if (!attrs.filterProperties || !attrs.filterProperties.contains('lastUpdated')) {
            if (lastUpdated)
            beanPersistentProps.remove(lastUpdated)
        }

        // Remove association properties.  We can't handle them directly.
        associatedProps = beanPersistentProps.findAll { it.association == true && !it.type.isEnum() }
        beanPersistentProps.removeAll(associatedProps)





        // Put the persistent props in the final map.
        beanPersistentProps.each {
            persistentProps[it.name] = it
        }

        // Add any associated properties.
        def associationNames = [:] // This prevents further churning through the list later.
        if (attrs.associatedProperties) {
            def ap = (attrs.associatedProperties instanceof List) ? attrs.associatedProperties : []
            if (attrs.associatedProperties instanceof String) {
                ap = attrs.associatedProperties.split(",") as List
            }

            ap.each {
                def name = it.trim()
                def parts = name.split("\\.")
                def association = associatedProps.find { it.name == parts[0] }
                if (association) {
                    def refDomain = association.referencedDomainClass
                    def refProperty = refDomain.persistentProperties.find { it.name == parts[1] }
                    if (refProperty) {
                        props[name] = refProperty
                        if (!associationNames[refProperty])
                            associationNames[refProperty] = []
                        associationNames[refProperty] << name
                    } else
                        log.error("Unable to find associated property for ${name}")
                } else {
                    log.error("Unable to find associated domain class for ${parts[0]}")
                }
            }
        }


        /*
         * Notes: bean.properties contains all properties as DefaultGrailsDomainClassProperty instances, including id, etc.
         * bean.persistentProperties excludes id and version
         * bean.constrainedProperties contains the same as persistent, but instances are ConstrainedProperty instances.
         */

        if (bean && props) {
            // def filterPaneId = attrs.id ?: 'filterPane'
            def action = attrs.action ?: 'list'
            def propsStr = ""
            int propsSize = props.size() - 1
            def sortKeys = []
            def sortProps = []
            props.eachWithIndex {p, i ->
                if (!p.association && persistentProps[p.name]) {
                    sortKeys << p.name
                    sortProps << p
                    propsStr += p.name
                    if (i < propsSize) { propsStr += ',' }
                }
            }

            def propKeys = []
            props.each { p ->
               propKeys << p.name
            }

            // Added for 0.6.4 as a new attribute to the tag.
            boolean customForm = "true".equals(attrs.customForm) || attrs.customForm == true
            def openFormTag = ''
            def closeFormTag = ''
            def applyButton = ''
            
            if (customForm == false) {
                if (beanParams.id && beanParams.associated) { // indicate a 'show all' action
                    openFormTag = """<form id="${formName}" name="${formName}" action="${g.createLink(controller: beanParams.controller, action: 'show', params: [ id: beanParams.id, associated : beanParams.associated ])}" method="post">"""
                }
                else {
                    openFormTag = """<form id="${formName}" name="${formName}" action="${g.createLink(controller: beanParams.controller, action:'list')}" method="post">"""
                }
                closeFormTag = '</form>'
                applyButton = """<span class="button">
        ${this.actionSubmit(action: action, value: g.message(code:'fp.tag.filterPane.button.apply.text', default:'Apply'))}
      </span>"""
            }
            

            def output = """\
            
<div id="${containerId}"
     title="${title}" style="display: none;">
     
  ${openFormTag}
  
  <input type="hidden" name="filterProperties" value="${propsStr}" />
  <table cellspacing="0" cellpadding="0" class="filterTable">
"""
            int count = 0
            for (it in props) {
                if (count++ > 100) break;
                def assocName = associationNames[it]
                if (assocName) {
                    assocName = assocName.remove(0)
                }
                output += this.buildPropertyRow(bean, it, assocName, attrs, params)
            }
            output += """\
  </table>
  <div>
  """
      output += "<br/>"  
      output += this.select(id: 'showFilterRow', name: 'showFilterRow', noSelection: ['': 'Add Column...'],
            from: props, keys: propKeys, optionValue: "naturalName", 
            onChange: "var row = this.options[this.selectedIndex].value; row = 'filter-' + row; \$('#'+row).show(); \$('#columns-'+row).attr('checked', true); this.value = null;")
            
      output += "<br/>"  
      def orderByLabel = message(code:'fp.tag.filterPane.sort.orderByText', default:'Order By...')
      // Do an if check.  If the messages don't exist, default to the legacy natural name.'
      def valueMessagePrefix = attrs.sortValueMessagePrefix ?: 'fp.property.text'
      def firstKeyMsg = g.message(code:"${valueMessagePrefix}.${sortKeys[0]}", default:'false')
      if (firstKeyMsg.equals('false')) {
          output += this.select(name: "sort", from: sortProps, keys:sortKeys,
            optionValue: "naturalName",
            noSelection: ['': orderByLabel],
            value: params.sort)
      } else {
          output += this.select(name: "sort", from: sortProps, keys: sortKeys,
            valueMessagePrefix: valueMessagePrefix,
            noSelection: ['': orderByLabel],
            value: params.sort)
      }
output += """\
      &nbsp;
      ${this.radio(name: "order", value: "asc", checked: params.order == 'asc',)}
      &nbsp;${g.message(code:'fp.tag.filterPane.sort.ascending', default:'Ascending')}&nbsp;
      ${this.radio(name: "order", value: "desc", checked: params.order == 'desc')}
      &nbsp;${g.message(code:'fp.tag.filterPane.sort.descending', default:'Descending')}
      
  </div>
  <br/>
  ${closeFormTag}
</div>
      """
            out << output
        }
    }

    private String createFilterControl(def attrs, def body, boolean visible) {
        def stream = ""
        if (attrs.filterBean && attrs.filterProperty) {
            def mc = attrs.filterBean.getMetaClass()
            def mp = BeanUtils.getNestedMetaProperty(mc, attrs.filterProperty)
            def type = mp.type.getSimpleName()
            def finalAttrs = new java.util.HashMap(attrs)
            finalAttrs.remove('filterBean')
            finalAttrs.remove('filterProperty')
            finalAttrs.remove('values')
            finalAttrs.remove('valuesToken')
            finalAttrs.remove('out')
            if (!visible) {
                if (finalAttrs.style) {
                    finalAttrs.style = "display:none;${finalAttrs.style}"
                } else {
                    finalAttrs.style = "display:none"
                }
            }
            if (!finalAttrs.name) finalAttrs.name = attrs.filterProperty
            if (attrs['name']) finalAttrs['name'] = attrs['name']
            if (!finalAttrs['id']) finalAttrs['id'] = finalAttrs['name']

            if (type == "String" || type == "char" || Number.class.isAssignableFrom(mp.type)
                || type == "int" || type == "long" || type == "double" || type == "float") {
                if (attrs.values) {
                    def valueToken = attrs.valuesToken ? attrs.valuesToken : ' '
                    def valueList = (attrs.values instanceof List) ? attrs.values : attrs.values.tokenize(valueToken)
                    finalAttrs.putAll([from: valueList, value: params[attrs.filterProperty]])
                    stream = this.select(finalAttrs)
                } else {
                    def ctrlWriter = new StringWriter()
                    def markup = new groovy.xml.MarkupBuilder(ctrlWriter)
                    markup.doubleQuotes = true
                    finalAttrs.putAll([type: 'text', value: attrs.value ? attrs.value : params[attrs.filterProperty]])
                    if (attrs.id) finalAttrs['id'] = attrs.id
                    if (attrs.maxlength) finalAttrs['maxlength'] = attrs.maxlength
                    if (attrs['size']) finalAttrs['size'] = attrs['size']
                    markup.input(finalAttrs)
                    stream = ctrlWriter.toString()
                }
            } else if (type == "Date") {
                finalAttrs['value'] = BeanUtils.parseDateFromDatePickerParams(finalAttrs.name, params)
                finalAttrs['years'] = 2000..2020
                finalAttrs['year'] = 2010
                
                //println "Final date attrs are ${finalAttrs}"
                def spanStyle = (finalAttrs.style) ? "style=\"${finalAttrs.style}\"" : ""
                stream = "<span id=\"" + finalAttrs['id'] + "-container\" ${spanStyle}>" + this.datePicker(finalAttrs) + "</span>"
            } else if (type == "Boolean" || type == "boolean") {
                finalAttrs.putAll(labels: ['Yes', 'No'], values: [true, false], value: params[attrs.filterProperty])
                stream = this.radioGroup(finalAttrs) { "<label for=\"${attrs.filterProperty}\">${it.label} </label>${it.radio}" }
            } else {

            }
        }
        return stream
    }

    private def createFilterControl(def property, def formPropName, def attrs, def params, def opId, def type) {
        //def type = property.type
        
        def out = ""
        if (type == 'association' ||type == 'text' || type == 'enum' || type == 'numeric') {
            
            if (attrs.values) {
                def valueList = []
                if ((type == 'enum' && attrs.displayProperty) || type == 'association') {
                    
                    valueList = []
                    if (type == 'enum') {
                        type.enumConstants.each {
                            def value = it
    //                        if (attrs.valueProperty) {
    //                            if (attrs.valueProperty == 'ordinal') {
    //                                value = it.ordinal()
    //                            } else {
    //                                value = it[attrs.valueProperty]
    //                            }
    //                        }
                            // the name property of an enum does not conform to the bean spec.  The method is just "name()"
                            def display = attrs.displayProperty == 'name' ? it.name() : it[attrs.displayProperty]
                            valueList << [id:value, name:display]
                        }
                    } else {
                        attrs.values.each {
                            valueList << [id:it.id, name: it.name]
                        }
                    }
                    attrs.optionKey = 'id'
                    attrs.optionValue = 'name'
                } else {
                    def valueToken = attrs.valuesToken ?: ' '
                    valueList = attrs.values instanceof List ? attrs.values : attrs.values.tokenize(valueToken)
                }

                attrs.putAll([from: valueList, value: params[formPropName], onChange:"selectDefaultOperator('${opId}')"])

                def valueMessagePrefix = "fp.property.text.${property.name}"
                def valueMessageAltPrefix = "${property.domainClass.propertyName}.${property.name}"

                def messageSource = grailsAttributes.getApplicationContext().getBean("messageSource")
                def locale = org.springframework.web.servlet.support.RequestContextUtils.getLocale(request)
                if (messageSource.getMessage(valueMessagePrefix, null, null, locale) != null) {
                     attrs.valueMessagePrefix = valueMessagePrefix
                } else if (messageSource.getMessage(valueMessageAltPrefix, null, null, locale) != null) {
                     attrs.valueMessagePrefix = valueMessageAltPrefix
                }
                out = this.select(attrs)
            } else if( type != 'association'){
                attrs.onChange = "selectDefaultOperator('${opId}')"
                out = this.textField(attrs)
            }
        } else if (type == 'date') {
            Date d = BeanUtils.parseDateFromDatePickerParams(formPropName, params)
            attrs.value = d
            attrs.onChange = "selectDefaultOperator('${opId}')"
            attrs['years'] = 2000..2020
            attrs['year'] = 2010
            String style = attrs.style ? "style=\"${attrs.style}\"" : ''
            boolean isDayPrecision = attrs.precision == 'day'
            String strDayPrecision = ""
            if (!formPropName.endsWith("To")) {
                strDayPrecision = """<input type="hidden" name="filter.${property.domainClass.name}.${property.name}_isDayPrecision" value="${isDayPrecision ? 'y' : 'n'}" />"""
            }
            out = """<span id="${attrs.id}-container" ${style}>${this.datePicker(attrs)}</span>${strDayPrecision}"""

        } else if (type == 'boolean') {
            def yes = radio(id:"${formPropName}.yes", name:formPropName, value:'true', checked:params[formPropName] == 'true', onClick:"selectDefaultOperator('${opId}')")
            def no = radio(id:"${formPropName}.no", name:formPropName, value:'false', checked:params[formPropName] == 'false', onClick:"selectDefaultOperator('${opId}')")
            out = """\
            <label for="${formPropName}.yes">${g.message(code:'fp.tag.filterPane.property.boolean.true', default:'Yes ')}</label>
            ${yes}
            <label for="${formPropName}.no">${g.message(code:'fp.tag.filterPane.property.boolean.false', default:'No ')}</label>
            ${no}
            """
        }
        return out
    }

    private def buildPropertyRow(def bean, def property, def propertyNameKey, def attrs, def params) {
        def fullPropName = (property.domainClass == bean) ? property.name : propertyNameKey
        def paramName = "filter.${fullPropName}"
        def opName = "filter.op.${fullPropName}"
        def type = BeanUtils.getOperatorMapKey(property.type)
        def columns = []
        if (params.columns && params.columns instanceof String) {
            columns.append(params.columns)
        } else if (params.columns) {
            columns = params.columns
        }
        // Display summary or properties that are displayed

        boolean isSummary = property.domainClass.constrainedProperties[property.name]?.getAppliedConstraint('summary') != null
        boolean hideRow = !columns.contains(property.name); //(!isSummary && !params[opName])
        
        //if (log.isDebugEnabled()) log.debug("property ${property} key ${propertyNameKey} fullPropName ${fullPropName} type ${type}");

        def filterCtrlAttrs = [name: paramName, value: params[paramName]]
        if (attrs.filterPropertyValues && (attrs.filterPropertyValues[property.name] || attrs.filterPropertyValues[propertyNameKey])) {
            if (attrs.filterPropertyValues[property.name])
                filterCtrlAttrs.putAll(attrs.filterPropertyValues[property.name])
            if (attrs.filterPropertyValues[propertyNameKey])
                filterCtrlAttrs.putAll(attrs.filterPropertyValues[propertyNameKey])
        }
        if (!filterCtrlAttrs.id) {
            filterCtrlAttrs.id = property.name
        }
        

        // filter operator keys and text default to max available for the type.
        //def opText = []; opText.addAll(this.availableOpsByType[type].text)
        def opKeys = []; opKeys.addAll(this.availableOpsByType[type])

        // Remove all but = <> for enum properties
        //        if (property.type.isEnum()) {
        //            opKeys.clear()
        //            opKeys.addAll(this.availableOpsByType['enum'])
        //        }

        // If The property is not nullable, no need to allow them to filter in is or is not null.
        def constrainedProperty = property.domainClass.constrainedProperties[property.name]
        if ((constrainedProperty && !constrainedProperty.isNullable()) || property.name == 'id') {
            //            opText.remove('is-not-null')
            //            opText.remove('is-null')
            opKeys.remove("IsNotNull")
            opKeys.remove("IsNull")
        }

        // If the user did not specify a value list and the property is constrained with one, use the domain class's list.
        if (!filterCtrlAttrs.values) {
            List inList = constrainedProperty?.getInList()

            if (inList) {
                filterCtrlAttrs.values = inList
            }
            else if (property.type.isEnum()) {
                filterCtrlAttrs.values = property.type.enumConstants
            } else { // try to get values from property's allowedValues constraint. For associated objects
                def allowedValues = constrainedProperty?.getAppliedConstraint('allowedValues')
                if (allowedValues) {
                    def allowedList = allowedValues.getValues(bean.clazz.newInstance())
                    if (allowedList) {
                        filterCtrlAttrs.values = allowedList?.sort { it ? it.name : it}
                        opKeys.remove("NotEqual")
                        opKeys.remove("IsNotNull")
                    }
                }
            }
        }
        
        // If the values list is now specified and it is not a associated property, limit the operators to == or <>
        if (filterCtrlAttrs.values && type != 'association') {
            opKeys = ['Equal', 'NotEqual']
        }

        def dropDown = []
        opKeys.each {
            dropDown.add([key: it, display: opDisplayNames[it] ?: it])
        }
        def noSelection = ['': '']
        // Create the operator dropdown.
        def opDropdown = this.select(id: opName, name: opName, noSelection: noSelection,
            from: dropDown, optionKey: 'key', optionValue: 'display',
            value: params[opName], valueMessagePrefix:'query.op',
            onChange: "filterOpChange('${opName}', '${filterCtrlAttrs.id}');")
            
        if (params[opName] == "IsNull" || params[opName] == "IsNotNull") {
            filterCtrlAttrs.style = 'display:none;'
        }

        // Take care of the name.
        def fieldNameKey = "fp.property.text.${fullPropName}" // Default.
        def fieldNameAltKey = fieldNameKey // default for alt key.
        def fieldNamei18NTemplateKey = "${bean.propertyName}.${property.name}"
        def fieldName = property.naturalName

        if (property.domainClass != bean) { // association.
            fieldNameKey = "fp.property.text.${property.domainClass.propertyName}.${property.name}"
            fieldNamei18NTemplateKey = "${property.domainClass.propertyName}.${property.name}"
            fieldName = "${property.domainClass.naturalName}'s ${fieldName}"
        }
        fieldName = g.message(code:fieldNameKey, default: g.message(code:fieldNameAltKey, default:g.message(code:fieldNamei18NTemplateKey, default:fieldName)))

        def row = """\
    <tr id="filter-${property.name}" ${ hideRow ? 'style="display: none;"' : ''}>
     <td>
      <span style="display: none;">
        <input id="columns-filter-${property.name}" type="checkbox" value="${property.name}" ${hideRow ? '' : "checked='checked'"} name="columns"/>
      </span>
      <table>
        <tr>
          <td colspan="2"><b><span style="font-size: 130%;">${fieldName}</span></b> 
            
          </td>
          <td style="text-align: right;">
          <a href="#" onClick="\$('#filter-${property.name}').hide(); \$('#columns-filter-${property.name}').attr('checked', false); return false;"> 
          [remove column]</a></span</td>
          

        </tr>
      """
      if (property.isPersistent()) {
      row += """\
        <tr>
          <td width="200px" style="text-align: right;">Filter Row if ${fieldName}: </td>
          <td>${opDropdown}</td>
          <td>
            ${this.createFilterControl(property, paramName, filterCtrlAttrs, params, opName, type)}
       """
    
            // For numeric and date types, build the "To" control, in case they select between.
            if (type == "numeric" || type == "date") {
                filterCtrlAttrs.name += 'To'
                filterCtrlAttrs.id += 'To'
                filterCtrlAttrs.value = params[filterCtrlAttrs.name]
                boolean showToCtrl = params[opName] == "Between" && filterCtrlAttrs?.value?.toString().trim() != ""
                row += """\
          <span style="${showToCtrl ? '' : 'display:none'}" id="between-span-${property.name}">
            &nbsp;${g.message(code:'fp.tag.filterPane.property.betweenValueSeparatorText', default:'and')}&nbsp;
            ${this.createFilterControl(property, filterCtrlAttrs.name, filterCtrlAttrs, params, opName, type)}
          </span>
                """;
           }

            row +=  "</td></tr>"
            
        } else {
          row += "<tr><td></td><td></td><td></td></tr>";
        }
        
        def us = property.domainClass.constrainedProperties[property.name]?.getAppliedConstraint('userSettings')
        if (us != null) {
            def settings = us.userSettings
            settings?.each { key, value ->
               row += """<tr>
                   <td width="200px" style="text-align: right;">${value.label ?: key}: </td>
                   <td><input class="date txt" type='text' id='propertyUserSettings-${property.name}-${key}' name='propertyUserSettings.${property.name}-${key}'  />
                   <script type='text/javascript'>
                        \$(document).ready(function() {
                           \$('#propertyUserSettings-${property.name}-${key}').val( \$.cookie('propertyUserSettings-${property.name}-${key}') );
                          \$('#propertyUserSettings-${property.name}-${key}').change( function() {
                                 \$.cookie('propertyUserSettings-${property.name}-${key}', \$(this).val());
                            });
                         });

                   </script>
                   </td><td></td></tr>
                   """;
            }
        
        } 
        row += """\
       <tr><td></td><td></td><td></td></tr>
      </table>
      <td>
     </tr>
    """
        return row
    }
}