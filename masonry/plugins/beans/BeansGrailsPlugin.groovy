import com.cloudmasons.beans.AllowedValuesConstraint
import com.cloudmasons.beans.GroupConstraint
import com.cloudmasons.beans.RoleConstraint
import com.cloudmasons.beans.SummaryConstraint
import com.cloudmasons.beans.RenderOptionsConstraint
import com.cloudmasons.beans.UserSettingsConstraint
import com.cloudmasons.beans.PhoneNumberConstraint

import org.codehaus.groovy.grails.validation.ConstrainedProperty
import org.codehaus.groovy.grails.plugins.PluginManagerHolder
import org.codehaus.groovy.grails.plugins.GrailsPlugin
import org.codehaus.groovy.grails.commons.TagLibArtefactHandler

import org.springframework.core.io.*
import org.apache.commons.io.FilenameUtils
import org.apache.commons.lang.StringUtils

class BeansGrailsPlugin {
    // the plugin version
    def version = "0.1"
    // the version or versions of Grails the plugin is designed for
    def grailsVersion = "1.2.1 > *"
    // the other plugins this plugin depends on
    def dependsOn = [:]
    // resources that are excluded from plugin packaging
    def pluginExcludes = [
            "grails-app/views/error.gsp"
    ]

    // TODO Fill in these fields
    def author = "Cloudmasons"
    def authorEmail = ""
    def title = "Plugin summary/headline"
    def description = '''\\
Brief description of the plugin.
'''

    // URL to the plugin's documentation
    def documentation = "http://grails.org/plugin/beans"
    def watchedResources = findWatchedResources()

    private static List findWatchedResources() {
        def buildConfig = new ConfigSlurper().parse(BuildConfig)
        def result = []
        def pluginLocation = buildConfig?.grails.plugin.location.beans
        if (pluginLocation) {
             result << "file:${pluginLocation}/grails-app/services/**/*Service.groovy"
             result << "file:${pluginLocation}/grails-app/controllers/**/*Controller.groovy"
             result << "file:${pluginLocation}/grails-app/taglib/**/*TagLib.groovy"
        }
        return result
    }
    
    def doWithWebDescriptor = { xml ->
        // TODO Implement additions to web.xml (optional), this event occurs before 
    }

    def doWithSpring = {
        ConstrainedProperty.registerNewConstraint(
                        GroupConstraint.GROUP_CONSTRAINT, 
                        GroupConstraint.class);
        ConstrainedProperty.registerNewConstraint(
                        SummaryConstraint.SUMMARY_CONSTRAINT, 
                        SummaryConstraint.class);
        ConstrainedProperty.registerNewConstraint(
                        AllowedValuesConstraint.ALLOWED_VALUES_CONSTRAINT, 
                        AllowedValuesConstraint.class);
        ConstrainedProperty.registerNewConstraint(
                        RoleConstraint.ROLE_CONSTRAINT, 
                        RoleConstraint.class);
        ConstrainedProperty.registerNewConstraint(
                        UserSettingsConstraint.USER_SETTINGS_CONSTRAINT, 
                        UserSettingsConstraint.class);
        ConstrainedProperty.registerNewConstraint(
                        PhoneNumberConstraint.PHONE_NUMBER_CONSTRAINT,
                        PhoneNumberConstraint.class);
        ConstrainedProperty.registerNewConstraint(
                        RenderOptionsConstraint.RENDER_OPTIONS_CONSTRAINT,
                        RenderOptionsConstraint.class);

        // TODO Implement runtime spring config (optional)
    }

    def doWithDynamicMethods = { ctx ->
        // TODO Implement registering dynamic methods to classes (optional)
    }

    def doWithApplicationContext = { applicationContext ->
        // TODO Implement post initialization spring config (optional)
    }


    def onChange = { event ->
        println "Beans plugin change detected..."
        if (event.source) {
            if (event.source instanceof Class) {
                def name = event.source.name
                // for some reason event.plugin was not set to the the services or controller plugin
                // we'll use the plugin manager to notify them of the change.
                if (name.endsWith('Service')) {
                    def servicesPlugin = PluginManagerHolder.pluginManager.getGrailsPlugin('services')
                    println "    Reloading ${name}"
                    servicesPlugin.notifyOfEvent(GrailsPlugin.EVENT_ON_CHANGE, event.source)
                } else if (name.endsWith('Controller')) {
                    def controllersPlugin = PluginManagerHolder.pluginManager.getGrailsPlugin('controllers')
                    println "    Reloading ${name}"
                    controllersPlugin.notifyOfEvent(GrailsPlugin.EVENT_ON_CHANGE, event.source)
                }
            } 
            // detect changes in taglibs
            if (application.isArtefactOfType(TagLibArtefactHandler.TYPE, event.source)) {
                def groovyPagesPlugin = PluginManagerHolder.pluginManager.getGrailsPlugin('groovyPages')
                println "    Reloading ${event.source.name}"
                groovyPagesPlugin.notifyOfEvent(GrailsPlugin.EVENT_ON_CHANGE, event.source)
            }
        }
    }

    def onConfigChange = { event ->
        // TODO Implement code that is executed when the project configuration changes.
        // The event is the same as for 'onChange'.
    }
}
