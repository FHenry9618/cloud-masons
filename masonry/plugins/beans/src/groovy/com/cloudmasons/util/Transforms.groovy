package com.cloudmasons.util

import java.text.ParseException

/**
 * Default transforms for data feeds
 */
class Transforms {
    public static final String IS_BLANK = '**IS_BLANK**'    
    public static final String IS_NOT_BLANK = '**IS_NOT_BLANK**'    
    public static final String TRIM = '**TRIM**'    
    public static final String FIRST_VALID_DATE = '**FIRST_VALID_DATE**'
    public static final String EARLIEST_VALID_DATE = '**EARLIEST_VALID_DATE**'
    public static final String FIRST_VALID_AMOUNT = '**FIRST_VALID_AMOUNT**'
    public static final String APPEND_IF_NOT_PRESENT = '**APPEND_IF_NOT_PRESENT**'
    public static final String TO_INTEGER = '**TO_INTEGER**'
    public static final String TO_LONG = '**TO_LONG**'

    public static final Map transformScripts = [
            (IS_BLANK): 'value == null',                               
            (IS_NOT_BLANK): 'value != null',                               
            (TRIM): 'value?.trim()',                               
            (FIRST_VALID_DATE): 'Transforms.firstValidDate(value, currentValue)',
            (EARLIEST_VALID_DATE): 'Transforms.earliestValidDate(value, currentValue)',
            (FIRST_VALID_AMOUNT): 'Transforms.firstValidAmount(value, currentValue)',
            (APPEND_IF_NOT_PRESENT): 'Transforms.appendIfNotPresent(value, currentValue)',
            (TO_INTEGER): 'Transforms.toInteger(value, currentValue)',
            (TO_LONG): 'Transforms.toLong(value, currentValue)'

            ]
    
    public static String getTransform(String t) {
        if (transformScripts.containsKey(t)) {
            return transformScripts[t]
        } else {
            return t
        }
    }
    
    public static Date firstValidDate(Object value, Object currentValue=null) {
        if (value == null) return null;
        if (value instanceof Date) return value;
        Date date = null;

        if (value instanceof String) {
            String sValue = (String) value;
            def values = sValue.split();
            for (v in values) {
                try {
                    date = Date.parse('MM/dd/yy', v)
                } catch (ParseException pe) {}
                if (date) break;
                try {
                    date = Date.parse('MM/dd/yyyy', v)
                } catch (ParseException pe) {}
                if (date) break;
                try {
                    date = Date.parse('MM-dd-yy', v)
                } catch (ParseException pe) {}
                if (date) break;
                try {
                    date = Date.parse('MM-dd-yyyy', v)
                } catch (ParseException pe) {}
                if (date) break;
                try {
                    date = Date.parse('MM.dd.yy', v)
                } catch (ParseException pe) {}
                if (date) break;
                try {
                    date = Date.parse('MM.dd.yyyy', v)
                } catch (ParseException pe) {}
                if (date) break;
            }
        }
        return date ?: currentValue
    }
    
    public static Date earliestValidDate(def value, def currentValue=null) {
        return null
    }
    public static Integer toInteger(def value, def currentValue=null) {
        Integer result = currentValue 
        if (value && value instanceof String) {
            if (value.isInteger()) result = value.toInteger();
            if (value.isBigDecimal()) result = value.toBigDecimal().intValue();
        } 
        
        return result
    }
    
    public static Long toLong(def value, def currentValue=null) {
        Integer result = currentValue 
        if (value && value instanceof String) {
            if (value.isLong()) result = value.toLong();
            if (value.isBigDecimal()) result = value.toBigDecimal().longValue();
        } 
        
        return result
    }

    public static BigDecimal sumFirstValidAmount(Object value, def currentValue=null) {
        def result = currentValue ?: 0.0

        def amount = firstValidAmount(value, null);
        if (amount != null) {
            result += amount;
        }
        return result        
    }   

    public static BigDecimal firstValidAmount(Object value, def currentValue=null) {
        if (value == null) return null;
        if (value instanceof Number) return value;
        
        // print "firstValid amount ${value} "
        BigDecimal result = null
        if (value instanceof String) {
            String sValue = (String) value;
            def values = sValue.split();
            for (v in values) {
                v = v.replaceAll('\\$', '')
                v = v.replaceAll(',', '')
                if (v.isBigDecimal()) {
                    result = v.toBigDecimal()
                    break
                } 
            }
        }
        return result ?: currentValue
    }
    
    public static String appendIfNotPresent(String value, String currentValue) {
        if (value == null) return currentValue;
        if (currentValue == null) return "${value}. "
        
        if (currentValue.indexOf(value) >= 0) return currentValue
        
        return "${currentValue}${value}. "
    }

}