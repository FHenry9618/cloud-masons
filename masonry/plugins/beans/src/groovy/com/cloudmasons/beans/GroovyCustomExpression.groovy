package com.cloudmasons.beans

import ar.com.fdvs.dj.domain.CustomExpression


class GroovyCustomExpression implements CustomExpression {

    def script
    def scriptService
    String className
    
    def GroovyCustomExpression(def script, def scriptService) {
        this.scriptService = scriptService
        this.script = script
    }

    public Object evaluate(Map fields, Map variables, Map parameters) {
        Map model = [ fields: fields, 
                      variables: variables,
                      params: parameters ]
        
        Object result = scriptService.evaluate(script, model)   
        if (className == null) {
        	className = result?.getClass().name
        }
        return result
    }

    public String getClassName() {
    	if (className) {
    		return className
    	}
        String.name
    }

}