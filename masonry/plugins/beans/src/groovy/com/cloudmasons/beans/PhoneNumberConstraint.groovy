package com.cloudmasons.beans



import org.codehaus.groovy.grails.validation.AbstractConstraint
import org.springframework.validation.Errors

/**
 * Contraint to validate if entered a valid phone number
 *
 */
class PhoneNumberConstraint extends AbstractConstraint {
    
    private static final String DEFAULT_MESSAGE_CODE = "default.phonenumber.not.valid.message";
    public static final String PHONE_NUMBER_CONSTRAINT = "phoneNumber";
    
    private boolean validNumber
    
    
    public void setParameter(Object constraintParameter) {
        if (!(constraintParameter instanceof Boolean))
            throw new IllegalArgumentException("Parameter for constraint ["
                    + PHONE_NUMBER_CONSTRAINT + "] of property ["
                    + constraintPropertyName + "] of class ["
                    + constraintOwningClass + "] must be a boolean value");

        this.validNumber = ((Boolean) constraintParameter).booleanValue()
        super.setParameter(constraintParameter);
    }

    protected void processValidate(Object target, Object propertyValue, Errors errors) {
        if (validNumber && !validate(target, propertyValue)) {
            def args = (Object[]) [constraintPropertyName, constraintOwningClass,
                    propertyValue]
            super.rejectValue(target, errors, DEFAULT_MESSAGE_CODE,
                    "not.valid." + PHONE_NUMBER_CONSTRAINT, args);
        }
    }
    
    boolean supports(Class type) {
        return type != null;
    }
    
    boolean validate(target, propertyValue) {
        if (propertyValue) {
            return propertyValue ==~ /(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})/
        }
        return true
    }
    
    String getName() {
        return PHONE_NUMBER_CONSTRAINT;
    }
}