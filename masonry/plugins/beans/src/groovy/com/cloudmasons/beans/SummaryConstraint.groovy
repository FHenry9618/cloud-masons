package com.cloudmasons.beans

import org.codehaus.groovy.grails.validation.AbstractConstraint
import org.springframework.validation.Errors
 
/**
 * Custom constraint to allow for specifiing a property as a summary
 * attribute.  A summary attribute is displayed in default lists and
 * in various places that require a summary of a given domain class.
 */
class SummaryConstraint extends AbstractConstraint {
 
    private static final String DEFAULT__MESSAGE_CODE = "default.not.valid.message";
    public static final String SUMMARY_CONSTRAINT = "summary";
 
    private Integer order;
    
    public Integer getOrder() {
    	return order
    }
    
    public void setParameter(Object constraintParameter) {
    	//println "setParameter ${constraintParameter} on group constraint"
        if(constraintParameter instanceof Number) {
        	order = ((Number)constraintParameter).toInteger()
        } else if (constraintParameter instanceof String) {
        	order = ((String)constraintParameter).toInteger()
        } else if (constraintParameter instanceof Boolean) {
        	order = 0
        } else {
        	throw new IllegalArgumentException("Parameter for constraint ["
               +GROUP_CONSTRAINT+"] of property ["
               +constraintPropertyName+"] of class ["
               +constraintOwningClass+"] must be a String or a integer");
        } 
        super.setParameter(constraintParameter);
    }
 
    protected void processValidate(Object target, Object propertyValue, Errors errors) {
    }
 
    boolean supports(Class type) {
        return type != null;
    }
 
    String getName() {
        return SUMMARY_CONSTRAINT;
    }
 
}