package com.cloudmasons.beans

import org.codehaus.groovy.grails.validation.AbstractConstraint
import org.springframework.validation.Errors
 
/**
 * Contraint to allow users to specify settings effecting the given property.
 * Used for derived/calculated transients.  For example, you may have a calculated total
 * number of "clicks" and you allow the users to specify a date range for the clicks.
 *
 * The derived properties are mostly used for reports and the list interface (which
 * doubles as a report generator).
 */
class UserSettingsConstraint extends AbstractConstraint {
 
    private static final String DEFAULT__MESSAGE_CODE = "default.not.valid.message";
    public static final String USER_SETTINGS_CONSTRAINT = "userSettings";
 
    private Map userSettings;
    
    public Map getUserSettings() {
        return userSettings
    }
    
    public void setParameter(Object constraintParameter) {
        // println "setParameter ${constraintParameter} on userSettings constraint"
        if(constraintParameter instanceof Map) {
            userSettings = (Map) constraintParameter
        } else {
            throw new IllegalArgumentException("Parameter for constraint ["
               +USER_SETTINGS_CONSTRAINT+"] of property ["
               +constraintPropertyName+"] of class ["
               +constraintOwningClass+"] must be a map defining settings");
        } 
        super.setParameter(constraintParameter);
    }
 
    protected void processValidate(Object target, Object propertyValue, Errors errors) {
        // Do nothing
    }
 
    boolean supports(Class type) {
        return type != null;
    }
 
    String getName() {
        return USER_SETTINGS_CONSTRAINT;
    }
 
}