package com.cloudmasons.beans

import ar.com.fdvs.dj.domain.CustomExpression


class ToStringCustomExpression implements CustomExpression {

    def fieldName

    def ToStringCustomExpression(fieldName) {
        this.fieldName = fieldName;
    }

    public Object evaluate(Map fields, Map variables, Map parameters) {
        fields[(fieldName)].toString()
    }

    public String getClassName() {
        String.name
    }

}