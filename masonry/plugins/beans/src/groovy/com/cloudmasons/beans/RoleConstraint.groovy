package com.cloudmasons.beans

import org.codehaus.groovy.grails.validation.AbstractConstraint
import org.springframework.validation.Errors
 
/**
 * Custom constraint to allow for specifiing property groups
 */
class RoleConstraint extends AbstractConstraint {
 
    private static final String DEFAULT__MESSAGE_CODE = "default.not.valid.message";
    public static final String ROLE_CONSTRAINT = "role";
 
    private String editRole;

    public String getEditRole() {
        return editRole;
    }
    
    public void setParameter(Object constraintParameter) {
        //println "setParameter ${constraintParameter} on group constraint"
        if(constraintParameter instanceof String) {
            editRole = (String) constraintParameter
        } else {
            throw new IllegalArgumentException("Parameter for constraint ["
               +ROLE_CONSTRAINT+"] of property ["
               +constraintPropertyName+"] of class ["
               +constraintOwningClass+"] must be a String or a map");
        } 
        super.setParameter(constraintParameter);
    }
 
    protected void processValidate(Object target, Object propertyValue, Errors errors) {
    }
 
    boolean supports(Class type) {
        return type != null;
    }
 
    String getName() {
        return ROLE_CONSTRAINT;
    }
 
}