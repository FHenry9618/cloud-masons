package com.cloudmasons.beans

import groovy.lang.Closure;
import org.codehaus.groovy.grails.validation.AbstractConstraint
import org.springframework.validation.Errors
 
/**
 * Alternative for inList that runs a closure to determine what values can 
 * be used for a simple property or a many-to-one property
 */
class AllowedValuesConstraint extends AbstractConstraint {
 
    private static final String DEFAULT__MESSAGE_CODE = "default.not.valid.message";
    public static final String ALLOWED_VALUES_CONSTRAINT = "allowedValues";
 
    private Closure valueProducer;
    
    public List getValues() {
        return valueProducer.call()
    }
    public List getValues(Object delegate) {
        def vp = valueProducer.clone()
        vp.delegate = delegate
        def result = vp.call()

        // Clone the result to allow for protection and sanity
        // since most of the time the result will be a lazy collection
        // in a domain class where modification to the list will modify
        // the underlying domain
        return new ArrayList (result);
    }
    
    public void setParameter(Object constraintParameter) {
        if(!(constraintParameter instanceof Closure))
            throw new IllegalArgumentException("Parameter for constraint ["+ConstrainedProperty.VALIDATOR_CONSTRAINT+"] of property ["+constraintPropertyName+"] of class ["+constraintOwningClass+"] must be a Closure");

        valueProducer = (Closure)constraintParameter;
        super.setParameter(constraintParameter);

    }
 
    protected void processValidate(Object target, Object propertyValue, Errors errors) {
    }
 
    boolean supports(Class type) {
        return type != null;
    }
 
    String getName() {
        return ALLOWED_VALUES_CONSTRAINT;
    }
 
}