package com.cloudmasons.beans

import org.apache.poi.poifs.eventfilesystem.POIFSReader
import org.springframework.core.io.*
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.*;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;


/**
 * 
 */
class CSVFeedProcessor {

    Map defaultRecordValues = [:]

	public process(File file, Feed feed, def recordProcessor) {
        println("Processing xls spreadsheet: $file")
        def columns
        
        file.withInputStream() { ins ->
            ins.eachCsvLine { tokens ->
                if (columns == null) {
                    columns = tokens.collect { it.trim(); }
                } else {
                    def record = [:]
                    int idx = 0;
                    tokens.eachWithIndex { col, i ->
                        
                        record[columns[i]] = col?.trim();
                    }
                    //println record;
                    recordProcessor(record, feed)
                }
            }
        }
    }

}