package com.cloudmasons.beans

import org.apache.poi.poifs.eventfilesystem.POIFSReader
import org.springframework.core.io.*
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.*;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;


/**
 * 
 */
class XLSFeedProcessor {

    Map defaultRecordValues = [:]

	public process(File file, Feed feed, def recordProcessor) {
        println("Processing xls spreadsheet: $file")
        file.withInputStream() { ins ->
            Workbook wb = openWorkbook(ins)
            def numOfSheets = wb.numberOfSheets
            for (int i=0; i < numOfSheets; i++) {
                def sheet = wb.getSheetAt(i)
                def sheetName = wb.getSheetName(i)
                def columnPropertyMapping = [:]
                def records = parseSheet(sheet, columnPropertyMapping)
                for (r in records) {
                	recordProcessor(r, feed)
                }
            }
        }
    }

	private Workbook openWorkbook(InputStream input) {
	    new HSSFWorkbook(input)
	}

    /**
     * Parse a sheet's rows to a list of property maps using the provided
     * column name to property name mapping.
     */
    private List parseSheet(Sheet sheet, Map columnPropertyMapping) {
        List results = []
        if (!sheet) return results

        def creationHelper = sheet.workbook.creationHelper
        def formulaEvaluator = creationHelper.createFormulaEvaluator() 
        // To contain mapping of propertyNames to column index
        def columnMapping = [:]
        for (Row row : sheet) {
            if (row.rowNum == 0) {
                columnMapping = getColumnMapping(row, columnPropertyMapping)
                continue
            }
            def properties = getProperties(row, columnMapping, formulaEvaluator)
            if (properties) {
                results.add(properties)
            }
        }
        return results
    }

    /**
     * Create a mapping of property name to column index based on the mapping
     * of spreadsheet column name to property name
     */
    private Map getColumnMapping(Row firstRow, Map columnPropertyMapping) {

        Map result = [:]
        for (Cell cell : firstRow) {
            String columnName = getCellValue(cell)
            if (!columnName) continue;
            columnName = columnName.replace('\n', '')
           
            def index = cell.getColumnIndex()
            // if the column doesn't exist in the mapping then just add it to the properties
            // with the column name
            String propertyName = columnPropertyMapping[columnName] ?: columnName
            result[propertyName] = index
        }
        return result
    }

    /**
     * Convert a row into a map of property names to values based on the
     * given mapping of property name to column index
     */
    private Map getProperties(Row row, Map columnMapping, FormulaEvaluator formulaEvaluator) {
        Map result = defaultRecordValues.clone()
        boolean hasAnyValue = false
        columnMapping?.each { key, value ->
            result[key] = getCellValue(row.getCell(value), formulaEvaluator)
            if (result[key] != null) hasAnyValue = true
        }
        return hasAnyValue ? result : null
    }
    
    private getCellValue(Cell cell, FormulaEvaluator formulaEvaluator=null) {
        def value = null;
        if (!cell) return value

        switch(cell.getCellType()) {
        case Cell.CELL_TYPE_STRING:
            value = cell.getRichStringCellValue().getString();
            if (value) value = value.trim()
            break;
        case Cell.CELL_TYPE_NUMERIC:
            if(DateUtil.isCellDateFormatted(cell)) {
                value = cell.getDateCellValue()
            } else {
                // Use the data formatter because the getNumericCellValue
                // returns a double and you can't tell if 9323 is 9323 of 9323.00
                // Often the user is expecting a string of 9323 so 9323.00 is unacceptable
                // We're essentially saying here that we want to return the data
                // how its displayed, even if there is a loss of information.
                
                value = new DataFormatter().formatCellValue(cell)
            }
            break;
        case Cell.CELL_TYPE_BOOLEAN:
                value = cell.getBooleanCellValue()
            break;
        case Cell.CELL_TYPE_FORMULA:
            if (formulaEvaluator) {
                try {
                    value = new DataFormatter().formatCellValue(cell, formulaEvaluator)
                } catch (Exception e) {
                    // Cells with formulas with errors cause this condition.
                    value = cell.getCellFormula();
                }
            } else {
                value = cell.getCellFormula()
            }
            break;
        }

        return value;
    }
}