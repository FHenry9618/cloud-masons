package com.cloudmasons.beans

import org.codehaus.groovy.grails.validation.AbstractConstraint
import org.springframework.validation.Errors
 
/**
 * Custom constraint to allow for specifiing property groups
 */
class GroupConstraint extends AbstractConstraint {
 
    private static final String DEFAULT__MESSAGE_CODE = "default.not.valid.message";
    public static final String GROUP_CONSTRAINT = "group";
 
    private String groupName;
    private Integer order;
    
    public String getGroupName() {
    	return groupName
    }
    
    public void setParameter(Object constraintParameter) {
    	//println "setParameter ${constraintParameter} on group constraint"
        if(constraintParameter instanceof String) {
        	groupName = (String) constraintParameter
        } else {
        	throw new IllegalArgumentException("Parameter for constraint ["
               +GROUP_CONSTRAINT+"] of property ["
               +constraintPropertyName+"] of class ["
               +constraintOwningClass+"] must be a String or a map");
        } 
        super.setParameter(constraintParameter);
    }
 
    protected void processValidate(Object target, Object propertyValue, Errors errors) {
        //if (! validPostalCode(target, propertyValue)) {
        //    def args = (Object[]) [constraintPropertyName, constraintOwningClass, 
        //    propertyValue]
        //    super.rejectValue(target, errors, DEFAULT_NOT_POSTAL_CODE_MESSAGE_CODE, 
        //        "not." + POSTAL_CODE_CONSTRAINT, args);
        //}
    }
 
    boolean supports(Class type) {
        return type != null;
    }
 
    String getName() {
        return GROUP_CONSTRAINT;
    }
 
}