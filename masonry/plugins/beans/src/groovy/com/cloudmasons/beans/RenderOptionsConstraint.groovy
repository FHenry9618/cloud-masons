package com.cloudmasons.beans

import org.codehaus.groovy.grails.validation.AbstractConstraint
import org.springframework.validation.Errors

/**
 * Contraint to allow users to specify the rendering options of the given property.
 * For example, user may specify a property should be rendered spanning multiple lines. 
 * Or if pop up dialog is needed.
 *
 */
class RenderOptionsConstraint extends AbstractConstraint {
    
    private static final String DEFAULT__MESSAGE_CODE = "default.not.valid.message";
    public static final String RENDER_OPTIONS_CONSTRAINT = "renderOptions";
    
    private Map renderOptions;
    
    public Map getRenderOptions() {
        return renderOptions
    }
    
    public void setParameter(Object constraintParameter) {
        // println "setParameter ${constraintParameter} on renderOptions constraint"
        if(constraintParameter instanceof Map) {
            renderOptions = (Map) constraintParameter
        } else {
            throw new IllegalArgumentException("Parameter for constraint ["
            +RENDER_OPTIONS_CONSTRAINT+"] of property ["
            +constraintPropertyName+"] of class ["
            +constraintOwningClass+"] must be a map defining settings");
        } 
        super.setParameter(constraintParameter);
    }
    
    protected void processValidate(Object target, Object propertyValue, Errors errors) {
        // Do nothing
    }
    
    boolean supports(Class type) {
        return type != null;
    }
    
    String getName() {
        return RENDER_OPTIONS_CONSTRAINT;
    }
}