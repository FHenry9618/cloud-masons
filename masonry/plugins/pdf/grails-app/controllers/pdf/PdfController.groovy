package pdf

class PdfController {
    PdfService pdfService

    def pdfLink = {
        try {
            byte[] b
            def baseUri = request.scheme +
                    "://" +
                    request.serverName +
                    ":" +
                    request.serverPort +
                    grailsAttributes.getApplicationUri(request)
            if (params.pdfController) {
                def content = g.include(controller: params.pdfController, action: params.pdfAction, id: params.pdfId)
                b = pdfService.buildPdfFromString(content.readAsString(), baseUri)
            } else {
                def url = baseUri + params.url
                b = pdfService.buildPdf(url)
            }
            def fn = (params.filename ?: "document.pdf")
            response.setContentType("application/pdf")
            response.setHeader("Content-disposition", "attachment; filename=" + fn)
            response.setContentLength(b.length)
            response.getOutputStream().write(b)
        }
        catch (Throwable e) {
            println "there was a problem with PDF generation ${e}"
            if (params.pdfController) {
                redirect(controller: params.pdfController, action: params.pdfAction, params: params)
            }
            else {
                redirect(uri: params.url + '?' + request.getQueryString())
            }
        }
    }
}
