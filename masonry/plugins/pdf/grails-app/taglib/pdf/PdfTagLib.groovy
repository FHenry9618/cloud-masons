package pdf
/**
 * A simple taglib for producing links to the PDF creation for a page.
 */
class PdfTagLib {
    /**
     * Creates a PDF creation link for the supplied URL.
     * eg. <g:pdf url="/test.gsp" filename="sample.pdf" icon="true"/>
     */
    def pdfLink = { attrs, body ->

        String pdfController = attrs['pdfController']
        String pdfAction = attrs['pdfAction']
        String c = attrs['class'] ?: 'pdf'
        String pdfId = attrs['pdfId']
        String url = attrs['url']
        String filename = (attrs['filename'] ?: 'document').
                replaceAll("[^\\p{L}\\p{Nd}]+", "") + '.pdf'

        String link
        if (url) {
            link = createLink(
                    controller: 'pdf',
                    action: 'pdfLink',
                    params: [
                            url       : attrs.url,
                            'filename': filename
                    ]
            )
        }
        if (pdfController) {
            link = createLink(
                    controller: 'pdf',
                    action: 'pdfLink',
                    params: [
                            'pdfController': pdfController,
                            'pdfAction'    : pdfAction,
                            'pdfId'        : pdfId,
                            'filename'     : filename
                    ]
            )
        }
        out << """<a href="${link}" class="${c}" title="pdf">"""
        // setup icon/button inside of link if icon is set to 'true'
        if (attrs.icon) {
            out << "<img src='"
            out << createLinkTo(dir: 'images', file: 'pdf_button.png')
            out << "' alt='PDF Version' border='0'/>"
        }
        out << body()
        out << "</a>"
    }
}