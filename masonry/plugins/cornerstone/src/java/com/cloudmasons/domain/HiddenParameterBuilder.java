package com.cloudmasons.domain;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.codehaus.groovy.grails.commons.GrailsControllerClass;
import org.codehaus.groovy.grails.web.util.GrailsPrintWriter;

@SuppressWarnings("rawtypes")
public class HiddenParameterBuilder {
    public static final String ARGUMENT_ID = "id";

    private static final Set<String> EXCLUDES;
    static {
        EXCLUDES = new HashSet<String>();
        EXCLUDES.add(ARGUMENT_ID);
        EXCLUDES.add(GrailsControllerClass.ACTION);
        EXCLUDES.add(GrailsControllerClass.CONTROLLER);
    }

    /*
     * Appends all the request parameters to the URI buffer
     */
    public static void buildParameters(GrailsPrintWriter actualUriBuf, Map<Object, Object> params) {
        for (Map.Entry<Object, Object> entry : params.entrySet()) {
            Object name = entry.getKey();
            if (EXCLUDES.contains(name)) {
                continue;
            }
            Object value = entry.getValue();
            if (value instanceof Collection) {
                Collection values = (Collection) value;
                Iterator valueIterator = values.iterator();
                while (valueIterator.hasNext()) {
                    Object currentValue = valueIterator.next();
                    appendRequestParam(actualUriBuf, name, currentValue);
                }
            } else if (value != null && value.getClass().isArray()) {
                Object[] array = (Object[]) value;
                for (int j = 0; j < array.length; j++) {
                    Object currentValue = array[j];
                    appendRequestParam(actualUriBuf, name, currentValue);
                }
            } else {
                appendRequestParam(actualUriBuf, name, value);
            }
        }
    }

    /*
     * Appends a request parameters for the given aname and value
     */
    static void appendRequestParam(GrailsPrintWriter actualUriBuf, Object name, Object value) {
        if (value instanceof String && StringUtils.isBlank((String)value)) {
            return;
        }
        actualUriBuf.append("<input type=\"hidden\" name=\"");
        actualUriBuf.append(name).append("\" id=\"");
        actualUriBuf.append(name).append("\" value=\"");
        actualUriBuf.append(value).append("\">\n");
    }
}
