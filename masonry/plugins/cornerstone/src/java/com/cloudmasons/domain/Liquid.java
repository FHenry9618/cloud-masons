package com.cloudmasons.domain;

import org.codehaus.groovy.transform.GroovyASTTransformationClass;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;

/**
 * Annotation to indicate that a domain class is to have properties
 * inserted based on the liquid base definition.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@GroovyASTTransformationClass("com.cloudmasons.cornerstone.domain.LiquidEntityASTTransformation")
public @interface Liquid {
}