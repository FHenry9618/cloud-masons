package com.cloudmasons.domain;
import java.lang.reflect.Modifier;

import org.codehaus.groovy.ast.ASTNode;
import org.codehaus.groovy.ast.ClassNode;
import org.codehaus.groovy.ast.expr.ConstantExpression;
import org.codehaus.groovy.control.CompilePhase;
import org.codehaus.groovy.control.SourceUnit;
import org.codehaus.groovy.transform.ASTTransformation;
import org.codehaus.groovy.transform.GroovyASTTransformation;


/**
 * Performs an AST transformation on a class adding properties 
 * to enable a domain class to represent a secure file.
 */
@GroovyASTTransformation(phase = CompilePhase.CANONICALIZATION)
public class SecureFileASTTransformation implements ASTTransformation {

//    private static final Log LOG = LogFactory.getLog(DomainASTTransformation.class);
//    private static final ConfigObject CO = new ConfigSlurper().parse(getContents(new File("./grails-app/conf/Config.groovy")));
//    private static final Properties CONF = (new ConfigSlurper().parse(getContents(new File("./grails-app/conf/Config.groovy")))).toProperties();

    private void debug(Object o) {
        if (o == null) System.out.println("null");
        else System.out.println(o.toString());
    }

    public void visit(ASTNode[] astNodes, SourceUnit sourceUnit) {
        debug("Corernstone secure file transformer: visit");
        for (ASTNode astNode : astNodes) {
            if (astNode instanceof ClassNode) {
                ClassNode classNode = (ClassNode) astNode;
                debug("[Secure File ASTTransformation] Adding properties [status, name, description] to class [" + classNode.getName() + "]");
                
                // The display name 
                classNode.addProperty("name", Modifier.PUBLIC, new ClassNode(String.class), ConstantExpression.NULL, null, null);

                // The original file name / file name for downloads
                classNode.addProperty("originalFilename", Modifier.PUBLIC, new ClassNode(String.class), ConstantExpression.NULL, null, null);
                
                // Description of the file uploaded
                classNode.addProperty("description", Modifier.PUBLIC, new ClassNode(String.class), ConstantExpression.NULL, null, null);


                
                // The size of the unencrypted File
                classNode.addProperty("size", Modifier.PUBLIC, new ClassNode(Long.class), new ConstantExpression(0), null, null);
                
                
                // The encryption key (which is encrypted using application key)
                // So for someone to decrypt the file, they would have to have access to the 
                // stored cipher text, the database to get the cipher text for the key and
                // then a third cipher key which will be with the application.  If all of 
                // these items are on different servers then the file will have a high 
                // level of security
                classNode.addProperty("encryptionKey", Modifier.PUBLIC, new ClassNode(String.class), ConstantExpression.NULL, null, null);

                classNode.addProperty("encryptedSize", Modifier.PUBLIC, new ClassNode(Long.class), new ConstantExpression(0), null, null);
                // The name of the encrypted file
                classNode.addProperty("encryptedName", Modifier.PUBLIC, new ClassNode(String.class), ConstantExpression.NULL, null, null);

                // where the file is stored.  LOCAL or S3 for amazon S3
                classNode.addProperty("storageType", Modifier.PUBLIC, new ClassNode(String.class), ConstantExpression.NULL, null, null);
                // A path identifier to the encrypted file on the primary storage location
                classNode.addProperty("path", Modifier.PUBLIC, new ClassNode(String.class), ConstantExpression.NULL, null, null);
                // Status of the file - indicates the current operation being performed on the file (if any
                //   COPY_TO_SECONDARY - copying encrypted file to secondary file location
                //   DELETE_SECONDARY - secondary file location should be deleted
                classNode.addProperty("status", Modifier.PUBLIC, new ClassNode(String.class), ConstantExpression.NULL, null, null);

                // how the file is stored.  NONE, LOCAL or S3 for amazon S3
                // the secondary storage is mostly to keep track of two locations
                // while a copy is occurring.  
                // copy algorithm:
                //   Modify secure file to indicate where the file is being copied to - commit
                //   copy file
                //   Swap primary and secondary locations in the secure file object - commit
                //   delete old file
                //   Remove pointer from the secondary location - commit
                // 
                //   Cleanup - Cron job will restart any interrupted opearations.
                //
                // Should we allow an option to keep the secondary
                classNode.addProperty("secondaryStorageType", Modifier.PUBLIC, new ClassNode(String.class), ConstantExpression.NULL, null, null);
                classNode.addProperty("secondaryPath", Modifier.PUBLIC, new ClassNode(String.class), ConstantExpression.NULL, null, null);

            }
        }
    }
}
//
//    public void addTableAndIdMapping(ClassNode classNode){
//        FieldNode closure = classNode.getDeclaredField("mapping");
//
//        if(closure!=null){
//            boolean hasTable=hasFieldInClosure(closure,"table");
//            boolean hasId=hasFieldInClosure(closure,"id");
//
//            ClosureExpression exp = (ClosureExpression)closure.getInitialExpression();
//            BlockStatement block = (BlockStatement) exp.getCode();
//
//            //this just adds an s to the class name for the table if its not specified 
//            Boolean pluralize = (Boolean)getMap(CO,"stamp.mapping.pluralTable");
//            if(!hasTable && pluralize!=null && pluralize){
//                String tablename = GrailsClassUtils.getShortName(classNode.getName())+"s";
//                //LOG.info("Added new mapping to assign table: " + tablename);
//                MethodCallExpression tableMeth = new MethodCallExpression(
//                    VariableExpression.THIS_EXPRESSION,
//                    new ConstantExpression("table"),
//                    new ArgumentListExpression(new ConstantExpression(tablename)) 
//                    );
//                //block = (BlockStatement) exp.getCode();
//                block.addStatement(new ExpressionStatement(tableMeth));
//                //System.out.println(classNode.getName()+" - Added table mapping " + tablename );
//            }
//            //This adds the ID generator that we use for domian classes
//            Map tableconf = (Map)getMap(CO,"stamp.mapping.id");
//            if(!hasId && tableconf!=null){
//                NamedArgumentListExpression namedarg = new NamedArgumentListExpression();
//                if(tableconf.get("column") != null){
//                    namedarg.addMapEntryExpression(new ConstantExpression("column"), new ConstantExpression(tableconf.get("column").toString()));
//                }
//                if(tableconf.get("generator") != null){
//                    namedarg.addMapEntryExpression(new ConstantExpression("generator"), new ConstantExpression(tableconf.get("generator").toString()));
//                }
//                MethodCallExpression tableMeth = new MethodCallExpression(
//                    VariableExpression.THIS_EXPRESSION,
//                    new ConstantExpression("id"),
//                    namedarg
//                    );
//                //block = (BlockStatement) exp.getCode();
//                block.addStatement(new ExpressionStatement(tableMeth));
//                //System.out.println(classNode.getName() + " - Added ID mapping with "+ tableconf);
//            }
//        }
//        //System.out.println(block.toString());
//    }
//
//    public void addNullableConstraint(ClassNode classNode,String fieldName){
//        FieldNode closure = classNode.getDeclaredField("constraints");
//
//        if(closure!=null){
//
//            ClosureExpression exp = (ClosureExpression)closure.getInitialExpression();
//            BlockStatement block = (BlockStatement) exp.getCode();
//
//            if(!hasFieldInClosure(closure,fieldName)){
//                NamedArgumentListExpression namedarg = new NamedArgumentListExpression();
//                namedarg.addMapEntryExpression(new ConstantExpression("nullable"), new ConstantExpression(true));
//                MethodCallExpression constExpr = new MethodCallExpression(
//                    VariableExpression.THIS_EXPRESSION,
//                    new ConstantExpression(fieldName),
//                    namedarg
//                    );
//                block.addStatement(new ExpressionStatement(constExpr));
//                //System.out.println(classNode.getName() + " - Added nullabel constraint for "+ fieldName);
//            }
//        }
//        //System.out.println(block.toString());
//    }
//
//
//
//    public boolean hasFieldInClosure(FieldNode closure, String fieldName){
//        if(closure != null){
//            ClosureExpression exp = (ClosureExpression) closure.getInitialExpression();
//            BlockStatement block = (BlockStatement) exp.getCode();
//            List<Statement> ments = block.getStatements();
//            for(Statement expstat : ments){
//                if(expstat instanceof ExpressionStatement && ((ExpressionStatement)expstat).getExpression() instanceof MethodCallExpression){
//                    MethodCallExpression methexp = (MethodCallExpression)((ExpressionStatement)expstat).getExpression();
//                    ConstantExpression conexp = (ConstantExpression)methexp.getMethod();
//                    if(conexp.getValue().equals(fieldName)){
//                        return true;
//                    }
//                }
//            }
//        }
//        return false;
//    }
//
//
//    static public String getContents(File aFile) {
//        //...checks on aFile are elided
//        StringBuilder contents = new StringBuilder();
//
//        try {
//            //use buffering, reading one line at a time
//            //FileReader always assumes default encoding is OK!
//            BufferedReader input =  new BufferedReader(new FileReader(aFile));
//            try {
//                String line = null; 
//                while (( line = input.readLine()) != null){
//                    contents.append(line);
//                    contents.append(System.getProperty("line.separator"));
//                }
//            }
//            finally {
//                input.close();
//            }
//        }
//        catch (IOException ex){
//            ex.printStackTrace();
//        }
//
//        return contents.toString();
//    }
//
//    static public Object getMap(Map configMap, String keypath) {
//        String keys[] = keypath.split("\\.");
//        Map map = configMap;
//        for(String key : keys){
//            Object val = map.get(key);
//            if(val !=null){
//                //System.out.println("got a key for are " +key);
//                if(val instanceof Map){
//                    map = (Map)map.get(key);
//                } else{
//                    return val;
//                }
//            }else{
//                return null;
//            }
//        }
//        return map;    
//    }
//
//
//}
//
//
////FUTURE 
///**
//java.math.BigDecimal
//java.lang.Integer
//java.lang.Long
//java.util.Date
//java.lang.String
//java.lang.Boolean
//*/
//
///**
//since grails has everything default to nullable:false, we change that to nullable:true here since omost of the time we condider it ok
//explicity set nullable:false as the exception
//
//public void addConstraintDefaults(ClassNode classNode){
//    List<FieldNode>  fnlist = classNode.getFields();
//    for(FieldNode fnode : fnlist){
//        if(!fnode.isStatic()){
//            //check if the type is in our list
//            System.out.println("*" + fnode.getName() + " - " + fnode.getType().getName());
//        }
//    }
//    
//    boolean hasConstraint=false;
//
//}
//**/
//
///*
//org.codehaus.groovy.ast.stmt.BlockStatement@f4b2da[
//    org.codehaus.groovy.ast.stmt.ExpressionStatement@a0a4a[
//        expression:org.codehaus.groovy.ast.expr.MethodCallExpression@29aa5a[
//            object: org.codehaus.groovy.ast.expr.VariableExpression@6f0383[variable: this] 
//            method: ConstantExpression[discDate] 
//            arguments: org.codehaus.groovy.ast.expr.NamedArgumentListExpression@4fb195[
//                org.codehaus.groovy.ast.expr.MapEntryExpression@13becc(key: ConstantExpression[nullable], value: ConstantExpression[true])
//            ]
//        ]
//    ],.....
//
///*
//{ org.codehaus.groovy.ast.stmt.BlockStatement@f0bc0[
//    org.codehaus.groovy.ast.stmt.ExpressionStatement@cc9e15[
//        expression:org.codehaus.groovy.ast.expr.MethodCallExpression@9e94e8[
//            object: org.codehaus.groovy.ast.expr.VariableExpression@3c2282[variable: this] 
//            method: ConstantExpression[table] 
//            arguments: org.codehaus.groovy.ast.expr.ArgumentListExpression@42428a[ConstantExpression[SyncSteps]]
//        ]
//    ], 
//    org.codehaus.groovy.ast.stmt.ExpressionStatement@1eafb4[
//        expression:org.codehaus.groovy.ast.expr.MethodCallExpression@a17663[
//            object: org.codehaus.groovy.ast.expr.VariableExpression@3c2282[variable: this] 
//            method: ConstantExpression[id] 
//            arguments: org.codehaus.groovy.ast.expr.NamedArgumentListExpression@636202[
//                org.codehaus.groovy.ast.expr.MapEntryExpression@b781ea(
//                    key: ConstantExpression[column], value: ConstantExpression[OID]
//                ), 
//                org.codehaus.groovy.ast.expr.MapEntryExpression@b25934(
//                    key: ConstantExpression[generator], value: ConstantExpression[nineci.hibernate.NewObjectIdGenerator]
//                )
//            ]
//        ]
//    ], org.codehaus.groovy.ast.stmt.ExpressionStatement@fe6f06[
//        expression:org.codehaus.groovy.ast.expr.MethodCallExpression@2b0459[
//            object: org.codehaus.groovy.ast.expr.VariableExpression@3c2282[variable: this] 
//            method: ConstantExpression[syncBatch] 
//            arguments: org.codehaus.groovy.ast.expr.NamedArgumentListExpression@2a938f[
//                org.codehaus.groovy.ast.expr.MapEntryExpression@3dbf04(key: ConstantExpression[column], value: ConstantExpression[SyncBatchId])]]]] }
//
//
//*/
