package com.cloudmasons.domain;

import org.codehaus.groovy.transform.GroovyASTTransformationClass;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;

/**
 * Annotation indicating that the the domain class represents an arbitrary
 * set of bytes that needs to be persisted securely and retrieved.
 *  
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@GroovyASTTransformationClass("com.cloudmasons.domain.SecureFileASTTransformation")
public @interface SecureFile {
    public static final String LOCAL = "LOCAL";
    public static final String AMAZON_S3 = "AMAZON_S3";
    

}