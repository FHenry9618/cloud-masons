package com.cloudmasons.beans
import org.apache.commons.lang.StringUtils;

import java.text.NumberFormat;
import java.text.ParseException
import java.beans.PropertyEditorSupport;
import org.springframework.util.NumberUtils

/**
 * A Property editor for monetary amounts that displays wtih a currency
 * symbol and formatted for the current locale, but is more tolerant of
 * what it expects back.
 * 
 * This is currently bound to BigDecimal properties which will need
 * to be revisited eventually.
 */
public class PercentageEditor  extends PropertyEditorSupport  {

    NumberFormat percentFormat = NumberFormat.getPercentInstance();


    public void setAsText(String text) throws IllegalArgumentException {
        percentFormat.setMaximumFractionDigits(2);
        if(!text) {
            setValue(null);
        }
        try {
            //println "parsing as money ${text}"
            setValue(NumberUtils.parseNumber(text, BigDecimal, percentFormat));
        } catch (IllegalArgumentException e) {

        }
    }

    public String getAsText() {
        percentFormat.setMaximumFractionDigits(2);

        Object value = getValue();
        if (value == null) {
            return "";
        }
        return this.percentFormat.format(value);

    }
}
