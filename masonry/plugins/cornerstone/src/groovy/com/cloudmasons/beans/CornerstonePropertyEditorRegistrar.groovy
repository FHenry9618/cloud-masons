package com.cloudmasons.beans

import java.text.SimpleDateFormat
import java.text.NumberFormat
import java.util.Date
import org.codehaus.groovy.grails.web.binding.StructuredDateEditor
import org.springframework.beans.PropertyEditorRegistrar
import org.springframework.beans.PropertyEditorRegistry
import org.springframework.beans.propertyeditors.CustomNumberEditor

public class CornerstonePropertyEditorRegistrar implements PropertyEditorRegistrar {
    
  def g = new org.codehaus.groovy.grails.plugins.web.taglib.ApplicationTagLib()
    
  public void registerCustomEditors(PropertyEditorRegistry registry) {
      NumberFormat moneyFormat = NumberFormat.getCurrencyInstance();
      String dateFormat;
      try {
          dateFormat = g.message(code: 'default.edit.date.format', default: 'MM/dd/yyyy')
      }
      catch (java.lang.IllegalStateException e) {
          //println e
          dateFormat = 'MM/dd/yyyy'
      }
      registry.registerCustomEditor(Date.class, new StructuredDateEditor(new SimpleDateFormat(dateFormat), true))
      registry.registerCustomEditor(BigDecimal.class, new MonetaryAmountEditor());
      
  }
} 