package com.cloudmasons.beans
import org.apache.commons.lang.StringUtils;

import java.text.NumberFormat;
import java.text.ParseException
import java.beans.PropertyEditorSupport;
import org.springframework.util.NumberUtils

/**
 * A Property editor for monetary amounts that displays wtih a currency
 * symbol and formatted for the current locale, but is more tolerant of
 * what it expects back.
 * 
 * This is currently bound to BigDecimal properties which will need
 * to be revisited eventually.
 */
public class MonetaryAmountEditor  extends PropertyEditorSupport  {
    NumberFormat monetaryFormat = NumberFormat.getCurrencyInstance();
    NumberFormat floatFormat = NumberFormat.getInstance();

    public void setAsText(String text) throws IllegalArgumentException {
        if(!text) {
            setValue(null);
        }
        try {
            //println "parsing as money ${text}"
            setValue(NumberUtils.parseNumber(text, BigDecimal, monetaryFormat));
        } catch (IllegalArgumentException e) {
            try {
                //println "parsing as float ${text} ${e}"
                setValue(NumberUtils.parseNumber(text, BigDecimal, floatFormat));
            } catch(IllegalArgumentException e2) {
                //println "parsing as big decimal ${text} ${pe2}"
                setValue(text.toBigDecimal())
            }
        }
    }

    public String getAsText() {
        Object value = getValue();
        if (value == null) {
            return "";
        }
        return this.monetaryFormat.format(value);

    }
}
