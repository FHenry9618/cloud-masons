package com.cloudmasons

public class Theme {

    public static DEFAULT_THEME = new Theme(
            name: 'default', 
            description: 'The default cloudmasons theme',
            layout: 'main',
            plugin: 'cornerstone');
    
    String name
    
    String description

    String layout = 'main'
    
    String editPropertyPath = 'bean/edit'
    String showPropertyPath = 'bean/show'
    String printPropertyPath = 'bean/print'
    
    /** Themes can extend one another **/
    Theme parent
    
    // Theme is passed into the template resolver which
    // uses property path to find property templates.
    String propertyPath = editPropertyPath
    
    /** 
     * The name of the plugin containing the default
     * templates for the theme
     */
    String plugin = 'cornerstone'
    
    
    static transients = [ 'propertyPath']
    
    static mapping = {
       cache true
    }
    static constraints = {
        name(summary: true)
        description(nullable: true, summary: true)
        parent(nullable: true)
        plugin(nullable: true)
    }
    
}
