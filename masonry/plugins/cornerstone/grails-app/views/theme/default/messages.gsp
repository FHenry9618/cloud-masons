        <g:if test="${flash.message}">
            <div class="msg msg-ok">${flash.message}</div>
        </g:if>
        <g:hasErrors bean="${bean}">
            <div class="msg msg-error">
                <g:renderErrors bean="${bean}" as="list" />
            </div>
        </g:hasErrors>