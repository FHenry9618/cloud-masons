        <g:if test="${flash.message}">
            <div class="msg msg-ok">${flash.message}</div>
        </g:if>
        <g:hasErrors bean="${bean}">
            <div class="msg msg-error">
                <g:renderErrors bean="${bean}" as="list" />
            </div>
        </g:hasErrors>
        
<%
  if (!label) {
     label = "Edit ${domainNaturalName}: ${bean.name}"
  }
%>
                <div class="box box-75 altbox">
                    <div class="boxin">
                        <div class="header">
                            <h3>${label}</h3>
                        </div>
                        <g:form class="basic editForm" controller="${beanControllerName}" action="${beanControllerAction}" method="post">
                           <g:hiddenField name="id" value="${bean.id}" />
                           <g:hiddenField name="version" value="${bean.version}" />

                           <div id="accordion" class="ui-accordion ui-widget ui-helper-reset ui-accordion-icons">
                              <g:each in="${propertyGroups.keySet()}" var="${group}" status="i">
                              <%
                                 // Set the hidden style on render for accordian tabs that are
                                 // going to be hidden anyway by jquery (prevents flickering)
                                 String style = ''
                                 if (i > 1 || propertyGroups?.size() > 5) {
                                   style = 'style="display: none;"'
                                 }
                              %>
                                 <h3 class="ui-accordion-header ui-helper-reset ui-corner-top"><a href="#"><strong>${group}</strong></a></h3>
                                 <div ${style}>
                                 <theme:propertyGroup bean="${bean}" name="${group}" props="${propertyGroups[group]}" />
                                 </div>

                              </g:each>
                           </div>
                      

                       </g:form>

                    </div>
                </div>


                <div class="box box-25 altbox">
                    <div class="boxin">
                        <div class="header">
                            <h3>Summary</h3>
                        </div>

                            <form>
                            <fieldset>
                                <g:if test="${summaryProperties}">
                                    <dl class="compact">
                                    <g:each in="${summaryProperties}" var="p">
                                      <dt><bean:label bean="${bean}" property="${p}"/></dt><dd><theme:property bean="${bean}" beanPathPrefix="${beanPathPrefix}" name="${p.name}" readonly="${true}"/>&nbsp;</dd>
                                    </g:each>
                                    </dl>
                                </g:if>
                                <br/>
                                
                                Please make your changes and then click <b>Submit</b>. 
                                
                                <br/><br/>
                                
                                <div class="sep">
                                    <a class="button altbutton submitButton">Submit</a>
                                    <a class="button altbutton cancelButton">Cancel</a>
                                </div>
                                
                                <div class="sep">
                                  &nbsp;
                                </div>
                                <div class="sep">
                                </div>

                            </fieldset>
                        </form>
                    </div>
                </div>
  <script type="text/javascript">
  
     $(document).ready(function() {
          $('#accordion').accordion({
                      autoHeight: false,
                      collapsible: true,
                      active: ${(propertyGroups?.size() > 5) ? 'false' : '0'}
                      
                });
         $('.submitButton')
            .button()
            .click(function() {
                $('.editForm').submit();
            });
         $('.cancelButton')
            .button()
            .click(function() {
                if(confirm('Are you sure you wish to cancel?')) {
                    <g:if test="${previousRequest}">
                        window.location = "${createLink(action: previousRequest.action, controller: previousRequest.controller, params: previousRequest.params )}";
                    </g:if>
                    <g:else>
                    window.location = "${createLink(action: 'list', controller: beanControllerName )}";
                    </g:else>
                }
            });
            
         $('.money').priceFormat({
            prefix: '$ '
         }); 
     
    });
  </script>