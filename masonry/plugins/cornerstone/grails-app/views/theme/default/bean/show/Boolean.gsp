<g:if test="${value != null}">
<%
   if (value) {
     out << message(code: 'default.true.display', default: 'yes')
   } else {
     out << message(code: 'default.false.display', default: 'no')
   }
%>
</g:if>
<g:else>
<i>no value</i>
</g:else>
