    <%
       String renderClass = ''
       String size='30'
       if (domainClass?.constrainedProperties[property.name]?.getAppliedConstraint('renderOptions')?.renderOptions?.timepicker) {
          renderClass = 'time'
          size=null
       }
        
       def closure = domainClass?.constrainedProperties[property.name]?.getAppliedConstraint('renderOptions')?.renderOptions?.defaultValue
       if (!value && closure) {
          closure.resolveStrategy = Closure.DELEGATE_FIRST 
          closure.delegate = beanInstance
          value = closure();
       }
       def readonly = ''
       if (domainClass?.constrainedProperties[property.name]?.getAppliedConstraint('renderOptions')?.renderOptions?.readOnly) {
            readonly = 'readonly'
       }
      
    %>
<g:if test="${cp?.inList}">
    <g:select name="${beanPath}" from="${cp.inList.sort()}" value="${fieldValue(bean: bean, field: property.name)}" valueMessagePrefix="${field.domainClass.propertyName}.${field.name}" noSelection="['': 'select...']" />
</g:if>
<g:elseif test="${'textarea' == cp?.widget || (cp?.maxSize > 250 && !cp?.password && !cp?.inList)}">
    <g:if test="${readonly}">
        <g:textArea readonly="readonly" id="${beanPath}" name="${beanPath}" cols="60" rows="5" value="${value}" />
    </g:if>
    <g:else>
        <g:textArea  id="${beanPath}" name="${beanPath}" cols="60" rows="5" value="${value}" />
    </g:else>
</g:elseif>
<g:elseif test="${cp?.password  == true}">
   <input ${readonly} class="txt ${g.hasErrors(bean:bean, field: property.name, 'error')}"  type="password" id="${beanPath}" name="${beanPath}" size="30" value="shhhhhsecret" />
   <g:if test="${required}">
    <font color="red"> *</font>
   </g:if>
</g:elseif>
<g:else>
   <input ${readonly} class="${renderClass} txt ${g.hasErrors(bean:bean, field: property.name, 'error')}" type="text" id="${beanPath}" name="${beanPath}" size="${size}" value="${value}" />
   <g:if test="${required}">
    <font color="red"> *</font>
   </g:if>
</g:else>

