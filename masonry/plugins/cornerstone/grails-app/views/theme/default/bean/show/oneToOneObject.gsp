<g:if test="${value != null}">
    <g:if test="${beanControllerName != property.name && property.otherSide}">
        <g:link controller="${beanControllerName}" action="show" id="${beanInstance?.id}" params="[associated: property.name, propertyId: value?.id]">${value?.name}</g:link>
    </g:if>
    <g:else>
        ${value?.name?.encodeAsHTML()}
    </g:else>
</g:if>
<g:else>
<i>none</i>
</g:else>
