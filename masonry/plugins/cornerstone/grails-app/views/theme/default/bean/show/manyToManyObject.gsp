<g:if test="${value != null}">
${value?.join(', ')?.encodeAsHTML()}
</g:if>
<g:else>
<i>none</i>
</g:else>