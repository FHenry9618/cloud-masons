<g:if test="${cp.inList}">
   <%
       String noSelection = ''
       if (field.optional) {
          if (field.manyToOne || field.oneToOne) {
                noSelection = "noSelection=\"['null': '']\""
            }
            else {
                noSelection = "noSelection=\"['': '']\""
            }
        }
   %>
   <g:select name="${beanPath}" from="${cp.inList}" value="${fieldValue(bean: bean, field: field.name)}" valueMessagePrefix="${field.domainClass.propertyName}.${field.name}" ${noSelection} />
</g:if>
<g:else>
   <input class="txt${ error ? ' error' : ''} money" type="text" id="${beanPath}" name="${beanPath}" size="30" value="${fieldValue(bean: bean, field: field.name)}" />
</g:else>