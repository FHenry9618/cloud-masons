
<div id="${domainClass.propertyName}-table" class="content">
	<!-- content box 1 for tab switching -->
	<form class="plain" action="" method="post"
		enctype="multipart/form-data">
		<fieldset>
			<table cellspacing="0">
				<thead>
					<!-- universal table heading -->
					<tr>
						<%     summaryProperties.eachWithIndex { p, i ->
                                                   if (i < 6) {
                                                      if (p.isAssociation() || !p.isPersistent()) { %>
                                                       <th><g:message code="${domainClass.propertyName}.${p.name}.label" default="${p.naturalName}" /></th>
                                                 <%      } else { %>
                                                      <g:sortableColumn params="${beanParams}" property="${p.name}" title="${message(code: domainClass.propertyName +'.' + p.name + '.label', default: p.naturalName)}" />
                                              <%  }   }   } %>
						<td class="tc">Actions</td>
					</tr>
				</thead>
				<tfoot>
					<!-- table foot - what to do with selected items -->
				</tfoot>
				<tbody>
					<g:if test="${beans}">
						<g:each in="${beans}" status="i" var="bean">
							<tr class="${i == 0 ? 'first' : ''}">
								<%  summaryProperties.eachWithIndex { p, j ->
                                                 cp = domainClass.constrainedProperties[p.name]
                                                    if (p.type == Boolean.class || p.type == boolean.class) { %>
                                                       <td class="tc"><g:formatBoolean boolean="${bean.properties[p.name]}" /></td>
                                        <%          } else if (p.type == Date.class || p.type == java.sql.Date.class || p.type == java.sql.Time.class || p.type == Calendar.class) { %>
                                                       <td class="tc"><g:formatDate date="${bean.properties[p.name]}" /></td>
                                        <%          } else if (p.manyToOne || p.oneToOne) { %>
                                                       <td class="tc">${bean.properties[p.name]?.name}</td>                   
                                        <%          } else { %>
                                                       <td class="tc">${fieldValue(bean: bean, field: p.name) ?: ''}</td>
                                        <%          }   } %>
								<td class="tc">
									<ul class="actions">
										<bean:eachAuthorizedInstanceAction bean="${bean}"
											actions="${actions}">
											<li>[&nbsp;<bean:actionLink bean="${bean}"
													action="${action}" />&nbsp;]
											</li>
										</bean:eachAuthorizedInstanceAction>
									</ul>
								</td>
							</tr>
						</g:each>
					</g:if>
					<g:else>
						<tr>
							<td colspan="${summaryProperties.size() + 1}">
								<%-- plus one for action column --%> No Values
							</td>
					</g:else>
				</tbody>
			</table>
		</fieldset>
	</form>
	<div class="pagination">
		<ul>
		bbbbbbbbbbbbbbbbaaadslkjASDLsjdff
			<g:paginate total="${total}" offset="${offset}" max="${max}"
				params="${beanParams}" />
		</ul>
	</div>
</div>
<!-- end table list -->
