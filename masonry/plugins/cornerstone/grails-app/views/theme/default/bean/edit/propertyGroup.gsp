                            <fieldset>
                             <g:if test="${title}">
                               <legend>${title}</legend>
                             </g:if>
                             <table>
                               <g:each in="${properties}" var="${property}" status="i">
                                 <%
                                    def propertyLabel = g.message(code: "${domainClass.propertyName}.${property.name}.label", default: property.naturalName)
                                 %>
                               <g:if test="${domainClass?.constrainedProperties[property.name]?.getAppliedConstraint('renderOptions')?.renderOptions?.longLabel}">
                                   <tr>
                                        <td align="left" style="vertical-align: top;" colspan="2">
                                           <g:if test="${!(property.oneToMany && property.bidirectional)}"> <%-- Hide the label on what is usually a table for one-to-many --%>
                                              <label class="${g.hasErrors(bean:bean, field: property.name, 'error')} "for="${property.name}">
                                                <g:if test="${domainClass?.constrainedProperties[property.name]?.getAppliedConstraint('renderOptions')?.renderOptions?.labelOnly}">
                                                    <b><bean:label bean="${bean}" property="${property}"/>:</b>
                                                </g:if>
                                                <g:else>
                                                    <bean:label bean="${bean}" property="${property}"/>:
                                                </g:else>
                                              </label>
                                           </g:if>
                                        </td>
                                   </tr>
                                   <g:if test="${!domainClass?.constrainedProperties[property.name]?.getAppliedConstraint('renderOptions')?.renderOptions?.labelOnly}">
                                   <tr>
                                        <td align="left" style="vertical-align: top;" colspan="2"><theme:property bean="${bean}" beanPathPrefix="${beanPathPrefix}" name="${property.name}"/></td>
                                        
                                   </tr>
                                   </g:if>
                                   <tr>
                                        <td align="left" style="vertical-align: top;" colspan="2"><br/></td>
                                   </tr>
                               </g:if>
                               <g:else>
                                <tr>
                                    <td align="right" style="vertical-align: top;">
                                       <g:if test="${!(property.oneToMany && property.bidirectional)}"> <%-- Hide the label on what is usually a table for one-to-many --%>
                                          <label class="${g.hasErrors(bean:bean, field: property.name, 'error')} "for="${property.name}"><bean:label bean="${bean}" property="${property}"/>:</label>
                                       </g:if>
                                    </td>
                                    <td nowrap="true" style="white-space: nowrap;"><theme:property bean="${bean}" beanPathPrefix="${beanPathPrefix}" name="${property.name}"/></td>
                                </tr>
                               </g:else>
                                </g:each>
                              </table>
                            </fieldset>
