<%
   def format = cp?.format ?: message(code: 'default.edit.date.format', default: 'MM/dd/yyyy')
   
   def closure = domainClass?.constrainedProperties[property.name]?.getAppliedConstraint('renderOptions')?.renderOptions?.defaultValue
   if (!value && closure) {
      closure.resolveStrategy = Closure.DELEGATE_FIRST 
      closure.delegate = beanInstance
      value = closure();
   }
   def readonly = ''
   def dateClass = 'date'
   if (domainClass?.constrainedProperties[property.name]?.getAppliedConstraint('renderOptions')?.renderOptions?.readOnly) {
        readonly = 'readonly'
        dateClass = ''
   }
%>
<input ${readonly} class="${dateClass} txt" type="text" name="${beanPath}" id="${beanPath}" value="${value?.format(format)}"/>
<g:if test="${required}">
    <font color="red"> *</font>
</g:if>
