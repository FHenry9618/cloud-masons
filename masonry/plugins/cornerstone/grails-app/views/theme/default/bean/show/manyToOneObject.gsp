<g:if test="${value != null}">
${value?.name?.encodeAsHTML()}
</g:if>
<g:else>
<i>none</i>
</g:else>
