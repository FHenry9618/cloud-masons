<%
  // Honor the allowedValues constraint
  def values = []
  def allowedValues = cp?.getAppliedConstraint('allowedValues')
  if (allowedValues) {
      values = allowedValues.getValues(bean)
  } else {
      values = field.referencedDomainClass.clazz.list()
  }
  values = values.sort { it.name }

  def names = values.collect { it.name; }
  def labels = []
  def keys = []
  values?.each { v ->
    if (names.count(v.name) > 1 ) {
        labels.add("${v.name} - ${v.description ?: 'no description'}");
    } else {
        labels.add(v.name);
    }
    keys.add(v.id);
  }
 %>
<g:if test="${field.optional}">
   <g:if test="${field.manyToOne || field.oneToOne}">
      <g:select name="${beanPath}.id" from="${labels}" keys="${keys}"  value="${value?.id}" noSelection="['null': 'select...']" />
   </g:if>
   <g:else>
      <g:select name="${beanPath}.id" from="${labels}" keys="${keys}"  value="${value?.id}" noSelection="['': 'select...']" />
   </g:else>
</g:if>
<g:else>
   <g:select name="${beanPath}.id" from="${labels}" keys="${keys}"  value="${value?.id}" ${noSelection} />
</g:else>

