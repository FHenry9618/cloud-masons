                            <fieldset>

                             <table>
                               <g:each in="${properties}" var="${property}" status="i">
                                 <%
                                    def propertyLabel = g.message(code: "${domainClass.propertyName}.${property.name}.label", default: property.naturalName)
                                 %>
                               <g:if test="${domainClass?.constrainedProperties[property.name]?.getAppliedConstraint('renderOptions')?.renderOptions?.longLabel}">
                               
                                 <g:if test="${g.hasErrors(bean:bean, field: property.name)}">
                                 <tr>
                                    <td align="right" style="vertical-align: top;" colspan="2">
                                       error
                                       <g:if test="${!(property.oneToMany && property.bidirectional)}"> <%-- Hide the label on what is usually a table for one-to-many --%>
                                        <label class="error" for="${property.name}"><bean:label bean="${bean}" property="${property}"/>:</label>
                                       </g:if>
                                    </td>
                                 </tr>
                                 <tr>   
                                    <td colspan="2"><theme:property bean="${bean}" beanPathPrefix="${beanPathPrefix}" name="${property.name}" error="true" readonly="true"/></td>
                                 </tr>   
                                 <tr>
                                        <td align="left" style="vertical-align: top;" colspan="2"><br/></td>
                                 </tr>
                                 </g:if>
                                 <g:else>
                                 <tr>
                                    <td align="right" style="vertical-align: top;" colspan="2">
                                       <g:if test="${!(property.oneToMany && property.bidirectional)}"> <%-- Hide the label on what is usually a table for one-to-many --%>
                                          <b><label for="${property.name}"><bean:label bean="${bean}" property="${property}"/>:</label></b>
                                       </g:if>
                                    </td>
                                 </tr>
                                 <g:if test="${!domainClass?.constrainedProperties[property.name]?.getAppliedConstraint('renderOptions')?.renderOptions?.labelOnly}">
                                     <tr>   
                                        <td colspan="2"><theme:property bean="${bean}" beanPathPrefix="${beanPathPrefix}" name="${property.name}" readonly="true"/></td>
                                     </tr>   
                                 </g:if>
                                 </g:else>
                               </g:if>
                               <g:else>
                               <tr>
                                 <g:if test="${g.hasErrors(bean:bean, field: property.name)}">
                                    <td align="right" style="vertical-align: top;">
                                       error
                                       <g:if test="${!(property.oneToMany && property.bidirectional)}"> <%-- Hide the label on what is usually a table for one-to-many --%>
                                        <label class="error" for="${property.name}"><bean:label bean="${bean}" property="${property}"/>:</label>
                                       </g:if>
                                    </td>
                                    <td><theme:property bean="${bean}" beanPathPrefix="${beanPathPrefix}" name="${property.name}" error="true" readonly="true"/></td>
                                 </g:if>
                                 <g:else>
                                    <td align="right" style="vertical-align: top;">
                                       <g:if test="${!(property.oneToMany && property.bidirectional)}"> <%-- Hide the label on what is usually a table for one-to-many --%>
                                          <label for="${property.name}"><bean:label bean="${bean}" property="${property}"/>:</label>
                                       </g:if>
                                    </td>
                                    <td><theme:property bean="${bean}" beanPathPrefix="${beanPathPrefix}" name="${property.name}" readonly="true"/></td>
                                 </g:else>
                               </tr>
                               </g:else>
                                </g:each>
                              </table>
                            </fieldset>
