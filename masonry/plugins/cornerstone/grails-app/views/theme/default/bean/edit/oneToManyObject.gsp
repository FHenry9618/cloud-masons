
<%@ page import=" grails.util.GrailsNameUtils" %>
<%--
  todo: fields for create, javascript for create
        delete java script
        controller logic for delete
        fields for edit javascript for edit
 --%>

<%
    def nextIndex = "0"
    if (value) nextIndex="${value.size()}"
    
    // exclude the pointer back to this side
    def exclude = property.getOtherSide().name
    def newInstance = property.referencedDomainClass?.newInstance()
    newInstance.properties[exclude] = bean
    def referencedDomainNaturalName = GrailsNameUtils.getNaturalName(property.referencedDomainClass?.shortName)
%>
<%-- 
  Display an inline creation and edit forms if the property is the
  owning side of this one to many relationship.  Example, if Author
  owns books, then when editing Author, being able to add a book
  --%>
<g:if test="${property.owningSide}"> 
  <input type='hidden' autocomplete="off" id='${property.name}NextIndex' name='${property.name}NextIndex' value="${nextIndex}"/>
  <input type='hidden' autocomplete="off" id='removeFrom${property.name}' name='removeFrom${property.name}' value=""/>
  <%-- Create dialog --%>
  <div id="dialog-${domainClass.propertyName}-create-${property.name}-form" title="Create New ${property.referencedDomainClass?.naturalName}">
    <theme:propertyGroups bean="${newInstance}" beanPathPrefix="${property.name}[NEWINSTANCE]." exclude="${exclude}"/>
  </div>

  
  <div id="modified-${domainClass.propertyName}-edit-${property.name}-form">
  </div>
  <div id="${property.name}CreateValues">
  </div>
  <%-- Generate the dialog for editing each of the properties --%>
  <g:each in="${value}" status="i" var="b">
     <div id="modified-${domainClass.propertyName}-edit-${property.name}-form-${i}">
     
     </div>
   <div id="dialog-${domainClass.propertyName}-edit-${property.name}-form-${i}" title="Edit ${referencedDomainNaturalName}">
        <theme:propertyGroups bean="${b}"  beanPathPrefix="${property.name}[${i}]." exclude="${exclude}"/>
   </div>
  </g:each>
</g:if>

    <%-- Generate the summary table for each of the items --%>
 <div id="${domainClass.propertyName}-${property.name}-table">
    <table class="compact">
      <thead>
        <tr>
      <%     summaryProperties.eachWithIndex { p, i ->
          if (i < 6) { %>
                 <th><bean:label bean="${newInstance}" property="${p}"/></th>
          <% } } %>
                 <th class="tc">Actions</th>
        </tr>  
      </thead>
      
      <tbody>
        <g:each in="${value}" status="i" var="b">
           <tr rel="${b.id}" class="${domainClass.propertyName}-edit-${property.name}-form-${i}">
                
          <%  summaryProperties.eachWithIndex { p, j ->
                 out << "<td id=\"summary-${property.name}[$i].${p.name}\">"
                 out << theme.property(bean:b, beanPathPrefix:beanPathPrefix, name:p.name, readonly:true)
                 out << "</td>"
              } %>
                <td class="tc">
                  <g:if test="${property.owningSide}"> 
                    <a  class="ico show-${domainClass.propertyName}-edit-${property.name}-form-${i}" title="edit">Edit</a>
                    | <a class="ico delete-${domainClass.propertyName}-edit-${property.name}-form-${i}" href="#" id="delete-${property.name}-${b.id}" title="delete">Delete</a>
                  </g:if>
                  <g:else>
                    <g:link action="edit" id="${b.id}" controller="${property.referencedDomainClass?.propertyName}">View</g:link> 
                  </g:else>
                </td>
           </tr>

        </g:each>
           <tr class="${property.name}NewSummaryRow" style="display: none;">
              <%  summaryProperties.eachWithIndex { p, j ->
                 out << "<td id=\"summary-${property.name}[NEWINSTANCE].${p.name}\">"
                 out << "</td>"
              } %>
                <td id="${property.name}-actions-NEWINSTANCE" class="tc">
                   <a  class="ico" title="edit">Edit</a>
                </td>
           </tr>
     
      <tbody>
      <tfoot>
      </tfoot>
      
    </table>
 </div>

<%-- Display the create form only if the property owns the relationship --%>
<g:if test="${property.owningSide}"> 
  <a class="show-${domainClass.propertyName}-create-${property.name}-form">Create New ${referencedDomainNaturalName}</a>
  <script type="text/javascript">
    $(document).ready(function() {
      setupOneToMany('${domainClass.propertyName}', '${property.name}', ${nextIndex}) 
    });
  </script>
</g:if>


