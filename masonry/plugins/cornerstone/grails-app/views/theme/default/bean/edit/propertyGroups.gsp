
<g:each in="${propertyGroups.keySet()}" var="${group}" status="i">
    <b>${group}</b>
    <theme:propertyGroup bean="${bean}" name="${group}" beanPathPrefix="${beanPathPrefix}" props="${propertyGroups[group]}" />
    <br/>
</g:each>
