

<div id="${domainClass.propertyName}-grid" class="content">
	<!-- content box 2 for tabs switching (hidden by default) -->
	<form class="plain" action="" method="post" enctype="multipart/form-data">
		<fieldset>
			<div class="grid">
				<!-- grid view -->
				<g:each in="${beans}" status="i" var="bean">
					<g:if test="${i % 2 == 0L && i > 1}">
			</div>
			</g:if>
			<g:if test="${i % 2 == 0L}">
				<div class="line">
			</g:if>
			<div class="item">
				<div class="inner">
					<div class="data">
						<h4>
							<g:link controller="${domainClass.propertyName}" action="show" id="${bean.id}">
								${bean.name}
							</g:link>
						</h4>
						<g:if test="${summaryProperties}">
							<dl class="compact">
								<g:each in="${summaryProperties}" var="p">
									<dt>
										<bean:label bean="${bean}" property="${p}" />
									</dt>
									<dd>
										${bean.properties[p.name]}
										&nbsp;
									</dd>
								</g:each>
							</dl>
						</g:if>
						<g:else>
							<p>
								${bean.description}
							</p>
						</g:else>
						<ul class="actions">
							<bean:eachAuthorizedInstanceAction bean="${bean}" actions="${actions}">
								<li>
								    [&nbsp;<bean:actionLink bean="${bean}" action="${action}" />&nbsp;]
								</li>
							</bean:eachAuthorizedInstanceAction>
						</ul>
					</div>
				</div>
			</div>
			</g:each>
</div>
</fieldset>
</form>
<div class="pagination">
	<ul>
	aaaaaaaaaaaaaaaaaaaaaaaaaaa
		<g:paginate total="${total}" offset="${offset}" max="${max}" params="${beanParams}" />
	</ul>
</div>
</div>
<!-- end grid list -->
