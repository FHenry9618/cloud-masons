<g:each in="${properties}" var="${property}" status="i">
    <g:if test="${!domainClass?.constrainedProperties[property.name]?.getAppliedConstraint('renderOptions')?.renderOptions?.unprintable}">
        <g:if test="${property.association}">
            <li class="association ${property.name}">
                <theme:printProperty bean="${bean}" beanPathPrefix="${beanPathPrefix}" property="${property}"/>
            </li>
        </g:if>
        <g:else>
            <li>
                <g:set var="label_id">${domainClass.name}:${bean.id}_${property.name}</g:set>
                <label for="${label_id}"><bean:label bean="${bean}" property="${property}"/>:</label>
                <span id="${label_id}" class="property">
                    <theme:property bean="${bean}"
                                    readonly="true"
                                    beanPathPrefix="${beanPathPrefix}"
                                    name="${property.name}"
                                    style="print">
                    </theme:property>
                </span>
            </li>
        </g:else>
    </g:if>
</g:each>
