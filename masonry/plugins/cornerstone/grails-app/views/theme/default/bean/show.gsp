        <theme:message messageObj="${flash}"/>
        <g:hasErrors bean="${bean}">
            <div class="errors">
                <g:renderErrors bean="${bean}" as="list" />
            </div>
        </g:hasErrors>

<%
  if (!label) {
     label = "${domainNaturalName}: ${bean.name}"
  }
%>
            
            <g:if test="${path}">
                <div class="path">
                <theme:renderPath name="${domainNaturalName}" path="${path}"/>
                </div>
                <br/>
            </g:if>
            <g:if test="${printPDF}">
                <security:renderIfAuthorized domainClass="${beanInstance.getClass()}" action='print' bean="${beanInstance}">
                <div class='print-button'>
                    <g:pdfLink class='button print-button' pdfController="${beanControllerName}" pdfAction="print" pdfId="${beanInstance?.id}" filename="${grails.util.GrailsNameUtils.getPropertyName(beanInstance.name?.replaceAll('[^\\p{L}\\p{Nd}]+', ''))}">PDF Version</g:pdfLink>
                </div>
                </security:renderIfAuthorized>
            </g:if>
                <g:each in="${propertyGroups.keySet()}" status="idx" var="${group}">

                <div class="box box-50 altbox">
                    <div class="boxin">
                        <div class="header">
                            <h3>${group}</h3>
                            <theme:renderOption bean="${bean}" name="${group}" props="${propertyGroups[group]}"/>
                        </div>

                        <div class="content">
                             <theme:propertyGroup readonly="true" bean="${bean}" name="${group}" props="${propertyGroups[group]}" />
                        </div>

                    </div>
                </div>
                           
                </g:each>
 

