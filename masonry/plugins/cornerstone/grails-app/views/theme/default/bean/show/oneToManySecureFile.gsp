<g:if test="${value}">
    <table class="compact">
      <thead>
        <tr>
          <th>File</th>
          <th>Description</th>
          <th>Actions</th>
        </tr>  
      </thead>
    
      <tbody>
        <g:each in="${value}" status="i" var="f">
          <tr>
            <td><g:link controller="secureFile" action="download" id="${f.id}" params="[beanClass: property.referencedDomainClass.name]">${f.name}</g:link>
            </td>
            <td><g:link controller="secureFile" action="download" id="${f.id}" params="[beanClass: property.referencedDomainClass.name]">${f.description}</g:link>
            </td>
            <td><g:link controller="secureFile" action="download" id="${f.id}" params="[beanClass: property.referencedDomainClass.name]">[download]</g:link>
            </td>
          </tr>
         </g:each>
       </tbody>
    </table>
</g:if>
<g:else>
  <i>No files are attached to this ${domainClass.naturalName}</i>
</g:else>
