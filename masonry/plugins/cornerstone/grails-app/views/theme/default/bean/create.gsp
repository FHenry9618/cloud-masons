
<g:if test="${flash.message}">
	<div class="msg msg-ok">
		${flash.message}
	</div>
</g:if>
<g:if test="${path}">
	<div class="path">
		<theme:renderPath name="${domainNaturalName}" path="${path}" />
	</div>
	<br />
</g:if>
<g:hasErrors bean="${bean}">
	<div class="msg msg-error">
		<g:renderErrors bean="${bean}" as="list" />
	</div>
</g:hasErrors>

<div class="box box-75 altbox">
	<div class="boxin">
		<div class="header">
			<h3>
				Add New
				${domainNaturalName}
			</h3>
		</div>
		<g:form class="createForm" controller="${beanControllerName}"
			action="save" method="post"
			${multiPart ? ' enctype="multipart/form-data"' : ''}>
			<g:hiddenField name="id" value="${bean.id}" />
			<g:hiddenField name="version" value="${bean.version}" />
			<g:hiddenField name="associated" value="${beanAssociated}" />
			<g:hiddenField name="parentBeanId" value="${parentBean?.id}" />
			<div id="accordion">
				<g:each in="${propertyGroups.keySet()}" var="${group}">
					<h3 class="ui-accordion-header ui-helper-reset ui-corner-top">
						<a href="#"><strong>
								${group}
						</strong></a>
					</h3>
					<div>
						<theme:propertyGroup bean="${bean}" name="${group}"
							props="${propertyGroups[group]}" />
					</div>
				</g:each>
			</div>


		</g:form>

	</div>
</div>


<div class="box box-25 altbox">
 <div class="boxin">
  <div class="header">
   <h3>Summary</h3>
  </div>
  <fieldset>
   Please make your changes and then click <b>Create</b>. <br />
   <div class="sep">
    <a class="button altbutton createButton">Create</a> 
    <a class="button altbutton cancelButton">Cancel</a>
   </div>
   <div class="sep">&nbsp;</div>
   <div class="sep"></div>
  </fieldset>
 </div>
</div>

<g:if test="${previousRequest}">
	<g:set var="windowLoc" value="${createLink(action: previousRequest.action, controller: previousRequest.controller, id:params.id)}"/>
</g:if>
<g:else>
    <g:set var="windowLoc" value="${createLink(action: beanActionName ?: 'list', controller: beanControllerName, id:params.id)}"/>
</g:else> 

<script type="text/javascript">
     $(document).ready(function() {
          $('#accordion').accordion({autoHeight: false, collapsible: true});

         $('.createButton')
            .button()
            .click(function() {
                $('.createForm').submit();
            });
         $('.cancelButton')
            .button()
            .click(function() {
                // delete the plan of service
            	if(confirm('Are you sure you wish to cancel?')) {
                 window.location = "${windowLoc}"
            	}
            });
         $('.money').priceFormat({
            prefix: '$ '
         }); 
    });
  </script>