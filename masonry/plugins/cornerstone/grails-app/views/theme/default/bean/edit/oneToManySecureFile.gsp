<%-- Generate the dialog for editing each of the properties --%>
<g:each in="${value}" status="i" var="b">
     <div id="modified-${domainClass.propertyName}-edit-${property.name}-form-${i}">
     
     </div>
   <div id="dialog-${domainClass.propertyName}-edit-${property.name}-form-${i}" title="Edit ${property.referencedDomainClass.naturalName}">
        <theme:propertyGroup bean="${b}" props="${['name', 'description']}"  beanPathPrefix="${property.name}[${i}]." />
   </div>
</g:each>

<table class="compact">
  <tbody>
    <g:each in="${value}" status="i" var="f">
      <tr class="${domainClass.propertyName}-edit-${property.name}-form-${i}">
        <td><g:link controller="secureFile" action="download" id="${f.id}" params="[beanClass: property.referencedDomainClass.name]">${f.name}</g:link>
        </td>
        <td>
           <a class="ico show-${domainClass.propertyName}-edit-${property.name}-form-${i}" title="edit"><img src="${resource(dir:'css/boxie/img/led-ico',file:'pencil.png')}" alt="edit" />Edit</a> |
           <a class="ico delete-${domainClass.propertyName}-edit-${property.name}-form-${i}" href="#" id="delete-${property.name}-${i}" title="delete"><img src="${resource(dir:'css/boxie/img/led-ico',file:'delete.png')}" alt="delete" />Delete</a>
        </td>
      </tr>
     </g:each>
   </tbody>
</table>

<br/>
<p>New Files:
<p><i>note: ${domainClass.naturalName} must be saved before new files are available for download.</i>
<script type="text/javascript">
$(document).ready(function() {
    $("#uploadify").uploadify({
        'uploader'       : "${resource(dir: 'upload', file:'uploadify.swf', plugin:'cornerstone', absolute: true)}",
        'script'         : "${createLink(controller: 'secureFile', action:'upload', absolute: true)}",
        'cancelImg'      : "${resource(dir: 'upload', file:'cancel.png', plugin:'cornerstone', absolute: true)}",
        'folder'         : 'uploads',
        'queueID'        : 'fileQueue',
        'buttonText'     : 'Upload Files',
        'auto'           : true,
        'multi'          : true,
        'scriptData'     : {'jsessionid': '${g.cookie(name: 'JSESSIONID')}'},
        'onComplete'     : function(event, ID, fileObj, response, data) {
                               responseData = jQuery.parseJSON(response)
                               var offsetStr = $('#numberOfSecureFiles').val()
                               var offset = parseInt(offsetStr)
                               var secureFile = '<p><b>'+responseData.name+'</b> pending save </p>';

                               for (var key in responseData) {
                                  secureFile = secureFile + '<input type="hidden" name="${property.name}['+offset+'].' + key + '" value="'+responseData[key]+'" />';
                               }
                               $('#secureFiles').append(secureFile);
                               $('#numberOfSecureFiles').val(offset + 1 )
                               return true;
                            }
    });
    setupOneToMany('${domainClass.propertyName}', '${property.name}', ${value ? value.size() : 0});
});


</script>

<g:hiddenField id="numberOfSecureFiles" name="numberOfSecureFiles" value="${value ? value.size() : 0}" />
<input type='hidden' autocomplete="off" id='removeFrom${property.name}' name='removeFrom${property.name}' />

<div id="secureFiles">

</div>
<div id="fileQueue"></div>
<input type="file" class="uploadify" name="uploadify" id="uploadify" />
<p><a href="javascript:jQuery('#uploadify').uploadifyClearQueue()">Cancel All Uploads</a></p>

    

