
<!-- Used by logic in the update processing to not accidently remove a field -->
<%
  // Honor the allowedValues constraint
  def values = []
  def allowedValues = cp?.getAppliedConstraint('allowedValues')
  if (allowedValues) {
      values = allowedValues.getValues(bean)
  } else {
      values = field.referencedDomainClass.clazz.list()
  }
  values = values?.sort { it.name }
 %>
<g:hiddenField name="${beanPath}Present" value="true" />
<g:select name="${beanPath}" from="${values}" multiple="yes" optionKey="id" optionValue="name" size="5" value="${value}" />