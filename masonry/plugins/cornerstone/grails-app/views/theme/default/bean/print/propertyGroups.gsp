<g:if test="${bean}">
    <g:each in="${propertyGroups}" var="entry">
        <g:if test="${entry && entry.key && entry.value}">

            <g:set var="propertyName" value="${entry.value.name[0]}"/>
            <g:set var="constrainedProperty" value="${domainClass.constrainedProperties[propertyName]}"/>
            <g:set var="renderOptions" value="${constrainedProperty?.getAppliedConstraint('renderOptions')?.renderOptions}"/>
            <g:set var="printable" value="${renderOptions?.unprintable == null}"/>

            <g:if test="${printable}">
                <div class="group">
                    <h2><%=entry.key%></h2>
                    <ul>
                        <theme:propertyGroup
                                bean="${bean}"
                                name="${entry.key}"
                                beanPathPrefix="${beanPathPrefix}"
                                props="${entry.value}"
                                style="print"/>
                    </ul>
                </div>
            </g:if>
        </g:if>
    </g:each>
</g:if>