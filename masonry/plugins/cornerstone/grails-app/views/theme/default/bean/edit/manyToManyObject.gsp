<!-- Used by logic in the update processing to not accidently remove a field -->
<g:hiddenField name="${beanPath}Present" value="true" />
<g:select name="${beanPath}" from="${property.referencedDomainClass.clazz.list()}" multiple="yes" optionKey="id" optionValue="name" size="5" value="${value}" />