
<g:if test="${value}">
    <%-- Generate the summary table the items --%>
    <table class="compact">
      <thead>
        <tr>
       <% summaryProperties.eachWithIndex { p, i ->
          if (i < 6) { %>
                 <th><bean:label bean="${property.referencedDomainClass?.newInstance()}" property="${p}"/></th>
       <% } } %>
              <th>Actions</th>
        </tr>  
      </thead>
      
      <tbody>
        <g:each in="${value}" status="i" var="bean">
           <tr>
             <%  
              summaryProperties.eachWithIndex { p, j ->
                 out << "<td>"
                 out << theme.property(bean:bean, beanPathPrefix:beanPathPrefix, name:p.name, readonly:true)
                 out << "</td>"
             
              } %>
                <td class="tc">
                  <a  class="ico show-${domainClass.propertyName}-edit-${property.name}-form-${i}" title="view item">View</a>
                </td>
           </tr>
        </g:each>
     
      <tbody>
      <tfoot>
      </tfoot>
      
    </table>
 
  <g:each in="${value}" status="i" var="b">
   <div id="dialog-${domainClass.propertyName}-edit-${property.name}-form-${i}" title="Show  ${property.referencedDomainClass.naturalName}">
        <theme:propertyGroups bean="${b}" readonly="${true}"/>
   </div>
  </g:each>

  <script type="text/javascript">
    $(document).ready(function() {
      setupOneToMany('${domainClass.propertyName}', '${property.name}', ${value ? value.size() : 0}) 
    });
  </script>
</g:if>
<g:else>
  <i>No ${property.name} for this ${domainClass.naturalName}</i>
</g:else>


