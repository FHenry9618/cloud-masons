
<g:set var="truncatedSummaryProperties" value="${summaryProperties[0..Math.min(summaryProperties.size()-1,5)]}" />
<g:if test="${flash.message}">
 <div class="msg msg-ok">
  ${flash.message}
 </div>
</g:if>
<g:if test="${path}">
 <div class="path">
  <theme:renderPath name="${domainNaturalName}" path="${path}" />
 </div>
 <br />
</g:if>
<div id="${domainClass.propertyName}" class="box box-100 altbox">
 <!-- box full-width -->
 <div class="boxin">
  <div class="header">
   <h3>
    <g:message code="default.list.label" args="[domainNaturalName]" default="${domainNaturalName}" />
   </h3>
   <bean:eachAuthorizedStaticAction beanControllerName="${beanControllerName}" bean="${domainClass.clazz}"
    actions="${actions}" params="${params}">
    <g:if test="${action.action != 'list'}">
     <a class="button altbutton"
      href="${g.createLink(controller: beanControllerName, action: action.action, params: action?.params)}"> ${action.label}
     </a>
    </g:if>
   </bean:eachAuthorizedStaticAction>
   <a class="button altbutton showFilterPane" href="#">Filter</a>
   <g:if test="${listStyle == 'both'}">
    <ul>
     <li><a rel="${domainClass.propertyName}-table" href="#" class="active">list view</a></li>
     <!-- insert ID of content related to this tab into the rel attribute of this tab -->
     <li><a rel="${domainClass.propertyName}-grid" href="#">grid view</a></li>
     <!-- insert ID of content related to this tab into the rel attribute of this tab -->
    </ul>
   </g:if>
  </div>
  <g:if test="${listStyle == 'table' || listStyle == 'both'}">
   <div id="${domainClass.propertyName}-table" class="content">
    <!-- content box 1 for tab switching -->
    <form class="plain" action="" method="post" enctype="multipart/form-data">
     <fieldset>
      <table cellspacing="0">
       <thead>
        <!-- universal table heading -->
        <tr>
         <g:each in="${truncatedSummaryProperties}" status="i" var="p">
          <bean:column beanControllerName="${beanControllerName}" action="${beanActionName}" params="${beanParams}"
           property="${p}"
           title="${message(code: domainClass.propertyName +'.' + p.name + '.label', default: p.naturalName)}" />
         </g:each>
         <td class="tc">Actions</td>
        </tr>
       </thead>
       <tfoot>
        <!-- table foot - what to do with selected items -->
       </tfoot>
       <tbody>
        <g:if test="${beans}">
         <g:each in="${beans}" status="i" var="bean">
          <tr class="${i == 0 ? 'first' : ''}">
           <g:each in="${truncatedSummaryProperties}" var="p" status="j">
            <g:set var="cp" value="${ domainClass.constrainedProperties[p.name]}" />
            <g:if test="${p.type == Boolean.class || p.type == boolean.class}">
             <td class="tc"><g:formatBoolean boolean="${bean.properties[p.name]}" /></td>
            </g:if>
            <g:elseif
             test="${p.type == Date.class || p.type == java.sql.Date.class || p.type == java.sql.Time.class || p.type == Calendar.class}">
             <td class="tc"><g:formatDate date="${bean.properties[p.name]}" /></td>
            </g:elseif>
            <g:elseif test="${p.manyToOne || p.oneToOne}">
             <td class="tc">
              ${bean.properties[p.name]?.name}
             </td>
            </g:elseif>
            <g:else>
             <td class="tc"><g:fieldValue bean="${bean}" field="${p.name ?: ''}" /></td>
            </g:else>
           </g:each>
           <td class="tc">
            <ul class="actions">
             <bean:eachAuthorizedInstanceAction beanControllerName="${beanControllerName}" bean="${bean}"
              actions="${actions}" domainClassName="${domainClass.propertyName}" params="${params}">
              <li>[ <bean:actionLink beanControllerName="${beanControllerName}" bean="${bean}" action="${action}" />
               ]
              </li>
             </bean:eachAuthorizedInstanceAction>
            </ul>
           </td>
          </tr>
         </g:each>
        </g:if>
        <g:else>
         <tr>
          <td colspan="${summaryProperties.size() + 1}">
           <%-- plus one for action column --%> No Values
          </td>
        </g:else>
       </tbody>
      </table>
     </fieldset>
    </form>
    <div class="pagination">
     <ul>
      <theme:paginate total="${total}" offset="${offset}" max="${max}" />
      <li>&nbsp;</li>
      <li class="bottomRightLink"><a href="#" reportFormat="PDF">PDF</a></li>
      <li class="bottomRightLink"><a href="#" reportFormat="XLS">XLS</a></li>
      <li class="bottomRightLink">Download Report:</li>
     </ul>
    </div>
   </div>
   <!-- end table list -->
  </g:if>
  <g:if test="${listStyle == 'grid' || listStyle == 'both'}">
   <div id="${domainClass.propertyName}-grid" class="content" style="${(listStyle == 'both') ?  'display: none;' : ''}">
    <!-- content box 2 for tabs switching (hidden by default) -->
    <form class="plain" action="" method="post" enctype="multipart/form-data">
     <fieldset>
      <div class="grid">
       <!-- grid view -->
       <g:each in="${beans}" status="i" var="bean">
        <g:if test="${i % 2 == 0L && i > 1}">
      </div>
  </g:if>
  <g:if test="${i % 2 == 0L}">
   <div class="line">
  </g:if>
  <div class="item">
   <div class="inner">
    <div class="data">
     <h4>
      <a href="#"> ${bean.name}
      </a>
     </h4>
     <g:if test="${summaryProperties}">
      <dl class="compact">
       <g:each in="${summaryProperties}" var="p">
        <dt>
         <bean:label bean="${bean}" property="${p}" />
        </dt>
        <dd>
         ${bean.properties[p.name]}
         &nbsp;
        </dd>
       </g:each>
      </dl>
     </g:if>
     <g:else>
      <p>
       ${bean.description}
      </p>
     </g:else>
     <ul class="actions">
      <bean:eachAuthorizedInstanceAction beanControllerName="${beanControllerName}" bean="${bean}" actions="${actions}">
       <li>[ <bean:actionLink beanControllerName="${beanControllerName}" bean="${bean}" action="${action}" /> ]
       </li>
      </bean:eachAuthorizedInstanceAction>
     </ul>
    </div>
   </div>
  </div>
  </g:each>
 </div>
 </fieldset>
 </form>

 <div class="pagination">
  <ul>
   <theme:paginate total="${total}" offset="${offset}" max="${max}" />
   <li>&nbsp;</li>
   <li class="bottomRightLink"><a href="#" reportFormat="PDF">PDF</a></li>
   <li class="bottomRightLink"><a href="#" reportFormat="XLS">XLS</a></li>
   <li class="bottomRightLink">Download Report:</li>
  </ul>
 </div>


</div>
<!-- end grid list -->
</g:if>
</div>
</div>

<g:form name="paginateForm" method="POST" action="${beanActionName}" controller="${beanControllerName}">
 <g:hiddenField name="listStyle" value="${listStyle}" />
 <g:if test="${params.id}">
  <g:hiddenField name="id" value="${params.id}"/>
 </g:if>
 <theme:paginateFormParameters total="${total}" offset="${offset}" max="${max}" params="${beanParams}" />
</g:form>

<g:form name="reportForm" method="POST" action="listReport" controller="beanReport">
 <g:hiddenField name="listStyle" value="${listStyle}" />
 <g:if test="${params.id}">
  <g:hiddenField name="id" value="${params.id}"/>
 </g:if>
 <g:hiddenField name="reportFormat" value="PDF" />
 <g:hiddenField name="entity" value="${domainClass.fullName}"/>
 <theme:paginateFormParameters total="${total}" offset="${offset}" max="${max}" params="${beanParams}" />
</g:form>

<beans:filterPane domainBean="${domainClass}" params="${beanParams}" />
<script type="text/javascript">
$('a[offset]').one('click', function() {
    // find the active link to determine the list style
    var gridOrTable = "grid";
    if ($('a[class=active]').attr('rel').indexOf("-table") != -1) {
    	gridOrTable = "table"
    }
    $('#listStyle').val(gridOrTable)
    // find the input w/
    var offset = $(this).attr('offset');
    var max = $(this).attr('max');
    $('#max').val(max)
    $('#offset').val(offset)
    $('form[name=paginateForm]').submit();
});

$('a[reportFormat]').one('click', function() {
  var format = $(this).attr('reportFormat');
  $('#reportFormat').val(format)
  $('form[name=reportForm]').submit();
 });

$(document).ready(function() {
    addListTabs("${domainClass.propertyName}");
		$('#filterPane').dialog({
			autoOpen : false,
			height : 400,
			width : 650,
			modal : true,
			buttons : {
				'Cancel' : function() {
					$(this).dialog('close');
				},
				'Filter' : function() {
					$('#filterForm').submit();
					$(this).dialog('close');
				}

			},
			close : function() {
			}
		});
		$('.showFilterPane').button().click(function() {
			$('#filterPane').dialog('open');
		});
	});
</script>
