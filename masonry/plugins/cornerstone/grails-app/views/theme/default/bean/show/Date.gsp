<%
   def format = cp?.format ?: message(code: 'default.date.format', default: 'MM/dd/yyyy')
%>
<g:if test="${value != null}">
${value?.format(format)}
</g:if>
<g:else>
<i>no value</i>
</g:else>
