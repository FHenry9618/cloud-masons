<g:applyLayout name="global">
    <head>
      <title><g:layoutTitle/></title>
      <g:layoutHead />
    </head>
    <body>
        <div id="header">
            <div class="inner-container clearfix">
                <h1 id="logo">
                    <a class="home" href="#" title="">
                        <g:message code="application.title"/>
                        <%--<span class="ir"></span> --%>
                    </a><br />
                </h1>
                <g:isLoggedIn>
                    <div id="userbox" class="userbox-right" >
                    <a id="logout" title="Click to logout" href="${createLink(controller:'logout')}">
                    <span class="ir"></span>
                    <span class="ar">log out</span>
                    </a>
                    </div>
                    
                    <div id="userbox" class="userbox-left">
                <g:link controller="mason" action="settings" params="${[cloudId: currentCloud?.id]}" id="${loggedInUserInfo(field:'id')}">
                        <strong><g:loggedInUserInfo field="name"/></strong>
                        <ul class="clearfix">
                            <li>settings</li>
                        </ul>
                        </g:link>
                    </div>

                </g:isLoggedIn>
                  <g:isNotLoggedIn>
                  <g:link controller="login">
                    <div class="inner" style="padding:23px 140px 25px 55px;">
                        <strong>Cloud&nbsp;Login</strong>
                    </div>
                    <a id="logout" title="Click to login" href="${createLink(controller:'login')}">log in<span class="ir"></span></a>
                  </g:link>
                  </g:isNotLoggedIn>
                </div><!-- #userbox -->
                
            </div><!-- .inner-container -->
        </div><!-- #header -->
          <div id="nav">
            <div class="inner-container clearfix">
                <div id="h-wrap">
                    <div class="inner">
                        <h2>
                          <nav:currentItem group="*" var="item" altTitle="${g.layoutTitle()}">
                             <span class="h-ico ico-${item.icon}"><span>${item.title}</span></span>
                          </nav:currentItem>
                          <span class="h-arrow"></span>
                        </h2>
                        <ul class="clearfix">

                            <nav:eachItem group="tabs" var="item">
                               <li><a class="h-ico ico-${item.icon}" href="${item.link}"><span>${item.title}</span></a></li>
                            </nav:eachItem>
                            <nav:eachItem group="admintabs" var="item">
                               <li><a class="h-ico ico-${item.icon}" href="${item.link}"><span>${item.title}</span></a></li>
                            </nav:eachItem>

                        </ul>
                    </div>
                </div><!-- #h-wrap -->
                <g:form controller="dashboard" action="search" method="get"><!-- Search form -->
                    <fieldset>
                        <label class="a-hidden" for="q">Search query:</label>
                        <input id="q" class="text fl search" type="text" name="q" size="20" value="search..." style="height: 24px"/>
                        <input class="hand fr" type="image" src="${resource(dir:'css/boxie/img',file:'search-button.png')}" alt="Search" />
                    </fieldset>
                </g:form>
            </div><!-- .inner-container -->
          </div><!-- #nav -->

        
        <div id="container">
            <div class="inner-container">
               <g:layoutBody />
            
               <div id="footer"><!-- footer, maybe you don't need it -->
                    <p>Created and maintained by <a href="#">Cloudmasons</a></p>
               </div>
            
            </div><!-- .inner-container -->
        </div><!-- #container -->   
         
<script type="text/javascript">

$(document).ready(function() {
  $('.search').focus(function()
  {  
    //alert($(this).val());
    if($(this).val()=="search...")
    {
        $(this).val("");
    }
  });
});
</script>
    </body>
</g:applyLayout>