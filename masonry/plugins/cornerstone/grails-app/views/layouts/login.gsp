<g:applyLayout name="global">
<html>
    <head>
      <title><g:layoutTitle/></title>
      <g:layoutHead />
      
      <style type="text/css">
.box .content table tr.even th {background: #ffffff; vertical-align: top;}
.box .content table tr.even td {background: #ffffff;}

</style>
    </head>

    <body id="login">

        <div class="box box-75 altbox">
            <div id="login-panel" class="boxin">
                <div class="header">
                    <h3><g:message code="application.title"/></h3>
                    <ul>
                        <li><a id="login-form-tab" rel="login-form" href="#" class="active">Login</a></li><!-- .active for active tab -->
                        <li><a id="reset-form-tab" rel="reset-form" href="#">Reset Password</a></li>
                        <li><a id="register-form-tab" rel="register-form" href="#">Register</a></li>

                    </ul>
                </div>
                <div id="login-form" class="content">                
                    <table style="width: 85%; ">
                      <tr>
                        <td style="background: #FFFFFF; vertical-align: middle;">
                          <img src="${resource(plugin: 'cornerstone', dir:'images/login',file:'login.png')}" ></img>
                        </td>
                        <td style="background: #FFFFFF; vertical-align: top;">
        
                            <form class="table" action="${postUrl}" method="post" id='loginForm'><!-- Default forms (table layout) -->

                                <div class="inner-form">
        
                                  <g:pageProperty name="page.loginForm"/>
                                </div>
                            </form>
                        </td>
                      </tr>
                    </table>

                </div>
                <div id="reset-form" class="content"> 
                    <table style="width: 85%; ">
                      <tr>
                        <td style="background: #FFFFFF; vertical-align: middle;">
                          <img src="${resource(plugin: 'cornerstone', dir:'images/login',file:'password.png')}" ></img>
                        </td>
                        <td style="background: #FFFFFF; vertical-align: top;">               
                            <form class="table" controller="login" action="startPasswordReset"  method="post" id='loginForm'><!-- Default forms (table layout) -->
                                <div class="inner-form">
                                 <g:pageProperty name="page.resetForm"/>
                                </div>
                            </form>
                        </td>
                      </tr>
                    </table>
                </div>

                <div id="register-form" class="content">                
                    <table style="width: 85%; ">
                      <tr>
                        <td style="background: #FFFFFF; vertical-align: middle;">
                          <img src="${resource(plugin: 'cornerstone', dir:'images/login',file:'register.png')}" ></img>
                        </td>
                        <td style="background: #FFFFFF; vertical-align: top;">
                          <form class="table" controller="login" action="startRegistration" method="post" id='registerForm'><!-- Default forms (table layout) -->
                            <div class="inner-form">
                               <g:pageProperty name="page.registerForm"/>
                            </div>
                          </form>
                        </td>
                      </tr>
                    </table>
                </div>

            </div>
        </div>
 <script type="text/javascript">
      var focusInput = function() {
            document.getElementById("j_username").focus();
      }
      $(document).ready(function() {
        <g:if test="${panel == 'register'}">
            $('#reset-form').hide(); // hide content related to inactive tab by default
            $('#login-form').hide(); // hide content related to inactive tab by default
            showTab('register-form');
        </g:if>
        <g:else>
            $('#reset-form').hide(); // hide content related to inactive tab by default
            $('#register-form').hide(); // hide content related to inactive tab by default
        </g:else>
        $('#login-panel .header ul a').click(function(){
            //$('#login-panel .header ul a').removeClass('active');
            //$(this).addClass('active'); // make clicked tab active
            //$('#login-panel .content').hide(); // hide all content
            //$('#login-panel').find('#' + $(this).attr('rel')).show(); // and show content related to clicked tab
            showTab($(this).attr('rel'));
            return false;
        });
        $('#show-login-form').click( function() {
            showTab('login-form');
        });
        $('#show-reset-form').click( function() {
            showTab('reset-form');
        });
        $('#show-register-form').click( function() {
            showTab('register-form');
        });
        $('#show-register-form2').click( function() {
            showTab('register-form');
        });

     });
     
    function showTab(tabName) {
            $('#login-panel .header ul a').removeClass('active');
            $('#'+tabName+'-tab').addClass('active'); // make clicked tab active
            $('#login-panel .content').hide(); // hide all content
            $('#login-panel').find('#' + tabName).show(); // and show content related to clicked tab
    }     
 </script>
    </body>
</html>
</g:applyLayout>