<!DOCTYPE #html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
    <head>
    
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta http-equiv="content-style-type" content="text/css" />
        <meta http-equiv="content-script-type" content="text/javascript" />
        <meta http-equiv="cache-control" content="no-cache" />
        <meta http-equiv="pragma" content="no-cache" />
        <meta http-equiv="expires" Content="0" /> 
        <meta http-equiv="X-UA-Compatible" content="chrome=1" />
                
        <title><g:layoutTitle default="Application" /></title>
        
        <%
           def themeCss = loggedInUserInfo(field: "theme") ?: 'black'
           themeCss += '.css'
         %>
        <link rel="stylesheet" type="text/css" href="${resource(plugin: 'cornerstone', dir:'css/smoothness',file:'jquery-ui-1.8.20.custom.css')}"  />
        <link rel="stylesheet" type="text/css" href="${resource(plugin: 'cornerstone', dir:'css/boxie',file:themeCss)}" media="screen, projection, tv" /><!-- Change name of the stylesheet to change colors (blue/red/black/green/brown/orange/purple) -->
        <link rel="stylesheet" type="text/css" href="${resource(plugin: 'cornerstone', dir:'css/boxie',file:'overrides.css')}" media="screen, projection, tv" /><!-- Change name of the stylesheet to change colors (blue/red/black/green/brown/orange/purple) -->
        

<style type="text/css">
    #ui-datepicker-div
    {
        z-index: 9999999;
    }

</style>
        <!--[if lte IE 7.0]><link rel="stylesheet" type="text/css" href="${resource(dir:'css/boxie',file:'ie.css', plugin: 'cornerstone')}" media="screen, projection, tv" /><![endif]-->
        <!--[if IE 8.0]>
            <style type="text/css">
                form.fields fieldset {margin-top: -10px;}
            </style>
        <![endif]-->
        
        <script type="text/javascript" src="${resource(dir:'js',file:'jquery-1.7.2.min.js', plugin: 'cornerstone')}"></script>
        <script type="text/javascript" src="${resource(dir:'js',file:'jquery-ui-1.8.20.custom.min.js', plugin: 'cornerstone')}"></script>
        <script type="text/javascript" src="${resource(dir:'js',file:'jquery.form.js', plugin: 'cornerstone')}"></script>
        <script type="text/javascript" src="${resource(dir:'js',file:'jquery.cookie.min.js', plugin: 'cornerstone')}"></script>
        <script type="text/javascript" src="${resource(dir:'js',file:'jquery.price_format.1.3.js', plugin: 'cornerstone')}"></script>
        <script type="text/javascript" src="${resource(dir:'js',file:'jquery.ui.timepicker.js', plugin: 'cornerstone')}"></script>
        <script type="text/javascript" src="${resource(dir:'js',file:'cornerstone.js', plugin: 'cornerstone')}"></script>

        <%-- Include some tools only if the user is logged in. 
             dtg- did this so I didn't have to add this plugin resource to
             the list of unauthenticated urls. Maybe something a little more elegant
             can be done in the plugin--%>
        <g:isLoggedIn>
            <link  rel="stylesheet" type="text/css" href="${resource(dir: 'upload', file:'uploadify.css', plugin:'cornerstone')}"/>

            <script type="text/javascript" src="${resource(dir:'upload',file:'swfobject.js', plugin:'cornerstone')}"></script>
            <script type="text/javascript" src="${resource(dir:'upload',file:'jquery.uploadify.v2.1.0.js', plugin:'cornerstone')}"></script>
        
        </g:isLoggedIn>

        <!-- Adding support for transparent PNGs in IE6: -->
        <!--[if lte IE 6]>
            <script type="text/javascript" src="${resource(dir:'js',file:'ddpng.js', plugin: 'cornerstone')}"></script>
            <script type="text/javascript">
                DD_belatedPNG.fix('#nav #h-wrap .h-ico');
                DD_belatedPNG.fix('.ico img');
                DD_belatedPNG.fix('.msg p');
                DD_belatedPNG.fix('table.calendar thead th.month a img');
                DD_belatedPNG.fix('table.calendar tbody img');
                DD_belatedPNG.fix('h3 img');    
            </script>
        <![endif]-->
        <script type="text/javascript">

            
            $(document).ready(function() {

                    $('#j_username').focus();

                // Search input text handling on focus
                    var $searchq = $("#search-q").attr("value");
                    $('#search-q.text').css('color', '#999');
                    $('#search-q').focus(function(){
                        if ( $(this).attr('value') == $searchq) {
                            $(this).css('color', '#555');
                            $(this).attr('value', '');
                        }
                    });
                    $('#search-q').blur(function(){
                        if ( $(this).attr('value') == '' ) {
                            $(this).attr('value', $searchq);
                            $(this).css('color', '#999');
                        }
                    });
                // Switch categories
                    $('#h-wrap').hover(function(){
                            $(this).toggleClass('active');
                            $("#h-wrap ul").css('display', 'block');
                        }, function(){
                            $(this).toggleClass('active');
                            $("#h-wrap ul").css('display', 'none');
                    });
                    $('.sub-h-wrap').hover(function(){
                            $(this).toggleClass('active');
                            $(".sub-h-wrap ul").css('display', 'block');
                        }, function(){
                            $(this).toggleClass('active');
                            $(".sub-h-wrap ul").css('display', 'none');
                    });

                // Handling with tables (adding first and last classes for borders and adding alternate bgs)
                    $('tbody tr:even').addClass('even');
                    $('table.grid tbody tr:last-child').addClass('last');
                    $('tr th:first-child, tr td:first-child').addClass('first');
                    $('tr th:last-child, tr td:last-child').addClass('last');
                    $('form.fields fieldset:last-child').addClass('last');
                // Handling with lists (alternate bgs)
                    $('ul.simple li:even').addClass('even');
                // Handling with grid views (adding first and last classes for borders and adding alternate bgs)
                    $('.grid .line:even').addClass('even');
                    $('.grid .line:first-child').addClass('firstline');
                    $('.grid .line:last-child').addClass('lastline');
                // Tabs switching
                    $('#box1 .content#box1-grid').hide(); // hide content related to inactive tab by default
                    $('#box1 .header ul a').click(function(){
                        $('#box1 .header ul a').removeClass('active');
                        $(this).addClass('active'); // make clicked tab active
                        $('#box1 .content').hide(); // hide all content
                        $('#box1').find('#' + $(this).attr('rel')).show(); // and show content related to clicked tab
                        return false;
                    });
                // Date pickers
                $('input.date').datepicker();

                // Time pickers
                $('input.time').timepicker({
                    showPeriod: true,
                    showLeadingZero: true
                });

                
            });
        </script>
        <g:layoutHead />
    </head>
    <body id="${pageProperty(name:'body.id')}">
      <g:layoutBody />
      
    </body>
</html>