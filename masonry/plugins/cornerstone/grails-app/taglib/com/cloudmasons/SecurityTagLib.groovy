package com.cloudmasons;

class SecurityTagLib {
    static namespace = "security"
  
    def securityService
    
    def renderIfAuthorized = {attrs, body  ->
        def domainClass = attrs.domainClass
        def action = attrs.action
        def bean = attrs.bean
        
        if (securityService.isAuthorized(domainClass,action,bean)) {
            out << body()
        }
    }
    

}