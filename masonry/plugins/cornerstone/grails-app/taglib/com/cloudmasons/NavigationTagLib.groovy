package com.cloudmasons;

import org.codehaus.groovy.grails.commons.GrailsClassUtils
import org.codehaus.groovy.grails.commons.ServiceArtefactHandler
import org.codehaus.groovy.grails.plugins.springsecurity.AuthorizeTools

class NavigationTagLib {
    static namespace = "nav"
  
    def navigationService

    def grailsApplication
    

    
    def ifHasItems = { attrs, body -> 
        def categ = attrs.group ?: '*'
        def items = navigationService.byGroup[categ]
        if (items) {
            out << body()
        }
    }
    
    def ifHasNoItems = { attrs, body -> 
        def categ = attrs.group ?: '*'
        // this needs to take into account authz like iterate items
        def items = navigationService.byGroup[categ]
        if (!items) {
            out << body()
        }
    }
    
    /**
     * Renders the body only for the current item.
     */
    def currentItem = { attrs, body ->
    	def categ = attrs.group ?: '*'
        def var = attrs.var
        def altTitle = attrs.altTitle
        def altIcon = attrs.altIcon ?: 'comments'
        def items = navigationService.byGroup[categ]
        // actionMatch must be true if we show active items based on their action AND controller, else controller only
        // often only subitems match actions
        def actionMatch = attrs.actionMatch ? attrs.actionMatch.toString().toBoolean() : false

        if (items) {
            boolean activeFound = iterateItems(items, var, body, attrs.params, actionMatch, true)
            if (!activeFound && altTitle) {
                def item = [title: altTitle, icon: altIcon]
                out << body(var ? [(var):item] : item)
            }
        }   
    }

    /**
     * Iterate over nav items in a given category
     */
    def eachItem = { attrs, body ->
        def categ = attrs.group ?: '*'
        def var = attrs.var
        
        def items = navigationService.byGroup[categ]
        log.debug (items)
        // actionMatch must be true if we show active items based on their action AND controller, else controller only
        // often only subitems match actions
        def actionMatch = attrs.actionMatch ? attrs.actionMatch.toString().toBoolean() : false
        if (items) {
            iterateItems(items, var, body, attrs.params, actionMatch, false)
        }
    }
    
    /**
     * Iterate over nav sub items in a given category's item (by title)
     */
    def eachSubItem = { attrs, body ->
        def categ = attrs.group ?: '*'
        def itemTitle = attrs.remove('title')
        def con = attrs.remove('controller')
        def searchKey = itemTitle
        def searchKeyAuto = false
        
        // do we have a pre-supplied section title to work from?
        if (!searchKey) {
            // No, do we have a controller specified?
            if (!con) {
                // No, let's default to currently active controller
                searchKey = GrailsClassUtils.getLogicalName(controllerName, 'Controller')
                searchKeyAuto = true
            } else searchKey = con
        }
        def var = attrs.var
        
        def items = navigationService.byGroup[categ]
        // Match by title or controller as appropriate
        def item = items.find { it[itemTitle ? 'title' : 'controller'] == searchKey } 
        if (!item && !searchKeyAuto) 
            throwTagError("There is no item with ${itemTitle ? 'title' : 'controller'} [${searchKey}] in the group [$categ] - navigation items are: ${navigationService.byGroup}")
            
        if (item?.subItems) {
            // Match actions by default here, as we're subitems
            iterateItems(item.subItems, var, body, attrs.params, true, false)
        }
    }
    
        
    private iterateItems = { items, var, body, linkParams, actionMatch, activeOnly ->
        def last = items.size()-1
        boolean activeFound = false
        items.eachWithIndex { item, i ->
      
            def isAuthorized = true
            if (item.role) {
                isAuthorized = AuthorizeTools.ifAllGranted(item.role)
            } else if (item.anyrole) {
                isAuthorized = AuthorizeTools.ifAnyGranted(item.anyrole)
            }
 
            // isVisible is closure or null/false
            
            def isVisible = item['isVisible']
            
            if (isVisible instanceof Closure) {
                isVisible = isVisible.clone()
                def delegate = new VisibilityDelegate(grailsApplication, [
                    session:session,
                    request:request,
                    flash:flash,
                    params:params
                ])
                isVisible.delegate = delegate
                isVisible.resolveStrategy = Closure.DELEGATE_FIRST
            }
            
            if (isAuthorized && isVisible == null || isVisible == true || isVisible.call()) { 
                def data = [:]
                data.putAll(item)
                
                // Create a new map of params for createLink composed of the params from the item plus
                // (overriden by) those passed in in the linkParams from the tag attribute
                // ...without ever creating a map unless we need it
                def createLinkParams = data.params ? new HashMap(data.params) : null
                if (linkParams) {
                    if (createLinkParams) {
                        createLinkParams.putAll(linkParams)
                    } else {
                        createLinkParams = new HashMap(linkParams)
                    }
                }
                
                // Make the URL
                data.link = g.createLink(controller:data.controller, action:data.action, id:data.id, params:createLinkParams)
                if (i == 0) {
                    data.first = true
                } else if (i == last) {
                    data.last = true
                }
                if ((controllerName == data.controller) && 
                        (!actionMatch || (actionName == data.action)) && 
                        (!data.params || (data.params.every { e -> params.get(e.key)?.equals(e.value) })) ) {
                    data.active = true
                    activeFound = true
                }
                if (data.active || !activeOnly)  {
                	out << body(var ? [(var):data] : data)
                }
            }
        }
        return activeFound
    }


}

class VisibilityDelegate {
    def props
    def grailsApplication
    
    VisibilityDelegate(app, Map concreteProps) {
        props = concreteProps
        grailsApplication = app
    }
    
    /** 
     * Return a predefined property or bean from the context
     */
    def propertyMissing(String name) {
        if (this.@props.containsKey(name)) {
            return this.@props[name]
        } else {
            return this.@grailsApplication.mainContext.getBean( name )
        }
    }
}
