package com.cloudmasons

import grails.util.GrailsNameUtils
import groovy.text.Template;
import org.codehaus.groovy.grails.commons.GrailsDomainClass

import com.cloudmasons.domain.HiddenParameterBuilder;
import org.springframework.web.servlet.support.RequestContextUtils as RCU


/**
 * 
 */
public class ThemeTagLib {
    static namespace = "theme"

    def beanService
    def themeService
    def authenticateService
    def securityService

    /**
     * Dashboard panel
     */
    def panel = { attrs, body ->
        def templateName = "panel"
        Template t = themeService.getTemplate(templateName)
        def model = [:]
        model.width = attrs.width
        model.body = body
        t?.make( model )?.writeTo(out)
    }

    /**
     * Dashboard panel with tabs
     */
    def panelWithTabs = { attrs, body ->
    }

    /**
     * Display flash messages and errors 
     */
    def messages = { attrs, body ->
        def templateName = "messages"
        Template t = themeService.getTemplate(templateName)
        def model = [:]
        model.bean = attrs.bean
        t?.make( model )?.writeTo(out)
    }

    /**
     * Display the current themes list view
     */
    def list = { attrs, body ->
        def templateName = attrs.template ?: 'bean/list'
        Template t = themeService.getTemplate(templateName)
        def model = [body:body]
        log.debug( attrs )

        model.beans = attrs.beans
        model.total = attrs.total
        model.listStyle = attrs.listStyle ?: 'both'
        model.domainClass = BeanUtils.resolveDomainClass(grailsApplication, attrs.beanClass)
        model.domainNaturalName = GrailsNameUtils.getNaturalName(model.domainClass.getShortName())
        model.actions = beanService.getActions(model.domainClass, attrs.actions)
        model.beanParams = attrs.beanParams

        model.beanControllerName = attrs.beanControllerName ?: controllerName
        model.beanParams.controller = model.beanControllerName
        model.beanParams.columns = params.columns

        model.summaryProperties = beanService.getSummaryProperties((GrailsDomainClass) model.domainClass, params.columns)

        t?.make( model )?.writeTo(out)
    }

    /**
     * Renders only the content of a list panel.  Used for ajax refreshes.
     */
    def listPanelContent = { attrs, body ->
        def templateName = attrs.template ?: 'bean/listPanelContent'
        if (attrs.listStyle == 'grid') {
            templateName += 'Grid'
        } else {
            templateName += 'Table'
        }
        Template t = themeService.getTemplate(templateName)
        def model = [body:body]
        log.debug( attrs )
        model.beans = attrs.beans
        model.total = attrs.total
        model.domainClass = BeanUtils.resolveDomainClass(grailsApplication, attrs.beanClass)
        model.domainNaturalName = GrailsNameUtils.getNaturalName(model.domainClass.getShortName())
        model.actions = beanService.getActions(model.domainClass, attrs.actions)
        model.beanParams = attrs.beanParams
        model.summaryProperties = beanService.getSummaryProperties((GrailsDomainClass) model.domainClass)
        t?.make( model )?.writeTo(out)
    }

    def create = { attrs, body ->
        def templateName = attrs.template ?: 'bean/create'
        Template t = themeService.getTemplate(templateName)
        def model = [bean:attrs.bean]
        model.domainClass = BeanUtils.resolveDomainClass(grailsApplication, attrs.bean)
        model.domainNaturalName = GrailsNameUtils.getNaturalName(model.domainClass.getShortName())
        model.propertyGroups = beanService.getPropertyGroups(attrs.bean, attrs.propertyGroups)
        model.beanControllerName = attrs.beanControllerName ?: controllerName

        t?.make( model ?: [:])?.writeTo(out)
    }

    /**
     * Show a bean
     */
    def show = { attrs, body ->
        def templateName = attrs.template ?: 'bean/show'
        Template t = themeService.getTemplate(templateName)
        def model = [bean:attrs.bean]
        model.domainClass = BeanUtils.resolveDomainClass(grailsApplication, attrs.bean)
        model.domainNaturalName = GrailsNameUtils.getNaturalName(model.domainClass.getShortName())
        model.propertyGroups = beanService.getPropertyGroups(attrs.bean, attrs.propertyGroups)
        model.beanControllerName = attrs.beanControllerName ?: controllerName
        model.label = attrs.label ?: ''
        t?.make( model ?: [:])?.writeTo(out)
    }

    def edit = { attrs, body  ->
        checkRequiredAttrs('edit', attrs, ['bean'])

        def templateName = attrs.template ?: 'bean/edit'
        Template t = themeService.getTemplate(templateName)
        if (!t) {
            def msg = "ThemeTag: edit unable to resolve template for : ${templateName}"
            throw new IllegalArgumentException(msg)
        }

        // build the model
        def bean = attrs.bean
        def model = [bean:bean]
        model.propertyGroups = beanService.getPropertyGroups(bean, attrs.propertyGroups)
        model.summaryProperties = beanService.getSummaryProperties(bean)
        model.domainClass = BeanUtils.resolveDomainClass(grailsApplication, bean)
        model.domainNaturalName = GrailsNameUtils.getNaturalName(model.domainClass.getShortName())
        model.beanControllerName = attrs.beanControllerName ?: controllerName
        model.beanControllerAction = attrs.beanControllerAction ?: 'update'
        model.label = attrs.label ?: ''

        t?.make(model)?.writeTo(out)
    }

    def propertyGroups = { attrs, body  ->

        def style = attrs.readonly ? 'show' : 'edit'

        style = attrs.style ?: style

        def bean = attrs.bean
        def model = [bean:bean]


        // sigh stupid plan entry/service plan entry..
        if (style == 'print' && bean.hasProperty('skipPrint')) {
            bean = bean?.servicePlanEntry
        }

        def excludes = attrs.excludes ?: [attrs.exclude]
        def domainClass = BeanUtils.resolveDomainClass(grailsApplication, bean)
        def constraints = domainClass.constrainedProperties

        // support for an override using the template attribute
        def templates = []
        if (attrs.template) templates.add(attrs.template)

        templates.add( "bean/${style}/propertyGroups" )

        Template t = themeService.getTemplate(templates as String[], domainClass)

        model.beanPathPrefix = attrs.beanPathPrefix ?: ''
        model.propertyGroups = beanService.getPropertyGroups(bean, attrs.propertyGroups, excludes)
        model.domainClass = domainClass
        model.domainNaturalName = GrailsNameUtils.getNaturalName(model.domainClass.getShortName())
        model.summaryProperties = beanService.getSummaryProperties(bean)
        model.domainNaturalName = GrailsNameUtils.getNaturalName(model.domainClass.getShortName())
        model.beanControllerName = attrs.beanControllerName ?: controllerName
        model.beanControllerAction = attrs.beanControllerAction ?: 'update'
        model.label = attrs.label ?: ''

        t?.make( model ?: [:])?.writeTo(out)
    }

    def renderPath = { attrs, body  ->


        def name = attrs.name
        def pathNodes = attrs.path
        println pathNodes
        pathNodes.each { node -> out << """<a href="${g.createLink(controller: node.get('beanControllerName'), action: node.get('beanActionName'), id:node.get('bean')?.id, params:node.get('beanParams'))}">${node.get('bean')?.name}'s ${node.get('name')}</a> > """ }
        out << "${name}"
    }

    def renderOption = { attrs, body  ->

        def groupName = attrs.name

        def bean = attrs.bean
        //def beanControllerName = attrs.beanControllerName
        def domainClass = BeanUtils.resolveDomainClass(grailsApplication, bean)
        def constraints = domainClass.constrainedProperties
        def props = attrs.props ?: beanService.getProps(bean, groupName)
        def prop = props?.getAt(0)
        if (prop && prop.association) {
            def value = getValue(bean, prop.name)
            if (prop.oneToMany) {
                out << """<a class='button altbutton right' href="${g.createLink(controller: domainClass.propertyName, action: 'show', id:bean?.id, params:[associated: prop.name])}">Show All</a>"""
            }
            else if (prop.oneToOne && value && (prop.name != domainClass.propertyName)) {
                def assoicatedBean = prop?.type?.newInstance()
                if (securityService.isAuthorized(prop?.type, 'showDetails',assoicatedBean))  {
                    out << """<a class='button altbutton right' href="${g.createLink(controller: domainClass.propertyName, action: 'show', id:bean?.id, params:[associated: prop.name, propertyId: value?.id])}">Show Details</a>"""
                }
            } else if (prop.oneToOne && !value) {
                def assoicatedBean = prop?.type?.newInstance()
                if (securityService.isAuthorized(prop?.type, 'create',assoicatedBean))  {
                    out << """<a class='button altbutton right' href="${g.createLink(controller: domainClass.propertyName, action: 'show', id:bean?.id, params:[associated: prop.name])}">Create</a>"""
                }
            }
        }
        else {
            if (securityService.isAuthorized(bean.getClass(), 'edit',bean)) {
                def beanControllerName = GrailsNameUtils.getShortName(bean.getClass().name)
                beanControllerName = beanControllerName[0].toLowerCase()+beanControllerName.substring(1,beanControllerName.size())
                out << """<a class='button altbutton right' href="${g.createLink(controller: beanControllerName, action: 'edit', id:bean?.id, params:[group: groupName])}">Edit</a>"""
            }
        }
    }

    def propertyGroup = { attrs, body  ->

        def style = attrs.readonly ? 'show' : 'edit'
        style = attrs.style ?: style
        def groupName = attrs.name

        def excludes = attrs.excludes ?: [attrs.exclude]
        def bean = attrs.bean
        def domainClass = BeanUtils.resolveDomainClass(grailsApplication, bean)
        def constraints = domainClass.constrainedProperties

        def props = attrs.props ?: beanService.getProps(bean, groupName)
        def properties = props?.collect { p ->
            def prop = null
            if (p instanceof String) {
                prop = domainClass.getPropertyByName(p)
            } else {
                prop = p
            }
            prop
        }
        properties = properties.findAll { ! excludes.contains(it.name) }
        properties = properties.findAll { constraints[it.name]?.editable != false }

        def templates = []
        if (attrs.template) templates.add(attrs.template)
        if (groupName) templates.add( "${GrailsNameUtils.getPropertyName(groupName)}")

        templates.add( "bean/${style}/propertyGroup" )
        log.debug templates

        Template t = themeService.getTemplate(templates as String[], domainClass)
        def model = [bean:bean, group: groupName, properties: properties, fields: properties, readonly: attrs.readonly]
        model.beanPathPrefix = attrs.beanPathPrefix ?: ''
        model.domainClass = domainClass
        model.domainNaturalName = GrailsNameUtils.getNaturalName(model.domainClass.getShortName())
        model.title = attrs.title

        t?.make( model ?: [:])?.writeTo(out)
    }

    def property = { attrs ->
        String name = attrs.name
        //println "------------------------------------"
        //println name
        Theme theme = new Theme(Theme.DEFAULT_THEME.properties)
        def domainClass = BeanUtils.resolveDomainClass(grailsApplication, attrs.bean)
        //println domainClass
        //println "------------------------------------"

        def prop = domainClass?.getPropertyByName(name)
        def readonly = attrs.readonly
        def model = attrs.model ?: [:]
        model.domainClass = domainClass
        model.domainNaturalName = GrailsNameUtils.getNaturalName(model.domainClass.getShortName())
        model.cp = domainClass.constrainedProperties[prop.name]

        // See if the logged in user is authorized to edit this role
        def editRole = model.cp?.getAppliedConstraint('role')?.editRole
        if (readonly ||
        (editRole && !authenticateService.ifAnyGranted(editRole))) {
            log.debug "Edit role on property ${prop.name}: ${editRole}"
            theme.propertyPath = theme.showPropertyPath
            log.debug theme.propertyPath
        }

        model['property'] = prop
        model.bean = attrs.bean

        model.beanPathPrefix = attrs.beanPathPrefix ?: ''
        model.beanPath = "${model.beanPathPrefix}${prop.name}"
        model.beanPathPropertyName = model.beanPath.replace('[', '_');
        model.beanPathPropertyName = model.beanPathPropertyName.replace(']', '_');

        model.field = prop
        model.error = attrs.error ?: getError(bean, name);
        model.required = model.cp?.getAppliedConstraint('nullable')?.nullable ? false : true
        try {
            model.value = getValue(model.bean, prop.name)
            if (attrs.style == 'print' && model.value instanceof String ) {
                theme.propertyPath = theme.printPropertyPath
                model.value = getPrintableValue(model.value)
            }
        }
        catch (org.hibernate.ObjectNotFoundException e) {
            println e
        }

        if (prop.oneToMany) {
            model.summaryProperties = beanService.getSummaryProperties(prop.referencedDomainClass)
        }
        Template t = themeService.getTemplate(theme, prop)
        t?.make( model ?: [:])?.writeTo(out)
    }


    def printProperty = { attrs ->
        def property = attrs.property
        def bean = attrs.bean
        def propertyBeanObj = bean?."${property?.name}"

        String propertyName = property.name
        log.debug "Printing property: ${propertyName}"

        if (propertyBeanObj) {
            if (property.oneToMany) {
                // remove all the nulls..
                def props = propertyBeanObj.findAll { it }
                props.eachWithIndex { propertyBean, i ->
                    out << "<h3>${property.naturalName}</h3>"
                    out << propertyGroups(bean: propertyBean, style: 'print')
                }
            } else if (property.oneToOne) {
                out << propertyGroups(bean: propertyBeanObj, style: 'print')
            }
        }
    }

    def icon = { attrs ->
        if (attrs.name) {
            out << resource(dir:'css/boxie/img/led-ico',file:"${attrs.name}.png")
        }
    }

    private getError(def bean, def propName) {
        def result = null
        if(bean.metaClass.hasProperty( bean,'errors')) {
            def errors = bean.errors
            result = errors?.getFieldError(propName)
        }
        return result;
    }

    private getValue(def bean, def propName) {

        def result = null
        if(bean.metaClass.hasProperty( bean,'errors')) {
            def errors = bean.errors
            //println errors
            result = errors?.getFieldError(propName)?.rejectedValue
        }

        if (result == null) {
            result = bean?.properties[propName]
        }
        return result;
    }

    def renderHTML = { attrs ->
        String value = attrs.value
        out << value?.encodeAsHTML().replaceAll(/(\r\n)/, "<br/>")
    }

    def renderSpacing = { attrs ->
        String value = attrs.value
        out << value?.replaceAll(/(\r\n)/, "<br/>")
    }

    /**
     * strip out invalid character that prevent generating pdf
     * convert smart quotes to straight quotes.
     */
    private String getPrintableValue(String value) {
        return value.replaceAll( "[\\u2018\\u2019]", "\'" ).replaceAll( "[\\u201c\\u201d]", "\"" )?.encodeAsHTML();
    }
    private void checkRequiredAttrs(def tag, def attrs, List required) {
        required?.each { attrName ->
            if (attrs[attrName] == null) {
                def msg = "ThemeTagLib: ${tag} missing required attribute: ${attrName}"
                throw new IllegalArgumentException(msg)
            }
        }
    }

    def message = {attrs->
        def messageObj = attrs.messageObj
        def style = 'msg msg-ok'
        if (messageObj.error) {
            style = 'msg msg-error'
        }
        if (messageObj.message) {
            out << "<div class='${style}'>${messageObj?.message}</div>"
        }
        messageObj?.messages?.each { out << "<div class='${style}'>${it}</div>" }
    }


    /**
     * Setup all the form parameters.
     */
    def paginateFormParameters = { attrs ->
        if (attrs.total == null) {
            throwTagError("Tag [paginate] is missing required attribute [total]")
        }
        def linkParams = [:]
        if (attrs.params) {
            linkParams.putAll(attrs.params)
        }
        linkParams.total = attrs.int('total') ?: 0
        def max = params.int('max') ?: 10
        def offset = params.int('offset') ?: 0
        linkParams.max = max
        linkParams.offset = offset > max ? offset - max : 0
        if (params.sort) {
            linkParams.sort = params.sort
        }
        if (params.order) {
            linkParams.order = params.order
        }
        HiddenParameterBuilder.buildParameters(out, linkParams)
    }

    /**
     * Iterate through creating all the pagination links.
     */
    def paginate = { attrs ->
        def writer = out
        if (attrs.total == null) {
            throwTagError("Tag [paginate] is missing required attribute [total]")
        }
        def messageSource = grailsAttributes.messageSource
        def locale = RCU.getLocale(request)

        def total = attrs.int('total') ?: 0
        def action = (attrs.action ? attrs.action : (params.action ? params.action : "list"))
        def offset = params.int('offset') ?: 0
        def max = params.int('max')
        def maxsteps = (attrs.int('maxsteps') ?: 10)

        if (!offset) offset = (attrs.int('offset') ?: 0)
        if (!max) max = (attrs.int('max') ?: 10)

        def linkParams = [:]
        linkParams.offset = offset - max
        linkParams.max = max
        if (params.sort) linkParams.sort = params.sort
        if (params.order) linkParams.order = params.order

        def linkTagAttrs = [action:action]
        if (attrs.controller) {
            linkTagAttrs.controller = attrs.controller
        }
        if (attrs.id != null) {
            linkTagAttrs.id = attrs.id
        }
        linkTagAttrs.params = linkParams

        // determine paging variables
        def steps = maxsteps > 0
        int currentstep = (offset / max) + 1
        int firststep = 1
        int laststep = Math.round(Math.ceil(total / max))

        // display previous link when not on firststep
        if (currentstep > firststep) {
            linkTagAttrs.class = 'prevLink'
            linkParams.offset = offset - max
            writer << thelink(linkTagAttrs.clone()) {
                def dflt = messageSource.getMessage('default.paginate.prev', null, 'Previous', locale)
                (attrs.prev ?: messageSource.getMessage('paginate.prev', null, dflt, locale))
            }
        }

        // display steps when steps are enabled and laststep is not firststep
        if (steps && laststep > firststep) {
            linkTagAttrs.class = 'step'

            // determine begin and endstep paging variables
            int beginstep = currentstep - Math.round(maxsteps / 2) + (maxsteps % 2)
            int endstep = currentstep + Math.round(maxsteps / 2) - 1

            if (beginstep < firststep) {
                beginstep = firststep
                endstep = maxsteps
            }
            if (endstep > laststep) {
                beginstep = laststep - maxsteps + 1
                if (beginstep < firststep) {
                    beginstep = firststep
                }
                endstep = laststep
            }

            // display firststep link when beginstep is not firststep
            if (beginstep > firststep) {
                linkParams.offset = 0
                writer << thelink(linkTagAttrs.clone()) {firststep.toString()}
                writer << '<span class="step">..</span>'
            }

            // display paginate steps
            (beginstep..endstep).each { i ->
                if (currentstep == i) {
                    writer << "<span class=\"currentStep\">${i}</span>"
                }
                else {
                    linkParams.offset = (i - 1) * max
                    writer << thelink(linkTagAttrs.clone()) {i.toString()}
                }
            }

            // display laststep link when endstep is not laststep
            if (endstep < laststep) {
                writer << '<span class="step">..</span>'
                linkParams.offset = (laststep -1) * max
                writer << thelink(linkTagAttrs.clone()) { laststep.toString() }
            }
        }

        // display next link when not on laststep
        if (currentstep < laststep) {
            linkTagAttrs.class = 'nextLink'
            linkParams.offset = offset + max
            writer << thelink(linkTagAttrs.clone()) {
                def dflt = messageSource.getMessage('default.paginate.next', null, 'Next', locale)
                (attrs.next ? attrs.next : messageSource.getMessage('paginate.next', null, dflt, locale))
            }
        }
    }
    
    def thelink = { attrs, body ->
        out << "<a href=\"#\" offset=\"${attrs.params.offset}\" max=\"${attrs.params.max}\" class=\"${attrs.class}\">"
        out << body()
        out << "</a>"
    }
    
}
