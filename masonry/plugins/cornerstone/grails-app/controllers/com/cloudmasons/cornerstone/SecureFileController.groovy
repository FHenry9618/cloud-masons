package com.cloudmasons.cornerstone

import grails.converters.JSON
import com.cloudmasons.BeanUtils


/**
 * 
 */
class SecureFileController {
    def grailsApplication
    def secureFileService
    
    // create a progress object on the session..
    def upload = {
        println params

        def downloadedfile = request.getFile('Filedata');
        def filename = params.Filename
        downloadedfile.transferTo(new File("/tmp/${filename}"))
        def model = [name: filename,
                     originalFilename: filename,
                     description: filename,
                     size:downloadedfile.getSize(),
                     storageType: 'LOCAL',
                     encryptedName: filename,
                     encryptedSize: downloadedfile.getSize(),
                     encryptionKey: 'asdfasdf',
                     path: '/tmp/',
                     secondaryStorageType: 'S3',
                     secondaryPath: 'bucket1234',
                     status: 'COPY_TO_SECONDARY']
                     
        render model as JSON
    }

    def download = {
        def domainClass = BeanUtils.resolveDomainClass(grailsApplication, params.beanClass)
        if (!domainClass) {
            if (params.errorController && params.errorAction) {
               flash.message = "Unable to find secure file class for ${attrs.beanType}"
               redirect controller: params.errorController, action: params.errorAction
            }
            else {
                def model = [error:"Unable to find secure file class for ${attrs.beanType}"]
                render model as JSON
            }
           return
        }
        
        def bean = domainClass.clazz.get(params.id)
        if (!bean) {
           if (params.errorController && params.errorAction) {
               flash.message = "Unable to find secure file for ${attrs.beanType}:${params.id}"
               redirect controller: params.errorController, action: params.errorAction
           }
           else {
               def model = [error:"Unable to find secure file for ${attrs.beanType}:${params.id}"]
               render model as JSON
           }
           return
        }
        //bean.downloads++
        //bean.save()
        def inputStream = secureFileService.getInputStream(bean)
        if (inputStream != null) {
            def originalFileName = bean.originalFilename.encodeAsURL()
            def contentDisposition = params.contentDisposition ?: 'attachment'
            response.setContentType('application/octet-stream')
            response.setHeader("Content-disposition", "${contentDisposition}; filename=${originalFileName}")
            response.outputStream << inputStream
            return
        } else {
            if (params.errorController && params.errorAction) {
                flash.message = "Unable to find file"
                redirect controller: params.errorController, action: params.errorAction
            }
            else {
                def model = [error:"Unable to find file"]
                render model as JSON
            }
            return
        }
    }
}
