package com.cloudmasons;

import org.codehaus.groovy.grails.plugins.springsecurity.AuthorizeTools

class SecurityService {

    static transactional = false

    def grailsApplication

    public static final String SYSTEM_ADMINISTRATOR_ROLE = '**SYSTEM_ADMINISTRATOR_ROLE**'

    /**
     * Resolve a projects role name for certain key roles.  Some out of the box admin controllers
     * need to specify that they're only available by system administrators but the name of the
     * system administrator role may change from project to project
     *
     */
    public String resolveRoleName(String name) {
        String result = name;
        if (name == SYSTEM_ADMINISTRATOR_ROLE) {
            result = grailsApplication.config.cornerstone.security.systemAdminstratorRole ?: 'ROLE_ADMINISTRATOR'
        }
        return result;
    }

    /**
     * Check to see if the logged in user is authorized to perform an
     * action on the given domain class.  This uses the static "actions" map
     * on the domain class.  
     */
    public boolean isAuthorized(Class clazz, Object action, Object beanInstance=null) {
        def ret = doAuthorized(clazz, action, beanInstance)
        if (!ret) {
            log.debug "Authorization failure Class: '${clazz}', Action: '${action}', Bean Instance: '${beanInstance}'"
        }
        ret
    }
    
    boolean doAuthorized(Class clazz, Object action, Object beanInstance=null) {
        // no actions no security..
        def actionsPresent = clazz.metaClass.properties.find { it.name == 'actions' }
        if (!actionsPresent) {
            log.debug "Static 'actions' not found, defaulting to no security, for class: ${clazz}"
            return true
        }

        // no particular action no security..
        def actionMap =  clazz.actions?.find { it.action == action }
        if (!actionMap) {
            log.debug "Action '${action}' not found, defaulting to no security, for class: ${clazz}"
            return true
        }

        // no role == not authorized..
        if (!actionMap?.role) {
            log.debug "No role no authorization."
            return false
        }
        
        def role = actionMap?.role
        if (role instanceof String) {
            String roleName = resolveRoleName(role)
            return AuthorizeTools.ifAnyGranted(roleName)
        } else if (role instanceof Map) {
            // loop through all the role maps
            for (roleKey in role.keySet()) {
                String roleName = resolveRoleName(roleKey)
                boolean hasRole = AuthorizeTools.ifAnyGranted(roleName)
                // has the role but no filter is ok
                if (hasRole) {
                    def filter = role[roleKey]?.filter
                    if ('static' != actionMap.type && filter && beanInstance) {
                        filter.resolveStrategy = Closure.DELEGATE_FIRST 
                        filter.delegate = beanInstance
                        if (filter()) {
                            log.debug "Authorized on role '${roleName} and filter."
                            return true
                        }
                    } else {
                        return true
                    }
                }
            }
        }
        false
    }

}


