package com.cloudmasons;

import grails.util.GrailsNameUtils
import groovy.text.Template;

import java.util.concurrent.ConcurrentHashMap

import org.codehaus.groovy.grails.commons.GrailsClassUtils
import org.codehaus.groovy.grails.commons.GrailsDomainClassProperty
import org.codehaus.groovy.grails.commons.GrailsDomainClass
import org.codehaus.groovy.grails.commons.ServiceArtefactHandler
import org.codehaus.groovy.grails.plugins.GrailsPluginManager
import org.codehaus.groovy.grails.web.pages.GroovyPagesTemplateEngine



/**
 * Service to fetch the appropiate template for views based on the current theme 
 */
class ThemeService {

    static transactional = false

    def grailsApplication
    GroovyPagesTemplateEngine groovyPagesTemplateEngine
    GrailsPluginManager pluginManager

    // Cache for the template URI for each bean field.  Allowing for
    // the default templates to be overridden in an application requires
    // a caching mechanism to minimize the searches that are required
    // per field.
    private static Map TEMPLATE_CACHE = new ConcurrentHashMap()
    private static Map PATH_CACHE = new ConcurrentHashMap()
    private static Map MISSING_PATHS = new ConcurrentHashMap()

    public Template getTemplate(String[] templateNames) {
        return getTemplate(null, templateNames);
    }
    public Template getTemplate(String[] templateNames, GrailsDomainClass domainClass) {
        getTemplate(null, templateNames, domainClass)
    }

    public Template getTemplate(String templateName) {
        return getTemplate(null, templateName);
    }
    public Template getTemplate(String templateName, GrailsDomainClass domainClass) {
        getTemplate(null, templateName, domainClass)
    }
    public Template getTemplate(GrailsDomainClassProperty prop) {
        getTemplate(null, prop)
    }
    
    public Template getTemplate(Theme theme, String[] templateNames) {
        Template result = null
        if (templateNames == null) return result
        for (String templateName in templateNames) {
            result = getTemplate(theme, templateName)
            if (result) break
        }
        return result
    }
        
    public Template getTemplate(Theme theme, String templateName) {
        if (theme == null) theme = Theme.DEFAULT_THEME
        if (templateName == null) throw new IllegalArgumentException("No templateName specified")
        
        def pathGenerator = templatePaths.curry(null, theme, templateName)
        return getTemplate(theme, templateName, pathGenerator)
    }

    public Template getTemplate(Theme theme, String[] templateNames, GrailsDomainClass domainClass) {
        Template result = null
        if (templateNames == null) return result
        for (String templateName in templateNames) {
            result = getTemplate(theme, templateName, domainClass)
            if (result) break
        }
        return result
    }
    
    public Template getTemplate(Theme theme, String templateName, GrailsDomainClass domainClass) {
        if (theme == null) theme = Theme.DEFAULT_THEME
        if (templateName == null) throw new IllegalArgumentException("No templateName specified")
        
        def pathGenerator = templatePaths.curry(domainClass, theme, templateName)
        String uniqueTemplateName = null;
        if (domainClass) {
            uniqueTemplateName = "${domainClass.propertyName}/${templateName}"
        } else {
            uniqueTemplateName = templateName;
        }
        return getTemplate(theme, uniqueTemplateName, pathGenerator)
    }
    
    public Template getTemplate(Theme theme, GrailsDomainClassProperty prop) {
        if (theme == null) theme = Theme.DEFAULT_THEME
        if (prop == null) throw new IllegalArgumentException("No property specified")
        
        def pathGenerator = templatePathsForProp.curry(prop, theme)
        String templateName = "${theme.propertyPath}/${prop.domainClass.propertyName}/${prop.name}".toString()
        return getTemplate(theme, templateName, pathGenerator)      
    }
 
    /*
     * There are two caches since most properties will resolve to the same template 
     * and I didn't want to cache templates multiple times.  One caches a bean
     * field to resolved path and another a path to a template.
     * 
     * We use the cache for resolved paths but only use the template cache for 
     * production environments
     */
    public Template getTemplate(Theme theme, String templateName, Closure pathGenerator) {
        if (templateName == null) {
            return null
        }
        def engine = groovyPagesTemplateEngine
        
        log.debug "Looking for template ${templateName}"
        log.debug "Engine ${engine}"
        Template t = null
        if (MISSING_PATHS[templateName]) return null;
        String path = PATH_CACHE[templateName]
        if (path == null) {
            
            List paths = pathGenerator.call()
            
            log.debug "Finding template ${templateName} in the following paths ${paths}"
            for (p in paths) {
                t = engine.createTemplateForUri(p)
                if (t != null) {
                    path = p.toString()
                    TEMPLATE_CACHE.putIfAbsent(path, t)
                    PATH_CACHE.putIfAbsent(templateName, path)
                    break;
                }
            }
        }
        
        log.debug "Using path ${path}"
        if (path != null) {
            // don't use the template cache in dev environments
            if (engine.isReloadEnabled()) {
                t = engine.createTemplateForUri(path)
            } else {
                t = TEMPLATE_CACHE[path]
            }
        } else {
            MISSING_PATHS.putIfAbsent(templateName, true)
        }
        return t
    }
    
    
    def  templatePaths = { GrailsDomainClass domainClass, Theme theme, String templateName ->

        String dcName = domainClass?.propertyName ?: ''
        List results = []
        if (dcName) {
            results.add( "${dcName}/${templateName}.gsp" )
            results.add( "${dcName}/${theme.name}/${templateName}.gsp" )

        }
        results.add( "theme/${theme.name}/${templateName}.gsp" )
        results.add( "theme/default/${templateName}.gsp" )

        
        return prependPluginPath(theme.plugin, results)
    }
    
    /*
     * Produce a list of paths to look for templates based on the class
     * of the property.
     */
    def  templatePathsForProp = { GrailsDomainClassProperty prop, Theme theme ->
        String dcName = prop.domainClass.propertyName
        String defaultDir = theme.propertyPath
        Class clazz = prop.type
        String suffix = '.gsp'
        String prefix = ''
        if (prop.manyToOne) {
            prefix = 'manyToOne'
        } else if (prop.oneToOne) {
            prefix = 'oneToOne'
        } else if ((prop.oneToMany && !prop.bidirectional) || (prop.manyToMany && prop.isOwningSide())) {
            prefix = 'manyToMany'
        } else if (prop.oneToMany) {
            // one to many the class type is usually a collection... switch it 
            // to be what the object class contained within the collection
             prefix = 'oneToMany'
             clazz = prop.referencedDomainClass.clazz
        } else if (prop.manyToMany) {
            // one to many the class type is usually a collection... switch it 
            // to be what the object class contained within the collection
             prefix = 'manyToMany'
             clazz = prop.referencedDomainClass.clazz
        }
        if (clazz.isArray()) {
            clazz = clazz.componentType
            suffix = 'Array.gsp'
        }
        if (clazz.isPrimitive()) {
            clazz = primitiveToClass[(clazz)]
        }

     
        List results = []
        results.add("${dcName}/${prefix}${GrailsNameUtils.getShortName(clazz)}${suffix}")
        results.add("theme/${theme.name}/${defaultDir}/${prefix}${GrailsNameUtils.getShortName(clazz)}${suffix}")
        results.add("theme/default/${defaultDir}/${prefix}${GrailsNameUtils.getShortName(clazz)}${suffix}")
        
        // Use annotations as a possible file (ex. oneToManySecureFile)
        def annotations = clazz.annotations
        if (annotations) {
            for (anno in annotations) {
                String annoName = GrailsNameUtils.getShortName(anno.annotationType())
                results.add("${dcName}/${prefix}${annoName}${suffix}")
                results.add("theme/${theme.name}/${defaultDir}/${prefix}${annoName}${suffix}")
                results.add("theme/default/${defaultDir}/${prefix}${annoName}${suffix}")
            }
        }
        // Add paths for implemented interfaces
        for (ifc in clazz.interfaces) {
            String ifcName = GrailsNameUtils.getShortName(ifc)
            if (ifcName.equals('serializable') || ifcName.equals('comparable')) continue
            results.add("${dcName}/${prefix}${ifcName}${suffix}")
            results.add("theme/${theme.name}/${defaultDir}/${prefix}${ifcName}${suffix}")
            results.add("theme/default/${defaultDir}/${prefix}${ifcName}${suffix}")

        }
        
        // Add paths for superclasses
        while (clazz.superclass) {
            clazz = clazz.superclass
            String className = GrailsNameUtils.getShortName(clazz)
            results.add("${dcName}/${prefix}${className}${suffix}")
            results.add("theme/${theme.name}/${defaultDir}/${prefix}${className}${suffix}")
            results.add("theme/default/${defaultDir}/${prefix}${className}${suffix}")
        }       

        return prependPluginPath(theme.plugin, results)
    }
    

    private List prependPluginPath(String pluginName, List paths) {
         if (pluginName == null) return paths
         if (paths == null) return paths
         
        // For each path, we should look first in the application
        // and then in the plugin
        def pluginPath = pluginManager?.getPluginPath(pluginName) ?: ''
        if (pluginPath == null) {
            // todo: log error
            return paths
        }
         
        List allPaths = []
        for (p in paths) {
            allPaths.add(p)
            allPaths.add("${pluginPath}/grails-app/views/${p}")
        }
        return allPaths
    }
    
    private static final primitiveToClass = [(Boolean.TYPE): Boolean.class,
                                              (Character.TYPE): Character.class,
                                              (Short.TYPE): Short.class,
                                              (Void.TYPE): Void.class,
                                              (Byte.TYPE): Byte.class,
                                              (Integer.TYPE): Integer.class,
                                              (Long.TYPE): Long.class,
                                              (Float.TYPE): Float.class,
                                              (Double.TYPE): Double.class ]
    
}
