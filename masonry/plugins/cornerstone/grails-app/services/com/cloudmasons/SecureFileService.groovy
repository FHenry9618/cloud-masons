package com.cloudmasons;

import com.cloudmasons.domain.SecureFile

class SecureFileService {


    def getInputStream(def bean) {
        if (bean.storageType == SecureFile.LOCAL) {
            def file = new File(bean.path, bean.encryptedName)
            if (file.exists()) {
                return file.newInputStream();
            }
        }
        return null
    }

}
