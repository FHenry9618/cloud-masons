package com.cloudmasons

import javax.servlet.http.HttpServletRequest
import org.codehaus.groovy.grails.web.servlet.DefaultGrailsApplicationAttributes
import org.codehaus.groovy.grails.web.pages.GroovyPagesTemplateEngine
import org.springframework.web.context.request.RequestContextHolder
import org.codehaus.groovy.grails.web.context.ServletContextHolder
import org.springframework.web.context.support.WebApplicationContextUtils
import org.codehaus.groovy.grails.plugins.PluginManagerHolder
import org.codehaus.groovy.grails.commons.GrailsResourceUtils


/**
 * Service to render a template.  Used to send emails in html format using gsps.
 */
class CustomGrailsService {
    
    static transactional = false
    
    GroovyPagesTemplateEngine groovyPagesTemplateEngine
    
    String renderTemplate(String templateUri, Map model, boolean forLogging=false) {
        
        if(!groovyPagesTemplateEngine) throw new IllegalStateException("Property [groovyPagesTemplateEngine] must be set!")
        if (!templateUri) return null;
        
        def requestAttributes = RequestContextHolder.getRequestAttributes()
        boolean unbindRequest = false
        
        // outside of an executing request, establish a mock version
        if(!requestAttributes) {
            def servletContext  = ServletContextHolder.getServletContext()
            def applicationContext = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext)
            requestAttributes = grails.util.GrailsWebUtil.bindMockWebRequest(applicationContext)
            unbindRequest = true
        }
        
        def servletContext = requestAttributes.request.servletContext
        def request = requestAttributes.request
        
        def grailsAttributes = new DefaultGrailsApplicationAttributes(servletContext);
        
        def engine = groovyPagesTemplateEngine
        def t = engine.createTemplate(templateUri)
        
        def originalOut = requestAttributes.getOut();
        def out = new StringWriter();
        requestAttributes.setOut(out);
        
        try {
            t.make(model).writeTo(out)
        } finally {
            requestAttributes.setOut(originalOut)
            if(unbindRequest) {
                RequestContextHolder.setRequestAttributes(null)
            }
        }
        return out.toString();
    }
}
