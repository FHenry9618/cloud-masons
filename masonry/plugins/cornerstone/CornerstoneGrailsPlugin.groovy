
import com.mchange.v2.c3p0.ComboPooledDataSource

import org.codehaus.groovy.grails.commons.ConfigurationHolder
import org.codehaus.groovy.grails.commons.TagLibArtefactHandler
import org.codehaus.groovy.grails.plugins.PluginManagerHolder
import org.codehaus.groovy.grails.plugins.GrailsPlugin

import org.springframework.core.io.*
import org.apache.commons.io.FilenameUtils
import org.apache.commons.lang.StringUtils

class CornerstoneGrailsPlugin {
    // the plugin version
    def version = "1.0"
    // the version or versions of Grails the plugin is designed for
    def grailsVersion = "1.2.1 > *"
    // the other plugins this plugin depends on
    def dependsOn = [controllers:"1.0 > *"]
    def observe = ['controllers']
    def loadAfter = ['controllers', 'acegi']
    
    // resources that are excluded from plugin packaging
    def pluginExcludes = [
            "grails-app/views/error.gsp"
    ]

    def author = "Cloudmasons"
    def authorEmail = ""
    def title = "Cornerstone plugin for cloudmasons applications"
    def description = '''\\
Brief description of the plugin.
'''

    // URL to the plugin's documentation
    def documentation = "http://grails.org/plugin/cornerstone"
        def watchedResources = findWatchedResources()

        private static List findWatchedResources() {
            def buildConfig = new ConfigSlurper().parse(BuildConfig)
            def result = []
            def pluginLocation = buildConfig?.grails.plugin.location.cornerstone
            if (pluginLocation) {
                 result << "file:${pluginLocation}/grails-app/services/**/*Service.groovy"
                 result << "file:${pluginLocation}/grails-app/controllers/**/*Controller.groovy"
                 result << "file:${pluginLocation}/grails-app/taglib/**/*TagLib.groovy"
            }
            return result
        }

    def doWithWebDescriptor = { xml ->
        // TODO Implement additions to web.xml (optional), this event occurs before 
    }

    def doWithSpring = {

        customPropertyEditorRegistrar(com.cloudmasons.beans.CornerstonePropertyEditorRegistrar) 
        
        // c3P0 pooled data source that forces renewal of DB connections of certain age
        // to prevent stale/closed DB connections and evicts excess idle connections
        // Still using the JDBC configuration settings from DataSource.groovy
        // dtg - I imagine at sometime the cornerstone plugin is going to need to 
        //       be loaded after hibernate, thus breaking this.  We can either break
        //       this into a smaller plugin or apps can put this c3p0 datasource wrapper
        //       in their resources.groovy
        // dtg - You can tell if this is being invoked in time by setting an invalid
        //       password and confirming c3p0 is in the stacktrace
        dataSource(ComboPooledDataSource) { bean ->
           bean.destroyMethod = 'close'
           
           // use grails' datasource configuration for connection user, password, 
           // driver and JDBC url
           user = ConfigurationHolder.config.dataSource.username
           password = ConfigurationHolder.config.dataSource.password
           driverClass = ConfigurationHolder.config.dataSource.driverClassName
           jdbcUrl = ConfigurationHolder.config.dataSource.url
           
           // Force connections to renew after 4 hours
           // Get rid too many of idle connections after 30 minutes
           maxConnectionAge = 4 * 60 * 60
           maxIdleTimeExcessConnections = 30 * 60
        }
        
        def conf = ConfigurationHolder.config
        configureCAS.delegate = delegate
        configureCAS(conf)
            
    }

    def doWithDynamicMethods = { ctx ->
        refreshNavigation(application, applicationContext)    
    }

    def doWithApplicationContext = { applicationContext ->
        // TODO Implement post initialization spring config (optional)
    }

    def onChange = { event ->
        if (event.source) {
            if (event.plugin == this && event.source instanceof Class) {
                println "Cornerstone plugin change detected..."
                
                def name = event.source.name
                // for some reason event.plugin was not set to the the services or controller plugin
                // we'll use the plugin manager to notify them of the change.
                if (name.endsWith('Service')) {
                    def servicesPlugin = PluginManagerHolder.pluginManager.getGrailsPlugin('services')
                    println "    Reloading ${name}"
                    servicesPlugin.notifyOfEvent(GrailsPlugin.EVENT_ON_CHANGE, event.source)
                } else if (name.endsWith('Controller')) {
                    def controllersPlugin = PluginManagerHolder.pluginManager.getGrailsPlugin('controllers')
                    println "    Reloading ${name}"
                    controllersPlugin.notifyOfEvent(GrailsPlugin.EVENT_ON_CHANGE, event.source)
                }
                
                refreshNavigation(application, applicationContext)
            } 
            // detect changes in taglibs
            if (application.isArtefactOfType(TagLibArtefactHandler.TYPE, event.source)) {
                def groovyPagesPlugin = PluginManagerHolder.pluginManager.getGrailsPlugin('groovyPages')
                println "    Reloading ${event.source.name}"
                groovyPagesPlugin.notifyOfEvent(GrailsPlugin.EVENT_ON_CHANGE, event.source)
            }
        }
    }

    def onConfigChange = { event ->
        // TODO Implement code that is executed when the project configuration changes.
        // The event is the same as for 'onChange'.
    }
    
    private refreshNavigation(application, applicationContext) {
        def navService = applicationContext.navigationService
        navService.reset()

        application.controllerClasses.each { controllerClass ->
            // If there is a navigation property that is not null or false, we include it
            def nav = false
            if (controllerClass.clazz.metaClass.hasProperty(controllerClass.clazz, 'navigation')) {
                nav = controllerClass.clazz.navigation
            }
            if (nav) {
                navService.registerItem(controllerClass)
            }
        }

        navService.updated()
    }
    
    
    /**
     * Configure SSO via CAS for a given grails application
     */
    def configureCAS = { conf ->
        if (!conf.grails.cas.enabled) return;
        
        println "Configuring Central Authentication System" 
        String casHost = conf.grails.cas.casServer ?: 'localhost'
        println casHost
        int casPort = (conf.grails.cas.casServerPort ?: '443').toInteger()
        String casFilterProcessesUrl = conf.grails.cas.filterProcessesUrl ?: '/j_spring_cas_security_check'
        boolean sendRenew = Boolean.valueOf(conf.grails.cas.sendRenew ?: false)
        String proxyReceptorUrl = conf.grails.cas.proxyReceptorUrl ?: '/secure/receptor'
        String applicationHost = conf.grails.cas.applicationHost ?: System.getProperty('server.host') ?: 'localhost'
        int applicationPort = (conf.grails.cas.applicationPort ?: System.getProperty('server.port') ?: 8080).toInteger()
        //String applicationHost = System.getProperty('server.host') ?: 'localhost'
        //int applicationPort = (System.getProperty('server.port') ?: 8080).toInteger()
        String appName = application.metadata['app.name']
        String casHttp = conf.grails.cas.casServerSecure ? 'https' : 'http'
        String localHttp = conf.grails.cas.localhostSecure ? 'https' : 'http'


        proxyGrantingTicketStorage(org.jasig.cas.client.proxy.ProxyGrantingTicketStorageImpl)

        casProcessingFilter(org.springframework.security.ui.cas.CasProcessingFilter) {
                authenticationManager = ref('authenticationManager')
                authenticationFailureUrl = conf.grails.cas.failureURL ?: '/denied.jsp'
                defaultTargetUrl = conf.grails.cas.defaultTargetURL ?: '/'
                filterProcessesUrl = casFilterProcessesUrl
                proxyGrantingTicketStorage = proxyGrantingTicketStorage
                proxyReceptorUrl = proxyReceptorUrl
                useRelativeContext = true 
        }

        casServiceProperties(org.springframework.security.ui.cas.ServiceProperties) {
                service = "$localHttp://$applicationHost:$applicationPort/$appName$casFilterProcessesUrl"
                println "server url ${service}"
                sendRenew = sendRenew
        }

        String casLoginURL = conf.grails.cas.fullLoginURL ?: "$casHttp://$casHost:$casPort/cas/login"
        authenticationEntryPoint(org.springframework.security.ui.cas.CasProcessingFilterEntryPoint) {
                loginUrl = casLoginURL
                serviceProperties = casServiceProperties
        }

        String casServiceURL = conf.grails.cas.fullServiceURL ?: "$casHttp://$casHost:$casPort/cas"
        cas20ServiceTicketValidator(org.jasig.cas.client.validation.Cas20ServiceTicketValidator, casServiceURL) {
                proxyGrantingTicketStorage = proxyGrantingTicketStorage
                //proxyCallbackUrl = "$localHttp://$applicationHost:$applicationPort/$appName$proxyReceptorUrl"
        }
        // the CAS authentication provider key doesn't need to be anything special, it just identifies individual providers
        // so that they can identify tokens it previously authenticated
        String casAuthenticationProviderKey = conf.grails.cas.authenticationProviderKey ?: appName + System.currentTimeMillis()
        casAuthenticationProvider(org.springframework.security.providers.cas.CasAuthenticationProvider) {
                userDetailsService = ref(conf.grails.cas.userDetailsService ?: 'userDetailsService')
                serviceProperties = casServiceProperties
                ticketValidator = cas20ServiceTicketValidator
                key = casAuthenticationProviderKey
        }
        
        return true
    }

}
