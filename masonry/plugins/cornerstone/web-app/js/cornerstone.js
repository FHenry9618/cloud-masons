function setupOneToMany(beanName, propertyName, beanCount) {
    if (beanCount == 0) {
       $('#'+ beanName + '-' + propertyName + '-table').hide();
    }
    addInlineEdit('' + beanName + '-create-' + propertyName + '-form');
    addInlineCreate(beanName, propertyName)
    for (var i=0; i < beanCount; i++) {
        var id = '' + beanName + '-edit-' + propertyName +'-form-' + i;
        addInlineEdit(id);
        addCollectionDelete(beanName, propertyName, i)
     }
}

function addCollectionDelete(beanName, propertyName, offset) {
        var id = '' + beanName + '-edit-' + propertyName +'-form-' + offset;

        $('.delete-'+id)
            .button()
            .bind('click', {propertyName: propertyName, id: id, idx: offset}, function(event) {
                var databaseId = $('tr .'+event.data.id).attr('rel');
               
                if (confirm('Are you sure you want to delete this item?' )) {
                    
                    $('tr .'+event.data.id).hide();
                    var removals = $('#removeFrom'+event.data.propertyName).val() || '';
                    if (removals.length != 0) {
                        removals += '; ';
                    }
                    removals += databaseId;
                    $('#removeFrom'+event.data.propertyName).val(removals);
                }
                return false;
             });
}

function addInlineCreate(beanName, propertyName) {
    var baseId = '' + beanName + '-edit-' + propertyName +'-form';
        $('#dialog-'+ beanName + '-create-' + propertyName + '-form').dialog({
            autoOpen: false,
            height: 400,
            width: 650,
            buttons: {
                'Cancel': function() {
                    $(this).dialog('close');
                },
                'Save': function() {
                    var nextIndex = $('#' + propertyName +'NextIndex');
                    var index = nextIndex.val();
                    var clonedDiv = $('#modified-'+baseId).clone()
                    clonedDiv.attr('id', 'modified-'+baseId+'-'+index);
                    clonedDiv.appendTo('#'+ propertyName+'CreateValues');
                    
                    // A comment
                    var origDialog = $(this)
                    var clonedDialog = $(this).clone()
                    clonedDialog.attr('id', 'dialog-' + baseId +'-' + index);
                    clonedDialog.find('input').each(function(i) {
                         var templateName = this.name;
                         this.name = templateName.replace('NEWINSTANCE', index);
                    });
                    clonedDialog.find('textarea').each(function(i) {
                         var templateName = this.name;
                         var safeName = templateName.replace(/\./g, '\\.');  // escape all dots
                         var currentTextArea = $("textarea[id^='"+safeName+"']");
                         
                         this.name = templateName.replace('NEWINSTANCE', index);
                         this.value = currentTextArea[0].value
                    });
                    clonedDialog.find("select").each(function(i) {
                         var templateName = this.name;
                         // The clone of the dialog isn't copying over the select boxes
                         // current select value so we have to dig it out
                         var safeName = templateName.replace(/\./g, '\\.');  // escape all dots
                         var currentSelect = $("select[id^='"+safeName+"']");
                         var selectedIndex = currentSelect.attr('selectedIndex');
                         var currentValue = currentSelect.val();
                         this.name = templateName.replace('NEWINSTANCE', index);
                         this.selectedIndex = selectedIndex;
                         this.value = currentValue;
                    });
                    clonedDialog.appendTo('#' + propertyName + 'CreateValues');
                    var templateSummaryRow = $('tr .'+propertyName+'NewSummaryRow')
                    var summaryRow = templateSummaryRow.clone()
                    summaryRow.attr('class', baseId + '-' + index)
                    summaryRow.find("td").each(function(i) {
                        var templateId = this.id;
                        this.id = templateId.replace('NEWINSTANCE', index);
                        
                    });
                    summaryRow.find('a').addClass('show-'+beanName+'-edit-'+propertyName+'-form-'+index);
                    summaryRow.show();
                    summaryRow.insertBefore(templateSummaryRow);
                    var dialogId = baseId + '-' + index;
                    addInlineEdit(dialogId);
                    processEditDialog($('#dialog-'+dialogId), dialogId);
                    nextIndexNum = parseInt(index) + 1
                    nextIndex.attr('value', nextIndexNum.toString());
                    $('#'+ beanName + '-' + propertyName + '-table').show();
                    
                    $(this).dialog('close');
                }
            },
            close: function() {
            }
        });
        $('.show-'+baseId)
            .button()
            .click(function() {
                $('#dialog-'+id).dialog('open');
            });
      
        
}

function addInlineEdit(id) {
        $('#dialog-'+id).dialog({
            autoOpen: false,
            height: 400,
            width: 650,
            buttons: {
                'Close': function() {
                    processEditDialog($(this), id);
                    $(this).dialog('close');
                    
                }
            },
            close: function() {
            }
        });
        $('.show-'+id)
            .button()
            .click(function() {
                $('#dialog-'+id).dialog('open');
            });
      
        
}


function addListTabs(tableId) {
    // dtg: the tab is hidden during rendering
    //$('#' + tableId +'-grid').hide(); // hide content related to inactive tab by default
    $('#' + tableId +' .header ul a').click(function(){
        $('#' + tableId +' .header ul a').removeClass('active');
        $(this).addClass('active'); // make clicked tab active
        $('#' + tableId +' .content').hide(); // hide all content
        $('#' + tableId).find('#' + $(this).attr('rel')).show(); // and show content related to clicked tab
        return false;
    });
}

// Process the dialog and make sure all updates are put into the
// edit form for submit.  Also update the summary table
function processEditDialog(dialog, id) {
    var values = '';
    var valueArray = new Array()
    dialog.find("input").each(function(i) {
           var value = this.value
           var displayValue = this.value
           
           // Convert a checkbox into a displayble value and true/false for the
           // hidden input value
           if (this.type == "checkbox") {
              displayValue = this.checked  ? "yes" : "no"
              value = this.checked ? "true" : "false"
           } 
           // Create the input that will be placed back into the edit form
           values = values + "<input type='hidden' name='" + this.name +"' value='" + value +"'>";
           valueArray[this.name] = displayValue ? displayValue : '<i>no value</i>'
    });
    dialog.find("textarea").each(function(i) {
           values = values + "<input type='hidden' name='" + this.name +"' value='" + this.value +"'>";
           valueArray[this.name] = this.value ? this.value : '<i>no value</i>'
    });

    dialog.find("select").each(function(i) {
           values = values + "<input type='hidden' name='" + this.name +"' value='" + this.value +"'>";
           valueArray[this.name] = this.children[this.selectedIndex].text

    });
    
    for(var name in valueArray) {
         var safeName = name.replace('\.id', '');    // remove the .id extension for many-to-one select lists
         safeName = safeName.replace(/\./g, '\\.');  // escape all dots
         safeName = 'summary-' + safeName
         //alert(safeName + valueArray[name]);
        $("td[id^='"+safeName+"']").empty().append(valueArray[name])
    }
    $('#modified-'+id)
         .empty()
         .append(values)
}
      
