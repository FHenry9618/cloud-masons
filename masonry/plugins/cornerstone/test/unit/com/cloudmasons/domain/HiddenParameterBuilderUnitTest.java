package com.cloudmasons.domain;

import java.util.HashMap;
import java.util.Map;

import org.codehaus.groovy.grails.web.pages.FastStringWriter;
import org.junit.Test;

public class HiddenParameterBuilderUnitTest {

    @Test
    public void smokeTestParameterBuilder() {
        FastStringWriter wrt = new FastStringWriter();
        Map<Object, Object> params = new HashMap<Object, Object>();
        params.put("filter1", "value1");
        HiddenParameterBuilder.buildParameters(wrt, params);
        System.out.println("Parameters:");
        System.out.println(wrt.toString());
    }
}
